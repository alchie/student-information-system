(function($){

$('.sortable').sortable();

var apply_number_format = function() {
	 $('.number_format').each(function(){
      if( $(this)[0].tagName == 'INPUT') {
        $(this).val( numeral($(this).val().replace(/,/g,'')).format('0,0') );
      } else {
        $(this).text( numeral($(this).text().replace(/,/g,'')).format('0,0') );
      }
    });

    $('.money_format').each(function(){
      if( $(this)[0].tagName == 'INPUT') {
        $(this).val( numeral($(this).val().replace(/,/g,'') ).format('0,0.00') );
      } else {
        $(this).text( numeral($(this).text().replace(/,/g,'') ).format('0,0.00') );
      }
    });
 };
	
	$('#parent_search').click(function(){
		
	});
	$('.confirm').click(function(){
		if( confirm("Are you sure?") ) {
			return true;
		} else {
			return false;
		}
	});
	$('.datepicker').datepicker();
	$('.un-apply-payment').click(function(){
		$('.payment_total_amount').each(function(){
			$(this).val('0');
		});
		$('#applied-balance').text(0);
		apply_number_format();
	});

	$('.auto-apply-payment').click(function(){
		if( $('#payment_total_amount').val() == '' ) {
			$('#payment_total_amount').focus();
			return;
		}
		$('.payment_total_amount').attr('disabled',true);
		var amount = parseFloat( $('#payment_total_amount').val().replace(/,/g,'') );
		$('.payment_total_amount').each(function(){
			var check_id = $(this).attr('data-check_id');
			
			if( $('#'+check_id).prop('checked') == false) {
				$(this).val('0');
				return;
			}

			var balance = parseFloat($(this).attr('data-balance').replace(/,/g,''));
			if( balance < 0) {
				balance = 0;
			}

			if(amount>balance) {
				$(this).val(balance);
			} else {
				if( amount > 0) {
					$(this).val(amount||0);
				} else {
					$(this).val('0');
				}
			}
			if( amount > 0) {
				amount = Math.round((amount - balance) * 100) / 100;
			}
			$('.payment_total_amount').trigger('keyup');
		});
		$('.payment_total_amount').attr('disabled',false);
		//$('#addPaymentModalSubmit').attr('disabled', false);
		apply_number_format();
	});

	var timeOut = null;
	$('#payment_total_amount').keyup(function(){
		clearTimeout(timeOut);
		timeOut = setTimeout(function(){
			//$('#addPaymentModalSubmit').attr('disabled', true);
			$('#auto-apply-payment').trigger('click');
		},500);
	});
	
	var timeOut2 = null;
	$('.payment_total_amount').keyup(function(e){
		if(e.which != 9 && e.which != 16 && e.which != 40 && e.which != 38) {
			clearTimeout(timeOut2);
			timeOut2 = setTimeout(function(){
				var pta = 0;
				$('.payment_total_amount').each(function(){
					var value = parseFloat( $(this).val().replace(/,/g,'') ) || 0;
					pta += value;
					$('#applied-balance').text(pta);
				});
				//$('#addPaymentModalSubmit').attr('disabled', false);
				apply_number_format();
			},1000);
		}
		var inputs = $(this).closest('#addPaymentModal-applied').find('.payment_total_amount');
		current_index = inputs.index(this);
		var next_index = current_index + 1;
		var previous_index = current_index - 1;
		if( next_index > (inputs.length-1) ) {
			next_index = 0;
		}
	     if( e.which == 13 || e.which == 40 ) {

	        inputs.eq( next_index ).focus().select();
	     }
	     if( e.which == 38 ) {
	        inputs.eq( previous_index ).focus().select();
	     }
	}).focus(function(){
		$(this).select();
	});


$('.un-apply-discount').click(function(){
		$('.discount_total_amount').each(function(){
			$(this).val('0');
		});
		$('#applied-balance2').text(0);
		apply_number_format();
	});

	$('.auto-apply-discount').click(function(){
		if( $('#discount_total_amount').val() == '' ) {
			$('#discount_total_amount').focus();
			return;
		}
		$('.discount_total_amount').attr('disabled',true);
		var amount = parseFloat($('#discount_total_amount').val().replace(/,/g,''));
		$('.discount_total_amount').each(function(){
			var check_id = $(this).attr('data-check_id');
			if( $('#'+check_id).prop('checked') == false) {
				$(this).val('0');
				return;
			}
			var balance = parseFloat($(this).attr('data-balance').replace(/,/g,''));
			if( balance < 0) {
				balance = 0;
			}
			if(amount>balance) {
				$(this).val(balance);
			} else {
				if( amount > 0) {
					$(this).val(amount||0);
				} else {
					$(this).val('0');
				}
			}
			if( amount > 0) {
				amount = Math.round((amount - balance) * 100) / 100;
			}
			$('.discount_total_amount').trigger('keyup');
		});
		$('.discount_total_amount').attr('disabled',false);
		$('#addDiscounts2ModalSubmit').attr('disabled', false);
		apply_number_format();
	});

var timeOutd = null;
$('#discount_total_amount').keyup(function(){
		clearTimeout(timeOutd);
		timeOutd = setTimeout(function(){
			$('#auto-apply-discount').trigger('click');
		},500);
	});

var timeOutd2 = null;
	$('.discount_total_amount').keyup(function(e){
		if(e.which != 9 && e.which != 16 && e.which != 40 && e.which != 38) {
			clearTimeout(timeOutd2);
			timeOutd2 = setTimeout(function(){
				var pta = 0;
				$('.discount_total_amount').each(function(){
					var value = parseFloat( $(this).val().replace(/,/g,'') ) || 0;
					pta += value;
					$('#applied-balance2').text(pta);
				});
				apply_number_format();
			},1000);
		}
		var inputs = $(this).closest('#addDiscounts2Modal-applied').find('.discount_total_amount');
		current_index = inputs.index(this);
		var next_index = current_index + 1;
		var previous_index = current_index - 1;
		if( next_index > (inputs.length-1) ) {
			next_index = 0;
		}
	     if( e.which == 13 || e.which == 40 ) {

	        inputs.eq( next_index ).focus().select();
	     }
	     if( e.which == 38 ) {
	        inputs.eq( previous_index ).focus().select();
	     }
	}).focus(function(){
		$(this).select();
	});

 $('.show-hide').click(function(){
 	var id = $(this).attr('data-id');
 	var status = $('#'+id).css('display');
 	if( status == 'none' ) {
 		$('#'+id).fadeIn('slow');
 	} else {
 		$('#'+id).fadeOut('slow');
 	}
 });

 $('.show-hide-class').click(function(){
 	var cls = $(this).attr('data-class');
 	var btn = $(this);

 	$('.'+cls).each(function(){
 		var status = $(this).css('display');
 		if( status == 'none' ) {
 			$(this).fadeIn('slow', function(){
 				btn.text('Hide').removeClass('btn-success').addClass('btn-danger');
 			});
 			
	 	} else {
	 		$(this).fadeOut('slow', function(){
	 			btn.text('Show').removeClass('btn-danger').addClass('btn-success');
	 		});
	 	}
 	});
 });

 $('.autofill-select').change(function(){
 	var id = $(this).attr('data-autofill');
 	var value = $('option:selected', this).attr('data-value');
 	$('#'+id).val(value);
 });

 	$('.addServiceModal_button').click(function(){
 		var month_title = $(this).attr('data-month_title');
 		var month = $(this).attr('data-month');
 		var service_id = $(this).attr('data-service_id');
 		var service_name = $(this).attr('data-service_name');
 		var amount = $(this).text().replace(/,/g,'');

 		$('#addServiceModal-title').text(service_name + " - " + month_title);
 		$('#addServiceModal-month').val( month );
 		$('#addServiceModal-service_id').val( service_id );
 		$('#addServiceModal-amount').val( amount );
 	});

 	$('#addServiceModal').on('shown.bs.modal', function () {
	  	$('#addServiceModal-amount').focus().select();
	});

	$('.addService2Modal_button').click(function(){
 		var month_title = $(this).attr('data-month_title');
 		var month = $(this).attr('data-month');
 		var amount = $(this).text().replace(/,/g,'');

 		$('#addService2Modal-title').text( month_title);
 		$('#addService2Modal-month').val( month );
 		$('#addService2Modal-amount').val( amount.trim() );
 	});

 	$('#addService2Modal').on('shown.bs.modal', function () {
	  	$('#addService2Modal-amount').focus().select();
	});

$('.updateService2Modal_button').click(function(){
 		var month_title = $(this).attr('data-month_title');
 		var month = $(this).attr('data-month');
 		var service_id = $(this).attr('data-service_id');
 		var service_name = $(this).attr('data-service_name');
 		var id = $(this).attr('data-id');
 		var amount = $(this).text().replace(/,/g,'');
 		var href = $('#updateService2Modal-delete').attr('data-href');

 		$('#updateService2Modal-title').text(service_name + " - " + month_title);
 		$('#updateService2Modal-month').val( month );
 		$('#updateService2Modal-service_id').val( service_id );
 		$('#updateService2Modal-service_name').val( service_name );
 		$('#updateService2Modal-amount').val( amount.trim() );
 		$('#updateService2Modal-delete').attr('href', href + id);
 	});

 	$('#updateService2Modal').on('shown.bs.modal', function () {
	  	$('#updateService2Modal-amount').focus().select();
	});

 	$('.installment-override').click(function(){
 		var name = $(this).attr('data-name');
 		var fee_id = $(this).attr('data-id');
 		var installment = $(this).attr('data-installment');
 		var skip = $(this).attr('data-skip');
 		$('#installmentOverrideModal-title').text(name + " Override");
 		$('#installmentOverrideModal-fee_id').val(fee_id);
 		$('#installmentOverrideModal-installment').val(installment);
 		$('#installmentOverrideModal-skip').val(skip);
 	});
apply_number_format();

	$('.show_input_password').click(function(){
		var t = null;
		if( $('.input_password').prop('type') == 'password') {
			$(this).removeClass('btn-success').addClass('btn-danger').text('Hide');
			$('.input_password').prop('type', 'text');
			t = setTimeout(function(){
				$('.show_input_password').removeClass('btn-danger').addClass('btn-success');
				$('.show_input_password').text('Show');
				$('.input_password').prop('type', 'password');
			},2000);
		} else {
			$(this).removeClass('btn-danger').addClass('btn-success').text('Show');
			$('.input_password').prop('type', 'password');
			clearTimeout(t);
		}
	});
	$('.edit_usergroup').click(function(){
		$('#editUserGroupModal-group_name').val( $(this).attr('data-name') );
		$('#editUserGroupModal-group_id').val( $(this).attr('data-id') );
	});

	$('.group_permission').click(function(){
		var module = $(this).attr('data-module');
		var action = $(this).attr('data-action');
		var checked = ($(this).is(':checked')) ? 1 : 0;
		$.ajax({
			method: 'POST',
			url : ajax_url,
			data : {
				module : module,
				action : action,
				checked : checked
			},
			dataType : 'json',
			success : function(data) {
				console.log( data );
			}
		});

	});

	$('.user_permission').click(function(){
		var module_id = $(this).attr('data-moduleid');
		var module_name = $(this).attr('data-modulename');
		var action = $(this).attr('data-action');
		var on = $(this).attr('data-on');
		$('#userPermissionOverrideModal-module_name').html( module_name.replace('- - - -', '').trim() );
		$('#userPermissionOverrideModal-on').attr('data-module', module_id);
		$('#userPermissionOverrideModal-action').text(action);
		$('#userPermissionOverrideModal-on').attr('data-action', action);
		if( on == "1" ) {
			$('#userPermissionOverrideModal-on').prop('checked', true);
		} else {
			$('#userPermissionOverrideModal-on').prop('checked', false);
		}
	});

	$('#userPermissionOverrideModal-on').click(function(){
		var module = $(this).attr('data-module');
		var action = $(this).attr('data-action');
		var checked = ($(this).is(':checked')) ? 1 : 0;
		$.ajax({
			method: 'POST',
			url : ajax_url,
			data : {
				module : module,
				action : action,
				checked : checked
			},
			dataType : 'json',
			success : function(data) {
				console.log( data );
			}
		});
		$('#user_permission_' + module + '_' + action).attr('data-on', checked);
		var icon = $('#user_permission_' + module + '_' + action + ' > .glyphicon');
		
		if( checked == 1) {
			icon.removeClass('glyphicon-remove');
			icon.addClass('glyphicon-ok');
			icon.css('color', 'green');
		} else {
			icon.removeClass('glyphicon-ok');
			icon.addClass('glyphicon-remove');
			icon.css('color', 'red');
		}

	});
	
	$('.autosum-class').each(function(){
		var data_class = $(this).attr('data-class');
		var total = 0.00;
		$('.'+data_class).each(function(){
			total += parseFloat($(this).text().replace(/,/g,''));
		});
		$(this).text(total);
	});
	apply_number_format();

	$(window).keyup(function(e){
		if( e.altKey && e.ctrlKey && ( e.keyCode == 39 ) ) {
			if( $('#navArrowNext').prop('href') ) {
				window.location = $('#navArrowNext').prop('href');
			}
		}
		if( e.altKey && e.ctrlKey && ( e.keyCode == 37 ) ) {
			if( $('#navArrowPrevious').prop('href') ) {
				window.location = $('#navArrowPrevious').prop('href');
			}
		}
	});

	$(document).scroll(function(e){

		if( ( $(this).scrollTop() > 60 ) && ( $('.navbar-student').length > 0 ) ) {
			$('.navbar-student').addClass('fixed');
			$('#subnav').hide();
			$('body').css('padding-top', '60px');
		} else {
			$('.navbar-student').removeClass('fixed');
			$('#subnav').show();
			$('body').css('padding-top', '0');
		}
	});

 	$('.invoiceDetailsModal_button').click(function(){
 		$('#invoiceDetailsModal').attr('data-ajax', $(this).attr('data-ajax'));
 		var imgurl = $('#invoiceDetailsModal .modal-body').attr('data-img');
 		$('#invoiceDetailsModal .modal-body').html( $('<IMG/>').prop('src', imgurl) );
 	});

 	$('#invoiceDetailsModal').on('shown.bs.modal', function () { 		
	  	$.ajax({
			method: 'GET',
			url : $(this).attr('data-ajax'),
			dataType : 'html',
			success : function(data) {
				$('#invoiceDetailsModal .modal-body').html(data);
			}
		});
	});

 	$('.paymentDetailsModal_button').click(function(){
 		$('#paymentDetailsModal').attr('data-ajax', $(this).attr('data-ajax'));
 		var imgurl = $('#paymentDetailsModal .modal-body').attr('data-img');
 		$('#paymentDetailsModal .modal-body').html( $('<IMG/>').prop('src', imgurl) );
 	});

 	$('#paymentDetailsModal').on('shown.bs.modal', function () { 		
	  	$.ajax({
			method: 'GET',
			url : $(this).attr('data-ajax'),
			dataType : 'html',
			success : function(data) {
				$('#paymentDetailsModal .modal-body').html(data);
			}
		});
	});

	$('#checkall_delete_invoice').click(function(){
		if($(this).prop('checked')) {
			$('.delete_invoice_item').prop('checked',true);
		} else {
			$('.delete_invoice_item').prop('checked',false);
		}
	});
	$('.show_delete_invoice_input').click(function(){
		$('.delete_invoice_input_item').show();
	});

	$('#sync_sync_now').click(function(){
		var sync_url = $(this).attr('data-sync_url');
		var loader_img = $(this).attr('data-loader_img');
		var container = $('<div id="sync_sync_container"></div>');
		var loader = $('<div id="sync_sync_loader"><img src="'+loader_img+'"></div>');
		$('body').append( container );
		$('body').append( loader );
		$(this).prop('disabled',true);
		$.ajax({
			url : sync_url,
			complete : function(){
				$('#sync_sync_container').remove();
				$('#sync_sync_loader').remove();
				$('#sync_sync_now').prop('disabled',false);
			}
		});
	});

  $('[data-toggle="tooltip"]').tooltip();

  $('.assignGradeLevelModalBtn').click(function(){
  	var school_year_id = $(this).attr('data-school_year');
  	$('#assignGradeLevelModal_schoo_year').val( school_year_id );
  });

  $('.btn-group-select-all').click(function(){
  		$('.btn-group-select').removeClass('btn-success').addClass('btn-default');
  		$(this).addClass('btn-success').removeClass('btn-default');
  		var target = $(this).attr('data-target');
  		$(target).prop('checked', true);
  });
  $('.btn-group-select-none').click(function(){
  		$('.btn-group-select').removeClass('btn-success').addClass('btn-default');
  		$(this).addClass('btn-success').removeClass('btn-default');
  		var target = $(this).attr('data-target');
  		$(target).prop('checked', false);
  });

  $('.btn-switch').click(function(){
  		var action = $(this).attr('data-action');
  		var target = $(this).attr('data-target');
  		if( action == 'hide') {
  			$(this).removeClass('btn-success').addClass('btn-default');
  			$(this).attr('data-action', 'show');
  			$(target).slideUp();
  		} else {
  			$(this).removeClass('btn-default').addClass('btn-success');
  			$(this).attr('data-action', 'hide');
  			$(target).slideDown();
  		}
  });

})(jQuery);