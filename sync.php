<?php

ini_set('max_execution_time', 300);
error_reporting(E_ALL); 
ini_set("display_errors", 1); 

define('BASEPATH', '');
define('ENVIRONMENT', 'production');
require_once("_core/application/config/database.php");
require_once("_core/application/config/config.php");
require_once("_core/application/config/constants.php");
$dbconn = $db[$active_group];


function compress($filename, $dir, $files){ 
    $zip = new ZipArchive();

    if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
        return false;
    }

    foreach($files as $file) {
        $zip->addFile($file, str_replace($dir . '\\', '', $file));
    }

    $zip->close();

    return true;
} 

function extractZip( $filename, $dir ) {
    $zip = new ZipArchive;
    $res = $zip->open( realpath( $dir . DIRECTORY_SEPARATOR . $filename ) );
    if ($res === TRUE) {
      $zip->extractTo( realpath( $dir ) );
      $zip->close();
      return true;
    } else {
      return false;
    }
}

function backup_mysqldump($dbconn, $sync=false)
{
    $dir = ".";
    if( $sync ) {
        $filename = 'sync.sql';
    } else {
        $filename = $dbconn['database'] . "-" . date('m-d-y-h-i-s') . '-dump.sql';
    }
    
    //$cmd = 'D:\xampp\mysql\bin\mysqldump.exe ';
    $cmd = 'D:\wamp\bin\mysql\mysql5.6.17\bin\mysqldump.exe ';
    $cmd .= ' -u ' . $dbconn['username'];
    $cmd .= ' --password="' . $dbconn['password'] . '"';
    $cmd .= ' ' . $dbconn['database'];
    $cmd .= ' > ' . $dir . DIRECTORY_SEPARATOR . $filename;

    exec($cmd);
    compress($dir . DIRECTORY_SEPARATOR . $filename . ".zip", $dir, array( $dir . DIRECTORY_SEPARATOR . $filename ) );
    unlink($dir . DIRECTORY_SEPARATOR . $filename);
}

function importSQL($dbconn) {
    
    $conn = mysqli_connect($dbconn['hostname'], $dbconn['username'], $dbconn['password'], $dbconn['database']);

    $templine = '';

    $lines = file( realpath( './sync/sync.sql') ); 

    foreach ($lines as $line)
    {

        if (substr($line, 0, 2) == '--' || $line == '') {
            continue;
        }

        $templine .= $line;
        if (substr(trim($line), -1, 1) == ';')
        {
            mysqli_query($conn, $templine);
            $templine = '';
        }
    }
    return true;
} 

function maintenance_mode($remove=false) {
    if($remove) {
        unlink('MAINTENANCE');
    } else {
        $handle = fopen('MAINTENANCE', 'w');
        fclose($handle);
    }
}

if( $_SERVER['HTTP_HOST'] == K12MS_SYNC_SERVER_URI ) {

    if( isset( $_GET['action'] ) && ( $_GET['action']=='receive') ) {
        $public_key = (isset($_GET['public_key'])) ? $_GET['public_key'] : false;
        $secret_key = (isset($_POST['secret_key'])) ? $_POST['secret_key'] : false;
        if(( ( $public_key ) && ( $public_key == K12MS_SYNC_PUBLIC_KEY ) ) && ( ( $secret_key ) && ( $secret_key == K12MS_SYNC_SECRET_KEY ) )) {
            $uploaddir = realpath('./sync') . '/';
            
            if( isset($_FILES['userfile']) ) {
                $uploadfile = $uploaddir . basename( $_FILES['userfile']['name'] );
                if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
                    if( extractZip( $_FILES['userfile']['name'], $uploaddir ) ) {
                        unlink( realpath( $uploaddir . 'sync.sql.zip') );
                        maintenance_mode();
                        if( importSQL( $dbconn ) ) {
                            unlink( realpath( $uploaddir . 'sync.sql') );
                        }
                        maintenance_mode(true);
                    }
                }
            }
        }
    }

} else {

    if( isset($_GET['action']) && ($_GET['action']=='send') ) {
    
        backup_mysqldump( $dbconn, true ); 

        $server_url = 'http://' . K12MS_SYNC_SERVER_URI . "/sync.php?action=receive&public_key=" . K12MS_SYNC_PUBLIC_KEY;

        if (function_exists('curl_file_create')) { 
            $cFile = curl_file_create( realpath('sync.sql.zip') );
        } else { 
            $cFile = '@' . realpath('sync.sql.zip');
        }

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $server_url );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");   
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: multipart/form-data'));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);   
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);  
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, array('userfile' => $cFile, 'secret_key'=> K12MS_SYNC_SECRET_KEY) );
        $result = curl_exec( $ch ); 
        curl_close( $ch );
        unlink( realpath('sync.sql.zip') );
    }

}