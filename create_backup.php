<?php

define('BASEPATH', '');
define('ENVIRONMENT', 'production');
require_once("_core/application/config/database.php");
require_once("_core/application/config/config.php");
$dbconn = $db[$active_group];

$dir = "backups";

function compress($filename, $dir, $files){ 
    $zip = new ZipArchive();

    if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
        return false;
    }

    foreach($files as $file) {
        $zip->addFile($file, str_replace($dir . DIRECTORY_SEPARATOR, '', $file));
    }

    $zip->close();

    return true;
} 

function backup_tables_multiple($dbconn,$dir)
{
    
    $link = mysqli_connect($dbconn['hostname'],$dbconn['username'],$dbconn['password']);
    mysqli_select_db($link, $dbconn['database']);
    mysqli_query($link,"SET NAMES 'utf8'");


	$tables = array();
	$result = mysqli_query($link,'SHOW TABLES');
	while($row = mysqli_fetch_row($result))
	{
		$tables[] = $row[0];
	}

    $files = array();
    foreach($tables as $table)
    {
        $return='';
        $filename = $dir . DIRECTORY_SEPARATOR . $table . "-" . date('Y-m-d-h-i-s') . '.sql';
        $files[] = $filename;
        $handle = fopen($filename,'w+');
    
        $result = mysqli_query($link,'SELECT * FROM '.$table);
        $num_fields = mysqli_num_fields($result);

        $return.= 'DROP TABLE '.$table.';';
        $row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
        $return.= "\n\n".$row2[1].";\n\n";

        for ($i = 0; $i < $num_fields; $i++) 
        {
            while($row = mysqli_fetch_row($result))
            {
                $return.= 'INSERT INTO '.$table.' VALUES(';
                for($j=0; $j<$num_fields; $j++) 
                {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = str_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                    if ($j<($num_fields-1)) { $return.= ','; }
                }
                $return.= ");\n";
            }
        }

        fwrite($handle,$return);
        fclose($handle);
    }
    compress($dir . DIRECTORY_SEPARATOR . 'db-backup-' . date('Y-m-d-h-i-s') . "-multiple.sql.zip", $dir, $files );
    foreach($files as $source) {
        unlink($source);
    }

}

function backup_tables_single($dbconn,$dir)
{
    
    $link = mysqli_connect($dbconn['hostname'],$dbconn['username'],$dbconn['password']);
    mysqli_select_db($link, $dbconn['database']);
    mysqli_query($link,"SET NAMES 'utf8'");


    $tables = array();
    $result = mysqli_query($link,'SHOW TABLES');
    while($row = mysqli_fetch_row($result))
    {
        $tables[] = $row[0];
    }

    $return='';
    $filename = $dir . DIRECTORY_SEPARATOR . $dbconn['database'] . "-" . date('Y-m-d-h-i-s') . '.sql';
    $handle = fopen($filename,'w+');

    foreach($tables as $table)
    {
        
    
        $result = mysqli_query($link,'SELECT * FROM '.$table);
        $num_fields = mysqli_num_fields($result);

        $return.= 'DROP TABLE '.$table.';';
        $row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
        $return.= "\n\n".$row2[1].";\n\n";

        for ($i = 0; $i < $num_fields; $i++) 
        {
            while($row = mysqli_fetch_row($result))
            {
                $return.= 'INSERT INTO '.$table.' VALUES(';
                for($j=0; $j<$num_fields; $j++) 
                {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = str_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                    if ($j<($num_fields-1)) { $return.= ','; }
                }
                $return.= ");\n";
            }
        }
    }

    fwrite($handle,$return);
    fclose($handle);

    compress($dir . DIRECTORY_SEPARATOR . 'db-backup-' . date('Y-m-d-h-i-s') . "-single.sql.zip", $dir, array($filename) );
    unlink($filename);

}

function backup_mysqldump($dbconn,$dir)
{    
    $filename = $dbconn['database'] . "-" . date('Y-m-d-h-i-s') . '-single.sql';
    
    $cmd = 'D:\wamp\bin\mysql\mysql5.6.17\bin\mysqldump.exe ';
    $cmd .= ' -u ' . $dbconn['username'];
    $cmd .= ' --password="' . $dbconn['password'] . '"';
    $cmd .= ' ' . $dbconn['database'];
    $cmd .= ' > ' . $dir . DIRECTORY_SEPARATOR . $filename;

    exec($cmd);
    compress($dir . DIRECTORY_SEPARATOR . $filename . ".zip", $dir, array( $dir . DIRECTORY_SEPARATOR . $filename ) );
    unlink($dir . DIRECTORY_SEPARATOR . $filename);
}

function backup_mysqldump_multiple($dbconn,$dir)
{    
    
    $link = mysqli_connect($dbconn['hostname'],$dbconn['username'],$dbconn['password']);
    mysqli_select_db($link, $dbconn['database']);
    mysqli_query($link,"SET NAMES 'utf8'");


    $tables = array();
    $result = mysqli_query($link,'SHOW TABLES');
    while($row = mysqli_fetch_row($result))
    {
        $filename = $dbconn['database'] . "-" . $row[0] . "-" . date('Y-m-d-h-i-s') . '-dump.sql';
        $cmd = 'D:\wamp\bin\mysql\mysql5.6.17\bin\mysqldump.exe ';
        $cmd .= ' -u ' . $dbconn['username'];
        $cmd .= ' --password="' . $dbconn['password'] . '"';
        $cmd .= ' ' . $dbconn['database'];
        $cmd .= ' ' . $row[0];
        $cmd .= ' > ' . $dir . DIRECTORY_SEPARATOR . $filename;

        exec($cmd);
        $files[] = $dir . DIRECTORY_SEPARATOR . $filename;
    }

    
    compress($dir . DIRECTORY_SEPARATOR . $dbconn['database'] . '-' . date('Y-m-d-h-i-s') . "-multiple.sql.zip", $dir, $files );
    foreach($files as $source) {
        unlink($source);
    }
}

$backup_url = $config['base_url'] . $config['index_page'] . "/backup";
if(@$_GET['single']==1) {
    backup_mysqldump( $dbconn, $dir );
} else {
    backup_mysqldump_multiple( $dbconn, $dir );
}

header('location: ' . $backup_url);
exit;
