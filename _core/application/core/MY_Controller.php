<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		if( ! $this->session->userdata('session_started') ) {
			redirect("welcome", "refresh");
		}

		if( $this->session->userdata('session_mode') != K12MS_CONTROL_MODE ) {
			redirect("welcome/logout", "refresh");
		}	
		
	}

	public function _checkPermission($module, $action, $uri='welcome') {
		if( !isset( $this->session->user_permissions->$module ) 
			|| ($this->session->user_permissions->$module->$action==0) ) {
    		redirect( site_url($uri) . "?restricted=" . $module, "location");
  		}
	}

	public function load_school_years($uri='dashboard') {
		$this->load->model('School_year_model');
		$school_years = new $this->School_year_model;
		$this->data['school_years'] = $school_years->populate();
		$this->data['change_school_year_uri'] = $uri;
	}
	
}

class ADMIN_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		if( K12MS_CONTROL_MODE !== 'ADMIN') {
			redirect("welcome/logout", "refresh");
		}

		if( ! $this->session->userdata('session_started') ) {
			redirect("welcome", "refresh");
		}

		if( $this->session->userdata('session_mode') != K12MS_CONTROL_MODE ) {
			redirect("welcome/logout", "refresh");
		}
		
	}

	public function _checkPermission($module, $action, $uri='welcome') {
		if( !isset( $this->session->user_permissions->$module ) 
			|| ($this->session->user_permissions->$module->$action==0) ) {
    		redirect( site_url($uri) . "?restricted=" . $module, "location");
  		}
	}

	public function load_school_years($uri='dashboard') {
		$this->load->model('School_year_model');
		$school_years = new $this->School_year_model;
		$this->data['school_years'] = $school_years->populate();
		$this->data['change_school_year_uri'] = $uri;
	}
	
}

class STUDENT_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		if( K12MS_CONTROL_MODE !== 'STUDENT') {
			redirect("welcome/logout", "refresh");
		}

		if( ! $this->session->userdata('session_started') ) {
			redirect("welcome", "refresh");
		}

		if( $this->session->userdata('session_mode') != K12MS_CONTROL_MODE ) {
			redirect("welcome/logout", "refresh");
		}
		
	}

	public function _checkPermission($module, $action, $uri='welcome') {
		if( !isset( $this->session->user_permissions->$module ) 
			|| ($this->session->user_permissions->$module->$action==0) ) {
    		redirect( site_url($uri) . "?restricted=" . $module, "location");
  		}
	}
	
}