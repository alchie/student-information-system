<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Students_payments_model Class
 *
 * Manipulates `students_payments` table on database

CREATE TABLE `students_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enrolled_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `receipt_number` int(11) NOT NULL,
  `amount` float(10,5) NOT NULL DEFAULT '0.00000',
  `down` int(1) DEFAULT '0',
  `lastmod` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `memo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

 * @package			        Model
 * @version_number	        0.6
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Students_payments_model extends CI_Model {

	protected $id;
	protected $enrolled_id;
	protected $payment_date;
	protected $receipt_number;
	protected $amount;
	protected $down;
	protected $lastmod;
	protected $memo;
	protected $_table_name = 'students_payments';
	protected $_short_name = 'students_payments';
	protected $_dataFields = array();
	protected $_select = array();
	protected $_join = array();
	protected $_where = array();
	protected $_where_or = array();
	protected $_where_in = array();
	protected $_where_in_or = array();
	protected $_where_not_in = array();
	protected $_where_not_in_or = array();
	protected $_like = array();
	protected $_like_or = array();
	protected $_like_not = array();
	protected $_like_not_or = array();
	protected $_having = array();
	protected $_having_or = array();
	protected $_group_by = array();
	protected $_filter = array();
	protected $_order = array();
	protected $_exclude = array();
	protected $_required = array("enrolled_id","payment_date","receipt_number","amount");
	protected $_start = 0;
	protected $_limit = 10;
	protected $_results = FALSE;
	protected $_distinct = FALSE;
	protected $_cache_on = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		parent::__construct();
		$this->_short_name = ($short_name!=NULL && (trim($short_name)!='')) ? $short_name : $this->_short_name;
		if( $db_config ) {
			$this->load->database( $db_config, FALSE, TRUE );
		}
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

	public function getId() {
		return $this->id;
	}

	/**
	* Get row by `id`
	* @param id
	* @return QueryResult
	**/

	public function getById() {
		return $this->_get_by_field('id');
	}


	/**
	* Update table by `id`
	**/

	public function updateById() {
                return $this->_update_by_field('id');
	}


	/**
	* Delete row by `id`
	**/

	public function deleteById() {
		if($this->id != '') {
			return $this->db->delete('students_payments', array('students_payments.id' => $this->id ) );
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: enrolled_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `enrolled_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setEnrolledId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('enrolled_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `enrolled_id` variable
	* @access public
	* @return String;
	*/

	public function getEnrolledId() {
		return $this->enrolled_id;
	}

	/**
	* Get row by `enrolled_id`
	* @param enrolled_id
	* @return QueryResult
	**/

	public function getByEnrolledId() {
		return $this->_get_by_field('enrolled_id');
	}


	/**
	* Update table by `enrolled_id`
	**/

	public function updateByEnrolledId() {
                return $this->_update_by_field('enrolled_id');
	}


	/**
	* Delete row by `enrolled_id`
	**/

	public function deleteByEnrolledId() {
		if($this->enrolled_id != '') {
			return $this->db->delete('students_payments', array('students_payments.enrolled_id' => $this->enrolled_id ) );
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: enrolled_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: payment_date
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `payment_date` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setPaymentDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('payment_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `payment_date` variable
	* @access public
	* @return String;
	*/

	public function getPaymentDate() {
		return $this->payment_date;
	}

	/**
	* Get row by `payment_date`
	* @param payment_date
	* @return QueryResult
	**/

	public function getByPaymentDate() {
		return $this->_get_by_field('payment_date');
	}


	/**
	* Update table by `payment_date`
	**/

	public function updateByPaymentDate() {
                return $this->_update_by_field('payment_date');
	}


	/**
	* Delete row by `payment_date`
	**/

	public function deleteByPaymentDate() {
		if($this->payment_date != '') {
			return $this->db->delete('students_payments', array('students_payments.payment_date' => $this->payment_date ) );
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: payment_date
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: receipt_number
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `receipt_number` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReceiptNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('receipt_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `receipt_number` variable
	* @access public
	* @return String;
	*/

	public function getReceiptNumber() {
		return $this->receipt_number;
	}

	/**
	* Get row by `receipt_number`
	* @param receipt_number
	* @return QueryResult
	**/

	public function getByReceiptNumber() {
		return $this->_get_by_field('receipt_number');
	}


	/**
	* Update table by `receipt_number`
	**/

	public function updateByReceiptNumber() {
                return $this->_update_by_field('receipt_number');
	}


	/**
	* Delete row by `receipt_number`
	**/

	public function deleteByReceiptNumber() {
		if($this->receipt_number != '') {
			return $this->db->delete('students_payments', array('students_payments.receipt_number' => $this->receipt_number ) );
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: receipt_number
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: amount
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	* @return String;
	*/

	public function getAmount() {
		return $this->amount;
	}

	/**
	* Get row by `amount`
	* @param amount
	* @return QueryResult
	**/

	public function getByAmount() {
		return $this->_get_by_field('amount');
	}


	/**
	* Update table by `amount`
	**/

	public function updateByAmount() {
                return $this->_update_by_field('amount');
	}


	/**
	* Delete row by `amount`
	**/

	public function deleteByAmount() {
		if($this->amount != '') {
			return $this->db->delete('students_payments', array('students_payments.amount' => $this->amount ) );
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: amount
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: down
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `down` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDown($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('down', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `down` variable
	* @access public
	* @return String;
	*/

	public function getDown() {
		return $this->down;
	}

	/**
	* Get row by `down`
	* @param down
	* @return QueryResult
	**/

	public function getByDown() {
		return $this->_get_by_field('down');
	}


	/**
	* Update table by `down`
	**/

	public function updateByDown() {
                return $this->_update_by_field('down');
	}


	/**
	* Delete row by `down`
	**/

	public function deleteByDown() {
		if($this->down != '') {
			return $this->db->delete('students_payments', array('students_payments.down' => $this->down ) );
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: down
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lastmod
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lastmod` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastmod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('lastmod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `lastmod` variable
	* @access public
	* @return String;
	*/

	public function getLastmod() {
		return $this->lastmod;
	}

	/**
	* Get row by `lastmod`
	* @param lastmod
	* @return QueryResult
	**/

	public function getByLastmod() {
		return $this->_get_by_field('lastmod');
	}


	/**
	* Update table by `lastmod`
	**/

	public function updateByLastmod() {
                return $this->_update_by_field('lastmod');
	}


	/**
	* Delete row by `lastmod`
	**/

	public function deleteByLastmod() {
		if($this->lastmod != '') {
			return $this->db->delete('students_payments', array('students_payments.lastmod' => $this->lastmod ) );
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lastmod
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: memo
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	* @return String;
	*/

	public function getMemo() {
		return $this->memo;
	}

	/**
	* Get row by `memo`
	* @param memo
	* @return QueryResult
	**/

	public function getByMemo() {
		return $this->_get_by_field('memo');
	}


	/**
	* Update table by `memo`
	**/

	public function updateByMemo() {
                return $this->_update_by_field('memo');
	}


	/**
	* Delete row by `memo`
	**/

	public function deleteByMemo() {
		if($this->memo != '') {
			return $this->db->delete('students_payments', array('students_payments.memo' => $this->memo ) );
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: memo
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

        /**
	* set short name
	* @access public
	* @param  String
	* @return $this;
	*/
	
        public function set_short_name($short_name) {
                $this->_short_name = $short_name;
                return $this;
        }

        // --------------------------------------------------------------------

        /**
	* get short name
	* @access public
	* @return $this;
	*/
	
        public function get_short_name() {
                return $this->_short_name;
        }

        // --------------------------------------------------------------------

        /**
	* set db config
	* @access public
	* @param  String
	* @return $this;
	*/
	
        public function set_db_config($db_config) {
                $this->load->database( $db_config, FALSE, TRUE );
        }

        // --------------------------------------------------------------------
        
        /**
	* set field values and conditions
	* @access protected
	* @param  String
	* @return $this;
	*/

	protected function _set_field($field_name, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->$field_name = ( ($value == '') && ($this->$field_name != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.' . $field_name;
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			$this->__field_conditions($underCondition,$key, $value, $priority);
			
		}
		if( $set_data_field ) {
			$this->_dataFields[] = $field_name;
		}
		return $this;
	}

        // --------------------------------------------------------------------

        /**
	* _get_by_field
	* @access protected
	* @param  String
	* @return $this;
	*/
	
	protected function _get_by_field($field_name) {
		if($this->$field_name != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where($this->_table_name . ' ' . $this->_short_name, array( $this->_short_name . '.' . $field_name => $this->$field_name), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			return $query->result();
		}
		return false;
	}
	
        // --------------------------------------------------------------------

        /**
	* _update_by_field
	* @access protected
	* @param  String
	* @return $this;
	*/
	
	protected function _update_by_field($field_name) {
		if($this->$field_name != '') {
			$this->set_exclude($field_name);
			if( $this->get_data() ) {
				return $this->db->update($this->_table_name . ' ' . $this->_short_name, $this->get_data(), array($this->_short_name . '.' . $field_name => $this->$field_name ) );
			}
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Checks if result not empty 
	* @access public
	* @param  None
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupGroupBy();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get($this->_table_name . ' ' . $this->_short_name, 1);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->_results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->_results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete($this->_table_name);
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->_dataFields = array($fields);
			} else {
				$this->_dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->get_data() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update($this->_table_name . ' ' . $this->_short_name, $this->get_data() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->get_data() ) {
			if( $this->db->insert($this->_table_name, $this->get_data() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->get_data() ) {
			if( $this->db->replace($this->_table_name, $this->get_data() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupGroupBy();
		
		if( $this->_order ) {
			foreach( $this->_order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->_start);
		
		if( $this->_cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get($this->_table_name . ' ' . $this->_short_name);
		
		if( $this->_cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->_results = $result[0];
			
			return $this->_results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->_distinct ) {
			$this->db->distinct();
		}
		
		if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupGroupBy();
		
		if( $this->_order ) {
			foreach( $this->_order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->_limit > 0 ) {
			$this->db->limit( $this->_limit,$this->_start);
		}
		
		if( $this->_cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get($this->_table_name . ' ' . $this->_short_name);
		
		if( $this->_cache_on ) {
			$this->db->cache_off();
		}
			
		$this->_results = $query->result();
		
		return $this->_results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->_limit > 0 ) {
				$this->db->limit( $this->_limit,$this->_start);
			}
			if( $this->_select ) {
					$this->db->select( implode(',' , $this->_select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->_order ) {
				foreach( $this->_order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get($this->_table_name . ' ' . $this->_short_name);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->_where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->_where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

	public function set_field_where($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->_where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->_where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	
	public function setFieldValue($field_name, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field($field_name, $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
                return $this->get_data($exclude);
	}
	
	public function get_data($exclude=NULL) {
		$data = array();

		$fields = array("id","enrolled_id","payment_date","receipt_number","amount","down","lastmod","memo");
		if( count( $this->_dataFields ) > 0 ) {
    		            $fields = $this->_dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->_required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->_exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->_required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->_exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->_required ) ) 
		    && ( ! in_array( $field, $this->_exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}

        // --------------------------------------------------------------------
	
	/**
	* Field Conditions Clauses 
	* @access protected
	* @return Null;
	*/

	protected function __field_conditions($underCondition,$key, $value, $priority) {
		switch( $underCondition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}

	private function setupConditions() {
                return $this->setup_conditions();
	}
	
	private function setup_conditions() {
		$return = FALSE;
		
		$conditions = array_merge( $this->_where, $this->_filter, $this->_where_or, $this->_where_in, $this->_where_in_or, $this->_where_not_in, $this->_where_not_in_or, $this->_like, $this->_like_or, $this->_like_not, $this->_like_not_or, $this->_having, $this->_having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	
	private function hasConditions() {
                return $this->has_conditions();
	}
	
	private function has_conditions() {
		$conditions = array_merge( $this->_where, $this->_filter, $this->_where_or, $this->_where_in, $this->_where_in_or, $this->_where_not_in, $this->_where_not_in_or, $this->_like, $this->_like_or, $this->_like_not, $this->_like_not_or, $this->_having, $this->_having_or );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	
	public function setJoin($table, $connection, $option='left') {
		$this->set_join($table, $connection, $option);
	}

	public function set_join($table, $connection, $option='left') {
		$this->_join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		$this->setup_join();
	}

	private function setup_join() {
		if( $this->_join ) {
			foreach( $this->_join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/

	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
                $this->set_filter($field, $value, $table, $priority, $underCondition);
	}
	
	public function set_filter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = $this->_table_name; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->_filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}

        public function clearFilter($field=NULL) {
                $this->clear_filter( $field );
        }
        
	public function clear_filter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_filter);
			$this->_filter = array();
		} else {
			$newfilter = array();
			foreach($this->_filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->_filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/

	public function setWhere($field, $value=NULL, $priority=NULL) {
                $this->set_where($field, $value, $priority);
	}
	
	public function set_where($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}

        public function clearWhere($field=NULL) {
                $this->clear_where( $field );
        }
        
	public function clear_where($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where);
			$this->_where = array();
		} else {
			$newwhere = array();
			foreach($this->_where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->_where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
                $this->set_where_or($field, $value, $priority);
        }
        
	public function set_where_or($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}

	public function clearWhereOr($field=NULL) {
                $this->clear_where_or( $field );
	}
	
	public function clear_where_or($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_or);
			$this->_where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->_where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->_where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	
        public function setWhereIn($field, $value=NULL, $priority=NULL) {
                $this->set_where_in($field, $value, $priority);
        }
        
	public function set_where_in($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}

	public function clearWhereIn($field=NULL) {
                $this->clear_where_in( $field );
	}
	
	public function clear_where_in($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_in);
			$this->_where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->_where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->_where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/

	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
                $this->set_where_in_or($field, $value, $priority);
	}
	
	public function set_where_in_or($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	public function clearWhereInOr($field=NULL) {
                $this->clear_where_in_or($field);
	}
	
	public function clear_where_in_or($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_in_or);
			$this->_where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->_where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->_where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/

	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
                $this->set_where_not_in($field, $value, $priority);
	}
	
	public function set_where_not_in($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotIn($field=NULL) {
                $this->clear_where_not_in($field);
	}
	
	public function clear_where_not_in($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_not_in);
			$this->_where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->_where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->_where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
                $this->set_where_not_in_or($field, $value, $priority);
	}
	
	public function set_where_not_in_or($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotInOr($field=NULL) {
                $this->clear_where_not_in_or($field);
	}
	
	public function clear_where_not_in_or($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_not_in_or);
			$this->_where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->_where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->_where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/

	public function setLike($field, $value=NULL, $priority=NULL) {
                $this->set_like($field, $value, $priority);
	}
	
	public function set_like($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

        public function clearLike($field=NULL) {
                $this->clear_like( $field );
        }
        
	public function clear_like($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like);
			$this->_like = array();
		} else {
			$newlike = array();
			foreach($this->_like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->_like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/

	public function setLikeOr($field, $value=NULL, $priority=NULL) {
                $this->set_like_or($field, $value, $priority);
	}
	
	public function set_like_or($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		$this->clear_like_or($field);
	}

	public function clear_like_or($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_or);
			$this->_like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->_like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->_like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/

	public function setLikeNot($field, $value=NULL, $priority=NULL) {
                $this->set_like_not($field, $value, $priority);
	}
	
	public function set_like_not($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

        public function clearLikeNot($field=NULL) {
		$this->clear_like_not($field);
	}
	
	public function clear_like_not($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_not);
			$this->_like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->_like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->_like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/

	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
                $this->set_like_not_or($field, $value, $priority);
	}
	
	public function set_like_not_or($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		$this->clear_like_not_or($field);
	}

	public function clear_like_not_or($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_not_or);
			$this->_like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->_like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->_like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	
	public function setHaving($field, $value=NULL, $priority=NULL) {
		$this->set_having($field, $value, $priority);
	}

	public function set_having($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->_having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		$this->clear_having($field);
	}

	public function clear_having($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_having);
			$this->_having = array();
		} else {
			$newhaving = array();
			foreach($this->_having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->_having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/

	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		$this->set_having_or($field, $value, $priority);
	}

	public function set_having_or($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->_having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority )); 
	}

	public function clearHavingOr($field=NULL) {
		$this->clear_having_or($field);
	}
	
	public function clear_having_or($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_having_or);
			$this->_having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->_having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->_having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/

	public function setGroupBy($fields) {
                return $this->set_group_by($fields);
	}
	
	public function set_group_by($fields) {
		if( is_array( $fields ) ) { 
			$this->_group_by = array_merge( $this->_group_by, $fields );
		} else {
			$this->_group_by[] = $fields;
		}
		return $this;
	}

        private function setupGroupBy() {
                $this->setup_group_by();
        }
        
	private function setup_group_by() {
		if( $this->_group_by ) {
			$this->db->group_by( $this->_group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->_start = $value;
		return $this;
	}

	public function set_start($value) {
		$this->_start = $value;
		return $this;
	}

        // --------------------------------------------------------------------

        /**
	* Get Start  
	* @access public
	*/

	public function getStart() {
		return $this->_start;
	}
	
	public function get_start() {
		return $this->_start;
	}

        // --------------------------------------------------------------------
        
	/**
	* Set Limit  
	* @access public
	*/

	public function setLimit($value) {
		$this->_limit = $value;
		return $this;
	}
	
	public function set_limit($value) {
		$this->_limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

        /**
	* Get Limit  
	* @access public
	*/

	public function getLimit() {
		return $this->_limit;
	}
	
	public function get_limit() {
		return $this->_limit;
	}

	// --------------------------------------------------------------------
	
	/**
	* Set Order  
	* @access public
	*/
        public function setOrder($field, $orientation='asc') {
                return $this->set_order($field, $orientation);
        }
        
	public function set_order($field, $orientation='asc') {
		$this->_order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/
        public function setSelect($select, $priority=NULL) {
                return $this->set_select($select, $priority);
        }
        
	public function set_select($select, $priority=NULL) {
		if( is_array ( $select ) ) {
			$this->_select = array_merge( $this->_select, $select );
		} else {
			if( is_null( $priority ) ) {
				$this->_select[] = $select;
			} else {
				$this->_select[$priority] = $select;
			}
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/
	
        public function setExclude($exclude) {
                return $this->set_exclude( $exclude );
        }
        
	public function set_exclude($exclude) {
		if( is_array ( $exclude ) ) {
			$this->_exclude = array_merge( $this->_exclude, $exclude );
		} else {
			$this->_exclude[] = $exclude;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

        public function isDistinct($distinct=TRUE) {
		$this->_distinct = $distinct;
		return $this;
	}
	
	public function is_distinct($distinct=TRUE) {
		$this->_distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function countAll() {
		return $this->count_all();
	}

	public function count_all() {
		return  $this->db->count_all($this->_table_name . ' ' . $this->_short_name);
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function countAllResults() {
		return $this->count_all_results();
	}

	public function count_all_results() {
		$this->setup_conditions();
		$this->setup_join();
		$this->setup_group_by();
		$this->db->from($this->_table_name . ' ' . $this->_short_name);
		return  $this->db->count_all_results();
	}

	// --------------------------------------------------------------------

	/**
	* SQL Functions  
	* @access public
	*/
	public function sqlFunction($function, $field, $alias=false, $hasConditions=true) {
		return $this->sql_function($function, $field, $alias, $hasConditions);
	}

	public function sql_function($function, $field, $alias=false, $hasConditions=true) {
                if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
		}
		if( $alias ) {
                        $this->db->select($function.'('.$field.') AS ' . $alias);
		} else {
                        $this->db->select($function.'('.$field.')');
		}
		if( $hasConditions ) {
                        $this->setupJoin();
                        $this->setupConditions();
                        $this->setupGroupBy();
                }
		return  $this->db->get($this->_table_name . ' ' . $this->_short_name, 1);
	}	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/

	public function cacheOn() {
		$this->cache_on();
	}

	public function cache_on() {
		$this->_cache_on = TRUE;
	}

	public function cacheOff() {
		$this->cache_off();
	}

	public function cache_off() {
		$this->_cache_on = FALSE;
	}

	public function setDbCache($value=false) {
		$this->set_db_cache($value);
	}

	public function set_db_cache($value=false) {
		$this->_cache_on = $value;
	}
}

/* End of file students_payments_model.php */
/* Location: ./application/models/students_payments_model.php */
