<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "students";
		$this->data['sidebar_menu_sub'] = "students";
		
		$this->_checkPermission('students', 'view', 'welcome');
		
		$this->data['search_enabled'] = true;
		$this->data['current_year'] = get_ams_config('school', 'current_school_year');

		$this->load->model('Parents_information_model');
		$this->load->model('Students_enrolled_model');
		$this->load->model('Students_information_model');
		$this->load->model('Students_address_model');
		$this->load->model('Students_contacts_model');
		$this->load->model('Students_medical_model');
		$this->load->model('Students_parents_model');
		$this->load->model('Students_meta_model');

	}
	
	protected function _search_redirect() {
		if( $this->input->get("q") ) {
			header("location: " . site_url("students") . "?q=" . $this->input->get("q") );
			exit;
		}
	}
	
	protected function _navbar($student) {
		
		$enrolled = new $this->Students_enrolled_model;
		$enrolled->setStudentId($student->id, true);
		$enrolled->setOrder('date_enrolled', 'DESC');
		$enrolled->setJoin('school_year', 'school_year.id=students_enrolled.school_year');
		$enrolled->setJoin('sections', 'sections.id=students_enrolled.section_id');
		$enrolled->setSelect('students_enrolled.*, school_year.label as sy_label, sections.name as section_name');
		$this->data['enrollment_history'] = $enrolled->populate();
		
		$next_student = new $this->Students_enrolled_model('se');
		$next_student->setJoin('students_information si', 'si.id = se.student_id');
		$next_student->setJoin('school_year', 'school_year.id=se.school_year');
		$next_student->setJoin('sections', 'sections.id=se.section_id');
		$next_student->setSelect('se.*, school_year.label as sy_label, sections.name as section_name');
		$next_student->setWhere("se.student_id = (SELECT student_id from students_enrolled se JOIN students_information si ON se.student_id=si.id WHERE CONCAT(si.lastname,' ',si.firstname,' ',si.middlename) > '{$student->lastname} {$student->firstname} {$student->middlename}' ORDER BY si.lastname ASC, si.firstname ASC, si.middlename ASC LIMIT 1)");
		$next_student->setOrder('si.lastname', 'ASC');
		$this->data['next_student'] = $next_student->get();

		$previous_student = new $this->Students_enrolled_model('se');
		$previous_student->setJoin('students_information si', 'si.id = se.student_id');
		$previous_student->setJoin('school_year', 'school_year.id=se.school_year');
		$previous_student->setJoin('sections', 'sections.id=se.section_id');
		$previous_student->setSelect('se.*, school_year.label as sy_label, sections.name as section_name');
		$previous_student->setWhere("se.student_id = (SELECT student_id from students_enrolled se JOIN students_information si ON se.student_id=si.id WHERE CONCAT(si.lastname,' ',si.firstname,' ',si.middlename) < '{$student->lastname} {$student->firstname} {$student->middlename}' ORDER BY si.lastname DESC, si.firstname DESC, si.middlename DESC LIMIT 1)");
		$previous_student->setOrder('si.lastname', 'ASC');
		
		$this->data['previous_student'] = $previous_student->get();

	}

	public function index($start=0)
	{
		if( $start > 0 ) {
			$this->_search_redirect();
		}

		$students = new $this->Students_information_model('si');
		$students->setStart( $start );
		$students->setLimit( 20 );
		$students->setWhere( "si.trash = 0" );
		if( $this->input->get("q") ) {
			$students->setWhere( "(si.idn LIKE '%". $this->input->get("q") ."%'" );
			$students->setWhereOr( "si.lastname LIKE '%". $this->input->get("q") ."%'" );
			$students->setWhereOr( "si.firstname LIKE '%". $this->input->get("q") ."%'" );
			$students->setWhereOr( "si.middlename LIKE '%". $this->input->get("q") ."%')" );
		}
		
		$students->setOrder('si.lastname', 'ASC');
		$students->setOrder('si.firstname', 'ASC');
		$students->setOrder('si.middlename', 'ASC');
		$config['base_url'] = site_url("students/index");
		$config['total_rows'] = $students->count_all_results();
		$config['per_page'] = $students->getLimit();

		$this->data['pagination'] = bootstrap_pagination($config);
		$this->data['students'] = $students->populate();
		$this->data['students_count'] = $students->count_all_results();
		
		if( $students->count_all_results() > 0) {
			$this->load->view('students/students', $this->data);
		} else {
			$this->load->view('students/no_student_found', $this->data);
		}
	}
	
	public function add() {
		
		$this->_search_redirect();
		
		if( count($this->input->post()) > 0 ) {
			//$this->form_validation->set_rules('idn', 'Student ID', 'trim|required|is_unique[students_information.idn]');
			$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('middlename', 'Middle Name', 'trim');
			$this->form_validation->set_rules('birthday', 'Birthday', 'trim');
			$this->form_validation->set_rules('religion', 'Religion', 'trim');
			$this->form_validation->set_rules('lrn', 'LRN', 'trim|is_unique[students_information.lrn]');
			$this->form_validation->set_rules('google_id', 'Google ID', 'trim|is_unique[students_information.google_id]');
			$this->form_validation->set_rules('google_userid', 'Google User ID', 'trim|is_unique[students_information.google_userid]');
			if( $this->input->post('lrn') != '') {
				$this->form_validation->set_rules('lrn', 'LRN', 'is_unique[students_information.lrn]');
			}
			if ($this->form_validation->run() == TRUE)
			{
				$this->load->model('Students_information_model');
				$student = new $this->Students_information_model;
				$student->setIdn( $this->input->post('idn') );
				$student->setLastname( ucwords( strtolower( trim( $this->input->post('lastname') ) ) ) );
				$student->setFirstname( ucwords( strtolower( trim( $this->input->post('firstname') ) ) ) );
				$student->setMiddlename( ucwords( strtolower( trim( $this->input->post('middlename') ) ) ) );
				$student->setBirthday( date('Y-m-d', strtotime($this->input->post('birthday'))));
				$student->setBirthplace( $this->input->post('birthplace') );
				$student->setReligion( ucwords( strtolower( $this->input->post('religion') ) ) );
				$student->setGender( $this->input->post('gender') );
				$student->setLrn( $this->input->post('lrn') );
				$student->setTrash( 0 );
				$student->insert();
				redirect("students/update/" . $student->getId(), "location");
			}
		}
		
		$this->data['sidebar_menu_sub'] = "students/add";
		$this->load->view( 'students/students_add', $this->data );
	}
	
	public function update($id) {
		
		$this->data['method_name'] = 'profile';

		$this->_search_redirect();
		
		$student = new $this->Students_information_model;
		$student->setId( $id, true, false );
		
		if( $this->input->post() ) { 
			$this->form_validation->set_rules('idn', 'Student ID', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('middlename', 'Middle Name', 'trim');
			$this->form_validation->set_rules('birthday', 'Birthday', 'trim');
			$this->form_validation->set_rules('religion', 'Religion', 'trim');
			$this->form_validation->set_rules('lrn', 'LRN', 'trim');
			if ($this->form_validation->run() == TRUE)
			{
				$student->setIdn( $this->input->post('idn') );
				$student->setLastname( $this->input->post('lastname') );
				$student->setFirstname( $this->input->post('firstname') );
				$student->setMiddlename( $this->input->post('middlename') );
				$student->setBirthday( date('Y-m-d', strtotime($this->input->post('birthday'))));
				$student->setBirthplace( $this->input->post('birthplace') );
				$student->setReligion( ucwords( strtolower( $this->input->post('religion') ) ) );
				$student->setGender( $this->input->post('gender') );
				$student->setLrn( $this->input->post('lrn') );
				$student->update();
			}
		}
		
		$student->setSelect('students_information.*, (SELECT `meta_value` from `students_meta` m WHERE m.student_id = students_information.id AND m.meta_key="emergency" LIMIT 1) as emergency');
		
		$dStudent = $student->get();
		$this->data['student'] = $dStudent;
		
		
		$address = new $this->Students_address_model;
		$address->setStudentId( $id, true );
		$address->setOrder( 'id', 'DESC' );
		$this->data['addresses'] = $address->populate();
		

		$contacts = new $this->Students_contacts_model;
		$contacts->setStudentId( $id, true );
		$contacts->setOrder( 'id', 'DESC' );
		$this->data['contacts'] = $contacts->populate();
		

		$medical = new $this->Students_medical_model;
		$medical->setStudentId( $id, true );
		$medical->setOrder( 'id', 'DESC' );
		$this->data['medicals'] = $medical->populate();
		

		$parents = new $this->Students_parents_model;
		$parents->setStudentId( $id, true );
		$parents->setOrder( 'id', 'DESC' );
		//$parents->setJoin('students_information', 'students_information.id = students_parents.student_id');
		$parents->setJoin('parents_information', 'parents_information.id = students_parents.parent_id');
		$parents->setSelect('parents_information.*, students_parents.relationship, students_parents.id as sp_id');
		$this->data['parents'] = $parents->populate();
		

		$emergency = new $this->Students_parents_model;
		$emergency->setId( $dStudent->emergency, true );
		$emergency->setSelect( 'students_parents.*' );
		$emergency->setJoin('parents_information', 'parents_information.id = students_parents.parent_id');
		$emergency->setSelect( 'parents_information.lastname, parents_information.firstname' );
		$emergency->setSelect('(SELECT `meta_value` FROM `parents_meta` pm WHERE pm.parent_id=students_parents.parent_id AND pm.meta_key="primary_address") as primary_address_id');
		$emergency->setSelect('(SELECT `address` FROM `parents_address` pa WHERE pa.id=primary_address_id) as primary_address');
		$emergency->setSelect('(SELECT `meta_value` FROM `parents_meta` pm WHERE pm.parent_id=students_parents.parent_id AND pm.meta_key="primary_contact") as primary_contact_id');
		$emergency->setSelect('(SELECT `contact` FROM `parents_contacts` pa WHERE pa.id=primary_contact_id) as primary_contact_number');
		$emergency->setSelect('(SELECT `remarks` FROM `parents_contacts` pa WHERE pa.id=primary_contact_id) as primary_contact_remarks');
		$this->data['emergency'] = $emergency->get();
		
		$meta = new $this->Students_meta_model;
		$meta->setStudentId($id,true);
		$meta->setMetaKey('profile_pic', true);
		$this->data['profile_pic'] = $meta->get();

		$this->_navbar($dStudent);

		$this->load->view('students/students_update', $this->data);
	}


	public function update_google($id) {
		
		if( $this->input->post() ) {
			$this->form_validation->set_rules('google_id', 'Google ID', 'trim');
			$this->form_validation->set_rules('google_userid', 'Google User ID', 'trim');
			if ($this->form_validation->run() == TRUE)
			{
					$student = new $this->Students_information_model;
					$student->setId( $id, true, false );
					if( $this->input->post('google_id') ) {
						$student->setGoogleId( $this->input->post('google_id'), false, true );
					}
					if( $this->input->post('google_userid') ) {
						$student->setGoogleUserid( $this->input->post('google_userid'), false, true ); 
					}
					$student->update();
			} else {
				redirect( site_url( "students/update/" . $id ) . "?error=not_unique" );
			}
		}
		redirect("students/update/" . $id );
	}

	public function profile($id) {
		$this->data['sub_menu'] = 'profile';
		$this->update($id);
	}

	public function enrollment($id) {
		
		$this->_search_redirect();
		
		$this->data['sub_menu'] = 'enrollment';

		$this->load->model('Students_information_model');
		$student = new $this->Students_information_model;
		$student->setId( $id, true, false );
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('idn', 'Student ID', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('middlename', 'Middle Name', 'trim');
			$this->form_validation->set_rules('birthday', 'Birthday', 'trim');
			$this->form_validation->set_rules('religion', 'Religion', 'trim');
			$this->form_validation->set_rules('lrn', 'LRN', 'trim');
			if ($this->form_validation->run() == TRUE)
			{
				$student->setIdn( $this->input->post('idn') );
				$student->setLastname( $this->input->post('lastname') );
				$student->setFirstname( $this->input->post('firstname') );
				$student->setMiddlename( $this->input->post('middlename') );
				$student->setBirthday( date('Y-m-d', strtotime($this->input->post('birthday'))));
				$student->setBirthplace( $this->input->post('birthplace') );
				$student->setReligion( ucwords( strtolower( $this->input->post('religion') ) ) );
				$student->setGender( $this->input->post('gender') );
				$student->setLrn( $this->input->post('lrn') );
				$student->update();
				//redirect("students/update/" . $student->getId(), "refresh");
			}
		}
		
		$student->setSelect('students_information.*, (SELECT `meta_value` from `students_meta` m WHERE m.student_id = students_information.id AND m.meta_key="emergency" LIMIT 1) as emergency');
		
		$dStudent = $student->get();
		$this->data['student'] = $dStudent;
		
		$this->load->view('students/students_enrollment', $this->data);
	}

	public function ledger($id) {
		
		$this->_search_redirect();
		
		$this->data['sub_menu'] = 'ledger';
		
		$this->load->model('Students_information_model');
		$student = new $this->Students_information_model;
		$student->setId( $id, true, false );
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('idn', 'Student ID', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('middlename', 'Middle Name', 'trim');
			$this->form_validation->set_rules('birthday', 'Birthday', 'trim');
			$this->form_validation->set_rules('religion', 'Religion', 'trim');
			$this->form_validation->set_rules('lrn', 'LRN', 'trim');
			if ($this->form_validation->run() == TRUE)
			{
				$student->setIdn( $this->input->post('idn') );
				$student->setLastname( $this->input->post('lastname') );
				$student->setFirstname( $this->input->post('firstname') );
				$student->setMiddlename( $this->input->post('middlename') );
				$student->setBirthday( date('Y-m-d', strtotime($this->input->post('birthday'))));
				$student->setBirthplace( $this->input->post('birthplace') );
				$student->setReligion( ucwords( strtolower( $this->input->post('religion') ) ) );
				$student->setGender( $this->input->post('gender') );
				$student->setLrn( $this->input->post('lrn') );
				$student->update();
				//redirect("students/update/" . $student->getId(), "refresh");
			}
		}
		
		$student->setSelect('students_information.*, (SELECT `meta_value` from `students_meta` m WHERE m.student_id = students_information.id AND m.meta_key="emergency" LIMIT 1) as emergency');
		
		$dStudent = $student->get();
		$this->data['student'] = $dStudent;
		
		$this->load->view('students/students_ledger', $this->data);
	}
	
	
	public function address($id, $action='add') {
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('address', 'Address', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$this->load->model('Students_address_model');
				$address = new $this->Students_address_model;
				
				switch( $action ) {
					case 'add':
					default:
						$address->setStudentId( $id );
						$address->setAddress( $this->input->post('address') );
						$address->insert();
					break;
				}
			}
		}
		redirect( "students/update/" . $id );
	}
	
	public function update_address($id, $address_id) {
		
		$this->_search_redirect();
		
		$this->load->model('Students_address_model');
		$address = new $this->Students_address_model;
		$address->setSelect('students_address.*');
		$address->setSelect('(SELECT `meta_value` FROM `students_meta` pm WHERE pm.student_id=students_address.student_id AND pm.meta_key="primary_address") as primary_address');
		$address->setId($address_id, true, false);
		$address->setStudentId($id, true, false);
		
		if( $this->input->get('action') == 'delete' ) {
			$address->delete();
			redirect( "students/update/" . $id );
		}
		if( $this->input->get('action') == 'set_primary' ) {
			$this->load->model('Students_meta_model');
			$meta = new $this->Students_meta_model;
			$meta->setStudentId($id, true);
			$meta->setMetaKey('primary_address', true);
			$meta->setMetaValue($address_id);
			if( $meta->nonEmpty() == TRUE ) {
				$meta->setExclude(array('id', 'student_id', 'meta_key'));
				$meta->update();
			} else {
				$meta->insert();
			}
			redirect( "students/update_address/" . $id . "/" . $address_id);
		}
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('address', 'Address', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$address->setAddress($this->input->post('address'));
				$address->update();
				redirect( "students/update/" . $id );
			}
		}
		$this->data['address'] = $address->get();
		$this->data['student_id'] = $id;
		$this->data['address_id'] = $address_id;
		
		$this->load->view('students/update_address', $this->data);
	}
	
	public function contact_numbers($id, $action='add') {
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('number', 'Contact Number', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$this->load->model('Students_contacts_model');
				$contacts = new $this->Students_contacts_model;
				
				switch( $action ) {
					case 'add':
					default:
						$contacts->setStudentId( $id );
						$contacts->setContact( $this->input->post('number') );
						$contacts->setRemarks( $this->input->post('remarks') );
						$contacts->insert();
					break;
				}
			}
		}
		redirect( "students/update/" . $id );
	}
	
	public function update_contact($id, $contact_id) {
		
		$this->_search_redirect();
		
		$this->load->model('Students_contacts_model');
		$contacts = new $this->Students_contacts_model;
		$contacts->setSelect('students_contacts.*');
		$contacts->setSelect('(SELECT `meta_value` FROM `students_meta` pm WHERE pm.student_id=students_contacts.student_id AND pm.meta_key="primary_contact") as primary_contact');
		$contacts->setId($contact_id, true, false);
		$contacts->setStudentId($id, true, false);
		
		if( $this->input->get('action') == 'delete' ) {
			$contacts->delete();
			redirect( "students/update/" . $id );
		}
		if( $this->input->get('action') == 'set_primary' ) {
			$this->load->model('Students_meta_model');
			$meta = new $this->Students_meta_model;
			$meta->setStudentId($id, true);
			$meta->setMetaKey('primary_contact', true);
			$meta->setMetaValue($contact_id);
			if( $meta->nonEmpty() == TRUE ) {
				$meta->setExclude(array('id', 'student_id', 'meta_key'));
				$meta->update();
			} else {
				$meta->insert();
			}
			redirect( "students/update_contact/" . $id . "/" . $contact_id);
		}
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('number', 'Contact Number', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$contacts->setContact($this->input->post('number'));
				$contacts->setRemarks( $this->input->post('remarks') );
				$contacts->update();
				redirect( "students/update/" . $id );
			}
		}
		$this->data['contact'] = $contacts->get();
		$this->data['student_id'] = $id;
		$this->data['contact_id'] = $contact_id;
		
		$this->load->view('students/update_contact', $this->data);
	}

	public function medical_info($id, $action='add') {
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('info', 'Medical Info', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$this->load->model('Students_medical_model');
				$medical = new $this->Students_medical_model;
				
				switch( $action ) {
					case 'add':
					default:
						$medical->setStudentId( $id );
						$medical->setInfo( $this->input->post('info') );
						$medical->insert();
					break;
				}
			}
		}
		redirect( "students/update/" . $id );
	}
	
	public function update_medical($id, $medical_id) {
		
		$this->_search_redirect();
		
		$this->load->model('Students_medical_model');
		$medical = new $this->Students_medical_model;
		$medical->setId($medical_id, true, false);
		$medical->setStudentId($id, true, false);
		
		if( $this->input->get('action') == 'delete' ) {
			$medical->delete();
			redirect( "students/update/" . $id );
		}
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('info', 'Medical Info', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$medical->setInfo( $this->input->post('info') );
				$medical->update();
				redirect( "students/update/" . $id );
			}
		}
		$this->data['medical'] = $medical->get();
		$this->data['student_id'] = $id;
		$this->data['medical_id'] = $medical_id;
		
		$this->load->view('students/update_medical', $this->data);
	}
	
	public function assign_parent( $id, $parent_id=false ) {
		
		$this->_search_redirect();
		
		$this->load->model('Students_information_model');
		$student = new $this->Students_information_model;
		$student->setId( $id, true, false );
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('parent_id', 'Parent', 'required');
			$this->form_validation->set_rules('relationship', 'Relationship', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$this->load->model('Students_parents_model');
				$assignment = new $this->Students_parents_model;
				$assignment->setStudentId( $id );
				$assignment->setParentId( $this->input->post('parent_id') );
				$assignment->setRelationship( $this->input->post('relationship') );
				$assignment->insert();
				redirect("students/update/" . $id, "location");
			}
		}
		$dStudent = $student->get();
		$this->data['student'] = $dStudent;

		$this->data['parents'] = null;
		if( $this->input->get('name') ) {
			$parents = new $this->Parents_information_model;
			$parents->setWhereOr( "lastname LIKE '%". $this->input->get("name") ."%'" );
			$parents->setWhereOr( "firstname LIKE '%". $this->input->get("name") ."%'" );
			$parents->setWhereOr( "middlename LIKE '%". $this->input->get("name") ."%'" );
			$this->data['parents'] = $parents->populate();
		}

		if( $parent_id ) {
			$current_parent = new $this->Parents_information_model;
			$current_parent->setId($parent_id,true);
			$this->data['current_parent'] = $current_parent->get();
		}
		
		$this->_navbar($dStudent);

		$this->load->view('students/assign_parent', $this->data);
	}
	
	public function update_parent( $id, $sp_id ) {
		
		$this->_search_redirect();
		
		if( $this->input->get('action') == 'delete' ) {
			$this->load->model('Students_parents_model');
			$sp = new $this->Students_parents_model;
			$sp->setId( $sp_id, true );
			$sp->delete();
			redirect("students/update/" . $id, "location");
		}
		if( $this->input->get('action') == 'set_emergency' ) {
			$this->load->model('Students_meta_model');
			$meta = new $this->Students_meta_model;
			$meta->setStudentId($id, true);
			$meta->setMetaKey('emergency', true);
			$meta->setMetaValue($sp_id);
			if( $meta->nonEmpty() == TRUE ) {
				$meta->setExclude(array('id', 'student_id', 'meta_key'));
				$meta->update();
			} else {
				$meta->insert();
			}
			redirect( "students/update_parent/" . $id . "/" . $sp_id);
		}
		
		$this->load->model('Students_information_model');
		$student = new $this->Students_information_model;
		$student->setId( $id, true, false );
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('relationship', 'Relationship', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$this->load->model('Students_parents_model');
				$assignment = new $this->Students_parents_model;
				$assignment->setId( $sp_id, true );
				$assignment->setRelationship( $this->input->post('relationship') );
				$assignment->setExclude(array('student_id','parent_id'));
				$assignment->updateById();
				redirect("students/update/" . $id, "location");
			}
		}
		
		$this->data['student'] = $student->get();
		
		$this->load->model('Students_parents_model');
		$sp = new $this->Students_parents_model;
		$sp->setId( $sp_id, true );
		$sp->setSelect('students_parents.id as sp_id, students_parents.relationship as relationship');
		$sp->setJoin( 'parents_information', 'parents_information.id = students_parents.parent_id' );
		$sp->setSelect('parents_information.*');
		
		$sp->setJoin( 'students_information', 'students_information.id = students_parents.student_id' );
		$sp->setSelect('students_information.lastname as sLastname');
		$sp->setSelect('students_information.firstname as sFirstname');
		$sp->setSelect('(SELECT `meta_value` from `students_meta` m WHERE m.student_id = students_parents.student_id AND m.meta_key="emergency" LIMIT 1) as emergency');
		$this->data['sp'] = $sp->get();
		
		$this->load->view('students/update_parent', $this->data);
	}
	
	public function meta( $id ) {
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('key', 'Meta Key', 'required');
			$this->form_validation->set_rules('value', 'Meta Value', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$this->load->model('Students_meta_model');
				$meta = new $this->Students_meta_model;
				$meta->setStudentId( $id, true );
				$meta->setMetaKey( $this->input->post('key'), true );
				$meta->setMetaValue( $this->input->post('value') );
				if( $meta->nonEmpty() == FALSE ) {
					$meta->insert();
				} else {
					$meta->setExclude('id');
					$meta->update();
				}
			}
		}
		redirect( "students/update/" . $id );
	}

	public function delete($id) {

		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('confirmed', 'Confirmed', 'required');
			$this->form_validation->set_rules('id', 'Student ID', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				if( ($this->input->post('confirmed') == 'YES') && ( $id == $this->input->post('id') ) ) {
					$this->load->model('Students_information_model');
					$student = new $this->Students_information_model;
					$student->setId( $id, true);
					$student->setTrash(1,false,true);
					$student->update();
					//$student->delete();
/*
					$this->load->model('Students_address_model');
					$address = new $this->Students_address_model;
					$address->setStudentId($id, true);
					$address->delete();

					$this->load->model('Students_contacts_model');
					$contacts = new $this->Students_contacts_model;
					$contacts->setStudentId($id, true);
					$contacts->delete();

					$this->load->model('Students_enrolled_model');
					$enrolled = new $this->Students_enrolled_model;
					$enrolled->setStudentId($id, true);
					$enrolled->delete();

					$this->load->model('Students_medical_model');
					$medical = new $this->Students_medical_model;
					$medical->setStudentId($id, true);
					$medical->delete();

					$this->load->model('Students_meta_model');
					$meta = new $this->Students_meta_model;
					$meta->setStudentId($id, true);
					$meta->delete();

					$this->load->model('Students_parents_model');
					$parents = new $this->Students_parents_model;
					$parents->setStudentId($id, true);
					$parents->delete();
*/
				}
			}
		}
		redirect( "students" );
	}

	public function upload_picture($id) {

			$config['upload_path']          = './uploads/';
	        $config['allowed_types']        = 'gif|jpg|png';
	        $config['max_size']             = 100;
	        $config['max_width']            = 1024;
	        $config['max_height']           = 768;

	        $this->load->library('upload', $config);

	        if ( ! $this->upload->do_upload('picture'))
	        {
	                show_error( $this->upload->display_errors() );
	        }
	        else
	        {
	                $data = $this->upload->data();
	                $meta = new $this->Students_meta_model;
	                $meta->setStudentId($id,true);
	                $meta->setMetaKey('profile_pic');
	                $meta->setMetaValue($data['file_name']);
	                if($meta->nonEmpty()) {
	                	$results = $meta->getResults();
	                	$file = "./uploads/{$results->meta_value}";
	                	if( file_exists($file) ) {
	                		unlink($file);
	                	}
	                	$meta->update();
	                } else {
	                	$meta->insert();
	            	}
	                
	        }
	        redirect( "students/profile/{$id}" );
       
	}
}
