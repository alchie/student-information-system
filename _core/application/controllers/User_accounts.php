<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_accounts extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "administration";
		$this->data['sidebar_menu_sub'] = "user_accounts";
		
		$this->_checkPermission('administration', 'view', 'welcome');
		$this->_checkPermission('user_accounts', 'view', 'welcome');

		$this->load->model('User_groups_model');
		$this->load->model('User_accounts_model');
		$this->load->model('User_accounts_permissions_model');
		$this->load->model('User_groups_permissions_model');

		$this->load->model('Students_sessions_model');
		$this->load->model('Students_information_model');

		$this->data['search_enabled'] = true;

	}

	protected function _search_redirect() {
		if( $this->input->get("q") ) {
			header("location: " . site_url("user_accounts") . "?q=" . $this->input->get("q") );
			exit;
		}
	}
	
	public function index()
	{
		$users = new $this->User_accounts_model;
		$users->setJoin('user_groups', 'user_accounts.usergroup=user_groups.id');
		$users->setSelect('user_accounts.*, user_groups.group_name');
		$users->setOrder('name', 'ASC');
		$this->data['users'] = $users->populate();

		$groups = new $this->User_groups_model;
		$groups->setActive(1,true);
		$groups->setOrder('group_name', 'ASC');
		$this->data['usergroups'] = $groups->populate();		
		$this->load->view('administration/user_accounts', $this->data );
	}
	
	public function add_user() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('control_mode', 'Control Mode', 'required');
			$this->form_validation->set_rules('usergroup', 'Usergroup', 'required');
			$this->form_validation->set_rules('nickname', 'Nickname', 'required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[user_accounts.username]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			
			 if ($this->form_validation->run() == TRUE) {
				$user = new $this->User_accounts_model();
				$user->setUsername( $this->input->post("username", true) );
				$user->setPassword( md5( $this->input->post("password", true) ) );
				$user->setName( $this->input->post("nickname", true) );
				$user->setUsergroup( $this->input->post("usergroup", true) );
				$user->setControlMode( $this->input->post("control_mode", true) );
				$user->setStatus(1);
				if( $user->insert() ) {
					redirect("user_accounts/update_user/" . $user->getId(), "location");
				}
			}
		}
		redirect("user_accounts");
		
	}
	
	public function update_user($user_id) {
		
		$this->_search_redirect();
		
		$this->load->model('User_accounts_model');
		$user = new $this->User_accounts_model();
		$user->setId( $user_id, true, false );

		if($this->input->post('user_details')) {
			$user->setName($this->input->post('user_details')['nickname'], false, true);
			$user->setUsername($this->input->post('user_details')['username'], false, true);
			$user->setUsergroup($this->input->post('user_details')['usergroup'], false, true);
			$user->setStatus((isset($this->input->post('user_details')['status'])) ? 1 : 0, false, true);
			$user->update();
		}

		if($this->input->post('change_password')) {
			if( $this->input->post('change_password')['password'] == $this->input->post('change_password')['password2'] ) 
			{
				$user->setPassword( md5( $this->input->post('change_password')['password'] ), false, true );
				$user->update();
			}
		}

		$groups = new $this->User_groups_model;
		$groups->setActive(1,true);
		$groups->setOrder('group_name', 'ASC');
		$this->data['usergroups'] = $groups->populate();
		
		$user_data = $user->get();
		$this->data['user'] = $user_data;

		$permission = new $this->User_groups_permissions_model('gp');
		$permission->setGroupId($user_data->usergroup, true);
		$permission->setSelect('gp.*');
		$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="view" AND up.user_id='.$user_data->id.' AND up.module=gp.module) as view_o');
		$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="add" AND up.user_id='.$user_data->id.' AND up.module=gp.module) as add_o');
		$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="edit" AND up.user_id='.$user_data->id.' AND up.module=gp.module) as edit_o');
		$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="delete" AND up.user_id='.$user_data->id.' AND up.module=gp.module) as delete_o');
		$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="print" AND up.user_id='.$user_data->id.' AND up.module=gp.module) as print_o');
		$permission->setSelect('(SELECT IF((view_o IS NOT NULL),view_o,gp.view)) as view_p');
		$permission->setSelect('(SELECT IF((add_o IS NOT NULL),add_o,gp.add)) as add_p');
		$permission->setSelect('(SELECT IF((edit_o IS NOT NULL),edit_o,gp.edit)) as edit_p');
		$permission->setSelect('(SELECT IF((delete_o IS NOT NULL),delete_o,gp.delete)) as delete_p');
		$permission->setSelect('(SELECT IF((print_o IS NOT NULL),print_o,gp.print)) as print_p');
		$permission->setLimit(0);

		$this->data['group_permissions'] = $permission->populate();

		$upermission = new $this->User_accounts_permissions_model;
		$upermission->setUserId($user_data->id, true);
		$this->data['user_permissions'] = $upermission->populate();

		$this->load->view('administration/user_accounts_update', $this->data);
	}

	public function delete_user($user_id) {
				
		$this->load->model('User_accounts_model');
		$user = new $this->User_accounts_model();
		$user->setId( $user_id, true );
		$user->delete();

		redirect("user_accounts");

	}

	public function reset_permissions( $user_id ) {
		$permission = new $this->User_accounts_permissions_model;
		$permission->setUserId($user_id, true);
		$permission->delete();
		redirect("user_accounts/update_user/" . $user_id);
	}

	public function user_permission( $user_id ) {
		if( $this->input->post() ) {
			$data = $this->input->post();
			$permission = new $this->User_accounts_permissions_model;
			$permission->setUserId($user_id, true);
			$permission->setModule($this->input->post('module'),true);
			$permission->setAction($this->input->post('action'),true);
			$permission->setOn($this->input->post('checked'));
			if($permission->nonEmpty() === TRUE) {
				$permission->setExclude(array('user_id', 'module', 'action'));
				$permission->update();
				$data['action'] = 'update';
			} else {
				$permission->insert();
				$data['action'] = 'insert';
			}

			echo json_encode( $data );
			exit;
		}
		echo json_encode( "Error!" );
		exit;
	}

	public function groups()
	{

		$groups = new $this->User_groups_model;
		$groups->setSelect("*, (SELECT COUNT(*) FROM user_accounts WHERE usergroup=user_groups.id) as user_count");
		$groups->setActive(1,true);
		$groups->setOrder('group_name', 'ASC');
		$this->data['usergroups'] = $groups->populate();		
		$this->load->view('administration/user_groups', $this->data );
	}

	public function add_group() {
		if($this->input->post()) {
			$this->form_validation->set_rules('group_name', 'Group Name', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$groups = new $this->User_groups_model;
				$groups->setGroupName($this->input->post('group_name'), false, true);
				$groups->setActive(1, false, true);
				if($groups->insert()) {
					redirect( "user_accounts/update_group/" . $groups->getId() );
				}
			}
		}
		redirect("user_accounts/groups");
	}

	public function update_group( $group_id=false )
	{
		$usergroup_id = ($group_id) ? $group_id : $this->input->post('group_id');
		
		if( !$usergroup_id ) {
			redirect("user_accounts/groups");
		}

		$groups = new $this->User_groups_model;
		$groups->setId($usergroup_id, true, false);
		
		if($this->input->post('group_name')) {
			$groups->setGroupName($this->input->post('group_name'), false, true);
			$groups->update();
		}

		$this->data['usergroup'] = $groups->get();		

		$permission = new $this->User_groups_permissions_model('gp');
		$permission->setGroupId($group_id, true);
		$permission->setLimit(0);
		$this->data['permissions'] = $permission->populate();

		$users = new $this->User_accounts_model;
		$users->setUsergroup($group_id, true);
		$this->data['users_count'] = $users->count_all_results();

		$this->load->view('administration/user_groups_update', $this->data );
	}

	public function delete_group( $group_id )
	{
		$group = new $this->User_groups_model;
		$group->setId($group_id, true);

		$users = new $this->User_accounts_model;
		$users->setUsergroup($group_id, true);

		if( $users->count_all_results() > 0) {
			redirect("user_accounts/group_users/{$group_id}");
		} else {
			$group->delete();
			redirect("user_accounts/groups");
		}

		
	}

	public function group_users($group_id)
	{
		$users = new $this->User_accounts_model;
		$users->setJoin('user_groups', 'user_accounts.usergroup=user_groups.id');
		$users->setSelect('user_accounts.*, user_groups.group_name');
		$users->setUsergroup($group_id, true);
		$users->setOrder('name', 'ASC');
		$this->data['users'] = $users->populate();

		$groups = new $this->User_groups_model;
		$groups->setActive(1,true);
		$groups->setOrder('group_name', 'ASC');
		$this->data['usergroups'] = $groups->populate();		
		$this->load->view('administration/user_accounts', $this->data );
	}

	public function group_permission( $group_id ) {
		if( $this->input->post() ) {
			$permission = new $this->User_groups_permissions_model;
			$permission->setGroupId($group_id, true);
			$permission->setModule($this->input->post('module'),true);
			switch($this->input->post('action')) {
				case 'view':
					$permission->setView($this->input->post('checked'));
					$permission->setExclude(array('add', 'edit', 'delete', 'print'));
				break;
				case 'add':
					$permission->setAdd($this->input->post('checked'));
					$permission->setExclude(array('view', 'edit', 'delete', 'print'));
				break;
				case 'edit':
					$permission->setEdit($this->input->post('checked'));
					$permission->setExclude(array('view', 'add', 'delete', 'print'));
				break;
				case 'delete':
					$permission->setDelete($this->input->post('checked'));
					$permission->setExclude(array('view','add', 'edit', 'print'));
				break;
				case 'print':
					$permission->setPrint($this->input->post('checked'));
					$permission->setExclude(array('view', 'add', 'edit', 'delete'));
				break;
			}
			if($permission->nonEmpty() === TRUE) {
				$permission->setExclude(array('group_id', 'module'));
				$permission->update();
			} else {
				$permission->insert();
			}
			echo json_encode($this->input->post());
			exit;
		}
		echo json_encode("Error!");
		exit;
	}

	public function student_sessions()
	{
		$sessions = new $this->Students_sessions_model;
		$sessions->setOrder('user_email', 'ASC');
		$sessions->set_select('*');
		$sessions->set_select('(SELECT id FROM students_information WHERE google_userid=students_sessions.user_email) as student_id');
		$this->data['sessions'] = $sessions->populate();


		$this->load->view('administration/student_sessions', $this->data );
	}

	public function delete_student_session($session_id) {
		$sessions = new $this->Students_sessions_model;
		$sessions->setSessionId($session_id, true);
		$sessions->delete();
		redirect("user_accounts/student_sessions");
	}

	public function update_student_info($session_id, $student_id) {
		$sessions = new $this->Students_sessions_model;
		$sessions->setSessionId($session_id, true);
		$session_data = $sessions->get();

		$student = new $this->Students_information_model;
		$student->setId( $student_id, true );
		$student->setGoogleId( $session_data->user_id, false, true );
		$student->update();

		$sessions->delete();
		redirect("user_accounts/student_sessions");
	}
}
