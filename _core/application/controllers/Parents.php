<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "parents";
		$this->data['sidebar_menu_sub'] = "parents";
		
		$this->_checkPermission('parents', 'view', 'welcome');
		
		$this->data['search_enabled'] = true;

		$this->load->model('Parents_information_model');
		$this->load->model('Parents_address_model');
		$this->load->model('Parents_contacts_model');
		$this->load->model('Students_parents_model');
		$this->load->model('Parents_meta_model');
		
	}
	
	protected function _search_redirect() {
		if( $this->input->get("q") ) {
			header("location: " . site_url("parents") . "?q=" . $this->input->get("q") );
			exit;
		}
	}
	
	public function index($start=0)
	{

		$parent = new $this->Parents_information_model;
		$parent->setStart( $start );

		if( $this->input->get("q") ) {
			$parent->setWhereOr( "lastname LIKE '%". $this->input->get("q") ."%'" );
			$parent->setWhereOr( "firstname LIKE '%". $this->input->get("q") ."%'" );
			$parent->setWhereOr( "middlename LIKE '%". $this->input->get("q") ."%'" );
		}
		
		$config['base_url'] = site_url("parents/index");
		$config['total_rows'] = $parent->count_all_results();
		$config['per_page'] = $parent->getLimit();

		$this->data['pagination'] = bootstrap_pagination($config);
		$this->data['parents'] = $parent->populate();
		

		$this->load->view('parents/parents', $this->data);
	}
	
	public function add() {
		
		$this->_search_redirect();
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('lastname', 'Last Name', 'required');
			$this->form_validation->set_rules('firstname', 'First Name', 'required');

			if ($this->form_validation->run() == TRUE)
			{
				$parent = new $this->Parents_information_model;
				$parent->setLastname( ucwords( strtolower( $this->input->post('lastname') ) ) );
				$parent->setFirstname( ucwords( strtolower( $this->input->post('firstname') ) ) );
				$parent->setMiddlename( ucwords( strtolower( $this->input->post('middlename') ) ) );
				$parent->setGender( $this->input->post('gender') );
				$parent->setDeceased( (($this->input->post('deceased')) ? 1 : 0) );
				if( $parent->insert() ) {
					if( $this->input->get('next') ) {
						redirect( str_replace('$new_id', $parent->getId(), $this->input->get('next') ) );
					} else {
						redirect("parents/update/" . $parent->getId(), "location");
					}
				}
			}
		}
		
		$this->load->view( 'parents/parents_add', $this->data );
	}
	
	public function update($id) {
		
		$this->_search_redirect();
		
		$parent = new $this->Parents_information_model;
		$parent->setId( $id, true, false );
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('lastname', 'Last Name', 'required');
			$this->form_validation->set_rules('firstname', 'First Name', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				
				$parent->setLastname( ucwords( strtolower( $this->input->post('lastname') ) ) );
				$parent->setFirstname( ucwords( strtolower( $this->input->post('firstname') ) ) );
				$parent->setMiddlename( ucwords( strtolower( $this->input->post('middlename') ) ) );
				$parent->setGender( $this->input->post('gender') );
				$parent->setDeceased( (($this->input->post('deceased')) ? 1 : 0) );
				$parent->update();
				//redirect("students/update/" . $parent->getId(), "refresh");
			}
		}
		$this->data['parent'] = $parent->get();

		
		$address = new $this->Parents_address_model;
		$address->setParentId( $id, true );
		$address->setOrder( 'id', 'DESC' );
		$this->data['addresses'] = $address->populate();

		$contacts = new $this->Parents_contacts_model;
		$contacts->setParentId( $id, true );
		$contacts->setOrder( 'id', 'DESC' );
		$this->data['contacts'] = $contacts->populate();
		
		$students = new $this->Students_parents_model;
		$students->setParentId( $id, true );
		$students->setOrder( 'id', 'DESC' );
		$students->setJoin('students_information', 'students_information.id = students_parents.student_id');
		//$students->setJoin('parents_information', 'parents_information.id = students_parents.parent_id');
		$students->setSelect('students_information.*');
		$this->data['students'] = $students->populate();
		
		$this->load->view('parents/parents_update', $this->data);
	}
	
	public function delete($id) {
		
		$parent = new $this->Parents_information_model;
		$parent->setId( $id, true );
		$parent->delete();
		redirect( "parents/index" );

	}

	public function address($id, $action='add') {
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('address', 'Address', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$this->load->model('Parents_address_model');
				$address = new $this->Parents_address_model;
				
				switch( $action ) {
					case 'add':
					default:
						$address->setParentId( $id );
						$address->setAddress( $this->input->post('address') );
						$address->insert();
					break;
				}
			}
		}
		redirect( "parents/update/" . $id );
	}
	
	public function update_address($id, $address_id) {
		
		$this->_search_redirect();
		
		$address = new $this->Parents_address_model;
		$address->setSelect('parents_address.*');
		$address->setSelect('(SELECT `meta_value` FROM `parents_meta` pm WHERE pm.parent_id=parents_address.parent_id AND pm.meta_key="primary_address") as primary_address');
		$address->setId($address_id, true, false);
		$address->setParentId($id, true, false);
		
		if( $this->input->get('action') == 'delete' ) {
			$address->delete();
			redirect( "parents/update/" . $id );
		}
		if( $this->input->get('action') == 'set_primary' ) {

			$meta = new $this->Parents_meta_model;
			$meta->setParentId($id, true);
			$meta->setMetaKey('primary_address', true);
			$meta->setMetaValue($address_id);
			if( $meta->nonEmpty() == TRUE ) {
				$meta->setExclude(array('id', 'parent_id', 'meta_key'));
				$meta->update();
			} else {
				$meta->insert();
			}
			redirect( "parents/update_address/" . $id . "/" . $address_id);
		}
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('address', 'Address', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$address->setAddress($this->input->post('address'));
				$address->update();
				redirect( "parents/update/" . $id );
			}
		}
		$this->data['address'] = $address->get();
		$this->data['parent_id'] = $id;
		$this->data['address_id'] = $address_id;
		
		$this->load->view('parents/update_address', $this->data);
	}
	
	public function contact_numbers($id, $action='add') {
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('number', 'Contact Number', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$this->load->model('Parents_contacts_model');
				$contacts = new $this->Parents_contacts_model;
				
				switch( $action ) {
					case 'add':
					default:
						$contacts->setParentId( $id );
						$contacts->setContact( $this->input->post('number') );
						$contacts->setRemarks( $this->input->post('remarks') );
						$contacts->insert();
					break;
				}
			}
		}
		redirect( "parents/update/" . $id );
	}
	
	public function update_contact($id, $contact_id) {
		
		$this->_search_redirect();
		
		$this->load->model('Parents_contacts_model');
		$contacts = new $this->Parents_contacts_model;
		$contacts->setSelect('parents_contacts.*');
		$contacts->setSelect('(SELECT `meta_value` FROM `parents_meta` pm WHERE pm.parent_id=parents_contacts.parent_id AND pm.meta_key="primary_contact") as primary_contact');
		$contacts->setId($contact_id, true, false);
		$contacts->setParentId($id, true, false);
		
		if( $this->input->get('action') == 'delete' ) {
			$contacts->delete();
			redirect( "parents/update/" . $id );
		}
		if( $this->input->get('action') == 'set_primary' ) {
			$this->load->model('Parents_meta_model');
			$meta = new $this->Parents_meta_model;
			$meta->setParentId($id, true);
			$meta->setMetaKey('primary_contact', true);
			$meta->setMetaValue($contact_id);
			if( $meta->nonEmpty() == TRUE ) {
				$meta->setExclude(array('id', 'parent_id', 'meta_key'));
				$meta->update();
			} else {
				$meta->insert();
			}
			redirect( "parents/update_contact/" . $id . "/" . $contact_id);
		}
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('number', 'Contact Number', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$contacts->setContact($this->input->post('number'));
				$contacts->setRemarks( $this->input->post('remarks') );
				$contacts->update();
				redirect( "parents/update/" . $id );
			}
		}
		$this->data['contact'] = $contacts->get();
		$this->data['parent_id'] = $id;
		$this->data['contact_id'] = $contact_id;
		
		$this->load->view('parents/update_contact', $this->data);
	}
	
}
