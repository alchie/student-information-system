<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teachers extends ADMIN_Controller {
	
	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "school";
		$this->data['sidebar_menu_sub'] = "teachers";
		
		$this->_checkPermission('school', 'view', 'welcome');
		$this->_checkPermission('teachers', 'view', 'welcome');
		
		$this->load->model(array('Subjects_model', 'Subjects_levels_model'));
	}
	
	public function index($start=0)
	{
		$subjects = new $this->Subjects_model();
		$subjects->setStart($start);
		$this->data['subjects'] = $subjects->populate();
		$config['base_url'] = site_url("subjects/index");
		$config['total_rows'] = $subjects->count_all_results();
		$config['per_page'] = $subjects->getLimit();
		$this->data['pagination'] = bootstrap_pagination($config);
		
		$this->load->view('school/teachers', $this->data);
	}
	
	public function add() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('title', 'Subject Title', 'trim|required|min_length[5]|is_unique[subjects.title]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$title = $this->input->post("title", true);
				$status = $this->input->post("active", true);

				if( $status == "") {
					$status = 0;
				}
				
				$subjects = new $this->Subjects_model();
				$subjects->setTitle( $title );
				$subjects->setActive( $status );

				if( $subjects->insert() ) {
					redirect("subjects/update/" . $subjects->getId(), "location");
				}
			}
		}
		
		$this->load->view('school/teachers_add', $this->data);
	}
	
	public function update($id=false) {
		if( !$id ) {
			redirect("subjects" , "location");
		}
		$subject = new $this->Subjects_model();
		$subject->setId( $id, true );
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('title', 'Subject Title', 'trim|required|min_length[5]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$title = $this->input->post("title", true);
				$status = $this->input->post("active", true);

				if( $status == "") {
					$status = 0;
				}
				
				$subject->setTitle( $title );
				$subject->setActive( $status );

				$subject->update();
				redirect("subjects/update/" . $subject->getId(), "location");
				
			}
		}
		$this->data['subject'] = $subject->get();
		
		$levels = new $this->Subjects_levels_model();
		$levels->setSubjectId( $id, true );
		$this->data['levels'] = $levels->populate();
		
		$this->load->view('school/teachers_update', $this->data);
	}
	
	public function delete($id) {
		
		$subject = new $this->Subjects_model();
		$subject->setId( $id, true );
		$subject->delete();
		redirect("subjects/" , "location");
			
	}
	
}
