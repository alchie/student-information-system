<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_info extends STUDENT_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "my_info";
		$this->data['sidebar_menu_sub'] = "my_info";

		$this->load->model('Students_information_model');

	}
	
	public function index()
	{
		$info = new $this->Students_information_model;
		$info->setId($this->session->userdata('logged_uid'),true);
		$this->data['info'] = $info->get();
		$this->load->view('my/my_info', $this->data);
	}
}
