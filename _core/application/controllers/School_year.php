<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School_year extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "school";
		$this->data['sidebar_menu_sub'] = "school_year";
		
		$this->_checkPermission('school', 'view', 'welcome');
		$this->_checkPermission('school_year', 'view', 'welcome');
		
		$this->load->model('School_year_model');
	}
	
	public function index($start=0)
	{
		$school_year = new $this->School_year_model();
		$school_year->setStart($start);
		$school_year->setOrder('label', 'ASC');
		$config['base_url'] = site_url("school_year/index");
		$config['total_rows'] = $school_year->count_all_results();
		$config['per_page'] = $school_year->getLimit();

		$this->data['pagination'] = bootstrap_pagination($config);
		$this->data['school_year'] = $school_year->populate();
		
		$this->data['current_school_year'] = get_ams_config('school', 'current_school_year');
		
		$this->load->view('school/school_year', $this->data);
	}
	
	public function add() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('label', 'Label', 'trim|required|min_length[5]|is_unique[school_year.label]');
			//$this->form_validation->set_rules('start', 'Date Start', 'trim|required');
			//$this->form_validation->set_rules('end', 'Date End', 'trim|required');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$label = $this->input->post("label", true);
				$start = $this->input->post("start", true);
				$end = $this->input->post("end", true);
				$status = $this->input->post("active", true);

				if( $status == "") {
					$status = 0;
				}
				
				$school_year = new $this->School_year_model();
				$school_year->setLabel( $label );
				$school_year->setDateStart( date('Y-m-d', strtotime( $start)) );
				$school_year->setDateEnd( date('Y-m-d', strtotime( $end )) );
				$school_year->setActive( $status );

				$school_year->insert();
				redirect("school_year/update/" . $school_year->getId(), "location");
				
			}
		}
		
		$this->load->view('school/school_year_add', $this->data);
	}
	
	public function update($id) {
		
		$school_year = new $this->School_year_model();
		$school_year->setId( $id, true, false );
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('label', 'Label', 'trim|required|min_length[5]');
			//$this->form_validation->set_rules('start', 'Date Start', 'trim|required');
			//$this->form_validation->set_rules('end', 'Date End', 'trim|required');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$label = $this->input->post("label", true);
				$start = $this->input->post("start", true);
				$end = $this->input->post("end", true);
				$status = $this->input->post("active", true);

				if( $status == "") {
					$status = 0;
				}
				
				$school_year->setLabel( $label );
				$school_year->setDateStart( date('Y-m-d', strtotime($start)) );
				$school_year->setDateEnd( date('Y-m-d', strtotime($end)) );
				$school_year->setActive( $status );

				$school_year->update();
				redirect("school_year/update/" . $school_year->getId(), "location");
				
			}
		}
		
		$this->data['current_school_year'] = get_ams_config('school', 'current_school_year');
		$this->data['school_year'] = $school_year->get();

		$this->load->model('School_year_months_model');
		$months = new $this->School_year_months_model;
		$months->setSchoolYear($id, true);
		$months->setOrder('id', 'ASC');
		$months->setLimit(0);
		$this->data['months'] = $months->populate();
		
		$this->load->view('school/school_year_update', $this->data);
	}
	
	public function delete($id) {
		
		$school_year = new $this->School_year_model();
		$school_year->setId( $id, true, false );
		$school_year->delete();
		redirect("school_year", "location");
		
	}
	
	public function set_current($id) {
		
		update_ams_config('school', 'current_school_year' , $id);
		redirect("school_year/update/" . $id, "location");
		
	}
	
	public function grade_levels($id, $action='view', $lvl_id=false) {
		$this->load->model('School_year_levels_model');
		switch($action) {
			case 'add':
				if( $this->input->post('grade_level') ) {
					$levels = new $this->School_year_levels_model();
					$levels->setSchoolYear( $id );
					$levels->setLevel( $this->input->post('grade_level') );
					$levels->insert();
				}
				redirect("school_year/grade_levels/" . $id . "/view", "location");
			break;
			case 'delete':
				$levels = new $this->School_year_levels_model();
				$levels->setId( $lvl_id, true );
				$levels->setSchoolYear( $id, true );
				$levels->delete();
				redirect("school_year/grade_levels/" . $id . "/view", "location");
			break;
			case 'view':
			default:
				
				$levels = new $this->School_year_levels_model('syl');
				$levels->setSchoolYear( $id, true );
				$levels->setLimit(0);
				$levels->setSelect('syl.*');
				$levels->setSelect('(SELECT sum(amount) FROM school_year_level_fees sylf WHERE sylf.sylvl_id = syl.id) as total_fees');
				$this->data['levels'] = $levels->populate();
				
				$school_year = new $this->School_year_model();
				$school_year->setId( $id, true );
				$this->data['school_year'] = $school_year->get();
				
				$this->load->view('school/school_year_grade_levels', $this->data);
			break;
		}
		
	}
	
	public function subjects($id, $action='view', $subject_id=false) {
		$this->load->model(array('School_year_subjects_model', 'Subjects_model'));
		switch($action) {
			case 'add':
				if( $this->input->post('subject_id') ) {
					$sy_subjects = new $this->School_year_subjects_model();
					$sy_subjects->setSchoolYear( $id );
					$sy_subjects->setSubjectId( $this->input->post('subject_id') );
					$sy_subjects->insert();
				}
				redirect("school_year/subjects/" . $id . "/view", "location");
			break;
			case 'delete':
				$sy_subjects = new $this->School_year_subjects_model();
				$sy_subjects->setId( $subject_id, true );
				$sy_subjects->setSchoolYear( $id, true );
				$sy_subjects->delete();
				redirect("school_year/subjects/" . $id . "/view", "location");
			break;
			case 'view':
			default:
				
				$sy_subjects = new $this->School_year_subjects_model();
				$sy_subjects->setSchoolYear( $id, true );
				$sy_subjects->setLimit(0);
				$sy_subjects->setSelect('school_year_subjects.*, subjects.title');
				$sy_subjects->setJoin('subjects', 'subjects.id = school_year_subjects.subject_id');
				$this->data['sy_subjects'] = $sy_subjects->populate();
				
				$subjects = new $this->Subjects_model();
				$subjects->setLimit(0);
				$this->data['subjects'] = $subjects->populate();
				
				$school_year = new $this->School_year_model();
				$school_year->setId( $id, true );
				$this->data['school_year'] = $school_year->get();
				
				$this->load->view('school/school_year_subjects', $this->data);
			break;
		}
		
	}
	
	public function sections($id, $lvl_id, $action="view", $section_id=false) {
		$this->load->model(array('School_year_levels_model','School_year_level_sections_model'));
		switch($action) {
			case 'add':
				if( $this->input->post('section_id') ) {
					$section = new $this->School_year_level_sections_model();
					$section->setSylvlId( $lvl_id );
					$section->setSectionId( $this->input->post('section_id') );
					$section->insert();
				}
				redirect("school_year/sections/" . $id . "/" . $lvl_id . "/view", "location");
			break;
			case 'delete':
				$section = new $this->School_year_level_sections_model();
				$section->setSylvlId( $lvl_id, true );
				$section->setId( $section_id, true );
				$section->delete();
				redirect("school_year/sections/" . $id . "/" . $lvl_id . "/view", "location");
			break;
			case 'view':
			default:
				
				$levels = new $this->School_year_levels_model('syl');
				$levels->setId( $lvl_id, true );
				$this->data['level'] = $levels->get();
				
				$school_year = new $this->School_year_model();
				$school_year->setId( $id, true );
				$this->data['school_year'] = $school_year->get();
				
				$this->load->model('Sections_model');
				/*
				$sections = new $this->Sections_model();
				$sections->setLimit(0);
				$this->data['sections'] = $sections->populate();
				*/

				$sections2 = new $this->Sections_model('sec');
				$sections2->setLimit(0);
				$sections2->setSelect('sec.*');
				$sections2->setSelect('(SELECT `level` FROM `school_year_level_sections` syls JOIN school_year_levels syl ON syl.id=syls.sylvl_id WHERE syls.section_id=sec.id AND syl.school_year='.$id.' LIMIT 1) as assignment');
				$sections2->setHaving('assignment IS NULL');
				$sections2->setOrder('sec.name', 'ASC');
				
				$this->data['sections'] = $sections2->populate();

				$lvl_sections = new $this->School_year_level_sections_model();
				$lvl_sections->setSylvlId( $lvl_id, true );
				$lvl_sections->setLimit(0);
				$lvl_sections->setSelect('school_year_level_sections.*, sections.name');
				$lvl_sections->setOrder('sections.name', 'ASC');
				$lvl_sections->setJoin('sections', 'sections.id = school_year_level_sections.section_id');
				$this->data['lvl_sections'] = $lvl_sections->populate();
				
				$this->load->view('school/school_year_level_sections', $this->data);
			break;
		}
		
	}

	public function fees($id, $lvl_id, $action="view", $fee_id=false) {
		$this->load->model(array('School_fees_model', 'School_year_levels_model','School_year_level_fees_model'));
		switch($action) {
			case 'add':
				if( count($this->input->post()) > 0 ) {
					$this->form_validation->set_rules('fee_id', 'Fee', 'trim|required');
					if( $this->form_validation->run() == TRUE) {
						$fee = new $this->School_year_level_fees_model();
						$fee->setSylvlId( $lvl_id );
						$fee->setFeeId($this->input->post('fee_id') );
						$fee->setAmount( str_replace(',', '', $this->input->post('amount')) );
						$fee->setSkip($this->input->post('skip'));
						if( $fee->insert() ) {
							redirect("school_year/fees/" . $id . "/" . $lvl_id . "/view", "location");
						}
					}
				}
				$levels = new $this->School_year_levels_model();
				$levels->setId( $lvl_id, true );
				$grade_level = $levels->get();
				$this->data['level'] = $grade_level;
				
				$school_year = new $this->School_year_model();
				$school_year->setId( $id, true );
				$this->data['school_year'] = $school_year->get();

				$fees = new $this->School_fees_model('fees');
				$fees->setLimit(0);
				$fees->setSelect('fees.*');
				$fees->setSelect('(SELECT `fee_id` FROM `school_year_level_fees` sylf JOIN school_year_levels syl ON syl.id=sylf.sylvl_id WHERE syl.level=\''.$grade_level->level.'\' AND syl.school_year='.$id.' AND sylf.fee_id=fees.id LIMIT 1) as assignment');
				$fees->setHaving('assignment IS NULL');
				$this->data['fees'] = $fees->populate();

				$this->load->view('school/school_year_level_fees_add', $this->data);
			break;
			case 'delete':
				$section = new $this->School_year_level_fees_model();
				$section->setSylvlId( $lvl_id, true );
				$section->setId( $fee_id, true );
				$section->delete();
				redirect("school_year/fees/" . $id . "/" . $lvl_id . "/view", "location");
			break;
			case 'update':
				$levels = new $this->School_year_levels_model();
				$levels->setId( $lvl_id, true );
				$this->data['level'] = $levels->get();
				
				$school_year = new $this->School_year_model();
				$school_year->setId( $id, true );
				$this->data['school_year'] = $school_year->get();

				$fee = new $this->School_year_level_fees_model();
				$fee->setId( $fee_id, true, false );
				$fee->setSylvlId( $lvl_id, true, false );
				if( count($this->input->post()) > 0 ) {
					$this->form_validation->set_rules('fee_id', 'Fee', 'trim|required');
					if( $this->form_validation->run() == TRUE) {
						$fee->setFeeId($this->input->post('fee_id'));
						$fee->setAmount( str_replace(',', '', $this->input->post('amount')) );
						$fee->setSkip($this->input->post('skip'));
						$fee->update();
						redirect("school_year/fees/{$id}/{$lvl_id}");
					}
				}
				$this->data['fee'] = $fee->get();

				$fees = new $this->School_fees_model('fees');
				$fees->setLimit(0);
				$fees->setSelect('fees.*');			
				$this->data['fees'] = $fees->populate();

				$this->load->view('school/school_year_level_fees_update', $this->data);
			break;
			case 'reset_copy':
				if( $this->input->post('sylvl_id') ) {
					$delete_lvl_fees = new $this->School_year_level_fees_model();
					$delete_lvl_fees->setSylvlId( $lvl_id, true );
					$delete_lvl_fees->setLimit(0);
					$delete_lvl_fees->delete();

					$lvl_fees = new $this->School_year_level_fees_model();
					$lvl_fees->setSylvlId( $this->input->post('sylvl_id'), true );
					$lvl_fees->setLimit(0);
					
					foreach( $lvl_fees->populate() as $fees ) {
						$new_lvl_fees = new $this->School_year_level_fees_model();
						$new_lvl_fees->setSylvlId($lvl_id,true);
						$new_lvl_fees->setFeeId($fees->fee_id,true);
						$new_lvl_fees->setAmount($fees->amount);
						$new_lvl_fees->setSkip($fees->skip);
						if( ! $new_lvl_fees->nonEmpty() ) {
							$new_lvl_fees->insert();
						}
					}

					redirect('school_year/fees/'. $id . '/' . $lvl_id);
				}
			break;
			case 'view':
			default:
				
				$levels = new $this->School_year_levels_model();
				$levels->setId( $lvl_id, true );
				$this->data['level'] = $levels->get();
				
				$school_year = new $this->School_year_model();
				$school_year->setId( $id, true );
				$this->data['school_year'] = $school_year->get();
				
				$fees = new $this->School_fees_model('fees');
				$fees->setLimit(0);
				$fees->setSelect('fees.*');
				$fees->setOrder('name', 'ASC');
				$this->data['fees'] = $fees->populate();

				$this->load->model('School_fees_groups_model');
				$sfgrps = new $this->School_fees_groups_model;
				$sfgrps->setOrder('name', 'ASC');
				$this->data['school_fees_groups'] = $sfgrps->populate();

				$lvl_fees = new $this->School_year_level_fees_model();
				$lvl_fees->setSylvlId( $lvl_id, true );
				$lvl_fees->setLimit(0);
				$lvl_fees->setSelect('school_year_level_fees.*, school_fees.name, school_fees.group_id');
				$lvl_fees->setOrder('school_fees.name', 'ASC');
				$lvl_fees->setJoin('school_fees', 'school_fees.id = school_year_level_fees.fee_id');
				$this->data['lvl_fees'] = $lvl_fees->populate();
				
				$copy_source = new $this->School_year_level_fees_model('sylf');
				$copy_source->setLimit(0);
				$copy_source->setSelect('syl.level, syl.school_year, sy.label, syl.id');
				$copy_source->setJoin('school_year_levels syl', 'syl.id = sylf.sylvl_id');
				$copy_source->setJoin('school_year sy', 'sy.id = syl.school_year');
				$copy_source->setGroupBy('sylf.sylvl_id');
				$copy_source->setOrder('sy.label', 'DESC');
				$copy_source->setOrder('syl.level', 'ASC');
				$this->data['copy_source'] = $copy_source->populate();

				$this->load->view('school/school_year_level_fees', $this->data);
			break;
		}
		
	}

	public function months($id, $action='add',$month_id=false) {
		$this->load->model('School_year_months_model');
		switch($action) {
			case 'delete':
				if($month_id) {
					$month = new $this->School_year_months_model;
					$month->setSchoolYear($id, true);
					$month->setMonth($month_id, true);
					$month->delete();
				}
			break;
			case 'add':
			default:
				if( count($this->input->post()) > 0) {
					$this->form_validation->set_rules('month', 'Month', 'required|numeric');
					if( $this->form_validation->run() == TRUE) {
						$month = new $this->School_year_months_model;
						$month->setSchoolYear($id, true);
						$month->setMonth($this->input->post('month'), true);
						$month->setYear($this->input->post('year'));
						if($month->nonEmpty()===TRUE) {
							$month->setExclude('id');
							$month->update();
						} else {
							$month->insert();
						}
					}
				}
			break;
		}
		redirect('school_year/update/' . $id);
	}
	
}
