<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "school";
		$this->data['sidebar_menu_sub'] = "school";
		
		$this->_checkPermission('school', 'view', 'welcome');
	}
	
	public function index()
	{
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('name', 'School Name', 'required');
			$this->form_validation->set_rules('address', 'School Address', 'required');
			$this->form_validation->set_rules('telephone', 'Telephone', 'required');
			$this->form_validation->set_rules('email', 'Email Address', 'required');
			$this->form_validation->set_rules('website', 'Website', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				update_ams_config('school', 'school_name' , $this->input->post('name'));
				update_ams_config('school', 'school_address' , $this->input->post('address'));
				update_ams_config('school', 'school_phone' , $this->input->post('telephone'));
				update_ams_config('school', 'school_email' , $this->input->post('email'));
				update_ams_config('school', 'school_website' , $this->input->post('website'));
				redirect("school", "location");
			}
		}
		$this->load->view('school/school', $this->data);
	}
}
