<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campuses extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "school";
		$this->data['sidebar_menu_sub'] = "campuses";
		
		$this->_checkPermission('school', 'view', 'welcome');
		
		$this->load->model('Campuses_model');
		$this->load->model('School_year_model');
		$this->load->model('Campuses_school_year_model');
		$this->load->model('Campuses_grade_levels_model');
	}
	
	public function index($start=0)
	{
		$campuses = new $this->Campuses_model();
		$campuses->setStart($start);
		$campuses->setOrder('name', 'ASC');
		$config['base_url'] = site_url("campuses/index");
		$config['total_rows'] = $campuses->count_all_results();
		$config['per_page'] = $campuses->getLimit();

		$this->data['pagination'] = bootstrap_pagination($config);
		$this->data['campuses'] = $campuses->populate();
				
		$this->load->view('school/campuses', $this->data);
	}
	
	public function add() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Campus Name', 'trim|required');
			$this->form_validation->set_rules('address', 'Campus Address', 'trim|required');
			
			 if ($this->form_validation->run() == TRUE) {
			 				
				$campuses = new $this->Campuses_model();
				$campuses->setName( $this->input->post("name", true) );
				$campuses->setAddress( $this->input->post("address", true) );
				$campuses->setActive( (( $this->input->post("active") == "") ? 0 : 1) );
				$campuses->insert();
				redirect("campuses/update/" . $campuses->getId(), "location");
				
			}
		}
		
		$this->load->view('school/campuses_add', $this->data);
	}
	
	public function update($id) {
		
		$campuses = new $this->Campuses_model();
		$campuses->setId( $id, true, false );
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Campus Name', 'trim|required');
			$this->form_validation->set_rules('address', 'Campus Address', 'trim|required');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$campuses->setName( $this->input->post("name", true) );
				$campuses->setAddress( $this->input->post("address", true) );
				$campuses->setActive( (( $this->input->post("active") == "") ? 0 : 1) );
				$campuses->update();
				redirect("campuses/update/" . $campuses->getId(), "location");
				
			}
		}
		
		$this->data['campus'] = $campuses->get();
		
		$school_year = new $this->School_year_model;
		$school_year->set_where("(SELECT COUNT(*) FROM campuses_school_year csy WHERE csy.campus_id={$id} AND csy.school_year=school_year.id) = 0");
		$this->data['school_year'] = $school_year->populate();

		$cSY = new $this->Campuses_school_year_model;
		$cSY->setCampusId($id,true);
		$cSY->set_select("*");
		$cSY->set_select("(SELECT label FROM school_year WHERE id=campuses_school_year.school_year) as sy_label");
		$cSY->set_order('campuses_school_year.school_year', 'DESC');
		$cSY_data = $cSY->populate();

		foreach($cSY_data as $key => $csyd) { 
			$cSYg = new $this->Campuses_grade_levels_model;
			$cSYg->setCampusId($csyd->campus_id,true);
			$cSYg->setSchoolYear($csyd->school_year,true);
			$csyd->grade_levels = $cSYg->populate(); 
			$cSY_data[$key] = $csyd;
		}
		$this->data['campus_school_year'] = $cSY_data;

		$this->load->view('school/campuses_update', $this->data);
	}
	
	public function delete($id) {
		
		$campuses = new $this->Campuses_model();
		$campuses->setId( $id, true, false );
		$campuses->delete();
		redirect("campuses", "location");
		
	}

	public function update_school_year($id) {
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('school_year', 'School Year', 'trim|required');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$cSY = new $this->Campuses_school_year_model;
				$cSY->setCampusId($id,true);
				$cSY->setSchoolYear($this->input->post('school_year'),true);
				if( $cSY->nonEmpty() == false ) {
					$cSY->insert();
				}

			}
		}
		

		redirect("campuses/update/" . $id, "location");
	}

	public function update_grade_level($id) {

		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('school_year', 'School Year', 'trim|required');
			$this->form_validation->set_rules('grade_level', 'Grade Level', 'trim|required');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$cSY = new $this->Campuses_grade_levels_model;
				$cSY->setCampusId($id,true);
				$cSY->setSchoolYear($this->input->post('school_year'),true);
				$cSY->setGradeLevel($this->input->post('grade_level'),true);
				if( $cSY->nonEmpty() == false ) {
					$cSY->insert();
				}

			}
		}

		redirect("campuses/update/" . $id, "location");
	}

	public function delete_school_year($cid, $csyid) {
			$cSY = new $this->Campuses_school_year_model;
			$cSY->setCampusId($cid,true);
			$cSY->setId($csyid,true);
			$cSY->delete();

			redirect("campuses/update/" . $cid, "location");
	}

	public function delete_school_year_grade_level($cid, $syid, $glid) {
			$cSY = new $this->Campuses_grade_levels_model;
			$cSY->setCampusId($cid,true);
			$cSY->setSchoolYear($syid,true);
			$cSY->setId($glid,true);
			$cSY->delete();

			redirect("campuses/update/" . $cid, "location");
	}

	
	
}
