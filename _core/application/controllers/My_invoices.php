<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_invoices extends STUDENT_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "my_invoices";
		$this->data['sidebar_menu_sub'] = "my_invoices";

		$this->load->model(
			array(
				'School_year_model', 
				'Students_enrolled_model',
				'Students_payments_model',
				'School_year_months_model',
				)
		);

	}
	
	public function index()
	{
		$current_sy = get_ams_config('school', 'current_school_year');
			if( $current_sy ) {
				$school_year = new $this->School_year_model;
				$school_year->setId( $current_sy, true );
				$this->data['primary_school_year'] = $school_year->get();
				
				$enrolled = new $this->Students_enrolled_model;
				$enrolled->setSchoolYear($current_sy, true);
				$enrolled->setStudentId( $this->session->userdata('logged_uid'), true);
				$enrolled_data =  $enrolled->get();
				$this->data['enrolled'] = $enrolled_data;

				if( $enrolled_data ) {
					$sy_months = new $this->School_year_months_model('sym');
					$sy_months->set_select("sym.*");
					$sy_months->set_select("(SELECT SUM(si.amount) FROM students_invoices si WHERE si.enrolled_id={$enrolled_data->id} AND si.month=sym.month) as invoice_amount");
					$sy_months->setSchoolYear($current_sy, true);
					$sy_months->set_order('year', 'ASC');
					$sy_months->set_order('month', 'ASC');
					$sy_months->set_limit(0);
					$this->data['sy_months'] = $sy_months->populate();
				}
			}
		$this->load->view('my/my_invoices', $this->data);
	}
}
