<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fees extends ADMIN_Controller {
	
	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "school";
		$this->data['sidebar_menu_sub'] = "fees";
		
		$this->_checkPermission('school', 'view', 'welcome');
		$this->_checkPermission('fees', 'view', 'welcome');
		
		$this->load->model('School_fees_model');
		$this->load->model('School_fees_groups_model');
		
	}
	
	public function index($start=0)
	{
		$this->data['search_enabled'] = true;
		
		$fees = new $this->School_fees_model('fee');
		$fees->setJoin('school_fees_groups sfg', 'sfg.id=fee.group_id');
		$fees->setSelect('fee.*, sfg.name as group_name');
		$fees->setOrder('sfg.priority', 'ASC');
		$fees->setOrder('fee.name', 'ASC');

		if( $this->input->get("q") ) {
			$fees->setWhere( "fee.name LIKE '%". $this->input->get("q") ."%'" );
		} else {
			$fees->setStart($start);
			$config['base_url'] = site_url("fees/index");
			$config['total_rows'] = $fees->count_all_results();
			$config['per_page'] = $fees->getLimit();
			$this->data['pagination'] = bootstrap_pagination($config);
		}

		$this->data['fees'] = $fees->populate();
		
		
		$this->load->view('school/fees', $this->data);
	}
	
	public function add() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('installment', 'Installment', 'trim|required');
			 
			 if ($this->form_validation->run() == TRUE) {
			 				
				$fee = new $this->School_fees_model();
				$fee->setName( $this->input->post("name") );
				$fee->setGroupId( ($this->input->post("group_id")) ? $this->input->post("group_id") : 0 );
				$fee->setInstallment( $this->input->post("installment") );
				$fee->setDiscount( (($this->input->post("discount"))?1:0) );
				$fee->setActive( (($this->input->post("active"))?1:0) );

				if( $fee->insert() ) {
					redirect("fees/update/" . $fee->getId(), "location");
				}
			}
		}

		$groups = new $this->School_fees_groups_model;
		$groups->setLimit(0);
		$this->data['groups'] = $groups->populate();
		
		$this->load->view('school/fees_add', $this->data);
	}
	
	public function update($id) {
		$fee = new $this->School_fees_model();
		$fee->setId($id, true);
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('installment', 'Installment', 'trim|required');
			 
			 if ($this->form_validation->run() == TRUE) {
			 				

				$fee->setName( $this->input->post("name") );
				$fee->setGroupId( ($this->input->post("group_id")) ? $this->input->post("group_id") : 0 );
				$fee->setInstallment( $this->input->post("installment") );
				$fee->setDiscount( (($this->input->post("discount"))?1:0) );
				$fee->setActive( (($this->input->post("active"))?1:0) );

				if( $fee->update() ) {
					if( $this->input->post('redirect_to') ) {
						redirect( $this->input->post('redirect_to') );
					}
				}
			}
		}
		$this->data['fee'] = $fee->get();

		$groups = new $this->School_fees_groups_model;
		$groups->setLimit(0);
		$this->data['groups'] = $groups->populate();
		
		$this->load->view('school/fees_update', $this->data);
	}
	
	public function delete($id) {
		
		$fee = new $this->School_fees_model();
		$fee->setId( $id, true );
		$fee->delete();
		redirect("fees/" , "location");
			
	}

	public function groups($action='view', $id=0)
	{
		$groups = new $this->School_fees_groups_model();
		
		switch ($action) {
			case 'add':
				if( count($this->input->post()) > 0) {
					$this->form_validation->set_rules('name', 'Group Name', 'trim|required');
					if( $this->form_validation->run() == TRUE){
						$groups->setName($this->input->post('name'));
						$groups->setPriority($this->input->post('priority'));
						$groups->setActive((($this->input->post('active'))?1:0));
						$groups->insert();
						redirect('fees/groups/update/' . $groups->getId());
					}
				}
				$this->load->view('school/fees_groups_add', $this->data);
				break;
			
			case 'update':
					$groups->setId($id, true, false);
					if( count($this->input->post()) > 0) {
						$this->form_validation->set_rules('name', 'Group Name', 'trim|required');
						if( $this->form_validation->run() == TRUE){
							
							$groups->setName($this->input->post('name'));
							$groups->setPriority($this->input->post('priority'));
							$groups->setActive((($this->input->post('active'))?1:0));
							$groups->update();
							//redirect('fees/groups/update/' . $groups->getId());
						}
					}
					$this->data['group'] = $groups->get();
					$this->load->view('school/fees_groups_update', $this->data);
				break;

			case 'delete':
					$groups->setId($id, true);
					$groups->delete();
					redirect('fees/groups');
				break;
			default:
				$groups->setStart($id);
				$groups->setOrder('priority', 'ASC');
				$this->data['groups'] = $groups->populate();
				$config['base_url'] = site_url("fees/index");
				$config['total_rows'] = $groups->count_all_results();
				$config['per_page'] = $groups->getLimit();
				$this->data['pagination'] = bootstrap_pagination($config);
				$this->load->view('school/fees_groups', $this->data);
				break;
		}
		
	}
	
}
