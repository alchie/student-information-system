<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "school";
		$this->data['sidebar_menu_sub'] = "services";

		$this->_checkPermission('school', 'view', 'welcome');
		$this->_checkPermission('services', 'view', 'welcome');
		
		$this->load->model('Services_model');
	}
	
	public function index($start=0)
	{
		$services = new $this->Services_model();
		$services->setStart($start);
		$this->data['services'] = $services->populate();
		$config['base_url'] = site_url("sections/index");
		$config['total_rows'] = $services->count_all_results();
		$config['per_page'] = $services->getLimit();
		$this->data['pagination'] = bootstrap_pagination($config);

		$this->load->view('school/services', $this->data);
	}

	public function add()
	{
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[5]|is_unique[services.name]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$service = new $this->Services_model();
				$service->setName( $this->input->post("name", true) );
				$service->setActive( ($this->input->post("active")) ? 1 : 0 );

				if( $service->insert() ) {
					redirect("services/update/" . $service->getId(), "location");
				}
			}
		}
		$this->load->view('school/services_add', $this->data);
	}

	public function update($id=false) {
		if( !$id ) {
			redirect("services" , "location");
		}
		$service = new $this->Services_model;
		$service->setId( $id, true, false );
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[5]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 				
				$service->setName( $this->input->post("name") );
				$service->setActive( ($this->input->post("active")) ? 1 : 0 );

				$service->update();
				redirect("services/update/" . $service->getId(), "location");
				
			}
		}
		$this->data['service'] = $service->get();
		
		$this->load->view('school/services_update', $this->data);
	}

	public function delete($id) {
		
		$service = new $this->Services_model;
		$service->setId( $id, true );
		$service->delete();
		redirect("services" , "location");
			
	}
	
}
