<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grade_levels extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "school";
		$this->data['sidebar_menu_sub'] = "grade_levels";
		
		$this->_checkPermission('school', 'view', 'welcome');
		$this->_checkPermission('grade_levels', 'view', 'welcome');
		
		$this->load->model('Grade_levels_model');
	}
	
	public function index($start=0)
	{
		$levels = new $this->Grade_levels_model();
		$levels->setStart($start);
		$levels->setOrder('name', 'ASC');
		$this->data['levels'] = $levels->populate();
		$config['base_url'] = site_url("grade_levels/index");
		$config['total_rows'] = $levels->count_all_results();
		$config['per_page'] = $levels->getLimit();

		$this->data['pagination'] = bootstrap_pagination($config);
		
		
		$this->load->view('school/grade_levels', $this->data);
	}

	public function add() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Grade Label', 'trim|required|min_length[5]|is_unique[grade_levels.name]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$name = $this->input->post("name", true);
				$status = $this->input->post("active", true);

				if( $status == "") {
					$status = 0;
				}
				
				$level = new $this->Grade_levels_model();
				$level->setName( $name );
				$level->setActive( $status );
				$level->insert();
				
				redirect("grade_levels/update/" . $level->getId(), "location");
				
			}
		}
		
		$this->load->view('school/grade_levels_add', $this->data);
	}
	
	public function update($id) {
		
		$level = new $this->Grade_levels_model();
		$level->setId( $id, true, false );
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Grade Label', 'trim|required|min_length[5]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$name = $this->input->post("name", true);
				$status = $this->input->post("active", true);

				if( $status == "") {
					$status = 0;
				}
				
				$level->setName( $name );
				$level->setActive( $status );
				$level->update();
				
				redirect("grade_levels/update/" . $level->getId(), "location");
				
			}
		}
		
		$this->data['level'] = $level->get();
		
		$this->load->view('school/grade_levels_update', $this->data);
	}
	
	public function delete($id) {
		
		$level = new $this->Grade_levels_model();
		$level->setId( $id, true, false );
		$level->delete();
		redirect("grade_levels", "location");
	}
}
