<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Finance extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "finance";
		$this->data['sidebar_menu_sub'] = "finance";
		
		$this->_checkPermission('finance', 'view', 'welcome');
		
		$this->load->model('Campuses_model');
		$this->load->model('Services_model');
		$this->load->model('School_year_model');
		$this->load->model('School_year_levels_model');
		$this->load->model('School_year_level_fees_model');
		$this->load->model('School_fees_groups_model');
		$this->load->model('School_year_months_model');
		$this->load->model('Students_invoices_model');
		$this->load->model('Students_payments_model');
		$this->load->model('Students_payments_applied_model');
		$this->load->model('Students_payments_applied2_model');
		$this->load->model('Students_discounts_model');
		$this->load->model('Students_oldaccounts_model');
		$this->load->model('Students_enrolled_model');
		$this->load->model('Students_services_model');
		$this->load->model('Students_month_exclude_model');
		$this->load->model('Students_fees_installment_override_model');
		$this->load->model('Students_promissory_model');

		$this->data['search_enabled'] = true;

		$this->data['current_year'] = get_ams_config('school', 'current_school_year');

		$this->load_school_years('finance');
	}
	
	protected function _navbar($student) {
		
		$this->load->model('Students_enrolled_model');
		$enrolled = new $this->Students_enrolled_model;
		$enrolled->setStudentId($student->student_id, true);
		$enrolled->setOrder('date_enrolled', 'DESC');
		$enrolled->setJoin('school_year', 'school_year.id=students_enrolled.school_year');
		$enrolled->setJoin('sections', 'sections.id=students_enrolled.section_id');
		$enrolled->setSelect('students_enrolled.*, school_year.label as sy_label, sections.name as section_name');
		$this->data['enrollment_history'] = $enrolled->populate();


		$next_student = new $this->Students_enrolled_model('se');
		$next_student->setJoin('students_information si', 'si.id = se.student_id');
		$next_student->setJoin('school_year', 'school_year.id=se.school_year');
		$next_student->setJoin('sections', 'sections.id=se.section_id');
		$next_student->setSelect('se.*, school_year.label as sy_label, sections.name as section_name');
		$next_student->setWhere("se.student_id = (SELECT student_id from students_enrolled se JOIN students_information si ON se.student_id=si.id WHERE se.grade_level = '{$student->grade_level}' AND se.campus_id = {$student->campus_id} AND CONCAT(si.lastname,' ',si.firstname,' ',si.middlename) > '{$student->lastname} {$student->firstname} {$student->middlename}' ORDER BY si.lastname ASC, si.firstname ASC, si.middlename ASC LIMIT 1)");
		$next_student->setGradeLevel($student->grade_level,true);
		$next_student->setCampusId($student->campus_id,true);
		$next_student->setOrder('si.lastname', 'ASC');
		$this->data['next_student'] = $next_student->get();

		$previous_student = new $this->Students_enrolled_model('se');
		$previous_student->setJoin('students_information si', 'si.id = se.student_id');
		$previous_student->setJoin('school_year', 'school_year.id=se.school_year');
		$previous_student->setJoin('sections', 'sections.id=se.section_id');
		$previous_student->setSelect('se.*, school_year.label as sy_label, sections.name as section_name');
		$previous_student->setWhere("se.student_id = (SELECT student_id from students_enrolled se JOIN students_information si ON se.student_id=si.id WHERE se.grade_level = '{$student->grade_level}' AND se.campus_id = {$student->campus_id} AND CONCAT(si.lastname,' ',si.firstname,' ',si.middlename) < '{$student->lastname} {$student->firstname} {$student->middlename}' ORDER BY si.lastname DESC, si.firstname DESC, si.middlename DESC LIMIT 1)");
		$previous_student->setGradeLevel($student->grade_level,true);
		$previous_student->setCampusId($student->campus_id,true);
		$previous_student->setOrder('si.lastname', 'ASC');
		
		$this->data['previous_student'] = $previous_student->get();

	}

	protected function _search_redirect() {
		if( $this->input->get("q") ) {
			header("location: " . site_url("finance") . "?q=" . $this->input->get("q") );
			exit;
		}
	}

	private function _list($id=false, $level_id=0, $campus_id=0,$start=0) {

		$this->data['grade_level'] = $level_id;
		$this->data['campus_id'] = $campus_id;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 
		
		$enrolled = new $this->Students_enrolled_model('se');
		$enrolled->setJoin('students_information si', 'si.id = se.student_id');
		$enrolled->setSelect('si.*, se.*, se.id as enroll_id');
		$enrolled->setJoin('campuses', 'campuses.id = se.campus_id');
		$enrolled->setSelect('campuses.name as campus_name');
		//$enrolled->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as student_account');
		//$enrolled->setSelect('(SELECT SUM(amount) FROM students_invoices si WHERE si.enrolled_id=se.id) as student_account');
		//$enrolled->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
		//$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND down=0) as payments');
		
		$enrolled->setWhere( "si.trash = 0" );
		if( $this->input->get("q") ) {
			$enrolled->setWhere( "(si.idn LIKE '%". $this->input->get("q") ."%'" );
			$enrolled->setWhereOr( "si.lastname LIKE '%". $this->input->get("q") ."%'" );
			$enrolled->setWhereOr( "si.firstname LIKE '%". $this->input->get("q") ."%'" );
			$enrolled->setWhereOr( "si.middlename LIKE '%". $this->input->get("q") ."%')" );
		}
		if( $level_id ) {
			$enrolled->setGradeLevel($level_id,true, false, null, null, 100);
		}
		if( $campus_id ) {
			$enrolled->setCampusId($campus_id,true, false, null, null, 100);

			$campus = new $this->Campuses_model;
			$campus->setId($campus_id, true);
			$this->data['campus'] = $campus->get();
		}
		$enrolled->setSchoolYear($current_school_year, true, false, null, null, 99);
		$enrolled->setOrder('si.lastname', 'ASC');
		$enrolled->setOrder('si.firstname', 'ASC');
		$enrolled->setOrder('si.middlename', 'ASC');
		$enrolled->setStart( $start );
		$enrolled->setLimit(20);
		
		$this->data['enrollees'] = $enrolled->populate();
		$this->data['enrollees_count'] = $enrolled->count_all_results();
		
		$config['base_url'] = site_url( sprintf("finance/index/%s/%s/%s", $current_school_year,$level_id,$campus_id) );
		$config['total_rows'] = $enrolled->count_all_results();
		$config['per_page'] = $enrolled->getLimit();

		$this->data['pagination'] = bootstrap_pagination( $config );

		$campuses = new $this->Campuses_model;
		$campuses->setLimit(0);
		$this->data['campuses'] = $campuses->populate();

		$this->load->model('School_year_levels_model');
		$grade_levels = new $this->School_year_levels_model;
		$grade_levels->setLimit(0);
		$grade_levels->setSchoolYear($current_school_year, true);
		$this->data['grade_levels'] = $grade_levels->populate();

	}

	public function index($id=false,$level=0,$campus_id=0,$start=0)
	{
		$current_sy = ($id) ? get_ams_config('school', 'current_school_year') : $this->session->current_school_year;
		$this->_list($current_sy,$level,$campus_id,$start);
		$this->load->view('finance/student_accounts', $this->data);
	}
		
	public function ledger($id, $enrolled_id)
	{
		$this->data['method_name'] = 'ledger';

		$this->_search_redirect();

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		$this->data['current_year'] = $current_school_year;

		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id=se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as whole_year');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=0) as payments');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=1) as downpayments');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_services ss WHERE ss.enrolled_id=se.id) as services');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$months = new $this->School_year_months_model('sym');
		$months->setSchoolYear($id, true);
		$months->setOrder('sym.id', 'ASC');
		$months->setSelect('sym.*');
		$months->setSelect('(SELECT id FROM students_month_exclude sme WHERE sme.excluded=sym.id AND sme.enrolled_id='.$enrolled_id.') as excluded');
		$months->setSelect('(SELECT SUM(amount) FROM students_invoices sin WHERE sin.month=sym.month AND sin.enrolled_id='.$enrolled_id.') as invoiced');
		$months->setSelect('(SELECT SUM(spa.amount) FROM students_invoices sin JOIN students_payments_applied2 spa ON sin.id=spa.invoice_id WHERE sin.month=sym.month AND sin.enrolled_id='.$enrolled_id.') as applied');
		$months->setSelect('(SELECT COUNT(*) FROM students_promissory sp WHERE sp.enrolled_id='.$enrolled_id.' AND sp.month=sym.month AND sp.settled=0) as promised');
		$months->setHaving('excluded IS NULL');
		$months_data = $months->populate();
		$this->data['months'] = $months_data;

		$sfgrps = new $this->School_fees_groups_model;
		$sfgrps->setOrder('priority', 'ASC');
		$sfgrps->setActive('1', true);
		$sfgrps->setLimit(0);
		$this->data['school_fees_groups'] = $sfgrps->populate();

		$sfees = new $this->School_year_level_fees_model('sylf');
		$sfees->setSelect('sylf.*');
		$sfees->setLimit(0);
		$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
		$sfees->setSelect('sf.name, sf.group_id, sf.installment, sf.discount as discountable');
		$sfees->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.fee_id=sf.id AND sd.enrolled_id='.$enrolled_id.') as discounts');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND spa.applied_to LIKE "school_fees") as payments');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND sp.down=1 AND spa.applied_to LIKE "school_fees") as downpayments');
		$sfees->setSylvlId($enroll_data->sylvl_id, true);
		$sfees->setOrder('sf.name', 'ASC');
		$this->data['school_fees'] = $sfees->populate();

		$sin = new $this->Students_invoices_model('si');
		$sin->setSelect('si.*');
		$sin->setLimit(0);
		$sin->setEnrolledId($enrolled_id, true);
		$this->data['invoices'] = $sin->populate();

		$payments = new $this->Students_payments_model('sp');
		$payments->setLimit(0);
		$payments->setEnrolledId($enrolled_id, true);
		$payments->setSelect('sp.*');
		$payments->setSelect('(SELECT SUM(amount) FROM students_payments_applied spa WHERE spa.payment_id=sp.id AND sp.down=1) as total_applied');
		$payments->setSelect('(SELECT SUM(amount) FROM students_payments_applied2 spa WHERE spa.payment_id=sp.id AND sp.down=0) as total_applied2');
		$payments->setOrder('payment_date', 'ASC');

		$this->data['payments'] = $payments->populate();

		$discounts = new $this->Students_discounts_model('sd');
		$discounts->setLimit(0);
		$discounts->setEnrolledId($enrolled_id, true);
		$discounts->setJoin('school_fees sf', 'sf.id=sd.fee_id');
		$discounts->setSelect('sd.*, sf.name as fee_name');
		$discounts->setOrder('sd.lastmod', 'DESC');
		$this->data['discounts'] = $discounts->populate();

		$oldaccount = new $this->Students_oldaccounts_model('so');
		$oldaccount->setEnrolledId($enrolled_id,true);
		$oldaccount->setSelect('so.*');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id) as payments');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id AND sp.down=0 AND spa.applied_to LIKE "school_fees") as old_payments');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id AND sp.down=1 AND spa.applied_to LIKE "school_fees") as old_down');
		$this->data['oldaccount'] = $oldaccount->get();

		$student_services = new $this->Students_services_model('ss');
		$student_services->setEnrolledId($enrolled_id, true);
		$student_services->setJoin('services s', 's.id=ss.service_id');
		$student_services->setSelect('ss.*, s.name as service_name');
		$student_services->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=ss.service_id AND sp.enrolled_id=ss.enrolled_id AND spa.applied_to LIKE "services") as applied');
		$student_services->setLimit(0);
		$this->data['student_services'] = $student_services->populate();

		$excluded = new $this->Students_month_exclude_model;
		$excluded->setEnrolledId($enrolled_id,true);
		$excluded->setJoin('school_year_months', 'school_year_months.id=students_month_exclude.excluded');
		$excluded->setSelect('students_month_exclude.*, school_year_months.school_year, school_year_months.month, school_year_months.year');
		$this->data['excluded_months'] = $excluded->populate();

		$promises = new $this->Students_promissory_model('spm');
		$promises->setEnrolledId($enrolled_id,true);
		$promises->setJoin('parents_information pi', 'pi.id=spm.parent_id');
		$promises->setSelect('spm.*');
		$promises->setSelect('pi.lastname, pi.firstname');
		$promises->setJoin('students_enrolled se', 'se.id=spm.enrolled_id');
		$promises->setSelect('se.student_id, se.school_year');
		$promises->setSelect('(SELECT year FROM school_year_months sym WHERE sym.school_year=se.school_year AND sym.month=spm.month) as year');
		$promises->setSelect('(SELECT relationship FROM students_parents stpa WHERE stpa.parent_id=spm.parent_id AND se.student_id=stpa.student_id) as relationship');
		$this->data['promises'] = $promises->populate();

		$applied2 = new $this->Students_payments_applied2_model('spa');
		$applied2->setJoin('students_payments sp', 'sp.id=spa.payment_id');
		$applied2->setSelect('SUM(spa.amount) as total_applied');
		$applied2->setWhere('sp.enrolled_id', $enrolled_id);
		$applied2->setLimit(0);
		$this->data['applied_payments2'] = $applied2->get();

		$applied = new $this->Students_payments_applied_model('spa');
		$applied->setJoin('students_payments sp', 'sp.id=spa.payment_id');
		$applied->setSelect('SUM(spa.amount) as total_applied');
		$applied->setWhere('sp.enrolled_id', $enrolled_id);
		$applied->setLimit(0);
		$this->data['applied_payments'] = $applied->get();

		$this->_navbar($enroll_data);

		$this->load->view('finance/ledger', $this->data);
	}

	public function print_summary($id, $enrolled_id)
	{

		$this->_search_redirect();

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		$this->data['current_year'] = $current_school_year;

		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as whole_year');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=0) as payments');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=1) as downpayments');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_services ss WHERE ss.enrolled_id=se.id) as services');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$months = new $this->School_year_months_model('sym');
		$months->setSchoolYear($id, true);
		$months->setOrder('sym.id', 'ASC');
		$months->setSelect('sym.*');
		$months->setSelect('(SELECT id FROM students_month_exclude sme WHERE sme.excluded=sym.id AND sme.enrolled_id='.$enrolled_id.') as excluded');
		$months->setHaving('excluded IS NULL');
		$this->data['months'] = $months->populate();

		$sfgrps = new $this->School_fees_groups_model;
		$sfgrps->setOrder('priority', 'ASC');
		$sfgrps->setActive('1', true);
		$sfgrps->setLimit(0);
		$this->data['school_fees_groups'] = $sfgrps->populate();

		$sfees = new $this->School_year_level_fees_model('sylf');
		$sfees->setSelect('sylf.*');
		$sfees->setLimit(0);
		$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
		$sfees->setSelect('sf.name, sf.group_id, sf.installment, sf.discount as discountable');
		$sfees->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.fee_id=sf.id AND sd.enrolled_id='.$enrolled_id.') as discounts');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND spa.applied_to LIKE "school_fees") as payments');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND sp.down=1 AND spa.applied_to LIKE "school_fees") as downpayments');
		$sfees->setSylvlId($enroll_data->sylvl_id, true);
		$sfees->setOrder('sf.name', 'ASC');
		$this->data['school_fees'] = $sfees->populate();

		$sin = new $this->Students_invoices_model('si');
		$sin->setSelect('si.*');
		$sin->setLimit(0);
		$sin->setEnrolledId($enrolled_id, true);
		$this->data['invoices'] = $sin->populate();

		$payments = new $this->Students_payments_model('sp');
		$payments->setLimit(0);
		$payments->setEnrolledId($enrolled_id, true);
		$payments->setOrder('payment_date', 'ASC');
		$this->data['payments'] = $payments->populate();

		$discounts = new $this->Students_discounts_model('sd');
		$discounts->setLimit(0);
		$discounts->setEnrolledId($enrolled_id, true);
		$discounts->setJoin('school_fees sf', 'sf.id=sd.fee_id');
		$discounts->setSelect('sd.*, sf.name as fee_name');
		$discounts->setOrder('sd.lastmod', 'DESC');
		$this->data['discounts'] = $discounts->populate();

		$oldaccount = new $this->Students_oldaccounts_model('so');
		$oldaccount->setEnrolledId($enrolled_id,true);
		$oldaccount->setSelect('so.*');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id) as payments');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id AND sp.down=0 AND spa.applied_to LIKE "school_fees") as old_payments');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id AND sp.down=1 AND spa.applied_to LIKE "school_fees") as old_down');
		$this->data['oldaccount'] = $oldaccount->get();

		$student_services = new $this->Students_services_model('ss');
		$student_services->setEnrolledId($enrolled_id, true);
		$student_services->setJoin('services s', 's.id=ss.service_id');
		$student_services->setSelect('ss.*, s.name as service_name');
		$student_services->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=ss.service_id AND sp.enrolled_id=ss.enrolled_id AND spa.applied_to LIKE "services") as applied');
		$student_services->setLimit(0);
		$this->data['student_services'] = $student_services->populate();

		$excluded = new $this->Students_month_exclude_model;
		$excluded->setEnrolledId($enrolled_id,true);
		$excluded->setJoin('school_year_months', 'school_year_months.id=students_month_exclude.excluded');
		$excluded->setSelect('students_month_exclude.*, school_year_months.school_year, school_year_months.month, school_year_months.year');
		$this->data['excluded_months'] = $excluded->populate();

		$this->_navbar($enroll_data);

		$this->load->view('finance/print_summary', $this->data);
	}

	public function oldaccount($id, $enrolled_id)
	{
		$this->_search_redirect();
		$this->data['method_name'] = 'oldaccount';

		$oldaccount = new $this->Students_oldaccounts_model;
		$oldaccount->setEnrolledId($enrolled_id,true);

		if( count($this->input->post()) > 0 ) {			
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			$this->form_validation->set_rules('installments', 'Installments', 'trim|required');
			if( $this->form_validation->run()==TRUE) {
				
				$oldaccount->setAmount(str_replace(',', '', $this->input->post('amount')));
				$oldaccount->setInstallment($this->input->post('installments'));
				
				$oldaccount->setExclude(array('id','lastmod'));

				if( $oldaccount->nonEmpty() ) {
					$oldaccount->update();
				} else {
					$oldaccount->insert();
				}
			}
			redirect("finance/ledger/{$id}/{$enrolled_id}");
		}

		$this->data['oldaccount'] = $oldaccount->get();

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$months = new $this->School_year_months_model;
		$months->setSchoolYear($id, true);
		$months->setOrder('id', 'ASC');
		$this->data['months'] = $months->populate();

		$this->_navbar($enroll_data);

		$this->load->view('finance/oldaccount', $this->data);
	}

	public function school_fees($id, $enrolled_id)
	{
		$this->_search_redirect();
		$this->data['method_name'] = 'school_fees';
		
		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$sfgrps = new $this->School_fees_groups_model;
		$sfgrps->setOrder('priority', 'ASC');
		$sfgrps->setActive('1', true);
		$this->data['school_fees_groups'] = $sfgrps->populate();

		$sfees = new $this->School_year_level_fees_model('sylf');
		$sfees->setSelect('sylf.*');
		$sfees->setLimit(0);
		$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
		$sfees->setSelect('sf.name, sf.group_id, sf.installment, sf.discount as discountable');
		$sfees->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.fee_id=sf.id AND sd.enrolled_id='.$enrolled_id.') as discounts');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND spa.applied_to LIKE "school_fees") as payments');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND sp.down=1 AND spa.applied_to LIKE "school_fees") as downpayments');
		$sfees->setSylvlId($enroll_data->sylvl_id, true);
		$sfees->setOrder('sf.name', 'ASC');
		$this->data['school_fees'] = $sfees->populate();

		$this->_navbar($enroll_data);

		$this->load->view('finance/school_fees', $this->data);
	}

	public function detailed_schedule($id, $enrolled_id)
	{
		
		$this->data['method_name'] = 'detailed_schedule';

		$this->_search_redirect();

		if($this->input->get('delete_exclude')) {
			$exc = new $this->Students_month_exclude_model;
			$exc->setEnrolledId($enrolled_id,true);
			$exc->setId($this->input->get('delete_exclude'),true);
			$exc->delete();
			redirect(uri_string());
		}

		if($this->input->post('exclude')) {
			$exc = new $this->Students_month_exclude_model;
			$exc->setEnrolledId($enrolled_id,true);
			$exc->setExcluded($this->input->post('exclude'),true);
			if( $exc->nonEmpty() ) {
				$exc->setExclude('id');
				$exc->update();
			} else {
				$exc->insert();
			}
		}

		if($this->input->post('override')) {
			$override = new $this->Students_fees_installment_override_model;
			$override->setEnrolledId($enrolled_id,true,false);
			$override->setFeeId($this->input->post('override')['fee_id'],true);
			$override->setInstallment($this->input->post('override')['installment']);
			$override->setSkip($this->input->post('override')['skip']);
			if( $override->nonEmpty() ) {
				$override->setExclude(array('id', 'fee_id'));
				$override->update();
			} else {
				$override->insert();
			}
		}


		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as whole_year');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id) as payments');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=1) as downpayments');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$months = new $this->School_year_months_model('sym');
		$months->setSchoolYear($id, true);
		$months->setOrder('sym.id', 'ASC');
		$months->setSelect('sym.*');
		$months->setSelect('(SELECT id FROM students_month_exclude sme WHERE sme.excluded=sym.id AND sme.enrolled_id='.$enrolled_id.') as excluded');
		$months->setHaving('excluded IS NULL');
		$this->data['months'] = $months->populate();

		$sfgrps = new $this->School_fees_groups_model;
		$sfgrps->setOrder('priority', 'ASC');
		$sfgrps->setActive('1', true);
		$this->data['school_fees_groups'] = $sfgrps->populate();

		$sfees = new $this->School_year_level_fees_model('sylf');
		$sfees->setSelect('sylf.*');
		$sfees->setLimit(0);
		$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
		$sfees->setSelect('sf.name, sf.group_id, sf.installment, sf.discount as discountable');
		$sfees->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.fee_id=sf.id AND sd.enrolled_id='.$enrolled_id.') as discounts');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND spa.applied_to LIKE "school_fees") as payments');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND sp.down=1 AND spa.applied_to LIKE "school_fees") as downpayments');
		$sfees->setSelect('(SELECT sfio.installment FROM students_fees_installment_override sfio WHERE sfio.enrolled_id='.$enrolled_id.' AND sfio.fee_id = sylf.fee_id) as installment2');
		$sfees->setSelect('(SELECT sfio.skip FROM students_fees_installment_override sfio WHERE sfio.enrolled_id='.$enrolled_id.' AND sfio.fee_id = sylf.fee_id) as skip2');
		$sfees->setSylvlId($enroll_data->sylvl_id, true);
		$sfees->setOrder('sf.name', 'ASC');
		$this->data['school_fees'] = $sfees->populate();
		
		$payments = new $this->Students_payments_model('sp');
		$payments->setLimit(0);
		$payments->setEnrolledId($enrolled_id, true);
		$payments->setOrder('payment_date', 'ASC');
		$this->data['payments'] = $payments->populate();

		$oldaccount = new $this->Students_oldaccounts_model('so');
		$oldaccount->setEnrolledId($enrolled_id,true);
		$oldaccount->setSelect('so.*');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id AND sp.down=0) as old_payments');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id AND sp.down=1) as old_down');
		$this->data['oldaccount'] = $oldaccount->get();

		$excluded = new $this->Students_month_exclude_model;
		$excluded->setEnrolledId($enrolled_id,true);
		$excluded->setJoin('school_year_months', 'school_year_months.id=students_month_exclude.excluded');
		$excluded->setSelect('students_month_exclude.*, school_year_months.school_year, school_year_months.month, school_year_months.year');
		$this->data['excluded_months'] = $excluded->populate();

		$services = new $this->Services_model;
		$services->setOrder('name', 'ASC');
		$services->setActive(1, true);
		$services->setLimit(0);
		$this->data['services'] = $services->populate();

		$student_services = new $this->Students_services_model('ss');
		$student_services->setEnrolledId($enrolled_id, true);
		$student_services->setLimit(0);
		$this->data['student_services'] = $student_services->populate();

		$this->_navbar($enroll_data);
		
		$this->load->view('finance/detailed_schedule', $this->data);
	}

	public function delete_service($id, $enrolled_id, $service_id)
	{
		$student_services = new $this->Students_services_model;
		$student_services->setEnrolledId($enrolled_id, true);
		$student_services->setId($service_id, true);
		$student_services->delete();
		redirect("finance/services/{$id}/{$enrolled_id}");
	}
	public function services($id, $enrolled_id)
	{
		$this->_search_redirect();
		$this->data['method_name'] = 'services';

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		if( count($this->input->post()) > 0 ) {			
			$this->form_validation->set_rules('month', 'Month', 'trim|required');
			$this->form_validation->set_rules('service_id', 'Service', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			
			if( $this->form_validation->run()==TRUE) {
				$service = new $this->Students_services_model;
				$service->setAmount(str_replace(',', '', $this->input->post('amount')));
				$service->setMonth($this->input->post('month'), true);
				$service->setServiceId($this->input->post('service_id'), true);
				$service->setEnrolledId($enrolled_id, true);
				$service->setExclude(array('id','lastmod'));

				$invoice = new $this->Students_invoices_model;
				$invoice->setEnrolledId($enrolled_id,true);
				$invoice->setFeeId($this->input->post('service_id'),true);
				$invoice->setMonth($this->input->post('month'), true);
				$invoice->setFeeType('SERVICE',true);
				$invoice->setAmount(str_replace(',', '', $this->input->post('amount')));
				$invoice->setExclude(array('id','lastmod'));
				if( floatval( $this->input->post('amount') ) > 0 ) {
					if( $service->nonEmpty() ) {
						$service->update();
					} else {
						$service->insert();
					}
					if( $invoice->nonEmpty() ) {
						$invoice->update();
					} else {
						$invoice->insert();
					}
				} else {
					$service->delete();
					$invoice->delete();
				}
			}
		}

		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$months = new $this->School_year_months_model('sym');
		$months->setSchoolYear($id, true);
		$months->setOrder('sym.id', 'ASC');
		$months->setSelect('sym.*');
		$months->setSelect('(SELECT id FROM students_month_exclude sme WHERE sme.excluded=sym.id AND sme.enrolled_id='.$enrolled_id.') as excluded');
		$months->setHaving('excluded IS NULL');
		$this->data['months'] = $months->populate();

		$services = new $this->Services_model;
		$services->setOrder('name', 'ASC');
		$services->setActive(1, true);
		$services->setLimit(0);
		$this->data['services'] = $services->populate();

		$student_services = new $this->Students_services_model('ss');
		$student_services->setEnrolledId($enrolled_id, true);
		$student_services->setJoin('services s', 's.id=ss.service_id');
		$student_services->setSelect('ss.*, s.name as service_name');
		$student_services->setLimit(0);
		$this->data['student_services'] = $student_services->populate();

		$this->_navbar($enroll_data);

		$this->load->view('finance/services', $this->data);
	}

	public function add_payment($id, $enrolled_id)
	{
		if( count($this->input->post()) > 0 ) {
			
			$this->form_validation->set_rules('payment_date', 'Payment Date', 'trim|required');
			$this->form_validation->set_rules('receipt_number', 'Receipt Number', 'trim|required');
			$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required');
			if( $this->form_validation->run()==TRUE) {

				$payment = new $this->Students_payments_model('sp');
				$payment->setEnrolledId($enrolled_id,true);
				$payment->setPaymentDate(date('Y-m-d', strtotime($this->input->post('payment_date'))));
				$payment->setReceiptNumber($this->input->post('receipt_number'),true);
				$payment->setAmount(  str_replace(',', '', $this->input->post('total_amount')));
				$payment->setLastmod( date('Y-m-d H:i:s') );
				$payment->setDown($this->input->post('downpayment')?1:0);
				$payment->setMemo( $this->input->post('memo') );
				$payment_id = false;
				$payment->insert();
				$payment_id = $payment->getId();
				
				if( $this->input->post('downpayment') ) {
					redirect("finance/update_downpayment/{$id}/{$enrolled_id}/{$payment_id}");
				} else {
					redirect("finance/update_payment/{$id}/{$enrolled_id}/{$payment_id}");
				}

				if( $payment_id && $this->input->post('applied') ) {
					foreach($this->input->post('applied') as $applied_to=>$applied) {
						foreach($applied as $key=>$amount) {
							if( $amount > 0) {
								$apply = new $this->Students_payments_applied_model('spa');
								$apply->setPaymentId( $payment_id,true);
								$apply->setFeeId($key,true);
								$apply->setAppliedTo($applied_to,true);
								$apply->setAmount( str_replace(',', '', $amount));
								if( $apply->nonEmpty() == true ) {
									$apply->setExclude('id');
									$apply->update();
								} else {
									$apply->insert();
								}
							}
						}
					}
				}
			}
		}
		redirect('finance/ledger/' . $id . "/" . $enrolled_id);
	}

	public function delete_payment($id, $enrolled_id, $payment_id) {
		$payment = new $this->Students_payments_model;
		$payment->setId($payment_id, true);
		$payment->setEnrolledId($enrolled_id,true);
		$payment->delete();
		redirect("finance/ledger/{$id}/{$enrolled_id}");
	}

	public function update_downpayment($id, $enrolled_id, $payment_id) {

		$this->_search_redirect();

		$payment = new $this->Students_payments_model('sp');
		$payment->setId($payment_id, true);
		$payment->setEnrolledId($enrolled_id,true);

		if( $payment->nonEmpty() == FALSE) {
			redirect(site_url("finance/ledger/{$id}/{$enrolled_id}") . "?error_code=0101");
		} else {
			$payment_data = $payment->getResults();
			if( $payment_data->down == 0) {
				redirect(site_url("finance/ledger/{$id}/{$enrolled_id}") . "?error_code=0102");
			}
		}

		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('payment_date', 'Payment Date', 'trim|required');
			$this->form_validation->set_rules('receipt_number', 'Receipt Number', 'trim|required');
			$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required');
			if( $this->form_validation->run()==TRUE) {
				
				$payment->setPaymentDate(date('Y-m-d', strtotime($this->input->post('payment_date'))));
				$payment->setReceiptNumber($this->input->post('receipt_number'));
				$payment->setAmount( str_replace(',', '', $this->input->post('total_amount')) );
				$payment->setLastmod( date('Y-m-d H:i:s') );
				$payment->setExclude(array('id','down'));
				$payment->setMemo( $this->input->post('memo') );
				$payment->update();

				if( $payment_id && $this->input->post('applied') ) {
					
					$delete_existing = new $this->Students_payments_applied_model;
					$delete_existing->setPaymentId($payment_id,true);
					$delete_existing->delete();

					foreach($this->input->post('applied') as $applied_to=>$applied) {
						foreach($applied as $key=>$amount) {
							if( $amount > 0) {
								$apply = new $this->Students_payments_applied_model('spa');
								$apply->setPaymentId($payment_id,true);
								$apply->setFeeId($key,true);
								$apply->setAppliedTo($applied_to,true);
								$apply->setAmount( str_replace(',', '', $amount) );
								if( $apply->nonEmpty() == true ) {
									$apply->setExclude('id');
									$apply->update();
								} else {
									$apply->insert();
								}
							}
						}
					}
				}
				if( $this->input->get('redirect') ) {
					redirect( $this->input->get('redirect') );
				} else {
					redirect('finance/ledger/'.$id.'/'.$enrolled_id);
				}
			}
		}

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as whole_year');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id) as payments');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$sfgrps = new $this->School_fees_groups_model;
		$sfgrps->setOrder('priority', 'ASC');
		$sfgrps->setActive('1', true);
		$sfgrps->setLimit(0);
		$this->data['school_fees_groups'] = $sfgrps->populate();

		$sfees = new $this->School_year_level_fees_model('sylf');
		$sfees->setSelect('sylf.*');
		$sfees->setLimit(0);
		$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
		$sfees->setSelect('sf.name, sf.group_id, sf.installment, sf.discount as discountable');
		$sfees->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.fee_id=sf.id AND sd.enrolled_id='.$enrolled_id.') as discounts');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND spa.applied_to="school_fees") as payments');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND sp.id='.$payment_id.' AND spa.applied_to="school_fees") as applied');
		$sfees->setSylvlId($enroll_data->sylvl_id, true);
		$sfees->setOrder('sf.name', 'ASC');
		$this->data['school_fees'] = $sfees->populate();

		$oldaccount = new $this->Students_oldaccounts_model('so');
		$oldaccount->setEnrolledId($enrolled_id,true);
		$oldaccount->setSelect('so.*, (SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id='.$enrolled_id.') as payments');
		$oldaccount->setSelect('so.*, (SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id='.$enrolled_id.' AND sp.id='.$payment_id.') as applied');
		$oldaccount_data = $oldaccount->get();
		
		$this->data['oldaccount'] = $oldaccount_data;

		$this->data['payment'] = $payment->get();

		$student_services = new $this->Students_services_model('ss');
		$student_services->setEnrolledId($enrolled_id, true);
		$student_services->setJoin('services s', 's.id=ss.service_id');
		$student_services->setSelect('ss.*, s.name as service_name');
		$student_services->setSelect('SUM(ss.amount) as amount');
		$student_services->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=ss.service_id AND sp.enrolled_id=ss.enrolled_id AND sp.id='.$payment_id.' AND spa.applied_to="services") as applied');
		$student_services->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=ss.service_id AND sp.enrolled_id=ss.enrolled_id AND sp.id=spa.payment_id AND spa.applied_to="services") as total_applied');
		$student_services->setLimit(0);
		$student_services->setGroupBy('ss.service_id');
		$this->data['student_services'] = $student_services->populate();

		$this->_navbar($enroll_data);

		$this->load->view('finance/downpayment_update', $this->data);
	}

	public function reset_payment($id, $enrolled_id, $payment_id) {
		$delete_existing = new $this->Students_payments_applied2_model;
		$delete_existing->setPaymentId($payment_id,true);
		$delete_existing->delete();
		redirect("finance/update_payment/{$id}/{$enrolled_id}/{$payment_id}");
	}

	public function reset_payment_applied($id, $enrolled_id) {

		$applied2 = new $this->Students_payments_applied2_model('spa');
		$applied2->setJoin('students_payments sp', 'sp.id=spa.payment_id');
		$applied2->setSelect('spa.id');
		$applied2->setWhere('sp.enrolled_id', $enrolled_id);
		$applied2->setLimit(0);
		foreach( $applied2->populate() as $spa) {
			$delete_existing = new $this->Students_payments_applied2_model;
			$delete_existing->setId($spa->id,true);
			$delete_existing->delete();
		}

		$applied = new $this->Students_payments_applied_model('spa');
		$applied->setJoin('students_payments sp', 'sp.id=spa.payment_id');
		$applied->setSelect('spa.id');
		$applied->setWhere('sp.enrolled_id', $enrolled_id);
		$applied->setLimit(0);
		foreach( $applied->populate() as $spa) {
			$delete_existing = new $this->Students_payments_applied_model;
			$delete_existing->setId($spa->id,true);
			$delete_existing->delete();
		}
		
		redirect("finance/ledger/{$id}/{$enrolled_id}");
	}



	public function update_payment($id, $enrolled_id, $payment_id) {

		$this->_search_redirect();

		$payment = new $this->Students_payments_model('sp');
		$payment->setId($payment_id, true);
		$payment->setEnrolledId($enrolled_id,true);

		if( $payment->nonEmpty() == FALSE) {
			redirect(site_url("finance/ledger/{$id}/{$enrolled_id}") . "?error_code=0101");
		} else {
			$payment_data = $payment->getResults();
			if( $payment_data->down == 1) {
				redirect(site_url("finance/ledger/{$id}/{$enrolled_id}") . "?error_code=0102");
			}
		}



		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('payment_date', 'Payment Date', 'trim|required');
			$this->form_validation->set_rules('receipt_number', 'Receipt Number', 'trim|required');
			$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required');
			if( $this->form_validation->run()==TRUE) {
				
				$payment->setPaymentDate(date('Y-m-d', strtotime($this->input->post('payment_date'))));
				$payment->setReceiptNumber($this->input->post('receipt_number'));
				$payment->setAmount( str_replace(',', '', $this->input->post('total_amount')) );
				$payment->setLastmod( date('Y-m-d H:i:s') );
				$payment->setExclude(array('id','down'));
				$payment->setMemo( $this->input->post('memo') );
				$payment->update();

				if( $payment_id && $this->input->post('applied') ) {
					
					$delete_existing = new $this->Students_payments_applied2_model;
					$delete_existing->setPaymentId($payment_id,true);
					$delete_existing->delete();

					foreach($this->input->post('applied') as $invoice_id=>$amount) {
							if( $amount > 0) {
								$apply = new $this->Students_payments_applied2_model('spa');
								$apply->setPaymentId($payment_id,true);
								$apply->setInvoiceId($invoice_id,true);
								$apply->setAmount( str_replace(',', '', $amount) );
								if( $apply->nonEmpty() == true ) {
									$apply->setExclude('id');
									$apply->update();
								} else {
									$apply->insert();
								}
							}
					}
				}
				if( $this->input->get('redirect') ) {
					redirect( $this->input->get('redirect') );
				} else {
					redirect('finance/ledger/'.$id.'/'.$enrolled_id);
				}
			}
		}

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as whole_year');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
		$enroll->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id) as payments');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;
		$this->_navbar($enroll_data);

		$months = new $this->School_year_months_model('sym');
		$months->setSchoolYear($id, true);
		$months->setOrder('sym.id', 'ASC');
		$months->setSelect('sym.*');
		$months->setSelect('(SELECT id FROM students_month_exclude sme WHERE sme.excluded=sym.id AND sme.enrolled_id='.$enrolled_id.') as excluded');
		$months->setHaving('excluded IS NULL');
		$months_data = $months->populate();

		foreach( $months_data as $k=>$month_data ) {
			$invoice = new $this->Students_invoices_model('si');
			$invoice->setEnrolledId($enrolled_id,true);
			$invoice->setMonth($month_data->month,true);
			$invoice->setSelect('si.*');
			$invoice->setSelect('(IF(si.fee_type=\'SCHOOLFEE\',(SELECT name FROM school_fees WHERE id=si.fee_id),(IF(si.fee_type=\'SERVICE\',(SELECT name FROM services WHERE id=si.fee_id),"Old Account")))) as fee_name');
			$invoice->setSelect('(SELECT amount FROM students_payments_applied2 spa2 WHERE spa2.payment_id='.$payment_id.' AND spa2.invoice_id=si.id) as applied');
			$invoice->setSelect('(SELECT SUM(amount) FROM students_payments_applied2 spa2 WHERE spa2.invoice_id=si.id) as total_applied');
			$invoice->setLimit(0);
			$months_data[$k]->invoices = $invoice->populate();
		}

		$invoices = new $this->Students_invoices_model('si');
		$invoices->setEnrolledId($enrolled_id,true);
		$this->data['invoices_count'] = $invoices->count_all_results();
		//print_r( $months_data );
		$this->data['months'] = $months_data;

		$this->data['payment'] = $payment->get();

		$this->load->view('finance/payment_update', $this->data);
	}

	public function payment_detail($id, $enrolled_id, $payment_id, $output='') {

		$this->data['output'] = $output;
		$this->data['payment_id'] = $payment_id;
		$this->data['id'] = $id;
		$this->data['enrolled_id'] = $enrolled_id;

		$payment = new $this->Students_payments_model('sp');
		$payment->setId($payment_id, true);
		$payment->setEnrolledId($enrolled_id,true);
		$payment_detail = $payment->get();
		$this->data['payment_detail'] = $payment_detail;

		$this->data['applied_payments'] = false;
		if( $payment_detail->down == '1') {
			$applied = new $this->Students_payments_applied_model('spa');
			$applied->setPaymentId($payment_id,true);
			$applied->setSelect('spa.*');
			$applied->setSelect("(IF(spa.applied_to='school_fees',(SELECT name FROM school_fees sf WHERE sf.id=spa.fee_id),(IF(spa.applied_to='services',(SELECT name FROM services ser WHERE ser.id=spa.fee_id),'Old Account')))) as fee_name");
			$applied->setLimit(0);
			$applied->setOrder('spa.fee_id', 'ASC');
			$this->data['applied_payments'] = $applied->populate();
		} else {
			$applied = new $this->Students_payments_applied2_model('spa');
			$applied->setPaymentId($payment_id,true);
			$applied->setSelect('spa.*, si.month, si.fee_type');
			$applied->setSelect("(IF(si.fee_type='SCHOOLFEE',(SELECT name FROM school_fees sf WHERE sf.id=si.fee_id),(IF(si.fee_type='SERVICE',(SELECT name FROM services ser WHERE ser.id=si.fee_id),'Old Account')))) as fee_name");
			$applied->setSelect('(SELECT sym.year FROM school_year_months sym WHERE sym.school_year=se.school_year AND sym.month=si.month) as year');
			$applied->setJoin('students_invoices si', 'si.id=spa.invoice_id');
			$applied->setJoin('school_fees sf', 'sf.id=si.fee_id');
			$applied->setJoin('students_enrolled se', 'se.id=si.enrolled_id');
			$applied->setLimit(0);
			$applied->setOrder('sf.id', 'ASC');
			$this->data['applied_payments'] = $applied->populate();

			$applied->setOrder('si.month', 'ASC');
			$applied->setGroupBy('si.month');
			$this->data['amonths'] = $applied->populate();

		}

		if( $output != 'ajax') {
			$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
			$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

			if($current_school_year) {
				$school_year = new $this->School_year_model;
				$school_year->setId($current_school_year,true);
				if( $school_year->nonEmpty() === FALSE ) {
					redirect("school_year");
				}
				$this->data['primary_school_year'] = $school_year->get();
			} 

			$enroll = new $this->Students_enrolled_model('se');
			$enroll->setId($enrolled_id, true);
			$enroll->setJoin('students_information si', 'si.id = se.student_id');
			$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
			$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
			$enroll->setSelect('campuses.name as campus_name');
			$enroll->setSelect('si.*, se.*, se.id as enroll_id');
			$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
			$enroll->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as whole_year');
			$enroll->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
			$enroll->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id) as payments');
			$enroll_data = $enroll->get();
			$this->data['enroll_data'] = $enroll_data;
			$this->_navbar($enroll_data);
		}
		$this->load->view('finance/payment_detail', $this->data);

	}

	public function payment_detail2($id, $enrolled_id, $month, $output='') {

		$this->data['output'] = $output;
		$this->data['id'] = $id;
		$this->data['enrolled_id'] = $enrolled_id;
		$this->data['month'] = $month;


		$applied = new $this->Students_payments_applied2_model('spa');
		$applied->setJoin('students_invoices si', 'si.id=spa.invoice_id');
		$applied->setJoin('students_payments sp', 'sp.id=spa.payment_id');
		$applied->setSelect('spa.*');
		$applied->setSelect('sp.payment_date, sp.receipt_number, sp.id as payment_id');
		$applied->setSelect("(SELECT sf.name FROM students_invoices si JOIN school_fees sf ON sf.id=si.fee_id WHERE si.id=spa.invoice_id) as fee_name");
		$applied->setWhere('si.enrolled_id', $enrolled_id);
		$applied->setWhere('si.month', $month);
		$applied->setLimit(0);
		$this->data['applied_payments'] = $applied->populate();
		
		$this->load->view('finance/payment_detail2', $this->data);

	}

	public function add_discount($id, $enrolled_id)
	{
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('description', 'Description', 'trim|required');
			if( $this->form_validation->run()==TRUE) {
				foreach( $this->input->post('discount') as $fee_id=>$amount) {
					if( $amount > 0 ) {
						$discount = new $this->Students_discounts_model;
						$discount->setEnrolledId($enrolled_id);
						$discount->setFeeId($fee_id);
						$discount->setAmount( str_replace(',', '', $amount ) );
						$discount->setDescription($this->input->post('description'));
						$discount->setLastmod( date('Y-m-d H:i:s') );
						$discount->insert();
					}
				}
			}
			redirect('finance/discounts/' . $id . "/" . $enrolled_id);
		}
	}

	public function discounts($id, $enrolled_id, $discount_id=false)
	{
		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$this->_navbar($enroll_data);

		$sfgrps = new $this->School_fees_groups_model;
		$sfgrps->setOrder('priority', 'ASC');
		$sfgrps->setActive('1', true);
		$sfgrps->setLimit(0);
		$this->data['school_fees_groups'] = $sfgrps->populate();

		$sfees = new $this->School_year_level_fees_model('sylf');
		$sfees->setSelect('sylf.*');
		$sfees->setLimit(0);
		$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
		$sfees->setSelect('sf.name, sf.group_id, sf.installment, sf.discount as discountable');
		$sfees->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.fee_id=sf.id AND sd.enrolled_id='.$enrolled_id.') as discounts');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND spa.applied_to LIKE "school_fees") as payments');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' AND sp.down=1 AND spa.applied_to LIKE "school_fees") as downpayments');
		$sfees->setSylvlId($enroll_data->sylvl_id, true);
		$sfees->setOrder('sf.name', 'ASC');
		$this->data['school_fees'] = $sfees->populate();

		$discounts = new $this->Students_discounts_model('sd');
		$discounts->setLimit(0);
		$discounts->setEnrolledId($enrolled_id, true);
		$discounts->setJoin('school_fees sf', 'sf.id=sd.fee_id');
		$discounts->setSelect('sd.*, sf.name as fee_name');
		$discounts->setOrder('sd.lastmod', 'DESC');
		$this->data['discounts'] = $discounts->populate();

		$this->load->view('finance/discounts', $this->data);
	}

	public function discount($id, $enrolled_id, $discount_id=false)
	{

		$this->_search_redirect();
		
		$discount = new $this->Students_discounts_model('sd');
		$discount->setEnrolledId($enrolled_id,true);
		
		if( $discount_id ) {
			$discount->setId($discount_id,true);
		}

		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('fee_id', 'School Fee', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			$this->form_validation->set_rules('description', 'Description', 'trim|required');
			if( $this->form_validation->run()==TRUE) {
				$discount->setFeeId($this->input->post('fee_id'),true);
				$discount->setAmount( str_replace(',', '', $this->input->post('amount') ) );
				$discount->setDescription($this->input->post('description'));
				$discount->setLastmod( date('Y-m-d H:i:s') );
				if( $discount->nonEmpty()==TRUE ) {
					$discount->setExclude(array('id'));
					$discount->update();
				} else {
					$discount->insert();
				}
			}
			redirect('finance/discounts/' . $id . "/" . $enrolled_id);
		}
		//$discount->setJoin('school_fees sf', 'sf.id=sd.fee_id');
		$discount->setJoin('students_enrolled se', 'se.id=sd.enrolled_id');
		$discount->setJoin('school_year_levels syl', 'syl.school_year=se.school_year AND syl.level=se.grade_level');
		$discount->setSelect('sd.*');
		//$discount->setSelect('sf.name as fee_name');
		$discount->setSelect('syl.id as syl_id');
		$discount->setSelect('(SELECT sf.name FROM school_fees sf WHERE sf.id=sd.fee_id) as fee_name');
		$discount->setSelect('(SELECT sylf.amount FROM school_year_level_fees sylf WHERE sylf.sylvl_id = syl_id AND sylf.fee_id = sd.fee_id) as fee_amount');

		$this->data['discount'] = $discount->get();
		$this->data['discount_id'] = $discount_id;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$sfgrps = new $this->School_fees_groups_model;
		$sfgrps->setOrder('priority', 'ASC');
		$sfgrps->setActive('1', true);
		$sfgrps->setLimit(0);
		$this->data['school_fees_groups'] = $sfgrps->populate();

		$sfees = new $this->School_year_level_fees_model('sylf');
		$sfees->setSelect('sylf.*');
		$sfees->setLimit(0);
		$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
		$sfees->setSelect('sf.*');
		$sfees->setSylvlId($enroll_data->sylvl_id, true);
		$sfees->setOrder('sf.name', 'ASC');
		$sfees->setWhere('sf.discount = 1');
		$this->data['school_fees'] = $sfees->populate();

		$this->_navbar($enroll_data);

		$this->load->view('finance/discounts_update', $this->data);

	}

	public function delete_discount($id, $enrolled_id, $discount_id=false)
	{
		$discount = new $this->Students_discounts_model;
		$discount->setEnrolledId($enrolled_id,true);
		$discount->setId($discount_id,true);
		$discount->delete();
		redirect('finance/ledger/' . $id . "/" . $enrolled_id);

	}

	public function delete_invoice($id, $enrolled_id, $month)
	{
		$invoices = new $this->Students_invoices_model;
		$invoices->setEnrolledId($enrolled_id,true);
		$invoices->setMonth($month,true);
		$invoices->delete();
		redirect('finance/ledger/' . $id . "/" . $enrolled_id);

	}

	public function delete_invoices($id, $enrolled_id)
	{
		if( $this->input->post('delete_invoice_item') ) {
			foreach($this->input->post('delete_invoice_item') as $month) {
				$invoices = new $this->Students_invoices_model;
				$invoices->setEnrolledId($enrolled_id,true);
				$invoices->setMonth($month,true);
				foreach($invoices->populate() as $invoices_item) {
					$applied = new $this->Students_payments_applied2_model;
					$applied->setInvoiceId($invoices_item->id,true);
					$applied->delete();
				}
				$invoices->delete();
			}
		}
		redirect('finance/ledger/' . $id . "/" . $enrolled_id);

	}

	public function invoice_detail($id, $enrolled_id, $month, $output=null)
	{
		$this->data['output'] = $output;
		$this->data['month'] = $month;

		if( !$output ) {

			$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
			if($current_school_year) {
				$school_year = new $this->School_year_model;
				$school_year->setId($current_school_year,true);
				if( $school_year->nonEmpty() === FALSE ) {
					redirect("school_year");
				}
				$this->data['primary_school_year'] = $school_year->get();
			} 
			$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

		}

		$invoices = new $this->Students_invoices_model('si');
		$invoices->setEnrolledId($enrolled_id,true);
		$invoices->setMonth($month,true);
		$invoices->setSelect('si.*');
		$invoices->setSelect("(IF(si.fee_type='SCHOOLFEE',(SELECT name FROM school_fees sf WHERE sf.id=si.fee_id),(IF(si.fee_type='SERVICE',(SELECT name FROM services ser WHERE ser.id=si.fee_id),'Old Account')))) as fee_name");
		$invoices->setLimit(0);
		$this->data['invoices'] = $invoices->populate();

		if( !$output ) {
			$enroll = new $this->Students_enrolled_model('se');
			$enroll->setId($enrolled_id, true);
			$enroll->setJoin('students_information si', 'si.id = se.student_id');
			$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
			$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
			$enroll->setSelect('campuses.name as campus_name');
			$enroll->setSelect('si.*, se.*, se.id as enroll_id');
			$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
			$enroll_data = $enroll->get();
			$this->data['enroll_data'] = $enroll_data;

			$this->_navbar($enroll_data);
		}

		$this->load->view('finance/invoice_detail', $this->data);
	}

	public function recalculate($id, $enrolled_id)
	{
		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		/* Delete Existing Invoices */
		$invoices = new $this->Students_invoices_model;
		$invoices->setEnrolledId($enrolled_id,true);
		$invoices->setLimit(0);
		foreach($invoices->populate() as $inv) {
			$applied = new $this->Students_payments_applied2_model;
			$applied->setInvoiceId($inv->id,true);
			$applied->delete();
		} 
		$invoices->delete();

		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		//$enroll->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as student_account');

		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$months = new $this->School_year_months_model('sym');
		$months->setSchoolYear($id, true);
		$months->setOrder('sym.id', 'ASC');
		$months->setSelect('sym.*');
		$months->setSelect('(SELECT id FROM students_month_exclude sme WHERE sme.excluded=sym.id AND sme.enrolled_id='.$enrolled_id.') as excluded');
		$months->setHaving('excluded IS NULL');
		$syms = $months->populate();

		$this->load->model('School_year_level_fees_model');
		$sfees = new $this->School_year_level_fees_model('sylf');
		$sfees->setSelect('sylf.*');
		$sfees->setLimit(0);
		$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
		$sfees->setSelect('sf.name, sf.group_id, sf.installment');
		$sfees->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.fee_id=sf.id AND sd.enrolled_id='.$enrolled_id.') as discounts');
		$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrolled_id.' and sp.down=1) as downpayments');
		$sfees->setSelect('(SELECT sfio.installment FROM students_fees_installment_override sfio WHERE sfio.enrolled_id='.$enrolled_id.' AND sfio.fee_id = sylf.fee_id) as installment2');
		$sfees->setSelect('(SELECT sfio.skip FROM students_fees_installment_override sfio WHERE sfio.enrolled_id='.$enrolled_id.' AND sfio.fee_id = sylf.fee_id) as skip2');
		$sfees->setSylvlId($enroll_data->sylvl_id, true);
		$school_fees = $sfees->populate();

		$excluded = new $this->Students_month_exclude_model;
		$excluded->setEnrolledId($enrolled_id,true);
		$excluded->setJoin('school_year_months', 'school_year_months.id=students_month_exclude.excluded');
		$excluded->setSelect('students_month_exclude.*, school_year_months.school_year, school_year_months.month, school_year_months.year');
		$excluded_months = $excluded->populate();

		$total_whole_year_fee = 0;
		$schedule_fee = array();
		$schedule_invoices = array();

		foreach( $school_fees as $sf) { 
				$total_fee_amount = ($sf->amount - $sf->discounts) - $sf->downpayments;
				$sfim = ($sf->installment2) ? $sf->installment2 : $sf->installment;
			    //$sf_installment = (($sfim - count($excluded_months))>0) ? ($sfim - count($excluded_months)): 1;
			    $sf_installment = ($sfim) ? $sfim : 1; 
                  if( $sf_installment > count($syms) ) {
                     $sf_installment = count($syms);
                  }
			    $month_installment = $total_fee_amount / $sf_installment ;
			    $skip = ($sf->skip2) ? intval($sf->skip2) : intval($sf->skip);

				if($month_installment > 0) {
					for($i=0;$i<$sf_installment;$i++) {
							$i2 = $i + $skip;
							$invoice = new $this->Students_invoices_model;
							$invoice->setFeeId($sf->fee_id,true);
							$invoice->setEnrolledId($enrolled_id,true);
							$invoice->setMonth($syms[$i2]->month,true);
							$invoice->setAmount($month_installment);
							$invoice->setLastmod( date('Y-m-d H:i:s') );
							$invoice->setFeeType('SCHOOLFEE',true);
							if( $invoice->nonEmpty()==TRUE ) {
								$invoice->setExclude(array('id'));
								$invoice->update();
							} else {
								$invoice->insert();
							}
						if( !isset($schedule_fee[$i]) ) {
		          			$schedule_fee[$i] = 0;
		        		}
		        		$schedule_fee[$i2] = $schedule_fee[$i2] + $month_installment;
					}
				}
			}

		$oldaccount = new $this->Students_oldaccounts_model('so');
		$oldaccount->setEnrolledId($enrolled_id,true);
		$oldaccount->setSelect('so.*');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id AND sp.down=0) as old_payments');
		$oldaccount->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=so.enrolled_id AND sp.down=1) as old_down');
		$oldaccount_data = $oldaccount->get();

		if( $oldaccount_data ) {
			$old_amount = ($oldaccount_data) ? (($oldaccount_data->amount - $oldaccount_data->old_down) / $oldaccount_data->installment) : 0;
			$oldaccount_i = ($oldaccount_data) ? $oldaccount_data->installment : 1;

			$months = new $this->School_year_months_model;
			$months->setSchoolYear($id, true);
			$months->setOrder('id', 'ASC');
			$n = 0;

			if( $old_amount > 0 ) {
				foreach($months->populate() as $month) {
					$invoice = new $this->Students_invoices_model;
					$invoice->setFeeId('0',true);
					$invoice->setEnrolledId($enrolled_id,true);
					$invoice->setMonth($month->month,true);
					$invoice->setAmount($old_amount);
					$invoice->setLastmod( date('Y-m-d H:i:s') );
					$invoice->setFeeType('OLDACCOUNT',true);
						if( $n < $oldaccount_i) {
							if($invoice->nonEmpty()) {
								$invoice->setExclude('id');
								$invoice->update();
							} else {
								$invoice->insert();
							}
						} else {
							$invoice->delete();
						}
					$n++;
				}
			}
		}

		$student_services = new $this->Students_services_model('ss');
		$student_services->setEnrolledId($enrolled_id, true);
		$student_services->setLimit(0);
		$services_data = $student_services->populate();
		if( $services_data ) {
			foreach($services_data as $service) {
					$invoice = new $this->Students_invoices_model;
					$invoice->setFeeId($service->service_id,true);
					$invoice->setEnrolledId($enrolled_id,true);
					$invoice->setMonth($service->month,true);
					$invoice->setAmount($service->amount);
					$invoice->setLastmod( date('Y-m-d H:i:s') );
					$invoice->setFeeType('SERVICE',true);
					if($invoice->nonEmpty()) {
						$invoice->setExclude('id');
						$invoice->update();
					} else {
						$invoice->insert();
					}
				}
		}
		
		redirect('finance/ledger/' . $id . "/" . $enrolled_id);
	}

	public function print_soa($id=false,$level=0,$campus_id=0) {

		$this->_search_redirect();

		$this->data['grade_level'] = $level;
		$this->data['campus_id'] = $campus_id;
		$this->data['installment'] = $this->input->get('installment');

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

				$this->data['preview_first'] = ($this->input->get('preview_first')) ? 1 : 0;

				$sym1 = new $this->School_year_months_model;
				$sym1->setLimit(0);
				$this->data['months'] = $sym1->populate();

				$sym = new $this->School_year_months_model;
				$sym->setId($this->input->get('month'), true);
				$current_month = $sym->get();
				$balance_month = $sym->get();

				if($this->input->get('month_balance')) {
					$sym2 = new $this->School_year_months_model;
					$sym2->setId($this->input->get('month_balance'), true);
					$balance_month = $sym2->get();
				}

				$enrolled = new $this->Students_enrolled_model('se');
				$enrolled->setJoin('students_information si', 'si.id = se.student_id');
				$enrolled->setSelect('si.*, se.*, se.id as enroll_id');
				$enrolled->setJoin('campuses', 'campuses.id = se.campus_id');
				$enrolled->setSelect('campuses.name as campus_name');
				$enrolled->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as student_account');
				$enrolled->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
				$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=0) as payments');
				$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=1) as downpayments');
				$enrolled->setSelect('(SELECT id FROM `school_year_levels` syl WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as syl_id');
				$enrolled->setSelect('(SELECT SUM(sin.amount) FROM students_invoices sin WHERE sin.month='.$balance_month->month.' AND sin.enrolled_id=se.id) as amount_due');
				$enrolled->setSelect('(SELECT SUM(so.amount) FROM students_oldaccounts so WHERE so.enrolled_id=se.id) as old_account');
				$enrolled->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=se.id) as old_payments');
				$enrolled->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied2 spa JOIN students_payments sp ON spa.payment_id = sp.id JOIN students_invoices si ON si.id = spa.invoice_id WHERE si.fee_id=0 AND sp.enrolled_id=se.id) as old_payments2');
				$enrolled->setSelect('(SELECT label FROM school_year sy WHERE se.school_year=sy.id) as school_year_label');
				if( $this->input->get('grade_level') != '' ) {
					$enrolled->setGradeLevel($this->input->get('grade_level'),true);
					$this->data['grade_level'] = $this->input->get('grade_level');
				}
				if( $this->input->get('campus') != '' ) {
					$enrolled->setCampusId($this->input->get('campus'),true);
					$campus = new $this->Campuses_model;
					$campus->setId($this->input->get('campus'), true);
					$this->data['campus'] = $campus->get();
				}
				if( $this->input->get('students') ) {
					$enrolled->setWhereIn('se.id', $this->input->get('students') );
				}
				$enrolled->setSchoolYear($current_school_year, true, false, null, null, 99);
				$enrolled->setOrder('si.lastname', 'ASC');
				$enrolled->setOrder('si.firstname', 'ASC');
				$enrolled->setLimit(0);
				$enrollees = $enrolled->populate();

				$sfgrps = new $this->School_fees_groups_model;
				$sfgrps->setOrder('priority', 'ASC');
				$sfgrps->setActive(1, true);
				$this->data['school_fees_groups'] = $sfgrps->populate();

				$services = new $this->Services_model;
				$services->setOrder('name', 'ASC');
				$services->setLimit(0);
				$this->data['services'] = $services->populate();

				foreach( $enrollees as $key => $enrollee) {
					$sfees = new $this->School_year_level_fees_model('sylf');
					$sfees->setSelect('sylf.*');
					$sfees->setLimit(0);
					$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
					$sfees->setSelect('sf.name, sf.group_id, sf.installment, sf.discount as discountable');
					$sfees->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.fee_id=sf.id AND sd.enrolled_id='.$enrollee->enroll_id.') as discounts');
					$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrollee->enroll_id.') as payments');
					$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrollee->enroll_id.' AND sp.down=1) as downpayments');
					$sfees->setSelect('(SELECT SUM(spa2.amount) FROM students_invoices sti JOIN students_payments_applied2 spa2 ON sti.id = spa2.invoice_id WHERE sti.fee_id=sf.id AND sti.enrolled_id='.$enrollee->enroll_id.' AND sti.fee_type=\'SCHOOLFEE\') as payment_applied');
					$sfees->setSylvlId($enrollee->syl_id, true);
					$sfees->setOrder('sf.name', 'ASC');
					$enrollee->school_fees = $sfees->populate();
					
					$invoices = new $this->Students_invoices_model;
					$invoices->setEnrolledId($enrollee->enroll_id,true);
					$invoices->setLimit(0);
					$enrollee->monthly_fees = $invoices->populate();

					$invoices2 = new $this->Students_invoices_model;
					$invoices2->setEnrolledId($enrollee->enroll_id,true);
					$invoices2->setMonth($current_month->month,true);
					$invoices2->setLimit(0);
					$enrollee->months_fee = $invoices2->populate();

					$services = new $this->Students_services_model('ss');
					$services->setEnrolledId($enrollee->enroll_id,true);
					$services->setJoin('services s', 's.id = ss.service_id');
					$services->setSelect('ss.*, s.name as service_name');
					$services->setWhere('((SELECT COUNT(si.id) FROM students_invoices si RIGHT JOIN students_payments_applied2 spa2 ON spa2.invoice_id=si.id WHERE ss.enrolled_id=si.enrolled_id AND ss.month=si.month AND ss.service_id=si.fee_id AND si.fee_type="SERVICE") = 0)');
					//$services->setMonth($current_month->month,true);
					
					$services->setLimit(0);
					$enrollee->services = $services->populate();

					$services2 = new $this->Students_services_model('ss');
					$services2->setEnrolledId($enrollee->enroll_id,true);
					$services2->setJoin('services s', 's.id = ss.service_id');
					$services2->setSelect('ss.*, s.name as service_name');
					$services2->setLimit(0);
					$enrollee->all_services = $services2->populate();

					$enrollee->current_month = $current_month;
					$enrollee->balance_month = $balance_month;
					$enrollee->due_date = $this->input->get('due_date');

					if( $this->input->get('students') ) {
						foreach($this->input->get('students') as $sKey=>$sId) {
							if( $enrollee->enroll_id == $sId) {
								$key = $sKey;
							}
						}
					}

					$enrollees[$key] = $enrollee;
				}
				
				$levels = new $this->School_year_levels_model('syl');
				$levels->setSchoolYear( $current_school_year, true );
				$levels->setLimit(0);
				$levels->setSelect('syl.*');
				$this->data['levels'] = $levels->populate();

				$campuses = new $this->Campuses_model('c');
				$campuses->setLimit(0);
				$campuses->setOrder('name', 'ASC');
				$campuses->setSelect("c.*");
				foreach( $levels->populate() as $level ) {
					$campuses->setSelect("(SELECT COUNT(*) FROM students_enrolled se WHERE se.school_year={$current_school_year} AND se.grade_level='{$level->level}' AND se.campus_id=c.id) as level_" . $level->level);
				}
				$this->data['campuses'] = $campuses->populate();

				$this->data['enrollees'] = $enrollees;
				$this->data['enrollees_count'] = $enrolled->count_all_results();

				$this->data['due_date'] = $this->input->get('due_date');

				$viewfile = ($this->input->get('masterlist')) ? 'finance/print_masterlist' : 'finance/print_soa';

		$this->load->view($viewfile, $this->data);
	}

	public function print_collection_letters($id=false,$level=0,$campus_id=0) {

		$this->_search_redirect();

		$this->data['grade_level'] = $level;
		$this->data['campus_id'] = $campus_id;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

				$sym1 = new $this->School_year_months_model;
				$sym1->setLimit(0);
				$this->data['months'] = $sym1->populate();

				$enrolled = new $this->Students_enrolled_model('se');
				$enrolled->setJoin('students_information si', 'si.id = se.student_id');
				$enrolled->setSelect('si.*, se.*, se.id as enroll_id');
				$enrolled->setJoin('campuses', 'campuses.id = se.campus_id');
				$enrolled->setSelect('campuses.name as campus_name');
				$enrolled->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as student_account');
				$enrolled->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
				$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=0) as payments');
				$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=1) as downpayments');
				$enrolled->setSelect('(SELECT id FROM `school_year_levels` syl WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as syl_id');
				$enrolled->setSelect('(SELECT SUM(so.amount) FROM students_oldaccounts so WHERE so.enrolled_id=se.id) as old_account');
				$enrolled->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=se.id) as old_payments');
				$enrolled->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied2 spa JOIN students_payments sp ON spa.payment_id = sp.id JOIN students_invoices si ON si.id = spa.invoice_id WHERE si.fee_id=0 AND sp.enrolled_id=se.id) as old_payments2');
				$enrolled->setSelect('(SELECT label FROM school_year sy WHERE se.school_year=sy.id) as school_year_label');
				if( $this->input->get('grade_level') != '' ) {
					$enrolled->setGradeLevel($this->input->get('grade_level'),true);
					$this->data['grade_level'] = $this->input->get('grade_level');
				}
				if( $this->input->get('campus') != '' ) {
					$enrolled->setCampusId($this->input->get('campus'),true);
					$campus = new $this->Campuses_model;
					$campus->setId($this->input->get('campus'), true);
					$this->data['campus'] = $campus->get();
				}
				if( $this->input->get('students') ) {
					$enrolled->setWhereIn('se.id', $this->input->get('students') );
				}
				$enrolled->setSchoolYear($current_school_year, true, false, null, null, 99);
				$enrolled->setOrder('si.lastname', 'ASC');
				$enrolled->setOrder('si.firstname', 'ASC');
				$enrolled->setLimit(0);
				$enrollees = $enrolled->populate();

				
				$levels = new $this->School_year_levels_model('syl');
				$levels->setSchoolYear( $current_school_year, true );
				$levels->setLimit(0);
				$levels->setSelect('syl.*');
				$this->data['levels'] = $levels->populate();

				$campuses = new $this->Campuses_model('c');
				$campuses->setLimit(0);
				$campuses->setOrder('name', 'ASC');
				$campuses->setSelect("c.*");
				foreach( $levels->populate() as $level ) {
					$campuses->setSelect("(SELECT COUNT(*) FROM students_enrolled se WHERE se.school_year={$current_school_year} AND se.grade_level='{$level->level}' AND se.campus_id=c.id) as level_" . $level->level);
				}
				$this->data['campuses'] = $campuses->populate();

				$this->data['enrollees'] = $enrollees;
				$this->data['enrollees_count'] = $enrolled->count_all_results();

				$this->data['due_date'] = $this->input->get('due_date');

				$viewfile = 'finance/print_collection_letters';

		$this->load->view($viewfile, $this->data);
	}


	public function print_soa_options($id=false,$level=0,$campus_id=0) {

		$this->_search_redirect();

		$this->data['grade_level'] = $level;
		$this->data['campus_id'] = $campus_id;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$months = new $this->School_year_months_model;
		$months->setSchoolYear($id, true);
		$months->setOrder('id', 'ASC');
		$months->setLimit(0);
		$this->data['months'] = $months->populate();

		$levels = new $this->School_year_levels_model;
		$levels->setSchoolYear($id, true);
		$levels->setLimit(0);
		$this->data['levels'] = $levels->populate();

		$campuses = new $this->Campuses_model;
		$campuses->setLimit(0);
		$this->data['campuses'] = $campuses->populate();

		$viewfile = 'finance/print_soa_options';

		$this->load->view($viewfile, $this->data);
	}

	public function print_collection_letters_options($id=false,$level=0,$campus_id=0) {

		$this->_search_redirect();

		$this->data['grade_level'] = $level;
		$this->data['campus_id'] = $campus_id;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$levels = new $this->School_year_levels_model;
		$levels->setSchoolYear($id, true);
		$levels->setLimit(0);
		$this->data['levels'] = $levels->populate();

		$campuses = new $this->Campuses_model;
		$campuses->setLimit(0);
		$this->data['campuses'] = $campuses->populate();

		$sym1 = new $this->School_year_months_model;
		$sym1->setLimit(0);
		$this->data['months'] = $sym1->populate();

		$viewfile = 'finance/print_collection_letters_options';

		$this->load->view($viewfile, $this->data);
	}

	public function print_soa_students($id=false,$level=0,$campus_id=0) {

		$this->_search_redirect();

		$this->data['grade_level'] = $level;
		$this->data['campus_id'] = $campus_id;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$sym = new $this->School_year_months_model;
		$sym->setId($this->input->get('month'), true);
		$current_month = $sym->get();

		$enrolled = new $this->Students_enrolled_model('se');
		$enrolled->setJoin('students_information si', 'si.id = se.student_id');
		$enrolled->setSelect('si.*, se.*, se.id as enroll_id');
		$enrolled->setJoin('campuses', 'campuses.id = se.campus_id');
		$enrolled->setSelect('campuses.name as campus_name');
		if( $this->input->get('grade_level') != '' ) {
			$enrolled->setGradeLevel($this->input->get('grade_level'),true);
			$this->data['grade_level'] = $this->input->get('grade_level');
		}
		if( $this->input->get('campus') != '' ) {
			$enrolled->setCampusId($this->input->get('campus'),true);
			$this->data['campus_id'] = $this->input->get('campus');
			$campus = new $this->Campuses_model;
			$campus->setId($this->input->get('campus'), true);
			$this->data['campus'] = $campus->get();
		}
		$enrolled->setSchoolYear($current_school_year, true, false, null, null, 99);
		$enrolled->setOrder('si.lastname', 'ASC');
		$enrolled->setOrder('si.firstname', 'ASC');
		$enrolled->setLimit(0);

		$enrolled->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enrolled->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as whole_year');
		$enrolled->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
		$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=0) as payments');
		$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=1) as downpayments');
		$enrolled->setSelect('(SELECT SUM(amount) FROM students_services ss WHERE ss.enrolled_id=se.id) as services');
		
		$enrollees = $enrolled->populate();
		$this->data['enrollees'] = $enrollees;

		$this->load->view('finance/print_soa_students', $this->data);
	}

	public function print_collection_letters_students($id=false,$level=0,$campus_id=0) {

		$this->_search_redirect();

		$this->data['grade_level'] = $level;
		$this->data['campus_id'] = $campus_id;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$sym = new $this->School_year_months_model;
		$sym->setId($this->input->get('month'), true);
		$current_month = $sym->get();

		$enrolled = new $this->Students_enrolled_model('se');
		$enrolled->setJoin('students_information si', 'si.id = se.student_id');
		$enrolled->setSelect('si.*, se.*, se.id as enroll_id');
		$enrolled->setJoin('campuses', 'campuses.id = se.campus_id');
		$enrolled->setSelect('campuses.name as campus_name');
		if( $this->input->get('grade_level') != '' ) {
			$enrolled->setGradeLevel($this->input->get('grade_level'),true);
			$this->data['grade_level'] = $this->input->get('grade_level');
		}
		if( $this->input->get('campus') != '' ) {
			$enrolled->setCampusId($this->input->get('campus'),true);
			$this->data['campus_id'] = $this->input->get('campus');
			$campus = new $this->Campuses_model;
			$campus->setId($this->input->get('campus'), true);
			$this->data['campus'] = $campus->get();
		}
		$enrolled->setSchoolYear($current_school_year, true, false, null, null, 99);
		$enrolled->setOrder('si.lastname', 'ASC');
		$enrolled->setOrder('si.firstname', 'ASC');
		$enrolled->setLimit(0);

		$enrolled->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enrolled->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as whole_year');
		$enrolled->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
		$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=0) as payments');
		$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=1) as downpayments');
		$enrolled->setSelect('(SELECT SUM(amount) FROM students_services ss WHERE ss.enrolled_id=se.id) as services');

		$enrollees = $enrolled->populate();
		$this->data['enrollees'] = $enrollees;

		$this->load->view('finance/print_collection_letters_students', $this->data);
	}

	public function print_statement($id, $enrolled_id, $selected_month=false) {

			$this->data['preview_first'] = 1;

			$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
			if($current_school_year) {
				$school_year = new $this->School_year_model;
				$school_year->setId($current_school_year,true);
				if( $school_year->nonEmpty() === FALSE ) {
					redirect("school_year");
				}
				$this->data['primary_school_year'] = $school_year->get();
			} 

			$sym1 = new $this->School_year_months_model;
			$sym1->setLimit(0);
			$sym1->setSchoolYear($id,true);
			$this->data['months'] = $sym1->populate();

			$sym = new $this->School_year_months_model;
			$sym->setSchoolYear($id,true);
			$sym->setLimit(0);
			if( $selected_month ) {
				$sym->setId($selected_month, true);
			}

			$selected_months = $sym->populate();
			$this->data['selected_months'] = $selected_months;

			$excluded_months = array();
			$excluded = new $this->Students_month_exclude_model;
			$excluded->setEnrolledId($enrolled_id,true);
			$excluded->setJoin('school_year_months', 'school_year_months.id=students_month_exclude.excluded');
			$excluded->setSelect('students_month_exclude.*, school_year_months.school_year, school_year_months.month, school_year_months.year');
			foreach( $excluded->populate() as $exc) {
				$excluded_months[$exc->month] = $exc;
			}

				$enrollees = array();
			foreach($selected_months as $current_month) {
				if( isset($excluded_months[$current_month->month]) ) {
					continue;
				}
				$enrolled = new $this->Students_enrolled_model('se');
				$enrolled->setId($enrolled_id, true);
				$enrolled->setJoin('students_information si', 'si.id = se.student_id');
				$enrolled->setSelect('si.*, se.*, se.id as enroll_id');
				$enrolled->setJoin('campuses', 'campuses.id = se.campus_id');
				$enrolled->setSelect('campuses.name as campus_name');
				$enrolled->setSelect('(SELECT SUM(amount) FROM `school_year_level_fees` sylf JOIN `school_year_levels` syl ON sylf.sylvl_id=syl.id WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as student_account');
				$enrolled->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.enrolled_id=se.id) as discounts');
				$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=0) as payments');
				$enrolled->setSelect('(SELECT SUM(amount) FROM students_payments sd WHERE sd.enrolled_id=se.id AND sd.down=1) as downpayments');
				$enrolled->setSelect('(SELECT id FROM `school_year_levels` syl WHERE syl.school_year=se.school_year AND syl.level=se.grade_level) as syl_id');
				$enrolled->setSelect('(SELECT SUM(sin.amount) FROM students_invoices sin WHERE sin.month='.$current_month->month.' AND sin.enrolled_id=se.id  AND sin.fee_type=\'SCHOOLFEE\') as amount_due');
				$enrolled->setSelect('(SELECT SUM(so.amount) FROM students_oldaccounts so WHERE so.enrolled_id=se.id) as old_account');
				$enrolled->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=0 AND sp.enrolled_id=se.id) as old_payments');
				$enrolled->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied2 spa JOIN students_payments sp ON spa.payment_id = sp.id JOIN students_invoices si ON si.id = spa.invoice_id WHERE si.fee_id=0 AND sp.enrolled_id=se.id) as old_payments2');
				$enrolled->setSelect('(SELECT label FROM school_year sy WHERE se.school_year=sy.id) as school_year_label');
				//$enrolled->setSchoolYear($current_school_year, true, false, null, null, 99);
				//$enrolled->setOrder('si.lastname', 'ASC');
				//$enrolled->setOrder('si.firstname', 'ASC');
				//$enrolled->setLimit(0);
				$enrollee_data = $enrolled->get();
				$enrollee_data->current_month = $current_month;
				$enrollee_data->balance_month = $current_month;
				$enrollee_data->due_date = $current_month->month . "/25/" . $current_month->year;

				$services = new $this->Students_services_model('ss');
				$services->setEnrolledId($enrolled_id,true);
				$services->setJoin('services s', 's.id = ss.service_id');
				$services->setSelect('ss.*, s.name as service_name');
				$services->setMonth($current_month->month,true);
				$services->setLimit(0);
				$enrollee_data->services = $services->populate();

				$services2 = new $this->Students_services_model('ss');
				$services2->setEnrolledId($enrolled_id,true);
				$services2->setJoin('services s', 's.id = ss.service_id');
				$services2->setSelect('ss.*, s.name as service_name');
				$services2->setLimit(0);
				$enrollee_data->all_services = $services2->populate();

				$enrollees[] = $enrollee_data;
			} 

				$sfgrps = new $this->School_fees_groups_model;
				$sfgrps->setOrder('priority', 'ASC');
				$sfgrps->setActive(1,true);
				$this->data['school_fees_groups'] = $sfgrps->populate();

				foreach( $enrollees as $key => $enrollee) {
					$sfees = new $this->School_year_level_fees_model('sylf');
					$sfees->setSelect('sylf.*');
					$sfees->setLimit(0);
					$sfees->setJoin('school_fees sf', 'sf.id=sylf.fee_id');
					$sfees->setSelect('sf.name, sf.group_id, sf.installment, sf.discount as discountable');
					$sfees->setSelect('(SELECT SUM(amount) FROM students_discounts sd WHERE sd.fee_id=sf.id AND sd.enrolled_id='.$enrollee->enroll_id.') as discounts');
					$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrollee->enroll_id.' AND spa.applied_to="school_fees") as payments');
					$sfees->setSelect('(SELECT SUM(spa.amount) FROM students_payments_applied spa JOIN students_payments sp ON spa.payment_id = sp.id WHERE spa.fee_id=sf.id AND sp.enrolled_id='.$enrollee->enroll_id.' AND sp.down=1 AND spa.applied_to="school_fees") as downpayments');
					$sfees->setSelect('(SELECT SUM(spa2.amount) FROM students_invoices sti JOIN students_payments_applied2 spa2 ON sti.id = spa2.invoice_id WHERE sti.fee_id=sf.id AND sti.enrolled_id='.$enrollee->enroll_id.' AND sti.fee_type=\'SCHOOLFEE\') as payment_applied');
					$sfees->setSylvlId($enrollee->syl_id, true);
					$sfees->setOrder('sf.name', 'ASC');

					$enrollee->school_fees = $sfees->populate(); 
					
					$invoices = new $this->Students_invoices_model;
					$invoices->setEnrolledId($enrollee->enroll_id,true);
					$invoices->setLimit(0);
					$enrollee->monthly_fees = $invoices->populate();

					$invoices2 = new $this->Students_invoices_model;
					$invoices2->setEnrolledId($enrollee->enroll_id,true);
					$invoices2->setMonth($current_month->month,true);
					$invoices2->setLimit(0);
					$enrollee->months_fee = $invoices2->populate();

					$enrollees[$key] = $enrollee;

				}
				
				$this->data['enrollees'] = $enrollees;
				$this->data['enrollees_count'] = $enrolled->count_all_results();
				$viewfile = 'finance/print_soa';
			


		$this->load->view($viewfile, $this->data);
	}

	public function add_promissory_note($id, $enrolled_id) {

		if( $this->input->post() ) {
			$this->form_validation->set_rules('date_filed', 'Date Filed', 'trim|required');
			$this->form_validation->set_rules('month', 'Month', 'trim|required');
			$this->form_validation->set_rules('reason', 'Reason', 'trim|required');
			$this->form_validation->set_rules('promised_date', 'Promised Date', 'trim|required');
			if( $this->form_validation->run()==TRUE) {
				$promise = new $this->Students_promissory_model('spm');
				$promise->setEnrolledId($enrolled_id);
				$promise->setDateFiled( date('Y-m-d', strtotime($this->input->post('date_filed'))));
				$promise->setExamDate( date('Y-m-d', strtotime( $this->input->post('exam_date'))));
				$promise->setMonth($this->input->post('month'));
				$promise->setParentId($this->input->post('parent_id'));
				$promise->setReason($this->input->post('reason'));
				$promise->setPromisedDate( date('Y-m-d', strtotime($this->input->post('promised_date'))));
				$promise->setContact($this->input->post('contact_number'));
				$promise->setSettled(0);
				if( $promise->insert() ) {
					redirect("finance/ledger/{$id}/{$enrolled_id}");
				}
			}
		}

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

			if($current_school_year) {
				$school_year = new $this->School_year_model;
				$school_year->setId($current_school_year,true);
				if( $school_year->nonEmpty() === FALSE ) {
					redirect("school_year");
				}
				$this->data['primary_school_year'] = $school_year->get();
			} 


		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$this->_navbar($enroll_data);

		$months = new $this->School_year_months_model('sym');
		$months->setSchoolYear($id, true);
		$months->setOrder('sym.id', 'ASC');
		$months->setSelect('sym.*');
		$months->setSelect('(SELECT id FROM students_month_exclude sme WHERE sme.excluded=sym.id AND sme.enrolled_id='.$enrolled_id.') as excluded');
		$months->setHaving('excluded IS NULL');
		$months_data = $months->populate();
		$this->data['months'] = $months_data;

		$this->load->model('Students_parents_model');
		$parents = new $this->Students_parents_model;
		$parents->setStudentId( $enroll_data->student_id, true );
		$parents->setOrder( 'id', 'DESC' );
		$parents->setJoin('parents_information', 'parents_information.id = students_parents.parent_id');
		$parents->setSelect('parents_information.*, students_parents.relationship, students_parents.id as sp_id, parents_information.id as parent_id');
		$this->data['parents'] = $parents->populate();

		$this->load->view('finance/add_promissory_note', $this->data);
	}

	public function edit_promissory_note($id, $enrolled_id, $promise_id) {

		$promise = new $this->Students_promissory_model('spm');
		$promise->setId($promise_id, true);
		if( $this->input->post() ) {
			$this->form_validation->set_rules('date_filed', 'Date Filed', 'trim|required');
			$this->form_validation->set_rules('month', 'Month', 'trim|required');
			$this->form_validation->set_rules('reason', 'Reason', 'trim|required');
			$this->form_validation->set_rules('promised_date', 'Promised Date', 'trim|required');
			if( $this->form_validation->run()==TRUE) {
				$promise->setDateFiled( date('Y-m-d', strtotime($this->input->post('date_filed'))));
				$promise->setExamDate( date('Y-m-d', strtotime( $this->input->post('exam_date'))));
				$promise->setMonth($this->input->post('month'));
				$promise->setParentId($this->input->post('parent_id'));
				$promise->setReason($this->input->post('reason'));
				$promise->setPromisedDate( date('Y-m-d', strtotime($this->input->post('promised_date'))));
				$promise->setContact( $this->input->post('contact_number') );
				$promise->setSettled( $this->input->post('settled') );
				$promise->set_exclude(array('id', 'enrolled_id'));
				if( $promise->update() ) {
					redirect("finance/ledger/{$id}/{$enrolled_id}");
				}
			}
		}
		$this->data['promise'] = $promise->get();

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";

			if($current_school_year) {
				$school_year = new $this->School_year_model;
				$school_year->setId($current_school_year,true);
				if( $school_year->nonEmpty() === FALSE ) {
					redirect("school_year");
				}
				$this->data['primary_school_year'] = $school_year->get();
			} 


		$enroll = new $this->Students_enrolled_model('se');
		$enroll->setId($enrolled_id, true);
		$enroll->setJoin('students_information si', 'si.id = se.student_id');
		$enroll->setJoin('school_year_levels', 'school_year_levels.id = se.student_id');
		$enroll->setJoin('campuses', 'campuses.id = se.campus_id');
		$enroll->setSelect('campuses.name as campus_name');
		$enroll->setSelect('si.*, se.*, se.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = se.school_year AND sylvl.level = se.grade_level LIMIT 1) as sylvl_id');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;

		$this->_navbar($enroll_data);

		$months = new $this->School_year_months_model('sym');
		$months->setSchoolYear($id, true);
		$months->setOrder('sym.id', 'ASC');
		$months->setSelect('sym.*');
		$months->setSelect('(SELECT id FROM students_month_exclude sme WHERE sme.excluded=sym.id AND sme.enrolled_id='.$enrolled_id.') as excluded');
		$months->setHaving('excluded IS NULL');
		$months_data = $months->populate();
		$this->data['months'] = $months_data;

		$this->load->model('Students_parents_model');
		$parents = new $this->Students_parents_model;
		$parents->setStudentId( $enroll_data->student_id, true );
		$parents->setOrder( 'id', 'DESC' );
		$parents->setJoin('parents_information', 'parents_information.id = students_parents.parent_id');
		$parents->setSelect('parents_information.*, students_parents.relationship, students_parents.id as sp_id, parents_information.id as parent_id');
		$this->data['parents'] = $parents->populate();

		$this->load->view('finance/edit_promissory_note', $this->data);
	}
	
}
