<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "welcome";
		$this->data['sidebar_menu_sub'] = "welcome";
		$this->load->model(
			array(
				'School_year_model', 
				'School_year_levels_model',
				'Students_enrolled_model',
				'Students_information_model',
				'Students_sessions_model',
				'Campuses_model',
				'User_groups_permissions_model',
				'User_accounts_model',
				'User_accounts_permissions_model'
				)
		);

	}
	
	public function index()
	{
		$session_started = $this->session->userdata('session_started');
		if( $session_started ) {
			redirect('dashboard');	
		} else {
			redirect('welcome/login');
		}
	}

	public function login() {
		switch( K12MS_CONTROL_MODE ) {
			case 'ADMIN':
				$this->_admin_login();
			break;
			case 'STUDENT':
				redirect( K12MS_STUDENT_LOGIN_URL );
			break;
		}
	}

	public function init_student_session($api_key='', $user_id='', $user_email='') {

		if( K12MS_CONTROL_MODE != 'STUDENT' ) {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('redirect'=>K12MS_STUDENT_LOGIN_ERROR_URL, 'error'=>'NOT IN STUDENT MODE')));
			return;
		}

		$results = array();
		$results['user_id'] = FALSE;
		$results['session_id'] = FALSE;
		$results['redirect'] = K12MS_STUDENT_LOGIN_ERROR_URL;

		if( $api_key == '' && $user_id == '') {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('redirect'=>K12MS_STUDENT_LOGIN_URL)));
			return;
		}

		if( $api_key == K12MS_STUDENT_LOGIN_API_KEY ) {

			$session_id = sha1( $user_id . time() );

			$session = new $this->Students_sessions_model;
			$session->setSessionId($session_id);
			$session->setUserId($user_id);
			$session->setUserEmail( urldecode( $user_email) );
			if( $session->insert() ) {
				$results['user_id'] = $user_id;
				$results['session_id'] = $session_id;
				$results['redirect'] = site_url("welcome/start_student_session/{$session_id}");
			}

		}

		$this->output->set_content_type('application/json')->set_output( json_encode( $results ) );

	}

	public function start_student_session($session_id='') {

		if( K12MS_CONTROL_MODE != 'STUDENT' ) {
			die("You're not in Student Mode!");
		}

		if( $session_id == '') {
			redirect( K12MS_STUDENT_LOGIN_URL );
		}

		$session = new $this->Students_sessions_model;
		$session->setSessionId($session_id,true);

		if( $session->nonEmpty() ) {
			$session_data = $session->getResults();

			$student = new $this->Students_information_model;
			$student->setGoogleId($session_data->user_id, true);
			$student->setGoogleUserId($session_data->user_email, true);
			if( $student->nonEmpty() ) {
				$student_info = $student->getResults();
				$this->session->set_userdata('session_started', TRUE);
				$this->session->set_userdata('session_mode', K12MS_CONTROL_MODE);
				$this->session->set_userdata('logged_name', $student_info->lastname . ", " . $student_info->firstname );
				$this->session->set_userdata('logged_uid', $student_info->id );
				$this->session->set_userdata('logged_google_id', $student_info->google_id );
				$session->delete();
				redirect("dashboard", "location");
			} else {
				redirect( K12MS_STUDENT_NOTFOUND_URL );
			}

		} else {
			redirect( K12MS_STUDENT_LOGIN_ERROR_URL );
		}

	}

	private function _admin_login()
	{
		$session_started = $this->session->userdata('session_started');
		if( $session_started ) {
			redirect("welcome");
		}

		if( count($this->input->post()) > 0 ) {
				$username = $this->input->post('username', TRUE);
				$password = $this->input->post('password', TRUE);
				
				$user = new User_accounts_model();
				$user->setUsername( $username , TRUE );
				$user->setPassword( md5( $password ), TRUE );
				$user->setStatus( 1, TRUE );
				$user->setControlMode( K12MS_CONTROL_MODE, TRUE );
				if( $user->nonEmpty() === TRUE ) {
					$result = $user->getResults();
					$this->session->set_userdata('session_started', TRUE);
					$this->session->set_userdata('session_mode', K12MS_CONTROL_MODE);
					$this->session->set_userdata('logged_name', $result->name);
					$this->session->set_userdata('logged_uid', $result->id);
					$this->session->set_userdata('logged_username', $result->username);

					$current_sy = get_ams_config('school', 'current_school_year');
					$this->session->set_userdata('current_school_year', $current_sy);

					$current_syd = new $this->School_year_model;
					$current_syd->setId($current_sy,true);
					$this->session->set_userdata('current_school_year_data', $current_syd->get());

					$permission = new $this->User_groups_permissions_model('gp');
					$permission->setGroupId($result->usergroup, true);
					$permission->setSelect('gp.*');
					$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="view" AND up.user_id='.$result->id.' AND up.module=gp.module) as view_o');
					$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="add" AND up.user_id='.$result->id.' AND up.module=gp.module) as add_o');
					$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="edit" AND up.user_id='.$result->id.' AND up.module=gp.module) as edit_o');
					$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="delete" AND up.user_id='.$result->id.' AND up.module=gp.module) as delete_o');
					$permission->setSelect('(SELECT up.on FROM user_accounts_permissions up WHERE up.action="print" AND up.user_id='.$result->id.' AND up.module=gp.module) as print_o');
					$permission->setSelect('(SELECT IF((view_o IS NOT NULL),view_o,gp.view)) as view_p');
					$permission->setSelect('(SELECT IF((add_o IS NOT NULL),add_o,gp.add)) as add_p');
					$permission->setSelect('(SELECT IF((edit_o IS NOT NULL),edit_o,gp.edit)) as edit_p');
					$permission->setSelect('(SELECT IF((delete_o IS NOT NULL),delete_o,gp.delete)) as delete_p');
					$permission->setSelect('(SELECT IF((print_o IS NOT NULL),print_o,gp.print)) as print_p');
					$permission->setLimit(0);
					
					$user_permissions = array(
						'welcome' => (object) array(
							'view' => 1
							) 
						);

					foreach( $permission->populate() as $perm) {
						$actions = (object) array(
							'view' => $perm->view_p,
							'add' => $perm->add_p,
							'edit' => $perm->edit_p,
							'delete' => $perm->delete_p,
							'print' => $perm->print_p
							);

						$user_permissions[$perm->module] = $actions;
					}

					$upermission = new $this->User_accounts_permissions_model;
					$upermission->setUserId($result->id, true);
					foreach( $upermission->populate() as $uperm) { 
						if( !isset($user_permissions[$uperm->module]) ) {
							$user_permissions[$uperm->module] = (object) array(
								'view' => 0,
								'add' => 0,
								'edit' => 0,
								'delete' => 0,
								'print' => 0,
								);
						}
						$action = $uperm->action;
						$user_permissions[$uperm->module]->$action = $uperm->on;
					}
					
					$this->session->set_userdata('user_permissions', (object) $user_permissions );

					redirect("welcome", "location");
				} else {
					$this->data['error'] = "Unable to Login!";
					//echo md5( $password );
				}
			}
			$this->load->view('welcome/login', $this->data );
	}

	public function change_school_year($sy_id, $uri='dashboard') {
			$current_syd = new $this->School_year_model;
			$current_syd->setId($sy_id,true);
			if( $current_syd->nonEmpty() ) {
				$this->session->set_userdata('current_school_year_data', $current_syd->get());
				$this->session->set_userdata('current_school_year', $sy_id);
			}
			redirect($uri);
	}

	public function change_password()
	{
		$session_started = $this->session->userdata('session_started');
		if( !$session_started ) {
			redirect("welcome/login");
		}

		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('current_password', 'Current Password', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('new_password2', 'Repeat New Password', 'trim|required|min_length[5]');
			if ($this->form_validation->run() == TRUE) {

				$user = new $this->User_accounts_model();
				$user->setId( $this->session->logged_uid , TRUE );
				$user->setPassword( md5( $this->input->post('current_password') ), TRUE );
				if( $user->nonEmpty() && ($this->input->post('new_password') == $this->input->post('new_password2')) ) {
					$user2 = new $this->User_accounts_model;
					$user2->setId( $this->session->logged_uid , TRUE, false );
					$user2->setPassword( md5( $this->input->post('new_password') ), false, true );
					$user2->update();
					redirect(uri_string() . "?success=true");
				}
			}
		}
		
		$this->load->view('welcome/change_password', $this->data );
	}

	public function logout()
	{
		$this->session->sess_destroy();
		switch( K12MS_CONTROL_MODE ) {
			case 'ADMIN':
				redirect(site_url("welcome"), "location");
			break;
			case 'STUDENT':
				redirect( K12MS_STUDENT_LOGOUT_URL );
			break;
		}
	}

	public function demo_student_session($user_id='123456789', $user_email='test@gmail.com') {

			$session_id = sha1( $user_id . time() );
			$session = new $this->Students_sessions_model;
			$session->setSessionId($session_id);
			$session->setUserId($user_id);
			$session->setUserEmail($user_email);
			if( $session->insert() ) {
				redirect( site_url("welcome/start_student_session/{$session_id}") );
			}

	}
}
