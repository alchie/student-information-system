<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_payments extends STUDENT_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "my_payments";
		$this->data['sidebar_menu_sub'] = "my_payments";

		$this->load->model(
			array(
				'School_year_model', 
				'Students_enrolled_model',
				'Students_payments_model',
				'School_year_months_model',
				)
		);
	}
	
	public function index()
	{
		$current_sy = get_ams_config('school', 'current_school_year');
			if( $current_sy ) {
				$school_year = new $this->School_year_model;
				$school_year->setId( $current_sy, true );
				$this->data['primary_school_year'] = $school_year->get();
				
				$enrolled = new $this->Students_enrolled_model;
				$enrolled->setSchoolYear($current_sy, true);
				$enrolled->setStudentId( $this->session->userdata('logged_uid'), true);
				$enrolled_data =  $enrolled->get();
				$this->data['enrolled'] = $enrolled_data;

				if( $enrolled_data ) {
					$payments = new $this->Students_payments_model('sp');
					$payments->setEnrolledId( $enrolled_data->id, true );
					$payments->set_limit(0);
					$payments->set_order('payment_date', 'DESC');
					$this->data['payments'] = $payments->populate();
				}
			}
		$this->load->view('my/my_payments', $this->data);
	}
}
