<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "finance";
		$this->data['sidebar_menu_sub'] = "finance";
		
		$this->_checkPermission('finance', 'view', 'welcome');
		
		$this->load->model('Campuses_model');
		$this->load->model('Services_model');
		$this->load->model('School_year_model');
		$this->load->model('School_year_levels_model');
		$this->load->model('School_year_level_fees_model');
		$this->load->model('School_fees_groups_model');
		$this->load->model('School_year_months_model');
		$this->load->model('Students_invoices_model');
		$this->load->model('Students_payments_model');
		$this->load->model('Students_payments_applied_model');
		$this->load->model('Students_payments_applied2_model');
		$this->load->model('Students_discounts_model');
		$this->load->model('Students_oldaccounts_model');
		$this->load->model('Students_enrolled_model');
		$this->load->model('Students_services_model');
		$this->load->model('Students_month_exclude_model');
		$this->load->model('Students_fees_installment_override_model');

		$this->data['search_enabled'] = false;

		$this->data['current_year'] = get_ams_config('school', 'current_school_year');
	}
	
	public function projected_monthly_collectibles($id) {

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		$grade_levels = new $this->School_year_levels_model;
		$grade_levels->setLimit(0);
		$grade_levels->setSchoolYear($current_school_year, true);
		$levels_data = $grade_levels->populate();
		$this->data['grade_levels'] = $levels_data;
		
		$sym = new $this->School_year_months_model('sym');
		$sym->setLimit(0);
		$sym->setSchoolYear($id,true);
		$sym->setSelect("sym.*");

		// invoices 
		$sym->setSelect("(SELECT SUM(amount) FROM students_invoices si WHERE si.month=sym.month) as total_invoices");
		foreach($levels_data as $level) {
			$sym->setSelect("(SELECT SUM(amount) FROM students_invoices si JOIN students_enrolled se ON si.enrolled_id=se.id WHERE si.month=sym.month AND se.grade_level='{$level->level}') as invoices_" . $level->level);

			$sym->setSelect("(SELECT SUM(amount) FROM students_payments sp JOIN students_enrolled se ON sp.enrolled_id=se.id WHERE se.grade_level='{$level->level}') payments_" . $level->level);

			$sym->setSelect("(SELECT SUM(spa2.amount) FROM students_invoices si JOIN students_enrolled se ON si.enrolled_id=se.id JOIN students_payments_applied2 spa2 ON spa2.invoice_id=si.id WHERE si.month=sym.month AND se.grade_level='{$level->level}') as applied_" . $level->level);
		}

		$this->data['months'] = $sym->populate();

		$this->load->view('reports/projected_monthly_collectibles', $this->data);
	}

	public function projected_monthly_collectibles_details($id,$level=0,$month=0) {

		$this->data['grade_level'] = $level;
		$this->data['campus_id'] = 0;
		$this->data['month'] = $month;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
			$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		} 

		$enrolled = new $this->Students_enrolled_model('se');
		$enrolled->setJoin('students_information si', 'si.id = se.student_id');
		$enrolled->setSelect('si.*, se.*, se.id as enroll_id');
		$enrolled->setJoin('campuses', 'campuses.id = se.campus_id');
		$enrolled->setSelect('campuses.name as campus_name');
		$enrolled->setSelect("(SELECT SUM(si.amount) FROM students_invoices si WHERE si.month=".$month." AND si.enrolled_id=se.id) as total_invoices");
		$enrolled->setSelect("(SELECT SUM(spa.amount) FROM students_payments_applied2 spa JOIN students_invoices si ON si.id=spa.invoice_id WHERE si.month=".$month." AND si.enrolled_id=se.id) as total_paid");
		$enrolled->setSchoolYear($current_school_year, true, false, null, null, 99);
		$enrolled->setGradeLevel($level,true, false, null, null, 100);
		$enrolled->setLimit(0);
		
		$this->data['enrolled'] = $enrolled->populate();

		$this->load->view('reports/projected_monthly_collectibles_details', $this->data);
	}

	public function payments($id=false,$level=0,$campus_id=0,$start=0) {

		$this->data['grade_level'] = $level;
		$this->data['campus_id'] = $campus_id;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
			$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		} 

		$sp = new $this->Students_payments_model('sp');
		$sp->setStart($start);
		$sp->setSelect("sp.*, sp.id as payment_id");
		$sp->setJoin('students_enrolled se', 'se.id=sp.enrolled_id');
		$sp->setSelect("se.*");
		$sp->setJoin('students_information si', 'si.id=se.student_id');
		$sp->setSelect("si.*");
		$sp->setJoin('campuses', 'campuses.id = se.campus_id');
		$sp->setSelect('campuses.name as campus_name');
		//$sp->setDown(0,true);
		$sp->setSelect("(IF(sp.down=1,(SELECT SUM(spa.amount) FROM students_payments_applied spa WHERE spa.payment_id=sp.id),(SELECT SUM(spa2.amount) FROM students_payments_applied2 spa2 WHERE spa2.payment_id=sp.id))) as applied_amount");

		//$sp->setOrder('applied_amount', 'ASC');
		$sp->setOrder('sp.payment_date', 'ASC');

		if( $level ) {
			$sp->setWhere('se.grade_level =', $level);
		}
		if( $campus_id ) {
			$sp->setWhere('se.campus_id =', $campus_id);

			$campus = new $this->Campuses_model;
			$campus->setId($campus_id, true);
			$this->data['campus'] = $campus->get();
		}

		$this->data['pagination'] = bootstrap_pagination( array(
			'base_url' => site_url( sprintf("reports/payments/%s/%s/%s", $current_school_year,$level, $campus_id) ),
			'total_rows' => $sp->count_all_results(),
			'per_page' => $sp->getLimit()
			) );

		$this->data['payments'] = $sp->populate();

		$campuses = new $this->Campuses_model;
		$campuses->setLimit(0);
		$this->data['campuses'] = $campuses->populate();

		$this->load->model('School_year_levels_model');
		$grade_levels = new $this->School_year_levels_model;
		$grade_levels->setLimit(0);
		$grade_levels->setSchoolYear($current_school_year, true);
		$this->data['grade_levels'] = $grade_levels->populate();

		$this->load->view('reports/payments', $this->data);

	}

	public function open_payments($id=false,$level=0,$campus_id=0,$start=0) {

		$this->data['grade_level'] = $level;
		$this->data['campus_id'] = $campus_id;

		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
			$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		} 

		$sp = new $this->Students_payments_model('sp');
		$sp->setStart($start);
		$sp->setSelect("sp.*, sp.id as payment_id");
		$sp->setJoin('students_enrolled se', 'se.id=sp.enrolled_id');
		$sp->setSelect("se.*");
		$sp->setJoin('students_information si', 'si.id=se.student_id');
		$sp->setSelect("si.*");
		$sp->setJoin('campuses', 'campuses.id = se.campus_id');
		$sp->setSelect('campuses.name as campus_name');
		//$sp->setDown(0,true);
		//$sp->setSelect("(IF(sp.down=1,(SELECT SUM(spa.amount) FROM students_payments_applied spa WHERE spa.payment_id=sp.id),(SELECT SUM(spa2.amount) FROM students_payments_applied2 spa2 WHERE spa2.payment_id=sp.id))) as applied_amount");

		//$sp->setOrder('applied_amount', 'ASC');
		$sp->setOrder('sp.payment_date', 'ASC');
		
		$sp->setJoin('students_payments_applied stpa', 'stpa.payment_id=sp.id');
		$sp->setJoin('students_payments_applied2 stpa2', 'stpa2.payment_id=sp.id');
		$sp->setWhere('stpa.fee_id IS NULL');
		$sp->setWhere('stpa2.invoice_id IS NULL');

		if( $level ) {
			$sp->setWhere('se.grade_level =', $level);
		}
		if( $campus_id ) {
			$sp->setWhere('se.campus_id =', $campus_id);

			$campus = new $this->Campuses_model;
			$campus->setId($campus_id, true);
			$this->data['campus'] = $campus->get();
		}

		$this->data['payments'] = $sp->populate();


		$this->data['pagination'] = bootstrap_pagination( array(
			'base_url' => site_url( sprintf("reports/open_payments/%s/%s/%s", $current_school_year,$level, $campus_id) ),
			'total_rows' => $sp->count_all_results(),
			'per_page' => $sp->getLimit()
			) );

		

		$campuses = new $this->Campuses_model;
		$campuses->setLimit(0);
		$this->data['campuses'] = $campuses->populate();

		$this->load->model('School_year_levels_model');
		$grade_levels = new $this->School_year_levels_model;
		$grade_levels->setLimit(0);
		$grade_levels->setSchoolYear($current_school_year, true);
		$this->data['grade_levels'] = $grade_levels->populate();

		$this->load->view('reports/open_payments', $this->data);

	}

	public function unsettled_promissory_notes($id=false) {
		$current_school_year = ($id) ? $id :  get_ams_config('school', 'current_school_year');
		
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("school_year");
			}
			$this->data['primary_school_year'] = $school_year->get();
			$this->data['sidebar_menu_sub'] = "finance/index/{$current_school_year}";
		} 

		$this->load->view('reports/unsettled_promissory_notes', $this->data);
	}
	
}
