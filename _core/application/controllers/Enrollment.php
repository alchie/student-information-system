<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enrollment extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "enrollment";
		$this->data['sidebar_menu_sub'] = "enrollment";
		
		$this->_checkPermission('enrollment', 'view', 'welcome');

		$this->load->model(array('School_year_model', 'Students_enrolled_model', 'Campuses_model'));
		$this->load->model('Students_information_model');

		$this->data['search_enabled'] = true;

		$this->data['current_year'] = ($this->session->current_school_year) ? $this->session->current_school_year : get_ams_config('school', 'current_school_year');

		$this->load_school_years('enrollment');
	}
	
	private function _list($id=false,$level_id=0,$campus_id=0,$start=0, $limit=20)
	{
		$this->data['grade_level'] = $level_id;
		$this->data['campus_id'] = $campus_id;

		$current_school_year = ($id) ? $id : get_ams_config('school', 'current_school_year');
		$this->data['current_year'] = $current_school_year;

		$this->data['sidebar_menu_sub'] = "enrollment/index/{$current_school_year}";

		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("welcome");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 
		
		$enrolled = new $this->Students_enrolled_model;
		$enrolled->setJoin('students_information', 'students_information.id = students_enrolled.student_id');
		$enrolled->setSelect('students_information.*, students_enrolled.*, students_enrolled.id as enroll_id');
		$enrolled->setJoin('campuses', 'campuses.id = students_enrolled.campus_id');
		$enrolled->setSelect('campuses.name as campus_name');

		$enrolled->setJoin('sections', 'sections.id=students_enrolled.section_id');
		$enrolled->setSelect('sections.name as section_name');
		if( $this->input->get("q") ) {
			$enrolled->setWhere( "students_information.idn LIKE '%". $this->input->get("q") ."%'" );
			$enrolled->setWhereOr( "students_information.lastname LIKE '%". $this->input->get("q") ."%'" );
			$enrolled->setWhereOr( "students_information.firstname LIKE '%". $this->input->get("q") ."%'" );
			$enrolled->setWhereOr( "students_information.middlename LIKE '%". $this->input->get("q") ."%'" );
		}

		if( $level_id ) {
			$enrolled->setGradeLevel($level_id,true);
		}

		if( $campus_id ) {
			$enrolled->setCampusId($campus_id,true);
			$campus = new $this->Campuses_model;
			$campus->setId($campus_id, true);
			$this->data['campus'] = $campus->get();
		}

		$enrolled->setSchoolYear($current_school_year, true, false, null, null, 99);
		//$enrolled->setOrder('students_information.lastname', 'ASC');
		//$enrolled->setOrder('students_information.firstname', 'ASC');
		//$enrolled->setOrder('students_information.middlename', 'ASC');
		$enrolled->setStart( $start );
		$enrolled->setLimit($limit);

		if( $this->input->get('sort_by') ) {
			$enrolled->setOrder($this->input->get('sort_by'), ($this->input->get('sort_order')?$this->input->get('sort_order'):'ASC'));
		}

		if( $this->input->get('filter') ) {
			$filters = $this->input->get('filter');
			if( isset($filters['status']) ) {
				$enrolled->setWhereIn( 'status', $filters['status'] );
			}
		}

		$this->data['enrollees'] = $enrolled->populate();
		$this->data['enrollees_count'] = $enrolled->count_all_results();
		
		$config['base_url'] = site_url(sprintf("enrollment/index/%s/%s/%s", $current_school_year,$level_id,$campus_id) );
		$config['total_rows'] = $enrolled->count_all_results();
		$config['per_page'] = $enrolled->getLimit();

		$this->data['pagination'] = bootstrap_pagination( $config );

		return $enrolled->count_all_results();
		
	}

	public function index($id=false,$level_id=0,$campus_id=0,$start=0)
	{
		$current_sy = ($id) ? $id : $this->session->current_school_year;
		$results = $this->_list($current_sy,$level_id,$campus_id,$start);

		$campuses = new $this->Campuses_model;
		$campuses->setLimit(0);
		$this->data['campuses'] = $campuses->populate();

		$this->load->model('School_year_levels_model');
		$grade_levels = new $this->School_year_levels_model;
		$grade_levels->setLimit(0);
		$grade_levels->setSchoolYear($this->data['current_year'], true);
		$this->data['grade_levels'] = $grade_levels->populate();

		$this->load->view('enrollment/enrollment', $this->data);
	}

	public function pr1nt($id=false,$level_id=0,$campus_id=0)
	{
		$results = $this->_list($id,$level_id,$campus_id, 0, 0);

		$campuses = new $this->Campuses_model;
		$campuses->setLimit(0);
		$campuses->setId($campus_id,true);
		$this->data['campus'] = $campuses->get();

		$this->load->model('School_year_levels_model');
		$grade_levels = new $this->School_year_levels_model;
		$grade_levels->setLimit(0);
		$grade_levels->setSchoolYear($this->data['current_year'], true);
		$grade_levels->setLevel($level_id,true);


		$this->data['grade_level'] = $grade_levels->get();

		$this->data['options'] = $this->input->get('option');

		$this->load->view('enrollment/print', $this->data);
	}
		
	public function enroll($id=false)
	{
		$current_school_year = ($id) ? $id : get_ams_config('school', 'current_school_year');
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('student_id', 'Student', 'trim|required');
			$this->form_validation->set_rules('grade_level', 'Grade Level', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$enroll = new $this->Students_enrolled_model;
				$enroll->setSchoolYear($current_school_year, true);
				$enroll->setStudentId($this->input->post('student_id'), true);
				$enroll->setCampusId($this->input->post('campus_id'));
				$enroll->setGradeLevel($this->input->post('grade_level'));
				$date_enrolled = ($this->input->post('date_enrolled')) ? date('Y-m-d', strtotime($this->input->post('date_enrolled'))) : date('Y-m-d');
				$enroll->setDateEnrolled( $date_enrolled );
				$enroll->setStatus('enrolled');
				$enroll->setLastmod( date('Y-m-d H:i:s') );

				$this->session->set_userdata('last_campus_id', $this->input->post('campus_id'));
				$this->session->set_userdata('last_grade_level', $this->input->post('grade_level'));

				if( $enroll->nonEmpty() === FALSE ) {
					$enroll->insert();
					redirect("enrollment/update/" . $enroll->getId());
				} else {
					$results = $enroll->getResults();
					$enroll->setExclude('id');
					$enroll->update();
					redirect("enrollment/update/" . $results->id);
				}
				
			}
		}
		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("enrollment");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 
		
		$search_results = false;
		if( $this->input->get("q") && (strlen( $this->input->get("q") ) >= 3) ) {
			$this->load->model('Students_information_model');
			$student = new $this->Students_information_model;
			$student->setLimit(10);
			$student->setWhere( 'idn != ""');
			$student->setWhere( "(idn LIKE '%". $this->input->get("q") ."%'" );
			$student->setWhereOr( "lastname LIKE '%". $this->input->get("q") ."%'" );
			$student->setWhereOr( "firstname LIKE '%". $this->input->get("q") ."%'" );
			$student->setWhereOr( "middlename LIKE '%". $this->input->get("q") ."%')" );
			$student->setSelect('students_information.*');
			$student->setSelect('(SELECT grade_level FROM students_enrolled s WHERE s.student_id=students_information.id AND s.school_year='.$current_school_year.') as grade_level');

			$search_results = $student->populate();
			
			$this->load->model('School_year_levels_model');
			$grade_levels = new $this->School_year_levels_model;
			$grade_levels->setLimit(0);
			$grade_levels->setSchoolYear($current_school_year, true);
			$this->data['grade_levels'] = $grade_levels->populate();

			$this->load->model('Campuses_model');
			$campuses = new $this->Campuses_model;
			$campuses->setLimit(0);
			$this->data['campuses'] = $campuses->populate();
		}
		$this->data['search_results'] = $search_results;
		
		$this->load->view('enrollment/enroll_students', $this->data);
	}
	
	public function update($id)
	{
		if( $this->input->get("q") ) {
			header("location: " . site_url("enrollment/enroll") . "?q=" . $this->input->get("q") );
			exit;
		}
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('grade_level', 'Grade Level', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$enroll = new $this->Students_enrolled_model;
				$enroll->setId($id, true, false);
				$enroll->setCampusId($this->input->post('campus_id'));
				$enroll->setGradeLevel($this->input->post('grade_level'));
				$enroll->setSectionId($this->input->post('section_id'));
				$date_enrolled = ($this->input->post('date_enrolled')) ? date('Y-m-d', strtotime($this->input->post('date_enrolled'))) : date('Y-m-d');
				$enroll->setDateEnrolled( $date_enrolled );
				$enroll->setLastmod( date('Y-m-d H:i:s') );
				$enroll->setExclude(array('student_id','school_year'));
				$enroll->setStatus($this->input->post('status'));
				$enroll->update();
			}
		}
		
		$enroll = new $this->Students_enrolled_model;
		$enroll->setId($id, true);
		$enroll->setJoin('students_information', 'students_information.id = students_enrolled.student_id');
		$enroll->setJoin('school_year_levels', 'students_information.id = students_enrolled.student_id');
		$enroll->setSelect('students_information.*, students_enrolled.*, students_enrolled.id as enroll_id');
		$enroll->setSelect('(SELECT `id` FROM `school_year_levels` sylvl WHERE sylvl.school_year = students_enrolled.school_year AND sylvl.level = students_enrolled.grade_level LIMIT 1) as sylvl_id');
		$enroll_data = $enroll->get();
		$this->data['enroll_data'] = $enroll_data;
		
		$this->data['current_year'] = $enroll_data->school_year;

		$school_year = new $this->School_year_model;
		$school_year->setId($enroll_data->school_year,true);
		$this->data['primary_school_year'] = $school_year->get();
		
		$this->load->model('School_year_levels_model');
		$grade_levels = new $this->School_year_levels_model;
		$grade_levels->setLimit(0);
		$grade_levels->setSchoolYear($enroll_data->school_year, true);
		$this->data['grade_levels'] = $grade_levels->populate();
		
		$this->load->model('School_year_level_sections_model');
		$sections = new $this->School_year_level_sections_model;
		$sections->setSylvlId($enroll_data->sylvl_id,true);
		$sections->setJoin('sections', 'sections.id=school_year_level_sections.section_id');
		$sections->setSelect('sections.*');
		$this->data['sections'] = $sections->populate();

		$this->load->model('Campuses_model');
		$campuses = new $this->Campuses_model;
		$campuses->setLimit(0);
		$this->data['campuses'] = $campuses->populate();

		$this->load->view('enrollment/enroll_update', $this->data);
	}
	
	public function delete($id) {
		$enroll = new $this->Students_enrolled_model;
		$enroll->setId($id, true);
		$enroll->delete();
		redirect("enrollment");
	}

	public function automation($id=false) {

		$current_school_year = ($id) ? $id : get_ams_config('school', 'current_school_year');
		$this->data['current_year'] = $current_school_year;

		if($current_school_year) {
			$school_year = new $this->School_year_model;
			$school_year->setId($current_school_year,true);
			if( $school_year->nonEmpty() === FALSE ) {
				redirect("welcome");
			}
			$this->data['primary_school_year'] = $school_year->get();
		} 

		if( $this->input->post('action') ) {
			switch( $this->input->post('action') ) {
				case 'autofill_google_userid':
					$enrolled = new $this->Students_enrolled_model;
					$enrolled->setJoin('students_information', 'students_information.id = students_enrolled.student_id');
					$enrolled->setSelect('students_information.*, students_enrolled.*, students_enrolled.id as enroll_id');

					$enrolled->setSchoolYear($current_school_year, true, false, null, null, 99);
					$enrolled->setOrder('students_information.lastname', 'ASC');
					$enrolled->setOrder('students_information.firstname', 'ASC');
					$enrolled->setOrder('students_information.middlename', 'ASC');
					$enrolled->setLimit(0);
					//$enrolled->setWhere('students_information.google_userid IS NULL');

					$guid_arr = array();
					foreach( $enrolled->populate() as $enrollee ) {
						
						if( $enrollee->google_userid == '' ) {

							$guid = ams_slugify(url_title($enrollee->lastname . " " .ams_initials($enrollee->firstname)), "_", true);

							$userid_check = true;
							$n = 2;
							$guid2 = $guid;
							while( $userid_check ) {

								if( in_array($guid2, $guid_arr)) {
									$guid2 = $guid . $n;
									$n++;
									continue;
								}

								$student = new $this->Students_information_model;
								$student->setWhere("google_userid LIKE", $guid2 . "@slcd.edu.ph");
								if( $student->nonEmpty() ) {
									$guid2 = $guid . $n;
									$n++;
									continue;
								} else {
									$userid_check = false;
									$guid_arr[] = $guid2;
									break;
								}
								
							}
							
							$student2 = new $this->Students_information_model;
							$student2->setId($enrollee->student_id, true);
							$student2->setGoogleUserid($guid2 . "@slcd.edu.ph", false, true);
							$student2->update();
							
						}
						
					}				
			}
			redirect( uri_string() );
		}
		$this->load->view('enrollment/automation', $this->data);
	}
}
