<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		
		$this->data['sidebar_menu_main'] = "administration";
		$this->data['sidebar_menu_sub'] = "administration";

		$this->_checkPermission('administration', 'view', 'welcome');

		$this->load->model(
			array(
				'School_year_model', 
				'School_year_levels_model',
				'Students_enrolled_model'
				)
		);

		$this->_checkPermission('administration', 'view', 'welcome');
		
	}
	
	public function index()
	{
		$this->load->view('administration/administration', $this->data);
	}

}
