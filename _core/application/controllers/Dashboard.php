<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "dashboard";
		$this->data['sidebar_menu_sub'] = "dashboard";
		$this->load->model(
			array(
				'School_year_model', 
				'School_year_levels_model',
				'Students_enrolled_model',
				'Students_payments_model',
				'Campuses_model',
				'User_groups_permissions_model',
				'User_accounts_model',
				'School_year_months_model',
				)
		);

		$this->load_school_years();
	}

	protected function _search_redirect() {
		if( $this->input->get("q") ) {
			header("location: " . site_url("students") . "?q=" . $this->input->get("q") );
			exit;
		}
	}

	public function index()
	{

		$this->_search_redirect();

		switch (K12MS_CONTROL_MODE) {
			case 'ADMIN':
				$this->_admin_dashboard();
				break;
			case 'STUDENT':
				$this->_student_dashboard();
				break;
			default:
				die("Control Mode not set");
				break;
		}
	}

	private function _admin_dashboard() {
			$current_sy = ($this->session->current_school_year) ? $this->session->current_school_year : get_ams_config('school', 'current_school_year');
			if( $current_sy ) {
				$school_year = new $this->School_year_model;
				$school_year->setId( $current_sy, true );
				$this->data['primary_school_year'] = $school_year->get();
				
				$grade_levels = new $this->School_year_levels_model;
				$grade_levels->setSchoolYear($current_sy,true);
				$grade_levels->setLimit(0);
				$grade_levels->setSelect("school_year_levels.*");
				$grade_levels->setSelect("(SELECT COUNT(*) FROM students_enrolled e WHERE e.grade_level=school_year_levels.level AND e.school_year=".$current_sy.") as total_enrollees");

				$campuses = new $this->Campuses_model();
				$campuses->setOrder('name', 'ASC');
				$this->data['campuses'] = $campuses->populate();

				foreach( $campuses->populate() as $campus ) {
					$grade_levels->setSelect("(SELECT COUNT(*) FROM students_enrolled e WHERE e.grade_level=school_year_levels.level AND e.school_year={$current_sy} AND e.campus_id={$campus->id}) as total_enrollees_{$campus->id}");
				}
				
				$this->data['grade_levels'] = $grade_levels->populate();
			}
			
			$this->load->view('dashboard/admin_dashboard', $this->data);
	}

	private function _student_dashboard() {
			$current_sy = get_ams_config('school', 'current_school_year');
			if( $current_sy ) {
				$school_year = new $this->School_year_model;
				$school_year->setId( $current_sy, true );
				$this->data['primary_school_year'] = $school_year->get();
				
				$enrolled = new $this->Students_enrolled_model;
				$enrolled->setSchoolYear($current_sy, true);
				$enrolled->setStudentId( $this->session->userdata('logged_uid'), true);
				$enrolled_data =  $enrolled->get();
				$this->data['enrolled'] = $enrolled_data;

				if( $enrolled_data ) {
					$sy_months = new $this->School_year_months_model('sym');
					$sy_months->set_select("sym.*");
					$sy_months->set_select("(SELECT SUM(si.amount) FROM students_invoices si WHERE si.enrolled_id={$enrolled_data->id} AND si.month=sym.month) as invoice_amount");
					$sy_months->setSchoolYear($current_sy, true);
					$sy_months->set_order('year', 'ASC');
					$sy_months->set_order('month', 'ASC');
					$sy_months->set_limit(0);
					$this->data['sy_months'] = $sy_months->populate();

					$payments = new $this->Students_payments_model('sp');
					$payments->setEnrolledId( $enrolled_data->id, true );
					$payments->set_limit(0);
					$payments->set_order('payment_date', 'DESC');
					$this->data['payments'] = $payments->populate();
				}
			}
			$this->load->view('dashboard/student_dashboard', $this->data);
	}

}
