<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends ADMIN_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "administration";
		$this->data['sidebar_menu_sub'] = "backup";

		$this->_checkPermission('administration', 'view', 'welcome');
		$this->_checkPermission('backup', 'view', 'welcome');

	}
	
	public function index()
	{
		$dir    = 'backups';
		$files = array_diff(scandir($dir), array('..', '.', '.htaccess', 'index.html'));
		$this->data['backup_files'] = $files;
		
		$this->load->view('administration/backup', $this->data);
	}

	public function download($file)
	{
		$file_dir = "backups/" . $file;
		if (file_exists($file_dir)) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($file_dir).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file_dir));
		    readfile($file_dir);
		    exit;
		}
	}

	public function delete($file)
	{
		$file_dir = "backups/" . $file;
		if (file_exists($file_dir)) {
			unlink($file_dir);
		    redirect("backup");
		    exit;
		}
	}
}
