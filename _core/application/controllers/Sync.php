<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sync extends ADMIN_Controller {

	var $data = array();

	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "administration";
		$this->data['sidebar_menu_sub'] = "sync";

		if( !file_exists( "sync.php" ) ) {
			die('Sync not Available...');
		}
	}
	
	public function index() {
		$this->load->view('administration/sync', $this->data);
	}

}
