<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sections extends ADMIN_Controller {
	
	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "school";
		$this->data['sidebar_menu_sub'] = "sections";
		
		$this->_checkPermission('school', 'view', 'welcome');
		$this->_checkPermission('sections', 'view', 'welcome');
		
		$this->load->model('Sections_model');
	}
	
	public function index($start=0)
	{
		$sections = new $this->Sections_model();
		$sections->setStart($start);
		$this->data['sections'] = $sections->populate();
		$config['base_url'] = site_url("sections/index");
		$config['total_rows'] = $sections->count_all_results();
		$config['per_page'] = $sections->getLimit();
		$this->data['pagination'] = bootstrap_pagination($config);
		
		$this->load->view('school/sections', $this->data);
	}
	
	public function add() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[5]|is_unique[sections.name]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$name = $this->input->post("name", true);
				$status = $this->input->post("active", true);

				if( $status == "") {
					$status = 0;
				}
				
				$section = new $this->Sections_model();
				$section->setName( $name );
				$section->setActive( $status );

				if( $section->insert() ) {
					redirect("sections/update/" . $section->getId(), "location");
				}
			}
		}
		
		$this->load->view('school/sections_add', $this->data);
	}
	
	public function update($id=false) {
		if( !$id ) {
			redirect("sections" , "location");
		}
		$section = new $this->Sections_model();
		$section->setId( $id, true );
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[5]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$name = $this->input->post("name", true);
				$status = $this->input->post("active", true);

				if( $status == "") {
					$status = 0;
				}
				
				$section->setName( $name );
				$section->setActive( $status );

				$section->update();
				redirect("sections/update/" . $section->getId(), "location");
				
			}
		}
		$this->data['section'] = $section->get();
		
		$this->load->view('school/sections_update', $this->data);
	}
	
	public function delete($id) {
		
		$section = new $this->Sections_model();
		$section->setId( $id, true );
		$section->delete();
		redirect("sections" , "location");
			
	}
	
}
