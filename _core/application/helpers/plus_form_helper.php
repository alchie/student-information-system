<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('plus_form'))
{
	function plus_form($type="text", $title="", $name="", $form=array(), $default="", $description="")
	{
		$html = "";
		$input = "";
		$form = array_merge(array("attributes"=>array(), "options"=>array()), $form );
		$attributes = array_merge(array("class"=>"form-control col-md-7 col-xs-12"), $form['attributes']);
			switch($type) {
				case "datepicker":
					$id = $name . "-date-picker";
					$attributes = array_merge($attributes, array("class"=>"datepicker form-control col-md-7 col-xs-12"));
					$input = '<input type="text" name="'.$name.'" '._stringify_attributes($attributes).' value="'.$default.'">';
				break;
				case "password":
					$input = '<input type="password" name="'.$name.'" '._stringify_attributes($attributes).' value="'.$default.'">';
				break;
				case "select_single":
					$input = '<select name="'.$name.'" '._stringify_attributes($attributes).'>';
						$input .= "<option value=\"\">- - Select a {$title} - -</option>";
						foreach($form['options'] as $key=>$value) {
							if( $default==$key ) {
								$input .= "<option value=\"{$key}\" selected=\"selected\">{$value}</option>";
							} else {
								$input .= "<option value=\"{$key}\">{$value}</option>";
							}
						}
					$input .= "</select>";
				break;
				case "checkbox":
					$input = '<div class="checkbox">';
						foreach($form['options'] as $key=>$value) {
							if( $default==$key ) {
								$input .= "<label><input type=\"checkbox\" class=\"flat\" checked=\"checked\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							} else {
								$input .= "<label><input type=\"checkbox\" class=\"flat\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							}
						}
					$input .= "</div>";
				break;
				case "radio":
				case "radio_single":
					$input = '<div class="radio">';
						foreach($form['options'] as $key=>$value) {
							if( $default==$key ) {
								$input .= "<label><input type=\"radio\" class=\"flat\" checked=\"checked\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							} else {
								$input .= "<label><input type=\"radio\" class=\"flat\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							}
						}
					$input .= "</div>";
				break;
				case "radio_multiline":
						$input = '';
						foreach($form['options'] as $key=>$value) {
							$input .= '<div class="radio">';
							if( $default==$key ) {
								$input .= "<label><input type=\"radio\" class=\"flat\" checked=\"checked\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							} else {
								$input .= "<label><input type=\"radio\" class=\"flat\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							}
							$input .= "</div>";
						}
					
				break;
				case "textarea":
					$input = '<textarea type="text" name="'.$name.'" '._stringify_attributes($attributes).'>'.$default.'</textarea>';
				break;
				default:
				case "text":
					$input = '<input type="text" name="'.$name.'" '._stringify_attributes($attributes).' value="'.$default.'">';
				break;

			}
$html = <<<HTML
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{$title}</label>
  <div class="col-md-8 col-sm-8 col-xs-12">{$input}
  	<div class="description small">{$description}</div>
  </div>
</div>
HTML;
		return $html;
	}
}

// ------------------------------------------------------------------------

