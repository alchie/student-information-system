<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('bootstrap_pagination'))
{
	function bootstrap_pagination($config=array())
	{
		$defaults['base_url'] = base_url();
		$defaults['total_rows'] = 0;
		$defaults['per_page'] = 0;
		$defaults['full_tag_open'] = '<div class="btn-group btn-group-sm">';
		$defaults['full_tag_close'] = '</div>';

		$defaults['cur_tag_open'] = '<div class="btn btn-default active"><strong>';
		$defaults['cur_tag_close'] = '</strong></div>';
		
		$config['attributes'] = array('class' => 'btn btn-default');
		
		$config2 = array_merge($defaults, $config);
		get_instance()->pagination->initialize($config2);
		return get_instance()->pagination->create_links();
	}
}

// ------------------------------------------------------------------------

