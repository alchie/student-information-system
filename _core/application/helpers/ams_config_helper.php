<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('add_ams_config'))
{
	function add_ams_config($module, $key, $value="", $active="1")
	{
		$load =& load_class('Loader');
		$load->model('Ams_config_model');
		$config = new Ams_config_model;
		$config->setModule( $module );
		$config->setKey( $key );
		$config->setValue( $value );
		$config->setActive( $active );
		if( $config->insert() ) {
			return true;
		} else {
			return false;
		}
	}
}

if ( ! function_exists('get_ams_config'))
{
	function get_ams_config($module, $key)
	{
		$load =& load_class('Loader');
		$load->model('Ams_config_model');
		$config = new Ams_config_model;
		$config->setModule( $module, true );
		$config->setKey( $key, true );
		$result = $config->get();
		
		return ($result) ? $result->value : $result;
	}
}

if ( ! function_exists('update_ams_config'))
{
	function update_ams_config($module, $key, $value="", $active="1")
	{
		$load =& load_class('Loader');
		$load->model('Ams_config_model');
		$config = new Ams_config_model;
		$config->setModule( $module, true );
		$config->setKey( $key, true );
		$config->setValue( $value );
		$config->setActive( $active );
		if( $config->nonEmpty() === FALSE ) {
			if( $config->insert() ) {
				return true;
			} else {
				return false;
			}
		} else {
			$results = $config->getResults();
			$config->setId($results->id);
			if( $config->update() ) {
				return true;
			} else {
				return false;
			}
		}
	}
}

// ------------------------------------------------------------------------

