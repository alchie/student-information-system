<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('ams_referrer'))
{
	function ams_referrer($in_case='welcome', $current='')
	{
		$load =& load_class('Loader');
		$referrer = $load->agent->referrer();
		$url = site_url($in_case);
		if( ($referrer!='') && ($referrer != site_url($current)) ) {
			$url = $referrer;
		} 
		return $url;
	}
}

// ------------------------------------------------------------------------
if ( ! function_exists('ams_initials'))
{
	function ams_initials($words)
	{
		$words = url_title($words, "_", true);
		$words = explode("_", $words);
		$acronym = "";

		foreach ($words as $w) {
			if( isset($w[0])) {
		  		$acronym .= $w[0];
			}
		}
		return $acronym;
	}
}

if ( ! function_exists('ams_slugify'))
{
	function ams_slugify($text)
	{

		$table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-'
    	);

	  // replace non letter or digits by -
	  $text = preg_replace('~[^\pL\d]+~u', '_', $text);

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  // trim
	  $text = trim($text, '_');

	  // remove duplicate -
	  $text = preg_replace('~-+~', '_', $text);

	  // lowercase
	  $text = strtolower(strtr($text, $table));

	  return $text;
	}
}


if( ! function_exists('hasAccess') ) {
	function hasAccess($module, $action='view') {
		$CI = get_instance();
		$auth = false;
		

    	if( isset( $CI->session->userdata['user_permissions'] ) ) {
			$user_permissions = $CI->session->userdata['user_permissions'];
            if( isset($user_permissions->$module ) ) {
                if( isset( $user_permissions->$module->$action ) ) {
                	$auth = $user_permissions->$module->$action;
            	} 
            } 
        }
    	return $auth;
	}
}
// ------------------------------------------------------------------------

