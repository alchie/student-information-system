<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('gentelella_form1'))
{
	function gentelella_form1($type="text", $title="", $name="", $form=array(), $default="")
	{
		$html = "";
		$input = "";
		$form = array_merge(array("attributes"=>array(), "options"=>array()), $form );
		$attributes = array_merge(array("class"=>"form-control col-md-7 col-xs-12"), $form['attributes']);
			switch($type) {
				case "text":
					$input = '<input type="text" name="'.$name.'" '._stringify_attributes($attributes).' value="'.$default.'">';
				break;
				case "datepicker":
					$id = $name . "-date-picker";
					$attributes = array_merge(array("class"=>"date-picker form-control col-md-7 col-xs-12", "id"=>$id), $attributes);
					$input = '<input type="text" name="'.$name.'" '._stringify_attributes($attributes).' value="'.$default.'">';
$input .= <<<SCRIPT
		<script type="text/javascript">
            $(document).ready(function() {
              $('#{$id}').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4",
				format : "YYYY-MM-DD"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
SCRIPT;
				break;
				case "password":
					$input = '<input type="password" name="'.$name.'" '._stringify_attributes($attributes).' value="'.$default.'">';
				break;
				case "select_single":
					$input = '<select name="'.$name.'" '._stringify_attributes($attributes).'>';
						$input .= "<option value=\"\">Choose {$title}</option>";
						foreach($form['options'] as $key=>$value) {
							if( $default==$key ) {
								$input .= "<option value=\"{$key}\" selected=\"selected\">{$value}</option>";
							} else {
								$input .= "<option value=\"{$key}\">{$value}</option>";
							}
						}
					$input .= "</select>";
				break;
				case "checkbox":
					$input = '<div class="checkbox">';
						foreach($form['options'] as $key=>$value) {
							if( $default==$key ) {
								$input .= "<label><input type=\"checkbox\" class=\"flat\" checked=\"checked\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							} else {
								$input .= "<label><input type=\"checkbox\" class=\"flat\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							}
						}
					$input .= "</div>";
				break;
				case "radio":
					$input = '<div class="radio">';
						foreach($form['options'] as $key=>$value) {
							if( $default==$key ) {
								$input .= "<label><input type=\"radio\" class=\"flat\" checked=\"checked\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							} else {
								$input .= "<label><input type=\"radio\" class=\"flat\" name=\"{$name}\" value=\"{$key}\" "._stringify_attributes($attributes)."> {$value}</label>";
							}
						}
					$input .= "</div>";
				break;
			}
$html = <<<HTML
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{$title}</label>
  <div class="col-md-6 col-sm-6 col-xs-12">{$input}</div>
</div>
HTML;
		return $html;
	}
}

// ------------------------------------------------------------------------

