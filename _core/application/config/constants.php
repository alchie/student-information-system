<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/* Level Names */
defined('LEVEL_NAMES') OR define('LEVEL_NAMES',  serialize(array(
	'K1' => 'Kinder 1',
	'K2' => 'Kinder 2',
	'G1' => 'Grade 1',
	'G2' => 'Grade 2',
	'G3' => 'Grade 3',
	'G4' => 'Grade 4',
	'G5' => 'Grade 5',
	'G6' => 'Grade 6',
	'G7' => 'Grade 7',
	'G8' => 'Grade 8',
	'G9' => 'Grade 9',
	'G10' => 'Grade 10',
	'G11' => 'Grade 11',
	'G12' => 'Grade 12',
))); 

/* Level Names */
defined('MODULES') OR define('MODULES',  serialize(
array(
	'school' => '<strong>School Information</strong>',

	    'campuses' => ' - - - - Campuses',
	    'school_year' => ' - - - - School Year',
	    'sections' => ' - - - - Sections',
	    'subjects' => ' - - - - Subjects',
	    'teachers' => ' - - - - Teachers',
	    'fees' => ' - - - - Fees',
	    'services' => ' - - - - Services',

	'students' => '<strong>Students Information</strong>',


	'parents' => '<strong>Parents Information</strong>',


	'enrollment' => '<strong>Enrollment Information</strong>',


	'finance' => '<strong>Finance Information</strong>',


	'administration' => '<strong>Administration</strong>',

	    'user_accounts' => ' - - - - User Accounts',
	    'backup' => ' - - - - Backup',
	    'sync' => ' - - - - Sync',
	    'automation' => ' - - - - Automation',
	    'switch_school_year' => ' - - - - Switch School Year',
	)
)); 

defined('K12MS_CONTROL_MODE')      OR define('K12MS_CONTROL_MODE', 'ADMIN'); // ADMIN, STUDENT

defined('K12MS_STUDENT_LOGIN_URL')      OR define('K12MS_STUDENT_LOGIN_URL', 'http://login.slcd.edu.ph/'); 
defined('K12MS_STUDENT_LOGOUT_URL')      OR define('K12MS_STUDENT_LOGOUT_URL', 'http://login.slcd.edu.ph/logout.php'); 
defined('K12MS_STUDENT_LOGIN_ERROR_URL')      OR define('K12MS_STUDENT_LOGIN_ERROR_URL', 'http://main.slcd.edu.ph/sis/session-error'); 
defined('K12MS_STUDENT_NOTFOUND_URL')      OR define('K12MS_STUDENT_NOTFOUND_URL', 'http://main.slcd.edu.ph/sis/student-not-found'); 

defined('K12MS_STUDENT_LOGIN_API_KEY')      OR define('K12MS_STUDENT_LOGIN_API_KEY', 'C2E2t1W2f6b8S3C3b2C3G1d1b5d1h1d5'); 

defined('K12MS_SYNC_SERVER_URI')      OR define('K12MS_SYNC_SERVER_URI', 'k12ms.slcd.edu.ph'); 
defined('K12MS_SYNC_PUBLIC_KEY')      OR define('K12MS_SYNC_PUBLIC_KEY', '548c94bd855752425c890dc4ab348481b16b7985');
defined('K12MS_SYNC_SECRET_KEY')      OR define('K12MS_SYNC_SECRET_KEY', 'e03740441bc5cf8912ff72124a0ef46d01689248');