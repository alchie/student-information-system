<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Assign Parent of: <?php echo $student->lastname; ?>, <?php echo $student->firstname; ?> (<?php echo $student->idn; ?>)</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

	<?php echo form_open("students/assign_parent/" . $student->id, array("method"=>"get")); ?>
	<div class="form-group">
         <div class="input-group">
		  <input type="text" class="form-control" placeholder="Search Parent Name" name="name" value="<?php echo $this->input->get("name"); ?>">
		  <span class="input-group-btn">
			<button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		  </span>
		</div><!-- /input-group -->
	</div>

	  </form>
                   <?php echo form_open("students/assign_parent/" . $student->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>

<?php if($parents) { ?>

				<label>Select a Parent</label>
	<div class="list-group">
	<?php foreach( $parents as $parent ) { ?>
		<span class="list-group-item">
		 <div class="checkbox">
		<label>
		<input type="checkbox" name="parent_id" value="<?php echo $parent->id; ?>"> <?php echo $parent->lastname; ?>, <?php echo $parent->firstname; ?>
		</label>
		</div>
		</span>
		
	<?php } ?>
	

<?php } ?>

<div class="list-group-item">
	<label>Relationship</label>
	<select name="relationship" class="form-control">
		<option value="">-- Select a Relationship --</option>
		<option value="father">Father</option>
		<option value="mother">Mother</option>
		<option value="guardian">Guardian</option>
	</select>
</div> 
</div> 
                  <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("students/update/" . $student->id ); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>
