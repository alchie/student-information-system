<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-6 col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a href="<?php echo site_url('parents/delete/' . $parent->id); ?>" class="pull-right btn btn-danger btn-xs confirm"><i class="glyphicon glyphicon-remove"></i> Delete</a>
			<h4>Update Parent</h4>
		  </div>
   			<div class="panel-body">

			
	<?php echo form_open("parents/update/" . $parent->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

	$forms = array(
		'lastname' => array("title"=>"Last Name", 'type'=>"text", "required"=>true, "default"=>$parent->lastname),
		'firstname' => array("title"=>"First Name", 'type'=>"text", "required"=>true, "default"=>$parent->firstname),
		'middlename' => array("title"=>"Middle Name", 'type'=>"text", "required"=>false, "default"=>$parent->middlename),
		'gender' => array("title"=>"Gender", 'type'=>"radio_multiline", "required"=>false,'options'=>array("m"=>"Male","f"=>"Female"), "default"=>$parent->gender),
		'deceased' => array("title"=>"Deceased", 'type'=>"checkbox", "required"=>false, 'options'=>array("1"=>"Deceased"), "default"=>$parent->deceased),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Update <i class="fa fa-save"></i></button>
						<a href="javascript:history.back(-1);" class="btn btn-warning"><i class="fa fa-arrow-left"></i></i> Back</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>
	
	<div class="col-sm-6 col-md-6">
	
        <div class="panel panel-default">
          <div class="panel-heading"><a role="button" data-toggle="modal" href="#addAddressModal" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i> Add</a><h4> Address </h4></div>
   			<div class="panel-body">
				<div class="list-group">
				<?php foreach( $addresses as $address ) { ?>
				  <a href="<?php echo site_url("parents/update_address/" . $parent->id . "/" . $address->id ); ?>" class="list-group-item">
				  <?php echo $address->address; ?></a>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="panel panel-default">
          <div class="panel-heading">
		  <a role="button" data-toggle="modal" href="#addContactModal" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i> Add</a>
		  <h4>Contact Numbers</h4>
		  </div>
   			<div class="panel-body">
				<div class="list-group">
				<?php foreach( $contacts as $contact ) { ?>
				 <a href="<?php echo site_url("parents/update_contact/" . $parent->id . "/" . $contact->id ); ?>" class="list-group-item">
				  <span class="badge"><?php echo $contact->remarks; ?></span><?php echo $contact->contact; ?></a>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="panel panel-default">
          <div class="panel-heading"><h4>Students</h4></div>
   			<div class="panel-body">
				<div class="list-group">
				<?php foreach( $students as $student ) { ?>
				  <a href="<?php echo site_url("students/update/" . $student->id ); ?>" class="list-group-item">
				  <?php echo $student->lastname; ?>, <?php echo $student->firstname; ?></a>
				<?php } ?>
				</div>
			</div>
		</div>
		
	</div>
	
</div>


<?php $this->load->view('parents/parents_update_modal'); ?>
<?php $this->load->view('footer'); ?>
