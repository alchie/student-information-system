<!--address modal-->
<div style="display: none;" id="addAddressModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Address</strong>
      </div>
	 <?php echo form_open("parents/address/" . $parent->id . "/add"); ?>
      <div class="modal-body">
         <div class="form-group">
			<label for="address">Address</label>
			<textarea name="address" class="form-control" id="address" placeholder="Address" rows="5"></textarea>
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>

<!--contact numbers modal-->
<div style="display: none;" id="addContactModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Contact</strong>
      </div>
	 <?php echo form_open("parents/contact_numbers/" . $parent->id ); ?>
      <div class="modal-body">
         <div class="form-group">
			<label for="address">Contact Number</label>
			<input type="text" name="number" class="form-control" id="number" placeholder="Contact Number">
		  </div>
		  <div class="form-group">
			<label for="remarks">Remarks</label>
			<input type="text" name="remarks" class="form-control" id="remarks" placeholder="Remarks">
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>