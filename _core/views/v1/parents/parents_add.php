<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-8 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Add New Parent</h4></div>

<form class="form-horizontal form-label-left" method="post">
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
                 
<?php

	$forms = array(
		'lastname' => array("title"=>"Last Name", 'type'=>"text", "required"=>true),
		'firstname' => array("title"=>"First Name", 'type'=>"text", "required"=>true),
		'middlename' => array("title"=>"Middle Name", 'type'=>"text", "required"=>false),
		'gender' => array("title"=>"Gender", 'type'=>"radio_multiline", "required"=>false,'options'=>array("m"=>"Male","f"=>"Female")),
		'deceased' => array("title"=>"Deceased", 'type'=>"checkbox", "required"=>false, 'options'=>array("1"=>"Deceased")),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form ); 
	
	}
?>
				
			
            </div>

			<div class="panel-footer">
            		<div class="">
                      
				        <button type="submit" class="btn btn-success">Update <i class="fa fa-save"></i></button>
						<a href="javascript:history.back(-1);" class="btn btn-warning"><i class="fa fa-arrow-left"></i></i> Back</a>
				            
				    </div>
            </div>
            </form>

	</div>
</div>

<?php $this->load->view('footer'); ?>
