<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><a href="<?php echo site_url('parents/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add New Parent</a> <h4>Parents List <?php echo ($this->input->get("q")) ? "<small>Search for \"<strong>" . $this->input->get("q") . "</strong>\"" : ""; ?></small></h4></div>
   			<div class="panel-body">
<?php if( $parents ) { ?>
              <table class="table table-striped">
				<thead>
				<tr>
				  <th>Last Name</th>
				  <th>First Name</th>
				  <th>Middle Name</th>
				  <th width="5%">Action</th>
				</tr>
			  </thead>
				<tbody>
				<?php foreach( $parents as $parent ) { ?>
					<tr>
						<td><?php echo $parent->lastname; ?></td>
						<td><?php echo $parent->firstname; ?></td>
						<td><?php echo $parent->middlename; ?></td>
						<td><a class="btn btn-warning btn-xs" href="<?php echo site_url("parents/update/" . $parent->id); ?>">Update</a></td>
					</tr>
				<?php } ?>
				<tbody>
              </table>
			  <center>
					<?php echo $pagination; ?>
			  </center>
<?php } else { ?>
	<div class="text-center" style="margin-bottom:20px;">No Parent Added Yet!</div>
<?php }  ?>

            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
