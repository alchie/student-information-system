<?php $this->load->view('header'); 
$level_names = unserialize(LEVEL_NAMES);
?>

<div class="row">
	<div class="col-sm-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a href="<?php echo site_url("enrollment/delete/" . $enroll_data->enroll_id); ?>" class="pull-right btn btn-danger btn-xs confirm"><i class="glyphicon glyphicon-trash"></i> Delete Record</a>
		  <h4><?php echo $enroll_data->lastname; ?>, <?php echo $enroll_data->firstname; ?> <?php echo substr($enroll_data->middlename,0,1); ?> (<?php echo $enroll_data->idn; ?>)</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

	
<?php echo form_open("enrollment/update/" . $enroll_data->enroll_id); ?>

	<div class="list-group">
	<span class="list-group-item">
		<label>Campus</label>
		<select name="campus_id" class="form-control" required>
			<option value="">-- Select a Campus --</option>
			<?php 
			foreach($campuses as $campus) { ?>
				<option value="<?php echo $campus->id; ?>" <?php echo ($enroll_data->campus_id==$campus->id) ? 'selected' : ''; ?>><?php echo $campus->name; ?></option>
			<?php } ?>
		</select>
	</span>
	<span class="list-group-item">
		<label>Grade Level</label>
		<select name="grade_level" class="form-control" required>
			<option value="">-- Select a Grade Level --</option>
			<?php 
				
			foreach($grade_levels as $level) { ?>
				<option value="<?php echo $level->level; ?>" <?php echo ($enroll_data->grade_level==$level->level) ? 'selected' : ''; ?>><?php echo $level_names[$level->level]; ?></option>
			<?php } ?>
		</select>
	</span>
	<span class="list-group-item">
		<label>Date Enrolled</label>
		<input type="text" class="form-control datepicker" placeholder="Date Enrolled" name="date_enrolled" value="<?php echo date("m/d/Y", strtotime($enroll_data->date_enrolled)); ?>">
	</span>
	<span class="list-group-item">
		<label>Section</label>
		<select name="section_id" class="form-control">
			<option value="">-- Select a Section --</option>
			<?php foreach($sections as $section) {
				$selected = ($enroll_data->section_id==$section->id) ? 'selected' : '';
				echo '<option value="'.$section->id.'" '.$selected.'>'.$section->name.'</option>';
			} ?>
		</select>
	</span>
	<span class="list-group-item">
		<label>Enrollment Status</label>
		<select name="status" class="form-control">
			<?php 
			foreach(array(
				'enrolled' => 'Enrolled',
				'dropped' => 'Dropped Out',
				'transferred' => 'Transferred Out',
				) as $key=>$status) {
				$selected = ($enroll_data->status==$key) ? 'selected' : '';
				echo '<option value="'.$key.'" '.$selected.'>'.$status.'</option>';
			} ?>
		</select>
	</span>
	</div> 

</div>
			
<div class="panel-footer">
	<div class="form-group">
                      
        <button type="submit" class="btn btn-success">Update <i class="fa fa-save"></i></button>
		<a href="javascript:history.back(-1);" class="btn btn-warning"><i class="fa fa-arrow-left"></i></i> Back</a>
            
    </div>
</div>
</form>
			
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
