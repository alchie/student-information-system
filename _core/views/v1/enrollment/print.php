<?php $level_names = unserialize(LEVEL_NAMES); ?>

<html>
<head>
<style>

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
  display: block;
}
body {
  line-height: 1;
}
ol, ul {
  list-style: none;
}
blockquote, q {
  quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
  content: '';
  content: none;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}
/* CSS */
html, body {
    height: 100%;
}
body {
  font-size:11px;
  font-family: Arial;
  padding:0;
  margin: 0;
}
p {
  padding: 0;
  margin: 0;
}
#school_name {
  font-weight: bold;
  font-size:12px;
  text-transform: uppercase;
}
#school_address,
#school_contacts {
  font-size: 10px;
}
.outer {
  margin:0 10px 10px 0;
  width: 100%;
}
.outer.outer-3,
.outer.outer-4 {
  margin-bottom:6px;
  }
.soa-body {
  padding: 3px;
  margin: 10px;
  position: relative;
}
.soa-title {
  text-transform: uppercase;
  margin-top:15px;
  font-weight: bold;
  font-family: serif;
  letter-spacing: 3px;
  margin-bottom:15px;
}
.border {
  border: 1px solid #000;
}
.float-left {
  float:left;
}
.text-center {
  text-align: center;
}
.text-right {
  text-align: right;
}
.text-left {
  text-align: left;
}
.bold {
  font-weight: bold;
}
.underlined {
  text-decoration: underline;
}
.student_details tr td {
  padding-top: 5px;
}
#student_name {
  text-decoration: underline;
  text-transform: uppercase;
  font-weight: bold;
}
#student_name span {
  font-size: 10px;
  font-weight: normal;
}
#grade_level, #campus {
  margin-top:5px;
  font-weight: bold;
  text-transform: uppercase;
}
#grade_level span, #campus span {
  font-size: 10px;
  font-weight: normal;
}
#fees, #services {
  border:1px solid #000;
  margin-top:10px;
}
#fees thead td,
#services thead td {
  padding:5px;
  text-transform: uppercase;
  font-size: 10px;
  font-weight: bold;
}
#fees tbody td,
#services tbody td {
  padding:2px 5px;
  font-size: 10px;
}
#amount_due {
  margin-top: 10px;
  font-size: 10px;
  text-transform: uppercase;
}
#amount_due td {
  padding: 5px;
  }
.bigger {
  font-size: 12px;
}
.logo {
  width: 90px;
  position: absolute;
  top:15px;
  left:15px;
}
.logo.right {
  left:auto;
  right:15px;
}
.details td {
  padding:3px 5px;
}
.allcaps {
  text-transform: uppercase;
}
.dark-background {
  background-color: #CCC;
}
.padding2 {
  padding:2px;
}
.pull-right {
  float:right;
}
</style>
<title>Enrollees List (<?php echo $primary_school_year->label; ?>)</title>
</head>
<body>
<center>

<table cellpadding="0" cellspacing="0" class="outer border">

<?php if( isset( $options ) && (isset($options['add_header'])) ) { ?>

  <thead>
  <tr>
    <td class="text-center">
      <div class="soa-body" style="min-height: 90px;">
      <img src="<?php echo base_url("assets/v1/img/logo160x160.png"); ?>" class="logo">
      <img src="<?php echo base_url("assets/v1/img/rcbdi_logo.png"); ?>" class="logo right">
      <h1 id="school_name"><?php echo get_ams_config('school', 'school_name'); ?></h1>
      <p id="school_address"><?php echo get_ams_config('school', 'school_address'); ?></p>
      <p id="school_contacts"><?php echo get_ams_config('school', 'school_phone'); ?> &middot; <?php echo get_ams_config('school', 'school_email'); ?></p>

      <h3 class="soa-title">
      <span class="underlined"><?php echo ( isset( $options ) && (isset($options['header_title']) && ($options['header_title'] != '')) ) ? $options['header_title'] : 'Enrollees List'; ?></span>
      <?php if( isset( $options ) && (isset($options['header_school_year'])) ) { ?> 
        <br><?php echo $primary_school_year->label; ?>
      <?php } ?>
      </h3>
       <p class="allcaps">
       <?php if( isset( $options ) && (isset($options['header_campus'])) ) { ?>
        Campus: <span class="bold underlined"><?php echo ( $campus ) ? $campus->name : "All"; ?></span>
       <?php } ?>
       <?php if( isset( $options ) && (isset($options['header_grade_level'])) ) { ?>
       <br> Grade Level: <span class="bold underlined"><?php echo ( $grade_level ) ? $level_names[$grade_level->level] : "All"; ?></span> 
       <?php } ?>
        <?php if( isset( $options ) && (isset($options['header_number_students'])) ) { ?>
       <br> Number of Students: <span class="bold underlined"><?php echo $enrollees_count; ?></span>
       <?php } ?>
       </p>
      </div>
    </td>
  </tr>
  </thead>

<?php } ?>

  <tbody>
<tr>
  <td style="padding:10px 10px 10px 10px">
    <table class="border outer details">
        <tr class="dark-background border bold">
<?php foreach(array(
     'n' => '#'
    ,'idn' => 'ID Number'
    ,'lastname' => 'Lastname'
    ,'firstname' => 'Firstname'
    ,'campus_name' => 'Campus'
    ,'grade_level' => 'Grade Level'
    ,'birthday' => 'Birthday'
    ,'birthplace' => 'Birth Place'
    ,'religion' => 'Religion'
    ,'gender' => 'Gender'
    ,'lrn' => 'LRN'
    ,'google_id' => 'Google ID'
    ,'google_userid' => 'Google User ID'
    ,'random_password' => 'Temp Password'
    ,'section_name' => 'Section Name'
    ,'status' => 'Status'
  ) as $key=>$title ) { 
if( isset($options['columns']) && (in_array($key, $options['columns'])) ) { 
    ?>
          <th class="text-left padding2"><?php echo $title; ?></th>
  <?php } 
  } ?>
        </tr>
<?php 
$n = 1;
foreach($enrollees as $enrollee) { ?>
        <tr>
<?php if( isset($options['columns']) && (in_array('n', $options['columns'])) ) { ?>
          <td><?php echo $n++; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('idn', $options['columns'])) ) { ?>
          <td><?php echo $enrollee->idn; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('lastname', $options['columns'])) ) { ?>
            <td><?php echo $enrollee->lastname; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('firstname', $options['columns'])) ) { ?>
            <td><?php echo $enrollee->firstname; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('campus_name', $options['columns'])) ) { ?>
            <td>
            <?php echo $enrollee->campus_name; ?>
            </td>
<?php } ?>

<?php if( isset($options['columns']) && (in_array('grade_level', $options['columns'])) ) { ?>
            <td><?php echo $level_names[$enrollee->grade_level]; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('birthday', $options['columns'])) ) { ?>
            <td><?php echo $enrollee->birthday; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('birthplace', $options['columns'])) ) { ?>
            <td><?php echo $enrollee->birthplace; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('religion', $options['columns'])) ) { ?>
            <td><?php echo $enrollee->religion; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('gender', $options['columns'])) ) { ?>
            <td class="allcaps"><?php echo $enrollee->gender; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('lrn', $options['columns'])) ) { ?>
            <td><?php echo $enrollee->lrn; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('google_id', $options['columns'])) ) { ?>
            <td><?php echo $enrollee->google_id; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('google_userid', $options['columns'])) ) { ?>
            <td><?php echo $enrollee->google_userid; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('random_password', $options['columns'])) ) { ?>
            <td><?php echo substr(sha1($enrollee->id . date('Y-m-d-H-i-s')), 0, 8); ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('section_name', $options['columns'])) ) { ?>
            <td><?php echo $enrollee->section_name; ?></td>
<?php } ?>
<?php if( isset($options['columns']) && (in_array('status', $options['columns'])) ) { ?>
            <td><?php echo ucwords($enrollee->status); ?></td>
<?php } ?>
        </tr>
<?php } ?>
    </table>
  </td>  
</tr>
</tbody>

<?php if( isset( $options ) && (isset($options['add_footer'])) ) { ?>
<tr>
    <td style="padding:0px 10px 10px 10px">
      <table class="outer details">
        <tr>
          <td>

          <?php if( isset( $options ) && (isset($options['footer_prepared'])) ) { ?>
          <span class="pull-right"><span class="allcaps bold">Date Printed:</span> <span class="underlined"><?php echo date('F d, Y'); ?></span></span>
          <?php } ?>

          <?php if( isset( $options ) && (isset($options['footer_dateprinted'])) ) { ?>
          <span class="bold allcaps">Prepeared By:</span> <span class="underlined"><?php echo $this->session->userdata('logged_name'); ?></span>
          <?php } ?>

          </td>
        </tr>
      </table>
    </td>
  </tr>
<?php } ?>
</table>

</center>
</body>
</html>