<?php $this->load->view('header'); 
$level_names = unserialize(LEVEL_NAMES);

?>
<?php if( !isset($primary_school_year) ) : ?>
<div class="alert alert-danger alert-dismissable">
	<strong>SCHOOL YEAR ERROR!</strong> No current school year found! Please assign a current school year or select a school year <a href="<?php echo site_url('school_year'); ?>">here</a>.
</div>
<?php else: ?>
<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
		 
      <a role="button" data-toggle="modal" href="#enrollStudentModal" data-href="<?php echo site_url('enrollment/enroll/' . $primary_school_year->id ); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Enroll Student</a> 

      <a role="button" data-toggle="modal" data-target="#enrollmentPrint" data-href="<?php echo site_url("enrollment/pr1nt/{$primary_school_year->id}/{$grade_level}/{$campus_id}" ); ?>" class="pull-right btn btn-primary btn-sm" style="margin-right:5px" target="_blank"><i class="glyphicon glyphicon-print"></i> Print</a>


		  <h4>
<?php 

if( ( $enrollees_count > 0) && ( hasAccess('automation', 'view') ) ) { ?>
  <a href="<?php echo site_url('enrollment/automation/' . $primary_school_year->id); ?>"><i class="glyphicon glyphicon-flash"></i></a>
<?php } ?>
      Enrolled Students: <?php echo $primary_school_year->label; ?>
<?php echo ($this->input->get("q")) ? " <small>Search for \"<strong>" . $this->input->get("q") . "</strong>\" <a href='".site_url(uri_string())."'>Clear Search</a></small> " : ""; ?>
<?php echo ((isset($grade_level) && ($grade_level!='0')) || (isset($campus_id) && ($campus_id!='0'))) ? " <small>Filter: " : ""; ?>
<?php echo (isset($campus)) ? " <strong>" . $campus->name . "</strong> " : ""; ?>
<?php echo (isset($grade_level)&&isset($campus)) ? " &middot; " : ""; ?>
<?php echo (isset($grade_level) && isset($level_names[$grade_level])) ? " <strong>" . $level_names[$grade_level] . "</strong> " : ""; ?>
<?php echo ((isset($grade_level) && ($grade_level!='0')) || (isset($campus_id) && ($campus_id!='0'))) ? " <a href='".site_url("enrollment")."'>Clear Filter</a></small> " : ""; ?>
		  </h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                    <thead>
                      <tr class="headings">
						  <th>ID Number</th>
						  <th>Last Name</th>
						  <th>First Name</th>
						  
						  <th>

<div class="btn-group">
  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo (isset($campus)) ? $campus->name : 'Campus'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
   <li><a href="<?php echo site_url(sprintf('enrollment/index/%s/%s/%s', $primary_school_year->id, $grade_level, 0)); ?>">Show All</a></li>
   <li role="separator" class="divider"></li>
  <?php 
  foreach($campuses as $campuss) { ?>
    <li><a href="<?php echo site_url(sprintf('enrollment/index/%s/%s/%s', $primary_school_year->id, $grade_level, $campuss->id)); ?>"><?php echo $campuss->name; ?></a></li>
  <?php } ?>
  </ul>
</div>

						  </th>
						  
						  <th>

<div class="btn-group">
  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo ($grade_level) ? $level_names[$grade_level] : 'Grade Level'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <li><a href="<?php echo site_url(sprintf('enrollment/index/%s/%s/%s', $primary_school_year->id, 0, $campus_id)); ?>">Show All</a></li>
  <li role="separator" class="divider"></li>
  <?php foreach($grade_levels as $gLevel) { ?>
    <li><a href="<?php echo site_url(sprintf('enrollment/index/%s/%s/%s', $primary_school_year->id, $gLevel->level, $campus_id)); ?>"><?php echo $level_names[$gLevel->level]; ?></a></li>
   <?php } ?>
  </ul>
</div>


						  </th>
						 
						  <th>Date Enrolled</th>
                        <th width="120px"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
<?php foreach( $enrollees as $enrollee ):   ?>
                      <tr class="<?php echo ($enrollee->status=='dropped'||$enrollee->status=='transferred') ? 'danger' : ''; ?>">
                        <td><?php echo $enrollee->idn; ?></td>
						<td><?php echo $enrollee->lastname; ?></td>
						<td><?php echo $enrollee->firstname; ?></td>
						
						<td>
						<?php echo $enrollee->campus_name; ?>
						</td>
						
						<td><?php echo $level_names[$enrollee->grade_level]; ?></td>
						
						<td><?php echo date('F d, Y', strtotime($enrollee->date_enrolled)); ?></td>
                        <td>
                        <a class="btn btn-warning btn-xs" href="<?php echo site_url("students/profile/" . $enrollee->student_id); ?>">Profile</a> 
                        <a class="btn btn-info btn-xs" href="<?php echo site_url("finance/ledger/{$primary_school_year->id}/{$enrollee->id}"); ?>">Ledger</a> 
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>
            </div>
            <div class="panel-footer">
            	<small>TOTAL ENROLLEES: <strong><?php echo $enrollees_count; ?></strong></small>
            	
            </div>
   		</div>

	</div>
</div>
<?php endif; ?>

<!--parents modal-->
<div style="display: none;" id="enrollStudentModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Search Students</strong>
      </div>
	 <?php echo form_open("enrollment/enroll/" . $primary_school_year->id, array("method"=>"get")); ?>
      <div class="modal-body">
         <div class="input-group">
		  <input type="text" class="form-control" placeholder="Search Student" name="q" value="<?php echo $this->input->get("q"); ?>">
		  <span class="input-group-btn">
			<button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		  </span>
		</div><!-- /input-group -->
      </div>
      <!--
	  <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  -->
	  </form>
  </div>
  </div>
</div>

<!--enrollmentPrint modal-->
<div style="display: none;" id="enrollmentPrint" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <strong>Print Options</strong>
      </div>
   <?php echo form_open("enrollment/pr1nt/{$primary_school_year->id}/{$grade_level}/{$campus_id}", array("method"=>"get", 'target'=>'_blank')); ?>
      <div class="modal-body">


<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <!-- Header -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingHeader">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseHeader" aria-expanded="true" aria-controls="collapseHeader">
          Header
        </a>
      </h4>
    </div>
    <div id="collapseHeader" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingHeader">
      <div class="panel-body">
       
        <div class="form-group">
            <label><input type="checkbox" name="option[add_header]" value="1" checked>Add Header</label>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Header Title" name="option[header_title]" value="Enrollees List">
        </div>

         <div class="form-group">
            <label><input type="checkbox" name="option[header_school_year]" value="1" checked>Add School Year</label>
        </div>

        <div class="form-group">
            <label><input type="checkbox" name="option[header_campus]" value="1" checked>Add Campus</label>
        </div>

        <div class="form-group">
            <label><input type="checkbox" name="option[header_grade_level]" value="1" checked>Add Grade Level</label>
        </div>

        <div class="form-group">
            <label><input type="checkbox" name="option[header_number_students]" value="1" checked>Add Number of Students</label>
        </div>

      </div>
    </div>
  </div>

  <!-- Columns -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingColumns">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseColumns" aria-expanded="true" aria-controls="collapseColumns">
          Columns
        </a>
      </h4>
    </div>
    <div id="collapseColumns" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingColumns">
      <div class="panel-body">
<div class="row">
<?php 
$default_columns = array('n', 'idn', 'lastname', 'firstname', 'campus_name', 'grade_level');
foreach(array(
     'n' => '#'
    ,'idn' => 'ID Number'
    ,'lastname' => 'Lastname'
    ,'firstname' => 'Firstname'
    ,'campus_name' => 'Campus'
    ,'grade_level' => 'Grade Level'
    ,'birthday' => 'Birthday'
    ,'birthplace' => 'Birth Place'
    ,'religion' => 'Religion'
    ,'gender' => 'Gender'
    ,'lrn' => 'LRN'
    ,'google_id' => 'Google ID'
    ,'google_userid' => 'Google User ID'
    ,'random_password' => 'Random Password'
    ,'section_name' => 'Section Name'
    ,'status' => 'Status'
  ) as $key=>$title ) { ?>
  <div class="col-md-4">
<div class="form-group">
            <label><input type="checkbox" name="option[columns][]" value="<?php echo $key; ?>" <?php echo (in_array($key, $default_columns)) ? 'CHECKED' : ''; ?>><?php echo $title; ?></label>
        </div>
        </div>
<?php } ?>
      </div>
      </div>
    </div>
  </div>

 <!-- Sort -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSort">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSort" aria-expanded="true" aria-controls="collapseSort">
          Sort by
        </a>
      </h4>
    </div>
    <div id="collapseSort" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSort">
      <div class="panel-body">
<div class="row">
  <div class="col-md-6">
  <div class="form-group">
<select class="form-control" name="sort_by">
<?php 
foreach(array(
    'lastname' => 'Lastname'
    ,'firstname' => 'Firstname'
    ,'campus_id' => 'Campus'
    ,'grade_level' => 'Grade Level'
    ,'birthday' => 'Birthday'
    ,'birthplace' => 'Birth Place'
    ,'religion' => 'Religion'
    ,'gender' => 'Gender'
    ,'lrn' => 'LRN'
    ,'google_id' => 'Google ID'
    ,'google_userid' => 'Google User ID'
    ,'random_password' => 'Random Password'
    ,'section_name' => 'Section Name'
    ,'idn' => 'ID Number'
    ,'status' => 'Status'
  ) as $key=>$title ) { ?>

<option value="<?php echo $key; ?>"><?php echo $title; ?></option>
       
<?php } ?>
</select>
 </div>
        </div>
        <div class="col-md-6">
          <label><input type="radio" name="sort_order" value="ASC" checked> ASC</label>
          <label><input type="radio" name="sort_order" value="DESC"> DESC</label>
        </div>
        </div>
      </div>
      </div>
    </div>

  <!-- Filters -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFilters">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFilters" aria-expanded="true" aria-controls="collapseFilters">
          Filters
        </a>
      </h4>
    </div>
    <div id="collapseFilters" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFilters">
      <div class="panel-body">
<div class="row">
  <div class="col-md-12">
  <div class="form-group">
  Status
          <label><input type="checkbox" name="filter[status][]" value="enrolled" checked> Enrolled</label>
          <label><input type="checkbox" name="filter[status][]" value="dropped" checked> Dropped Out</label>
          <label><input type="checkbox" name="filter[status][]" value="transferred" checked> Transferred Out</label>
 </div>
        </div>
        </div>
      </div>
      </div>
    </div>

  <!-- Footer -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFooter">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFooter" aria-expanded="true" aria-controls="collapseFooter">
          Footer
        </a>
      </h4>
    </div>
    <div id="collapseFooter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFooter">
      <div class="panel-body">
       
        <div class="form-group">
            <label><input type="checkbox" name="option[add_footer]" value="1" checked>Add Footer</label>
        </div>

         <div class="form-group">
            <label><input type="checkbox" name="option[footer_prepared]" value="1" checked>Add `Prepared By`</label>
        </div>

        <div class="form-group">
            <label><input type="checkbox" name="option[footer_dateprinted]" value="1" checked>Add Date Printed</label>
        </div>

      </div>
    </div>
  </div>

</div>


      </div>
    <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Print</button>
      </div>
    
    </form>
  </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>
