<?php $this->load->view('header'); 

$level_names = unserialize(LEVEL_NAMES);
				
?>

<div class="row">
	<div class="col-sm-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">
<a target="_blank" href="<?php echo site_url("students/add"); ?>" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i> Add New Student</a>
<a href="<?php echo site_url("enrollment/enroll/" .  $primary_school_year->id); ?>?q=<?php echo $this->input->get('q'); ?>" style="margin-right:5px;" class="btn btn-warning btn-xs  pull-right"><i class="glyphicon glyphicon-refresh"></i> Refresh List</a>
          <h4>Enroll Student: <?php echo $primary_school_year->label; ?></h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
  <?php echo form_open("enrollment/enroll/" . $primary_school_year->id, array("method"=>"get")); ?>
	<div class="form-group">
         <div class="input-group">
		  <input type="text" class="form-control" placeholder="Search Student" name="q" value="<?php echo $this->input->get("q"); ?>">
		  <span class="input-group-btn">
			<button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		  </span>
		</div><!-- /input-group -->
	</div>
	</form>
				
</div>
				
<?php if($search_results) { ?>
<?php echo form_open("enrollment/enroll/" . $primary_school_year->id); ?>
<div class="panel-body">
	<label>Select a Student</label>
	<div class="list-group">
	<?php 
	$show_more_fields = 0;
	$n=0;
	foreach( $search_results as $student ) { 
		if($student->grade_level=='') {
			$show_more_fields++;
		}
	?>
		<label class="list-group-item <?php //echo ($student->grade_level!='')?'danger':''; ?>" style="padding:0 0 0 10px;<?php echo ($student->grade_level!='')?'background-color: rgba(255, 0, 0, 0.1);':''; ?>">
		 <div class="radio">
		<?php if($student->grade_level=='') { ?>
			<input required type="radio" name="student_id" value="<?php echo $student->id; ?>" <?php echo ($n==0) ? 'checked' : ''; ?>> 
		<?php } else { ?>
			<span class="badge pull-right" style="margin-right:15px"><?php echo $level_names[$student->grade_level]; ?></span>
		<?php }  ?>
		<?php echo $student->lastname; ?>, <?php echo $student->firstname; ?> <?php echo substr($student->middlename,0,1); ?> (<?php echo $student->idn; ?>)
		</div>
		</label>
		
	<?php 
	$n++; 
} 
?> 
	</div>
	
<?php if($show_more_fields>0) { ?>
	<div class="list-group">
	<span class="list-group-item">
		<label>Campus</label>
		<select name="campus_id" class="form-control" required>
			<option value="">-- Select a Campus --</option>
			<?php 
			foreach($campuses as $campus) { ?>
				<option value="<?php echo $campus->id; ?>" <?php echo ($this->session->userdata('last_campus_id')==$campus->id) ? 'SELECTED' : ''; ?>><?php echo $campus->name; ?></option>
			<?php } ?>
		</select>
	</span>
	<span class="list-group-item">
		<label>Grade Level</label>
		<select name="grade_level" class="form-control" required>
			<option value="">-- Select a Grade Level --</option>
			<?php 
			foreach($grade_levels as $level) { ?>
				<option value="<?php echo $level->level; ?>" <?php echo ($this->session->userdata('last_grade_level')==$level->level) ? 'SELECTED' : ''; ?>><?php echo $level_names[$level->level]; ?></option>
			<?php } ?>
		</select>
	</span>
	<span class="list-group-item">
		<label>Date Enrolled</label>
		<input type="text" class="form-control datepicker" placeholder="Date Enrolled" name="date_enrolled" value="<?php echo date('m/d/Y'); ?>">
	</span>
	</div> 
<?php } ?>
</div>
<?php if($show_more_fields>0) { ?>
<div class="panel-footer">
	<div class="form-group">
                      
        <button type="submit" class="btn btn-success">Enroll <i class="fa fa-save"></i></button>
		<a href="javascript:history.back(-1);" class="btn btn-warning"><i class="fa fa-arrow-left"></i></i> Back</a>
            
    </div>
</div>
<?php } ?>
</form>
<?php } ?>
			
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
