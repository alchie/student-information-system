<?php $this->load->view('header');
$level_names = unserialize(LEVEL_NAMES);
 ?>
<div class="row">
  <div class="col-sm-6 col-md-6 col-md-offset-3">
  <form method="post">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Automation: <?php echo $primary_school_year->label; ?></h4></div>

          <div class="panel-body">


            <div class="list-group">
              <label class="list-group-item">
                <input type="radio" name="action" value="autofill_google_userid">
                AUTOFILL GOOGLE USER ID
                <p class="small">This will fill in data to Google User Id using student's lastname and firstname initials (lastname_f@slcd.edu.ph). Only the blank google user id will be updated. Existing user id will not be touched.</p>
              </label>
            </div>

                    
          </div>
          <div class="panel-footer">
            
            <div class="">
                      <div class="">
                        <button type="submit" class="btn btn-success pull-right">Proceed <i class="fa fa-arrow-right"></i></button>
                         <a href="javascript:history.back(-1);" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back</a>
          
                      </div>
                    </div>
          

          </div>
           </form>
      </div>

  </div>
</div>
<?php $this->load->view('footer'); ?>