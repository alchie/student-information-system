</div>
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center" style="margin-bottom:10px;">
			<small><a href="http://www.trokis.com/" target="footer_credits">Trokis Philippines</a> &copy; 2016 &middot; Developed by: <a href="http://www.chesteralan.com/" target="footer_credits">Chester Alan Tagudin</a></small>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url("assets/v1/js/jquery.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/v1/js/bootstrap.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/v1/jquery-ui/jquery-ui.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/v1/js/numeraljs/min/numeral.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/v1/js/scripts.js"); ?>"></script>
</body></html>