<?php 

$level_names = unserialize(LEVEL_NAMES);

?>
<html>
<head>
<style>

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
/* CSS */
html, body {
    height: 100%;
}
body {
	font-size:11px;
	font-family: Arial;
	padding:0;
	margin: 0;
}
p {
	padding: 0;
	margin: 0;
}
#school_name {
	font-weight: bold;
	font-size:12px;
	text-transform: uppercase;
}
#school_address,
#school_contacts {
	font-size: 10px;
}
.outer {
	margin:0 10px 10px 0;
	width: 100%;
}
.outer.outer-3,
.outer.outer-4 {
	margin-bottom:6px;
	}
.soa-body {
	padding: 3px;
	margin: 10px;
	position: relative;
}
.soa-title {
	text-transform: uppercase;
	margin-top:15px;
	font-weight: bold;
	font-family: serif;
	letter-spacing: 3px;
	margin-bottom:15px;
}
.border {
	border: 1px solid #000;
}
.float-left {
	float:left;
}
.text-center {
	text-align: center;
}
.text-right {
	text-align: right;
}
.text-left {
	text-align: left;
}
.bold {
	font-weight: bold;
}
.underlined {
	text-decoration: underline;
}
.student_details tr td {
	padding-top: 5px;
}
#student_name {
	text-decoration: underline;
	text-transform: uppercase;
	font-weight: bold;
}
#student_name span {
	font-size: 10px;
	font-weight: normal;
}
#grade_level, #campus {
	margin-top:5px;
	font-weight: bold;
	text-transform: uppercase;
}
#grade_level span, #campus span {
	font-size: 10px;
	font-weight: normal;
}
#fees, #services {
	border:1px solid #000;
	margin-top:10px;
}
#fees thead td,
#services thead td {
	padding:5px;
	text-transform: uppercase;
	font-size: 10px;
	font-weight: bold;
}
#fees tbody td,
#services tbody td {
	padding:2px 5px;
	font-size: 10px;
}
#amount_due {
	margin-top: 10px;
	font-size: 10px;
	text-transform: uppercase;
}
#amount_due td {
	padding: 5px;
	}
.bigger {
	font-size: 12px;
}
.logo {
	width: 90px;
	position: absolute;
	top:15px;
	left:15px;
}
.logo.right {
	left:auto;
	right:15px;
}
.details td {
	padding:5px 5px;
}
.allcaps {
	text-transform: uppercase;
}
.font110p {
	font-size: 110%;
}
a.show_details {
	color:#000;
	text-decoration: none;
}
</style>
</head>
<body>
<center>

<table cellpadding="0" cellspacing="0" class="outer border">
	<tr>
		<td class="text-center">
			<div class="soa-body">
			<img src="<?php echo base_url("assets/v1/img/logo160x160.png"); ?>" class="logo">
			<img src="<?php echo base_url("assets/v1/img/rcbdi_logo.png"); ?>" class="logo right">
			<h1 id="school_name"><?php echo get_ams_config('school', 'school_name'); ?></h1>
			<p id="school_address"><?php echo get_ams_config('school', 'school_address'); ?></p>
			<p id="school_contacts"><?php echo get_ams_config('school', 'school_phone'); ?> &middot; <?php echo get_ams_config('school', 'school_email'); ?></p>
			<p id="school_contacts"><?php echo $primary_school_year->label; ?></p>

			<h3 class="soa-title underlined">Projected Monthly Collectibles</h3>
			 <p class="allcaps bold">as of <?php echo date('F d, Y'); ?></p>
			</div>
		</td>
	</tr>
	
<tr>
		<td style="padding:10px 10px 0px 10px">
<?php $total_balances = array(); 
	$remaining_balances = array(); 
	$total_month = array();
?>
<table class="border outer details">
				<tr>
					<td class="allcaps bold underlined">Months</td>
	<?php foreach($grade_levels as $i=>$gLevel) { //if($i == 7 ) { break; } ?>
    <td class="text-right allcaps bold underlined"><?php 
$total_balances[$gLevel->level] = 0;
$remaining_balances[$gLevel->level] = 0;
    echo $level_names[$gLevel->level]; ?></td>
   <?php } ?>
   <td class="text-right allcaps bold underlined">TOTAL</td>
				</tr>
<?php 
$total_month = array();
foreach($months as $month) {  
	if( !isset($total_month[$month->month]) ) {
		$total_month[$month->month] = 0;
	}
	?>
	<tr>
		<td><?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></td>
		<?php foreach($grade_levels as $i=>$gLevel) { //if($i == 7 ) { break; } ?>
	    	<td class="text-right"><a href="<?php echo site_url('reports/projected_monthly_collectibles_details/' . $primary_school_year->id . '/' . $gLevel->level . '/' . $month->month); ?>" target="_blank" class="show_details"><?php 
	    	$invoices = 'invoices_' . $gLevel->level;
	    	$payments = 'payments_' . $gLevel->level;
	    	$applied = 'applied_' . $gLevel->level;
	    	$total_balances[$gLevel->level] += $month->$invoices;

	    	echo number_format(($month->$invoices-$month->$applied),2);
	    	$remaining_balances[$gLevel->level] += ($month->$invoices-$month->$applied);
	    	$total_month[$month->month] += ($month->$invoices-$month->$applied);
	    	?></a></td>
	   <?php } ?>
	   <td class="text-right"><?php echo number_format($total_month[$month->month],2); ?></td>
	</tr>
<?php } ?>
	<tr>
		<td><h3 class="allcaps bold underlined">TOTAL</h3></td>
	<?php 
$total_total = 0;
	foreach($grade_levels as $i=>$gLevel) { // if($i == 7 ) { break; } ?>
    <td class="text-right bold font110p"><?php 
    echo number_format($remaining_balances[$gLevel->level],2); 
$total_total += $remaining_balances[$gLevel->level];
    ?></td>
   <?php } ?>
   <td class="text-right bold font110p">
   	<?php echo number_format($total_total,2); ?>
   </td>
				</tr>
</table>

		</td>
</tr>



</table>

</center>
</body>
</html>