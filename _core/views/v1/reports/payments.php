<?php $this->load->view('header'); 
$level_names = unserialize(LEVEL_NAMES);
?>
<?php if( !isset($primary_school_year) ) : ?>
<div class="alert alert-danger alert-dismissable">
	<strong>SCHOOL YEAR ERROR!</strong> No current school year found! Please assign a current school year or select a school year <a href="<?php echo site_url('school_year'); ?>">here</a>.
</div>
<?php else: ?>
<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">


		  <h4>Payments: <?php echo $primary_school_year->label; ?>

<?php echo ($this->input->get("q")) ? " <small>Search for \"<strong>" . $this->input->get("q") . "</strong>\" <a href='".site_url(uri_string())."'>Clear Search</a></small> " : ""; ?>
<?php echo ((isset($grade_level) && ($grade_level!='0')) || (isset($campus_id) && ($campus_id!='0'))) ? " <small>Filter: " : ""; ?>
<?php echo (isset($campus)) ? " <strong>" . $campus->name . "</strong> " : ""; ?>
<?php echo (isset($grade_level)&&isset($campus)) ? " &middot; " : ""; ?>
<?php echo (isset($grade_level) && isset($level_names[$grade_level])) ? " <strong>" . $level_names[$grade_level] . "</strong> " : ""; ?>
<?php echo ((isset($grade_level) && ($grade_level!='0')) || (isset($campus_id) && ($campus_id!='0'))) ? " <a href='".site_url("finance")."'>Clear Filter</a></small> " : ""; ?>

		  </h4></div>
   			<div class="panel-body">
              <table class="table table-striped table-hover">
                    <thead>
                      <tr class="headings">
						  <th>ID Number</th>
						  <th>Last Name</th>
						  <th>First Name</th>
						  
 <th>

<div class="btn-group">
  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo (isset($campus)) ? $campus->name : 'Campus'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
   <li><a href="<?php echo site_url(sprintf('reports/payments/%s/%s/%s', $primary_school_year->id, $grade_level, 0)); ?>">Show All</a></li>
   <li role="separator" class="divider"></li>
  <?php 
  foreach($campuses as $campuss) { ?>
    <li><a href="<?php echo site_url(sprintf('reports/payments/%s/%s/%s', $primary_school_year->id, $grade_level, $campuss->id)); ?>"><?php echo $campuss->name; ?></a></li>
  <?php } ?>
  </ul>
</div>

						  </th>
						  
						  <th>

<div class="btn-group">
  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo ($grade_level) ? $level_names[$grade_level] : 'Grade Level'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <li><a href="<?php echo site_url(sprintf('reports/payments/%s/%s/%s', $primary_school_year->id, 0, $campus_id)); ?>">Show All</a></li>
  <li role="separator" class="divider"></li>
  <?php foreach($grade_levels as $gLevel) { ?>
    <li><a href="<?php echo site_url(sprintf('reports/payments/%s/%s/%s', $primary_school_year->id, $gLevel->level, $campus_id)); ?>"><?php echo $level_names[$gLevel->level]; ?></a></li>
   <?php } ?>
  </ul>
</div>


						  </th>

                        <th width="100px"><span class="nobr">Date Paid</span></th>
                        <th width="100px"><span class="nobr">Amount Paid</span></th>
                        <th width="120px"><span class="nobr">Amount Applied</span></th>
                      </tr>
                    </thead>

                    <tbody>
<?php 
foreach( $payments as $payment ): ?>
                      <tr class="<?php echo ($payment->down==1) ? 'danger' : ''; ?>">
                        <td><?php echo $payment->idn; ?></td>
						<td><?php echo $payment->lastname; ?></td>
						<td><?php echo $payment->firstname; ?></td>
						<td><?php echo $payment->campus_name; ?></td>
						<td><?php echo $level_names[$payment->grade_level]; ?></td>
            <td class=""><?php echo date('m/d/Y', strtotime($payment->payment_date)); ?></td>
						<td class="text-right money_format"><?php echo $payment->applied_amount; ?></td>
                        <td class="text-right">
                        <a href="<?php echo site_url("finance/update_". (($payment->down==1)? "downpayment" : "payment") ."/{$primary_school_year->id}/{$payment->enrolled_id}/{$payment->payment_id}"); ?>?redirect=<?php echo uri_string(); ?>" class="money_format"><?php echo $payment->amount; ?></a>
						</td>
            
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>

            </div>
           
   		</div>

	</div>
</div>
<?php endif; ?>



<?php $this->load->view('footer'); ?>
