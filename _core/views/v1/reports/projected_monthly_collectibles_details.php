<?php $this->load->view('header'); 
$level_names = unserialize(LEVEL_NAMES);
?>
<?php if( !isset($primary_school_year) ) : ?>
<div class="alert alert-danger alert-dismissable">
	<strong>SCHOOL YEAR ERROR!</strong> No current school year found! Please assign a current school year or select a school year <a href="<?php echo site_url('school_year'); ?>">here</a>.
</div>
<?php else: ?>
<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">


          <a href="<?php 
$uri = sprintf('finance/print_soa/%s/%s/%s', $primary_school_year->id, $grade_level, $campus_id);
          echo site_url($uri); ?>" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-print"></i> Print SOA</a>

          <!-- Single button -->
<div class="btn-group pull-right" style="margin-right:10px;">
  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   Print Reports <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a target="_blank" href="<?php echo site_url("reports/projected_monthly_collectibles/{$primary_school_year->id}"); ?>">Projected Monthly Collectibles</a></li>
  </ul>
</div>

 <!-- Single button -->
<div class="btn-group pull-right" style="margin-right:10px;">
  <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   View Report <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo site_url("reports/payments/{$primary_school_year->id}"); ?>">Payments</a></li>
    <li><a href="<?php echo site_url("reports/open_payments/{$primary_school_year->id}"); ?>">Open Payments</a></li>
  </ul>
</div>

		  <h4>Projected Monthly Collectibles Details: <?php echo $primary_school_year->label; ?>

		  </h4></div>
   			<div class="panel-body">
              <table class="table table-condensed table-hover">
                <thead>
                  <tr>
                    <th>Student Name</th>
                    <th class="text-right" width="13%">Month Due</th>
                    <th class="text-right" width="13%">Paid</th>
                    <th class="text-right" width="13%">Balance</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
$total_invoices = 0;
$total_paid = 0;
$total_balance = 0;
                foreach($enrolled as $en) { ?>
                  <tr>
                    <td>
                    <a href="<?php echo site_url("finance/ledger/{$primary_school_year->id}/{$en->id}"); ?>" target="_blank">
                    <?php echo $en->lastname; ?>, <?php echo $en->firstname; ?> <?php echo substr($en->middlename, 0, 1); ?>
                      </a>
                    </td>
                    <td class="text-right">
                    <a role="button" data-toggle="modal" href="#invoiceDetailsModal" class="money_format invoiceDetailsModal_button" data-ajax="<?php echo site_url("finance/invoice_detail/{$primary_school_year->id}/{$en->id}/{$month}/ajax"); ?>">
                    <?php echo $en->total_invoices; $total_invoices += $en->total_invoices; ?>
                    </a>
                    </td>
                    <td class="text-right">
                      <a role="button" data-toggle="modal" href="#paymentDetailsModal" class="money_format paymentDetailsModal_button" data-ajax="<?php echo site_url("finance/payment_detail2/{$primary_school_year->id}/{$en->id}/{$month}/ajax"); ?>">
                    <?php echo $en->total_paid; $total_paid += $en->total_paid; ?>
                    </a>
                    </td>
                    <td class="text-right money_format"><?php echo ($en->total_invoices-$en->total_paid); $total_balance += ($en->total_invoices-$en->total_paid); ?></td>
                  </tr>
                <?php } ?>
                <tr class="success">
                    <td><strong>TOTAL</strong></td>
                    <td class="text-right money_format bold"><?php echo $total_invoices; ?></td>
                    <td class="text-right money_format bold"><?php echo $total_paid; ?></td>
                    <td class="text-right money_format bold"><?php echo $total_balance; ?></td>
                  </tr>
                </tbody>
              </table>
        </div>
           
   		</div>

	</div>
</div>


<!--invoiceDetails modal-->
<div style="display: none;" id="invoiceDetailsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width:60%">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Invoice Detail</strong>
      </div>

      <div class="modal-body text-center" data-img="<?php echo base_url("assets/v1/img/ajax-loader.gif"); ?>">
         
      </div>

  </div>
  </div>
</div>

<!-- paymentDetails modal-->
<div style="display: none;" id="paymentDetailsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width:60%">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Payment Details</strong>
      </div>

      <div class="modal-body text-center" data-img="<?php echo base_url("assets/v1/img/ajax-loader.gif"); ?>">
         
      </div>

  </div>
  </div>
</div>

<?php endif; ?>



<?php $this->load->view('footer'); ?>
