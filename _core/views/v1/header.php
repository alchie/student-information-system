<!DOCTYPE html>
<html lang="en"><head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>K-12 Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="<?php echo base_url("assets/v1/css/bootstrap.css"); ?>" rel="stylesheet">
		
        <link href="<?php echo base_url("assets/v1/jquery-ui/jquery-ui.min.css"); ?>" rel="stylesheet">
        
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
		<link href="<?php echo base_url("assets/v1/fonts/css/font-awesome.css"); ?>" rel="stylesheet">
		<link href="<?php echo base_url("assets/v1/css/google-plus.css"); ?>" rel="stylesheet">
    </head>
<script type="text/javascript">
  <!--
    var base_url = '<?php echo base_url(); ?>';
  -->
</script>

    <body>
        
<div class="navbar navbar-fixed-top header">
        <div class="navbar-header">

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse1">
            <i class="glyphicon glyphicon-user"></i>
          </button>

          <a href="<?php echo site_url("welcome"); ?>" class="navbar-brand">
            <?php echo (get_ams_config('school', 'school_name')) ? get_ams_config('school', 'school_name') : 'K12MS'; ?>


          </a>
        
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse1">
		  

<?php if( isset($search_enabled) ) { ?>
          <form class="navbar-form navbar-left" method="get" role="search">
              <div class="input-group">
                <input class="form-control" placeholder="Search" name="q" id="srch-term" type="text" value="<?php echo $this->input->get('q'); ?>">
                <div class="input-group-btn"><button class="btn btn-default btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button></div>
              </div>
          </form>
    <?php } ?>

          <ul class="nav navbar-nav navbar-right">

<?php if(K12MS_CONTROL_MODE==='STUDENT') { ?>
            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Other Services</a>
              <ul class="dropdown-menu">
                <li><a href="http://mail.slcd.edu.ph" target="_blank">Gmail</a></li>
                <li><a href="http://drive.slcd.edu.ph" target="_blank">Google Drive</a></li>
                <li><a href="http://sites.slcd.edu.ph" target="_blank">Google Sites</a></li>
                <li><a href="https://login.microsoftonline.com" target="_blank">Microsoft Office 365</a></li>
                </ul>
            </li>
<?php } ?>

<?php if( K12MS_CONTROL_MODE == 'ADMIN' ) { ?>
<?php if( isset( $school_years ) && ($school_years) ) { ?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->current_school_year_data->label; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
          <?php foreach($school_years as $syc) { ?>
            <?php if($this->session->current_school_year==$syc->id) { ?>
              <li class="active"><a href="#"><?php echo $syc->label; ?></a></li>
            <?php } else { ?>
              <li><a href="<?php echo site_url("welcome/change_school_year/{$syc->id}/{$change_school_year_uri}"); ?>"><?php echo $syc->label; ?></a></li>
            <?php } ?>
          <?php } ?>
          </ul>
        </li>
<?php } ?>
<?php } ?>

             <li class="">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> <?php echo $this->session->userdata('logged_name'); ?></a>
                <ul class="dropdown-menu">
<?php if( K12MS_CONTROL_MODE == 'ADMIN' ) { ?>
                  <li><a href="<?php echo site_url("welcome/change_password"); ?>">Change Password</a></li>
<?php } ?>
                  <li><a href="<?php echo site_url("welcome/logout"); ?>">Logout <i class="fa fa-sign-out pull-right"></i></a></li>
                </ul>
             </li>
           </ul>
        </div>		
</div>

<div class="navbar navbar-default" id="subnav">
    <div class="col-md-12">
        <div class="navbar-header">
<?php 

$cYear = (isset($current_year)) ? $current_year : false;
$sy_title = (isset($primary_school_year)) ? $primary_school_year->label : "SCHOOL YEAR ERROR!";

$sidebar_menus_admin = array( 
  'dashboard' => array(  
      'main'=>array('title' => "Dashboard", "icon"=>"glyphicon glyphicon-stats","uri"=>"dashboard"),
      'sub' => array(
        array('title'=>"Dashboard","uri"=>"dashboard","icon"=>"glyphicon glyphicon-stats"),
      ) 
  ),
  
	'school' => array(	
			'main'=>array('title' => "School Information","icon"=>"glyphicon glyphicon-flag","uri"=>"school", 'hidden'=>true),
			'sub' => array(
        array('title' => "Back","uri"=>"user_accounts","icon"=>"glyphicon glyphicon-arrow-left"),
				array('title' => "School Information","uri"=>"school","icon"=>"glyphicon glyphicon-flag"),
				array('title' => "Campuses","uri"=>"campuses","icon"=>"glyphicon glyphicon-home"),
				array('title' => "School Year","uri"=>"school_year","icon"=>"glyphicon glyphicon-calendar"),
				array('title' => "Sections","uri"=>"sections","icon"=>"glyphicon glyphicon-th-large"),
        array('title' => "Subjects","uri"=>"subjects","icon"=>"glyphicon glyphicon-bookmark"),
				array('title' => "Teachers","uri"=>"teachers","icon"=>"glyphicon glyphicon-certificate"),
        array('title' => "School Fees","uri"=>"fees","icon"=>"glyphicon glyphicon-usd"),
				array('title' => "Services","uri"=>"services","icon"=>"glyphicon glyphicon-glass"),
			) 
	),

	'students' => array(	
			'main'=>array('title' => "Students Information","icon"=>"fa fa-graduation-cap","uri"=>"students"),
			'sub' => array(
        array('title' => "Add New Student","uri"=>"students/add","icon"=>"glyphicon glyphicon-plus"),
				array('title' => "Students List","uri"=>"students","icon"=>"glyphicon glyphicon-list-alt"),
			) 
	),
	'parents' => array(	
			'main'=>array('title' => "Parents Information","icon"=>"fa fa-users","uri"=>"parents"),
			'sub' => array(
				array('title' => "Parents List","uri"=>"parents","icon"=>"glyphicon glyphicon-list-alt"),
			) 
	),
	'enrollment' => array(	
			'main'=>array('title' => "Enrollment Information","icon"=>"glyphicon glyphicon-list","uri"=>"enrollment"),
			'sub' => array(
				array('title'=>$sy_title,"uri"=>"enrollment/index/" . $cYear,"icon"=>"glyphicon glyphicon-list"),
			) 
	),
	'finance' => array(	
			'main'=>array('title' => "Finance Information","icon"=>"glyphicon glyphicon-usd","uri"=>"finance"),
			'sub' => array(
				array('title'=>$sy_title,"uri"=>"finance/index/" . $cYear ,"icon"=>"glyphicon glyphicon-list"),
			) 
	),
  'administration' => array( 
      'main'=>array('title' => "Administration","icon"=>"glyphicon glyphicon-cog","uri"=>"user_accounts"),
      'sub' => array(
        array('title'=>'School Information',"uri"=>"school","icon"=>"glyphicon glyphicon-flag"),
        array('title'=>'User Accounts',"uri"=>"user_accounts","icon"=>"glyphicon glyphicon-user"),
        array('title'=>'Database Backup',"uri"=>"backup","icon"=>"glyphicon glyphicon-flag"),
      ) 
  ),
);

if( file_exists( "sync.php" ) && ($_SERVER['HTTP_HOST'] != K12MS_SYNC_SERVER_URI)) {  
  $sidebar_menus_admin['administration']['sub'][] = array('title'=>'Sync Online',"uri"=>"sync","icon"=>"glyphicon glyphicon-transfer"); 
}

$sidebar_menus_students = array( 
  'my_info' => array( 
      'main'=>array('title' => "My Information","icon"=>"fa fa-info-circle","uri"=>"my_info"),
  ),
   'my_invoices' => array( 
      'main'=>array('title' => "My Invoices","icon"=>"fa fa-calculator","uri"=>"my_invoices")
  ),
    'my_payments' => array( 
      'main'=>array('title' => "My Payments","icon"=>"fa fa-money","uri"=>"my_payments")
  ),
);

switch (K12MS_CONTROL_MODE) {
      case 'ADMIN':
          $sidebar_menus = $sidebar_menus_admin;
      break;
      case 'STUDENT':
          $sidebar_menus = $sidebar_menus_students;
      break;
}


?>
<a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus dropdown-toggle" data-toggle="dropdown">
<?php if( isset($sidebar_menus[$sidebar_menu_main]) ) { ?>
	<i class="<?php echo $sidebar_menus[$sidebar_menu_main]['main']['icon']; ?>" style="color:#dd1111;"></i> <?php echo $sidebar_menus[$sidebar_menu_main]['main']['title']; ?>
<?php } else { ?>
	<i class="glyphicon glyphicon-home" style="color:#dd1111;"></i> Home 
<?php } ?>
<small><i class="glyphicon glyphicon-chevron-down"></i></small>
 </a>		  
<ul class="nav dropdown-menu">
<?php 
foreach( $sidebar_menus as $menu_id=>$menu ): 

if(K12MS_CONTROL_MODE==='ADMIN') {
  if( !hasAccess($menu_id,'view') ) {
    continue;
  }
}

if( isset( $menu['main']['hidden'] )  && ($menu['main']['hidden'] === true) ) {
    continue;
  }

	//if( $menu['main']['uri'] != $sidebar_menu_main ) {
?>
    <li><a href="<?php echo site_url($menu['main']['uri']); ?>"><i class="<?php echo $menu['main']['icon']; ?>" style="color:#1111dd;"></i> <?php echo $menu['main']['title']; ?></a></li>
<?php 
//	}
endforeach; ?>           
		  
          </ul>
          
          
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
      
        </div>
<?php if( isset($sidebar_menus[$sidebar_menu_main]) ) { 

if( isset( $sidebar_menus[$sidebar_menu_main]['sub'] ) ) {
  ?>
        <div class="collapse navbar-collapse" id="navbar-collapse2">

<ul class="nav navbar-nav navbar-right">
			<?php foreach( $sidebar_menus[$sidebar_menu_main]['sub'] as $menu ):
  if( !isset( $this->session->user_permissions->$menu['uri'] ) || ($this->session->user_permissions->$menu['uri']->view==0) ) {
    continue;
  } 
       ?>
				<li class="<?php echo ($menu['uri']==$sidebar_menu_sub) ? "active" : ""; ?>"><a href="<?php echo site_url($menu['uri']); ?>"><?php echo (isset($menu['icon'])&&($menu['icon']!='')) ? '<i class="'.$menu['icon'].'"></i> ' : ''; ?><?php echo $menu['title']; ?></a></li>
			<?php endforeach; ?> 
           </ul>
        </div>
<?php } ?>    
<?php } ?>		
     </div>	
</div>

<!--main-->
<div class="container" id="main">