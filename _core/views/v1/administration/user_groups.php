<?php $this->load->view('header'); 
?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">
<a class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#addNewUserGroupModal"><i class="glyphicon glyphicon-user"></i> Add New Group</a>
<a class="btn btn-sm btn-warning pull-right" style="margin-right:5px;" href="<?php echo site_url("user_accounts"); ?>">Users</a>
          <h4>Usergroups</h4></div>
   			<div class="panel-body" style="padding-top:0">
				<table class="table table-default">
					<thead>
						<tr>
              <th>Group Name</th>
							<th>Number of Users</th>
							<th width="120px">Action</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach( $usergroups as $group ) { ?>
						<tr>
              <td><?php echo $group->group_name; ?></td>
							<td><?php echo $group->user_count; ?></td>
							<td>
							<!--<a class="btn btn-xs btn-warning edit_usergroup" data-toggle="modal" href="#editUserGroupModal" data-id="<?php echo $group->id; ?>" data-name="<?php echo $group->group_name; ?>">Update</a>-->
<?php if( $group->user_count > 0 ) { ?>
              <a class="btn btn-xs btn-success edit_usergroup" href="<?php echo site_url('user_accounts/group_users/' . $group->id); ?>" >Users</a>
<?php } ?>
							<a class="btn btn-xs btn-warning edit_usergroup" href="<?php echo site_url('user_accounts/update_group/' . $group->id); ?>" >Update</a>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
  
<!--addNewUserGroupModal modal-->
<div style="display: none;" id="addNewUserGroupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong id="installmentOverrideModal-title">Add New Usergroup</strong>
      </div>
<?php echo form_open("user_accounts/add_group"); ?>
      <div class="modal-body">
        <div class="form-group">
          <label>Group Name</label>
          <input class="form-control" type="text" name="group_name">
        </div>
      </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>
  </div>
  </div>
</div>

<!--editUserGroupModal modal-->
<div style="display: none;" id="editUserGroupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong id="editUserGroupModal-title">Edit Usergroup</strong>
      </div>
<?php echo form_open("user_accounts/update_group"); ?>
      <div class="modal-body">
      <input class="form-control" type="hidden" name="group_id" id="editUserGroupModal-group_id">
        <div class="form-group">
          <label>Group Name</label>
          <input class="form-control" type="text" name="group_name" id="editUserGroupModal-group_name">
        </div>
      </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>
  </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>
