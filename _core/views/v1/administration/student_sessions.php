<?php $this->load->view('header'); 
?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">
          <h4>Student Sessions</h4></div>
   			<div class="panel-body" style="padding-top:0">

<?php if( $sessions ) { ?>

				<table class="table table-default">
					<thead>
						<tr>
							<th>User ID</th>
							<th>User Email</th>
							<th>Student Exists</th>
							<th width="5%">Action</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach( $sessions as $session ) { ?>
						<tr>
              <td><?php echo $session->user_id; ?></td>
							<td><?php echo $session->user_email; ?></td>
							<td>								
								<a class="btn btn-xs btn-warning" href="<?php echo site_url("user_accounts/update_student_info/{$session->session_id}/{$session->student_id}"); ?>">Update Student</a>
							</td>
							<td><a class="btn btn-xs btn-danger" href="<?php echo site_url("user_accounts/delete_student_session/" . $session->session_id); ?>">Delete</a></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>

<?php } else { ?>
	<p class="text-center">No Sessions</p>
<?php } ?>

			</div>
		</div>
	</div>
</div>
  
 <?php $this->load->view('footer'); ?>
