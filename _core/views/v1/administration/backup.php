<?php $this->load->view('header'); 
?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">
<!-- Split button -->
<div class="btn-group pull-right">
  <a href="<?php echo base_url("create_backup.php"); ?>" class="btn btn-success btn-xs">Create Backup</a>
  <a type="button" class="btn btn-success  btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </a>
  <ul class="dropdown-menu">
    <li><a href="<?php echo base_url("create_backup.php?single=1"); ?>">Create Backup (Single Inner File)</a></li>
  </ul>
</div>

          <h4>Database Backup</h4></div>
   			<div class="panel-body" style="padding-top:0">
				<table class="table table-default">
					<thead>
						<tr>
							<th>Backup File</th>
							<th>Filesize</th>
							<th width="11%">Action</th>
						</tr>
					</thead>
					<tbody>
					<?php 
arsort($backup_files);
					foreach($backup_files as $file ) { ?>
						<tr>
							<td><?php echo $file; ?></td>
							<td><?php echo filesize("backups/" . $file); ?></td>
							<td>
								<a href="<?php echo site_url('backup/download/' . $file ); ?>" class="btn btn-success btn-xs">Download</a>
								<a href="<?php echo site_url('backup/delete/' . $file ); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
  
<?php $this->load->view('footer'); ?>
