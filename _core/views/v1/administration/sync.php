<?php $this->load->view('header'); 
?>

<div class="row">
	<div class="col-sm-6 col-md-6 col-md-offset-3">
        
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">
          <h4>Sync Online</h4></div>
   			<div class="panel-body text-center" style="padding-top:70px;padding-bottom:100px">

   					<button type="button" class="btn btn-danger btn-lg" id="sync_sync_now" data-sync_url="<?php echo base_url("sync.php"); ?>?action=send" data-loader_img="<?php echo base_url("assets/v1/img/sync.gif"); ?>"><i class="glyphicon glyphicon-transfer"></i> SYNC NOW</a>

			</div>
		</div>


	</div>
</div>
  
<?php $this->load->view('footer'); ?>
