<?php $this->load->view('header'); 

$grp_perm = array();
foreach($group_permissions as $perm) {
	$grp_perm[$perm->module] = $perm;
}

foreach($user_permissions as $uperm) {
  
  if( !isset( $grp_perm[$uperm->module] ) ) {
    $grp_perm[$uperm->module] = (object) array(
      'module'=>$uperm->module,
      'view' => 0,
      'add' => 0,
      'edit' => 0,
      'delete' => 0,
      'print' => 0,
      'view_o' => 0,
      'add_o' => 0,
      'edit_o' => 0,
      'delete_o' => 0,
      'print_o' => 0,
      'view_p' => 0,
      'add_p' => 0,
      'edit_p' => 0,
      'delete_p' => 0,
      'print_p' => 0,
      );
  }

  switch( $uperm->action) {
    case 'view':
      $grp_perm[$uperm->module]->view_o = $uperm->on;
      $grp_perm[$uperm->module]->view_p = $uperm->on;
    break;
    case 'add':
      $grp_perm[$uperm->module]->add_o = $uperm->on;
      $grp_perm[$uperm->module]->add_p = $uperm->on;
    break;
    case 'edit':
      $grp_perm[$uperm->module]->edit_o = $uperm->on;
      $grp_perm[$uperm->module]->edit_p = $uperm->on;
    break;
    case 'delete':
      $grp_perm[$uperm->module]->delete_o = $uperm->on;
      $grp_perm[$uperm->module]->delete_p = $uperm->on;
    break;
    case 'print':
      $grp_perm[$uperm->module]->print_o = $uperm->on;
      $grp_perm[$uperm->module]->print_p = $uperm->on;
    break;
  }
}

?>

<div class="row">
	<div class="col-xs-12 col-sm-4 col-md-4 <?php echo ($user->status==0) ? 'col-md-offset-4' : ''; ?>">
        <div class="panel panel-<?php echo ($user->status==1) ? 'default' : 'danger'; ?>">
          <div class="panel-heading" style="padding-bottom:0">
<?php if($user->status==0) { ?>
  <a href="<?php echo site_url("user_accounts/delete_user/{$user->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
<?php } ?>
          <h4>User Details</h4></div>
<form method="post">
   			<div class="panel-body" style="padding-top:0">
                 <div class="form-group">
          <label>Control Mode</label>
          <select class="form-control" name="control_mode" required>
            <option value="ADMIN">ADMIN</option>
            <option value="ACCOUNTING">ACCOUNTING</option>
            <option value="REGISTRAR">REGISTRAR</option>
            <option value="FACULTY">FACULTY</option>
            <option value="LIBRARY">LIBRARY</option>
          </select>
        </div>
   				<div class="form-group">
   					<label>Nickname</label>
   					<input type="text" class="form-control" name="user_details[nickname]" value="<?php echo $user->name; ?>">
   				</div>
   				<div class="form-group">
   					<label>Username</label>
   					<input type="text" class="form-control" name="user_details[username]" value="<?php echo $user->username; ?>">
   				</div>
   				<div class="form-group">
   					<label>Usergroup</label>
   					 <select class="form-control" name="user_details[usergroup]">
   					 <option class="">- - Select Usergroup - -</option>
          	<?php foreach( $usergroups as $group ) { ?>
          		<option <?php echo ($user->usergroup==$group->id) ? 'selected' : ''; ?> value="<?php echo $group->id; ?>"><?php echo $group->group_name; ?></option>
          	<?php } ?>
          </select>
   				</div>
          <div class="form-group">
            <label><input type="checkbox" name="user_details[status]" value="1" <?php echo ($user->status==1) ? 'checked' : ''; ?>> Active</label>
            
          </div>
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-success">Update Details</button>
				<a href="<?php echo site_url("user_accounts"); ?>" class="btn btn-warning">Back</a>
			</div>
</form>
		</div>

<?php if($user->status==1) { ?>
		<div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">

          <h4>Change Password</h4></div>
<form method="post">
   			<div class="panel-body" style="padding-top:0">
				<div class="form-group">
   					<label>New Password</label>
   					<input type="password" class="form-control" name="change_password[password]">
   				</div>
   				<div class="form-group">
   					<label>Repeat Password</label>
   					<input type="password" class="form-control" name="change_password[password2]">
   				</div>
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-success">Update Password</button>
			</div>
</form>
		</div>
<?php } ?>

	</div>

	<div class="col-xs-12 col-sm-8 col-md-8">

<?php if($user->status==1) { ?>

			<div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">
          <a href="<?php echo site_url("user_accounts/reset_permissions/" . $user->id); ?>" class="btn btn-xs btn-danger pull-right confirm">Reset Permissions</a>
          <h4>Permissions</h4></div>
<form method="post">
   			<div class="panel-body" style="padding-top:0">
   			<table class="table table-default table-hover table-condensed">
   			<thead>
   				<tr>
   					<th>Module</th>
   					<th width="10%" class="text-center">View</th>
   					<th width="10%" class="text-center">Add</th>
   					<th width="10%" class="text-center">Edit</th>
   					<th width="10%" class="text-center">Delete</th>
   					<th width="10%" class="text-center">Print</th>
   				</tr>
   			</thead>
   			<tbody>
<?php 

$modules = unserialize(MODULES);

foreach($modules as $module_id=>$module_name) { ?>
	<tr>
		<td><?php echo $module_name; ?></td>
		<td class="text-center">
			<a href="#userPermissionOverrideModal" class="user_permission" data-action="view" data-moduleid="<?php echo $module_id; ?>" data-modulename="<?php echo $module_name; ?>" data-toggle="modal" data-on="<?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->view_p == 1)) ? 1 : 0; ?>" id="user_permission_<?php echo $module_id; ?>_view"><?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->view_p == 1)) ? '<i class="glyphicon glyphicon-ok" style="color:green"></i>' : '<i class="glyphicon glyphicon-remove" style="color:red"></i>'; ?></a>
		</td>
		<td class="text-center">
			<a href="#userPermissionOverrideModal" class="user_permission" data-action="add" data-moduleid="<?php echo $module_id; ?>" data-modulename="<?php echo $module_name; ?>" data-toggle="modal" data-on="<?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->add_p == 1)) ? 1 : 0; ?>" id="user_permission_<?php echo $module_id; ?>_add"><?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->add_p == 1)) ? '<i class="glyphicon glyphicon-ok" style="color:green"></i>' : '<i class="glyphicon glyphicon-remove" style="color:red"></i>'; ?></a>
		</td>
		<td class="text-center">
			<a href="#userPermissionOverrideModal" class="user_permission" data-action="edit" data-moduleid="<?php echo $module_id; ?>" data-modulename="<?php echo $module_name; ?>" data-toggle="modal" data-on="<?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->edit_p == 1)) ? 1 : 0; ?>" id="user_permission_<?php echo $module_id; ?>_edit"><?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->edit_p == 1)) ? '<i class="glyphicon glyphicon-ok" style="color:green"></i>' : '<i class="glyphicon glyphicon-remove" style="color:red"></i>'; ?></a>
		</td>
		<td class="text-center">
			<a href="#userPermissionOverrideModal" class="user_permission" data-action="delete" data-moduleid="<?php echo $module_id; ?>" data-modulename="<?php echo $module_name; ?>" data-toggle="modal" data-on="<?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->delete_p == 1)) ? 1 : 0; ?>" id="user_permission_<?php echo $module_id; ?>_delete"><?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->delete_p == 1)) ? '<i class="glyphicon glyphicon-ok" style="color:green"></i>' : '<i class="glyphicon glyphicon-remove" style="color:red"></i>'; ?></a>
		</td>
		<td class="text-center">
			<a href="#userPermissionOverrideModal" class="user_permission" data-action="print" data-moduleid="<?php echo $module_id; ?>" data-modulename="<?php echo $module_name; ?>" data-toggle="modal" data-on="<?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->print_p == 1)) ? 1 : 0; ?>" id="user_permission_<?php echo $module_id; ?>_print"><?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->print_p == 1)) ? '<i class="glyphicon glyphicon-ok" style="color:green"></i>' : '<i class="glyphicon glyphicon-remove" style="color:red"></i>'; ?></a>
		</td>
	</tr>
<?php } ?>
</tbody>
</table>
   			</div>
   		</form>
   		</div>
	</div>

<?php } ?>

</div>
  
<!--userPermissionOverrideModal modal-->
<div style="display: none;" id="userPermissionOverrideModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong id="userPermissionOverrideModal-title">Permission Override</strong>
      </div>

      <div class="modal-body">
        <div class="form-group">
          <label>Module</label>
          <div class="form-control" id="userPermissionOverrideModal-module_name"></div>
        </div>
        <div class="form-group">
          <label>Action</label>
          <div class="form-control" id="userPermissionOverrideModal-action"></div>
        </div>
        <div class="form-group">
		    <input type="checkbox" class="form-control" name="checked" autocomplete="off" id="userPermissionOverrideModal-on">
        </div>
        </div>
  </div>
  </div>
</div>


  <script>
	<!--
	var ajax_url = '<?php echo site_url("user_accounts/user_permission/" . $user->id); ?>';
	-->
</script>

<?php $this->load->view('footer'); ?>
