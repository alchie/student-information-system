<?php $this->load->view('header'); 
$grp_perm = array();
foreach($permissions as $perm) {
	$grp_perm[$perm->module] = $perm;
}
?>

<div class="row">
	<div class="col-xs-12 col-sm-4 col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">
<?php if($users_count == 0) { ?>
<a href="<?php echo site_url("user_accounts/delete_group/{$usergroup->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
<?php } ?>
          <h4>Update Group</h4></div>
<form method="post">
   			<div class="panel-body" style="padding-top:0">
   				<div class="form-group">
   					<label>Group Name</label>
   					<input type="text" class="form-control" name="group_name" value="<?php echo $usergroup->group_name; ?>">
   				</div>
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-success">Update</button>
				<a href="<?php echo site_url("user_accounts/groups"); ?>" class="btn btn-warning">Back</a>
			</div>
</form>
		</div>
	</div>
	
	<div class="col-xs-12 col-sm-8 col-md-8">
			<div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">

          <h4>Permissions</h4></div>
<form method="post">
   			<div class="panel-body" style="padding-top:0">
   			<table class="table table-default table-hover table-condensed">
   			<thead>
   				<tr>
   					<th>Module</th>
   					<th width="10%" class="text-center">View</th>
   					<th width="10%" class="text-center">Add</th>
   					<th width="10%" class="text-center">Edit</th>
   					<th width="10%" class="text-center">Delete</th>
   					<th width="10%" class="text-center">Print</th>
   				</tr>
   			</thead>
   			<tbody>
<?php 
$modules = unserialize(MODULES);

foreach($modules as $module_id=>$module_name) { ?>
	<tr>
		<td><?php echo $module_name; ?></td>
		<td class="text-center"><input type="checkbox" <?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->view == 1)) ? 'checked="checked"' : ''; ?> class="group_permission" data-action="view" data-module="<?php echo $module_id; ?>"></td>
		<td class="text-center"><input type="checkbox" <?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->add == 1)) ? 'checked="checked"' : ''; ?> class="group_permission" data-action="add" data-module="<?php echo $module_id; ?>"></td>
		<td class="text-center"><input type="checkbox" <?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->edit == 1)) ? 'checked="checked"' : ''; ?> class="group_permission" data-action="edit" data-module="<?php echo $module_id; ?>"></td>
		<td class="text-center"><input type="checkbox" <?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->delete == 1)) ? 'checked="checked"' : ''; ?> class="group_permission" data-action="delete" data-module="<?php echo $module_id; ?>"></td>
		<td class="text-center"><input type="checkbox" <?php echo (isset($grp_perm[$module_id]) && ($grp_perm[$module_id]->print == 1)) ? 'checked="checked"' : ''; ?> class="group_permission" data-action="print" data-module="<?php echo $module_id; ?>"></td>
	</tr>
<?php } ?>
</tbody>
</table>
   			</div>
   		</form>
   		</div>
	</div>
</div>
<script>
	<!--
	var ajax_url = '<?php echo site_url("user_accounts/group_permission/" . $usergroup->id); ?>';
	-->
</script>
<?php $this->load->view('footer'); ?>
