<?php $this->load->view('header'); 
?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">
<a class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#addNewUserModal"><i class="glyphicon glyphicon-user"></i> Add New User</a>
<a class="btn btn-sm btn-warning pull-right" style="margin-right:5px;" href="<?php echo site_url("user_accounts/groups"); ?>">Usergroups</a>
<a class="btn btn-sm btn-danger pull-right" style="margin-right:5px;" href="<?php echo site_url("user_accounts/student_sessions"); ?>">Student Sessions</a>
          <h4>Users</h4></div>
   			<div class="panel-body" style="padding-top:0">
				<table class="table table-default">
					<thead>
						<tr>
							<th>Nickname</th>
							<th>Username</th>
							<th>Usergroup</th>
							<th width="5%">Action</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach( $users as $user ) { ?>
						<tr>
							<td><?php echo $user->name; ?></td>
							<td><?php echo $user->username; ?></td>
							<td><?php echo ucwords($user->group_name); ?></td>
							<td><a class="btn btn-xs btn-warning" href="<?php echo site_url("user_accounts/update_user/" . $user->id); ?>">Update</a></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
  
  <!--addNewUserModal modal-->
<div style="display: none;" id="addNewUserModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong id="installmentOverrideModal-title">Add New User</strong>
      </div>
<?php echo form_open("user_accounts/add_user"); ?>
      <div class="modal-body">

         <div class="form-group">
          <label>Control Mode</label>
          <select class="form-control" name="control_mode" required>
            <option value="ADMIN">ADMIN</option>
            <option value="ACCOUNTING">ACCOUNTING</option>
            <option value="REGISTRAR">REGISTRAR</option>
            <option value="FACULTY">FACULTY</option>
            <option value="LIBRARY">LIBRARY</option>
            <option value="USER" selected="selected">USER</option>
          </select>
        </div>

         <div class="form-group">
          <label>Account Type</label>
          <select class="form-control" name="usergroup" required>
            <option value="0">- - Select Usergroup - -</option>
            <?php foreach( $usergroups as $group ) { ?>
              <option value="<?php echo $group->id; ?>"><?php echo $group->group_name; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label>Nickname</label>
          <input class="form-control" type="text" name="nickname">
        </div>
         <div class="form-group">
          <label>Username</label>
          <input class="form-control" type="text" name="username">
        </div>
        <div class="form-group">
          <label>Password</label>
          <div class="input-group">
          <input class="form-control input_password" type="password" name="password">
           <span class="input-group-btn">
        <button class="btn btn-success show_input_password" type="button">Show</button>
      </span>
          </div>
        </div>
      </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>
  </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>
