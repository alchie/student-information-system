<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h4><?php echo $primary_school_year->label; ?> Invoices</h4></div>
   			<div class="panel-body">

			
<?php if( $enrolled ) { 
if( $sy_months ) {  ?>

        <table class="table table-default table-condensed table-hover">
          <thead>
            <tr>
              <th>Month</th>
              <th class="text-right">Amount</th>
            </tr>
          </thead>
          <tbody>
          <?php 
$total_invoices = 0;
          foreach($sy_months as $month) { ?>
            <tr>
              <td><?php echo date("F Y", strtotime($month->month."/1/".$month->year)); ?></td>
              <td class="text-right"><?php echo number_format($month->invoice_amount,2); $total_invoices += $month->invoice_amount; ?></td>
            </tr>
            <?php } ?>
<tr class="warning">
              <td class="bold">TOTAL</td>
              <td class="text-right bold"><?php echo number_format($total_invoices,2); ?></td>
            </tr>
          </tbody>
        </table>

<?php 
  }
} else { ?> 

  <p class="text-center">Not enrolled!</p>

<?php } ?>

            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
