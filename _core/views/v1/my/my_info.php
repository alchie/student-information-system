<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Student's Information</h4></div>
   			<div class="panel-body">


<div class="form-horizontal">

<div class="form-group">
    <label class="col-sm-3 control-label">Student Number</label>
    <div class="col-sm-9">
      <div class="form-control"><?php echo $info->idn; ?></div>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label">Lastname</label>
    <div class="col-sm-9">
      <div class="form-control"><?php echo $info->lastname; ?></div>
    </div>
  </div>

    <div class="form-group">
    <label class="col-sm-3 control-label">Firstname</label>
    <div class="col-sm-9">
      <div class="form-control"><?php echo $info->firstname; ?></div>
    </div>
  </div>

   <div class="form-group">
    <label class="col-sm-3 control-label">Middlename</label>
    <div class="col-sm-9">
      <div class="form-control"><?php echo $info->middlename; ?></div>
    </div>
  </div>


</div>

			
            </div>
   		</div>

	</div>

	<div class="col-sm-12 col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Parents Information</h4></div>
   			<div class="panel-body">




			
            </div>
   		</div>

	</div>

</div>

<?php $this->load->view('footer'); ?>
