<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h4><?php echo $primary_school_year->label; ?> Payments</h4></div>
   			<div class="panel-body">

<?php if( $enrolled ) { 
if( $payments ) {
  ?>

        <table class="table table-default table-condensed table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th class="text-center">Receipt Number</th>
              <th class="text-right">Amount</th>
            </tr>
          </thead>
          <tbody>

<?php 
$total_payments = 0;

          foreach($payments as $payment) { ?>
            <tr>
              <td><?php echo date("m/d/Y", strtotime($payment->payment_date)); ?></td>
              <td class="text-center"><?php echo $payment->receipt_number; ?></td>
              <td class="text-right"><?php echo number_format($payment->amount,2); $total_payments += $payment->amount; ?></td>
            </tr>
            <?php } ?>
<tr class="warning">
              <td class="bold" colspan="2">TOTAL PAYMENTS</td>
              <td class="text-right bold"><?php echo number_format($total_payments,2); ?></td>
            </tr>
          </tbody>
        </table>
<?php 
  }
} else { ?> 

  <p class="text-center">Not enrolled!</p>

<?php } ?>

			
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
