<?php $this->load->view('header'); 
$level_names  = unserialize(LEVEL_NAMES);
?>

<div class="row">
<?php if(isset($primary_school_year)) { ?>
<div class="col-sm-12 col-md-4">
        <div class="panel panel-default">
   			<div class="panel-body" style="">
<?php 

$sidebar_menus = array( 
	array('title' => "My Information","icon"=>"fa fa-info-circle","uri"=>"my_info"),
  array('title' => "My Invoices","icon"=>"fa fa-calculator","uri"=>"my_invoices"),
	array('title' => "My Payments","icon"=>"fa fa-money","uri"=>"my_payments"),
);

?>
<ul class="dashboard list-group" style="margin-bottom: 0">
<?php foreach( $sidebar_menus as $menu ) { ?>
  <a class="list-group-item" href="<?php echo site_url($menu['uri']); ?>"><span class="<?php echo $menu['icon']; ?>"></span> <span class="title"><?php echo $menu['title']; ?></span>
  </a>
<?php } ?>
</ul>

   			</div>
   		</div>
</div>

<div class="col-sm-12 col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0"><h4><?php echo $primary_school_year->label; ?> Invoices</h4></div>
   			<div class="panel-body" style="padding-top:0">
				
<?php if( $enrolled ) { 
if( $sy_months ) {  ?>

        <table class="table table-default table-condensed table-hover">
          <thead>
            <tr>
              <th>Month</th>
              <th class="text-right">Amount</th>
            </tr>
          </thead>
          <tbody>
          <?php 
$total_invoices = 0;
          foreach($sy_months as $month) { ?>
            <tr>
              <td><?php echo date("F Y", strtotime($month->month."/1/".$month->year)); ?></td>
              <td class="text-right"><?php echo number_format($month->invoice_amount,2); $total_invoices += $month->invoice_amount; ?></td>
            </tr>
            <?php } ?>
<tr class="warning">
              <td class="bold">TOTAL</td>
              <td class="text-right bold"><?php echo number_format($total_invoices,2); ?></td>
            </tr>
          </tbody>
        </table>

<?php 
  }
} else { ?> 

  <p class="text-center">Not enrolled!</p>

<?php } ?>

			</div>
		</div>
	</div>


<div class="col-sm-12 col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0"><h4><?php echo $primary_school_year->label; ?> Payments</h4></div>
   			<div class="panel-body" style="padding-top:0">
		

<?php if( $enrolled ) { 
if( $payments ) {
  ?>

        <table class="table table-default table-condensed table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th class="text-center">Receipt Number</th>
              <th class="text-right">Amount</th>
            </tr>
          </thead>
          <tbody>

<?php 
$total_payments = 0;

          foreach($payments as $payment) { ?>
            <tr>
              <td><?php echo date("m/d/Y", strtotime($payment->payment_date)); ?></td>
              <td class="text-center"><?php echo $payment->receipt_number; ?></td>
              <td class="text-right"><?php echo number_format($payment->amount,2); $total_payments += $payment->amount; ?></td>
            </tr>
            <?php } ?>
<tr class="warning">
              <td class="bold" colspan="2">TOTAL PAYMENTS</td>
              <td class="text-right bold"><?php echo number_format($total_payments,2); ?></td>
            </tr>
          </tbody>
        </table>
<?php 
  }
} else { ?> 

  <p class="text-center">Not enrolled!</p>

<?php } ?>


			</div>
		</div>
	</div>

<?php } ?>
</div>
  
<?php $this->load->view('footer'); ?>
