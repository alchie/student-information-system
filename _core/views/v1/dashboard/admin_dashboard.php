<?php $this->load->view('header'); 
$level_names  = unserialize(LEVEL_NAMES);
?>

<div class="row">
<?php if(isset($primary_school_year)) { ?>
<div class="col-sm-12 col-md-8">
        <div class="panel panel-default">
        	<div class="panel-heading" style="padding-bottom:0"><h4><?php echo $this->session->current_school_year_data->label; ?></h4></div>
   			<div class="panel-body" style="">
<form action="<?php echo site_url("enrollment"); ?>" class="navbar-form navbar-left" method="get" role="search" style="margin:10px 0 50px;">
              <div class="input-group">
                <input class="form-control" placeholder="Search Enrolled Student in <?php echo $this->session->current_school_year_data->label; ?>" name="q" id="srch-term" type="text" value="<?php echo $this->input->get('q'); ?>">
                <div class="input-group-btn"><button class="btn btn-default btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button></div>
              </div>
          </form>

<?php 

$sidebar_menus = array( 
	array('title' => "Students Information","icon"=>"fa fa-graduation-cap","uri"=>"students"),
	array('title' => "Finance Information","icon"=>"glyphicon glyphicon-usd","uri"=>"finance"),
	array('title' => "Parents Information","icon"=>"fa fa-users","uri"=>"parents"),
	array('title' => "Enrollment Information","icon"=>"glyphicon glyphicon-list","uri"=>"enrollment"),
	array('title' => "School Information","icon"=>"glyphicon glyphicon-flag","uri"=>"school"),
	array('title' => "School Year","uri"=>"school_year","icon"=>"glyphicon glyphicon-calendar"),
	array('title' => "Administration","icon"=>"glyphicon glyphicon-cog","uri"=>"user_accounts"),
	array('title' => "Campuses","uri"=>"campuses","icon"=>"glyphicon glyphicon-home"),
	array('title' => "Sections","uri"=>"sections","icon"=>"glyphicon glyphicon-th-large"),
	array('title' => "Subjects","uri"=>"subjects","icon"=>"glyphicon glyphicon-bookmark"),
	array('title' => "School Fees","uri"=>"fees","icon"=>"glyphicon glyphicon-usd"),
	array('title' => "Services","uri"=>"services","icon"=>"glyphicon glyphicon-glass"),
);
if( file_exists( "sync.php" ) && ($_SERVER['HTTP_HOST'] != K12MS_SYNC_SERVER_URI)) { 
 // $sidebar_menus[] = array('title'=>'Sync Online',"uri"=>"sync","icon"=>"glyphicon glyphicon-transfer"); 
}
?>
<ul class="dashboard text-center" style="margin-bottom: 20px;">
<?php foreach( $sidebar_menus as $menu ) {

  if( !isset( $this->session->user_permissions->$menu['uri'] ) || ($this->session->user_permissions->$menu['uri']->view==0) ) {
    continue;
  }

	?>
  <li class="item"><a href="<?php echo site_url($menu['uri']); ?>"><span class="<?php echo $menu['icon']; ?>"></span> <span class="title"><?php echo $menu['title']; ?></span>
  </a></li>
<?php } ?>
</ul>

   			</div>
   		</div>
</div>


<div class="col-sm-12 col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0"><h4>Statistics</h4></div>
   			<div class="panel-body" style="padding-top:0">
				<table class="table table-striped">
					 <thead>
                      <tr class="headings">
						  <th>Grade Level</th>
						  <?php 
						  $grand_total_enrollees = 0;
						  $total_enrollees = array();
						  foreach($campuses as $campus) { 
						  	$total_enrollees[$campus->id] = 0;
						  	?>
						 	 <th class="text-right" width="20%"><?php echo $campus->name; ?></th>
						  <?php } ?>
						  <th class="text-right" width="20%">Total</th>
                      </tr>
                    </thead>
					<tbody>
					<?php 
					foreach( $grade_levels as $level ) { ?>
						<tr class="">
							<td style="padding: 2px 8px;"><?php echo $level_names[$level->level]; ?></td>
							<?php 
							foreach($campuses as $campus) { 
							$var = 'total_enrollees_' . $campus->id;
							?>
							<td style="padding: 2px 8px;" align="right">
							<?php if( $level->$var ) { ?>
								<a href="<?php echo site_url("enrollment/index/{$this->session->current_school_year}/{$level->level}/{$campus->id}"); ?>">
							<?php echo $level->$var; 
								$total_enrollees[$campus->id]+=$level->$var;
							?>
								</a>
								<?php } ?>
							</td>
							<?php } ?>
							<td style="padding: 2px 8px;" align="right">
								<a href="<?php echo site_url("enrollment/index/{$primary_school_year->id}/{$level->level}/0"); ?>">
							<?php echo $level->total_enrollees; $grand_total_enrollees+=$level->total_enrollees; ?>
								</a>
							</td>
						</tr>
					<?php } ?>
						<tr class="success">
							<td><strong>TOTAL ENROLLEES</strong></td>
							<?php 
							foreach($campuses as $campus) { ?>
								<td align="right"><strong><?php echo $total_enrollees[$campus->id]; ?></strong></td>
							<?php } ?>
							<td align="right"><strong><?php echo $grand_total_enrollees; ?></strong></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<?php } ?>
</div>
  
<?php $this->load->view('footer'); ?>
