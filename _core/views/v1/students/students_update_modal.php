<!--Address modal-->
<div style="display: none;" id="addAddressModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Address</strong>
      </div>
	 <?php echo form_open("students/address/" . $student->id . "/add"); ?>
      <div class="modal-body">
         <div class="form-group">
			<label for="address">Address</label>
			<textarea name="address" class="form-control" id="address" placeholder="Address" rows="5"></textarea>
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>
<div style="display: none;" id="updateAddressModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Update Address</strong>
      </div>
	 <?php echo form_open("students/address/" . $student->id . "/update"); ?>
      <div class="modal-body">
         <div class="form-group">
			<label for="address">Address</label>
			<textarea name="address" class="form-control" id="address" placeholder="Address" rows="5"></textarea>
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>

<!--contact numbers modal-->
<div style="display: none;" id="addContactModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Contact</strong>
      </div>
	 <?php echo form_open("students/contact_numbers/" . $student->id ); ?>
      <div class="modal-body">
         <div class="form-group">
			<label for="address">Contact Number</label>
			<input type="text" name="number" class="form-control" id="number" placeholder="Contact Number">
		  </div>
		  <div class="form-group">
			<label for="remarks">Remarks</label>
			<input type="text" name="remarks" class="form-control" id="remarks" placeholder="Remarks">
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>
<!--medical info modal-->
<div style="display: none;" id="addMedicalInfoModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Medical Info</strong>
      </div>
	 <?php echo form_open("students/medical_info/" . $student->id . "/add"); ?>
      <div class="modal-body">
         <div class="form-group">
			<label for="info">Medical Info</label>
			<textarea name="info" class="form-control" id="info" placeholder="Medical Info" rows="5"></textarea>
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>
<!--parents modal-->
<div style="display: none;" id="assignParentsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Assign Parent</strong>
      </div>
	 <?php echo form_open("students/assign_parent/" . $student->id, array("method"=>"get")); ?>
      <div class="modal-body">
         <div class="input-group">
		  <input type="text" class="form-control" placeholder="Search Parent Name" name="name">
		  <span class="input-group-btn">
			<button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		  </span>
		</div><!-- /input-group -->
      </div>
      <!--
	  <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  -->
	  </form>
  </div>
  </div>
</div>

<!--updateParentRelationshipModal modal-->
<div style="display: none;" id="updateParentRelationshipModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Assign Parent</strong>
      </div>
	 <?php echo form_open("students/assign_parent/" . $student->id, array("method"=>"get")); ?>
      <div class="modal-body">
         <div class="input-group">
		  <input type="text" class="form-control" placeholder="Search Parent Name" name="name">
		  <span class="input-group-btn">
			<button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		  </span>
		</div><!-- /input-group -->
      </div>
      <!--
	  <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  -->
	  </form>
  </div>
  </div>
</div>

<!--Emergency modal-->
<div style="display: none;" id="updateEmergencyModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Change Emergency Contact</strong>
      </div>
	 <?php echo form_open("students/meta/" . $student->id); ?>
      <div class="modal-body">
			<input type="hidden" value="<?php echo $student->emergency; ?>" name="id">
			<input type="hidden" value="emergency" name="key">
		 	<div class="form-group">
				<select class="form-control" name="value">
					<option value="">-- Select Parent --</option>
					<?php foreach( $parents as $parent ) { ?>
						<option <?php echo ($student->emergency == $parent->sp_id) ? 'selected' : ''; ?> value="<?php echo $parent->sp_id; ?>"><?php echo $parent->lastname; ?>, <?php echo $parent->firstname; ?> (<?php echo ucwords( $parent->relationship ); ?>)</option>
					<?php } ?>
				</select>
			 </div>
		 
      </div>
  
	  <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>

	  </form>
  </div>
  </div>
</div>

<!--Confirm Delete modal-->
<div style="display: none;" id="confirmDeleteModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Confirm Delete Student</strong>
      </div>
   <?php echo form_open("students/delete/" . $student->id); ?>
      <div class="modal-body">
      <input type="hidden" value="<?php echo $student->id; ?>" name="id">
      Other details related to this student will be <strong>DELETE</strong> as well, including (but not limited to) the following:
      <ul>
        <li>Emergency Contact</li>
        <li>Addresses</li>
        <li>Contact Numbers</li>
        <li>Medical Information</li>
        <li>Enrollment History</li>
      </ul>
      <input type="text" class="form-control" placeholder="Type `YES` here to confirm delete" name="confirmed">


      </div>
  
    <div class="modal-footer">
          <button type="submit" class="btn btn-danger">PROCEED</button>
      </div>

    </form>
  </div>
  </div>
</div>

<!--studentPicturesModal modal-->
<div style="display: none;" id="profilePicModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Change Profile Picture</strong>
      </div>
      <div class="modal-body text-center">
     
     <?php 

$pic_url = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTlhYThkMDc2MyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1OWFhOGQwNzYzIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQ1LjUiIHk9Ijc0LjUiPjE0MHgxNDA8L3RleHQ+PC9nPjwvZz48L3N2Zz4=';

  if( $profile_pic ) { 
    $pic_url = base_url("uploads/{$profile_pic->meta_value}");
   }
    ?>
  <img src="<?php echo $pic_url; ?>" alt="Profile Pic" class="img-thumbnail">

      </div>
  
    <div class="modal-footer" style="border-top: 1px solid #e5e5e5;">
      <form method="post" enctype="multipart/form-data" action="<?php echo site_url("students/upload_picture/{$student->id}"); ?>">
      <div class="row">
        <div class="col-md-6"><input type="file" name="picture"></div>
        <div class="col-md-6"> <button type="submit" class="btn btn-success btn-xs">Upload</button></div>
      </div>
        
         
      </form>
      </div>

  </div>
  </div>
</div>