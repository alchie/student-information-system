<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-6 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Add New Student</h4></div>
 <?php echo form_open("students/add", array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
                  
<?php

	$forms = array(
		'idn' => array("title"=>"Student ID", 'type'=>"text", "required"=>false,'default'=>$this->input->post('idn'), 'attributes'=>array('autofocus'=>"autofocus")),
		'lastname' => array("title"=>"Last Name", 'type'=>"text", "required"=>true,'default'=>$this->input->post('lastname')),
		'firstname' => array("title"=>"First Name", 'type'=>"text", "required"=>true,'default'=>$this->input->post('firstname')),
		'middlename' => array("title"=>"Middle Name", 'type'=>"text", "required"=>false,'default'=>$this->input->post('middlename')),
		'birthday' => array("title"=>"Birthday", 'type'=>"datepicker", "required"=>false,'default'=>$this->input->post('birthday')),
		'birthplace' => array("title"=>"Birth Place", 'type'=>"text", "required"=>false,'default'=>$this->input->post('birthplace')),
		'religion' => array("title"=>"Religion", 'type'=>"text", "required"=>false,'default'=>$this->input->post('religion')),
		'gender' => array("title"=>"Gender", 'type'=>"radio_multiline", "required"=>false,'options'=>array("m"=>"Male","f"=>"Female"),'default'=>$this->input->post('gender')),
		'lrn' => array("title"=>"LRN", 'type'=>"text", "required"=>false,'default'=>$this->input->post('lrn')),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				

				
            </div>
            <div class="panel-footer">
            		<div class="">
                      
				        <button type="submit" class="btn btn-success">Update <i class="fa fa-save"></i></button>
						<a href="javascript:history.back(-1);" class="btn btn-warning"><i class="fa fa-arrow-left"></i></i> Back</a>
				            
				    </div>
            </div>
            </form>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>
