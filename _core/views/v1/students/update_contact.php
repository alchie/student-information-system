<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
<?php if( $contact->primary_contact != $contact->id ) { ?>
		  <a onclick="javascript:if(confirm('Are you sure?')){}else{return false;};" href="<?php echo site_url("students/update_contact/" . $student_id . "/" . $contact_id  ); ?>?action=set_primary" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i> Set as Primary</a>
<?php } ?>
		  <h4>Update Contact Number</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

<?php echo form_open("students/update_contact/" . $student_id . "/" . $contact_id ); ?>
<div class="form-group">
	<label>Contact Number</label>
	<input type="text" class="form-control" name="number" placeholder="Contact Number" value="<?php echo $contact->contact; ?>">
</div>
<div class="form-group">
	<label>Remarks</label>
	<input type="text" class="form-control" name="remarks" placeholder="Remarks" value="<?php echo $contact->remarks; ?>">
</div>
 
	</div> 
<div class="panel-footer">
<a href="<?php echo site_url("students/update_contact/" . $student_id . "/" . $contact_id  ); ?>?action=delete" class="pull-right btn btn-danger btn-xs confirm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
	<div class="form-group">
                      
        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
		<a href="<?php echo site_url("students/update/" . $student_id ); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
            
    </div>
</div>
                  
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>
