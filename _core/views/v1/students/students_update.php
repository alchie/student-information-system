<?php $this->load->view('header'); 
$level_names = unserialize(LEVEL_NAMES);
?>

<?php $this->load->view('students/students_navbar'); ?>

<div class="row">
	<div class="col-sm-6 col-md-6 <?php echo ( $student->idn == '' ) ? "col-md-offset-3" : ""; ?>">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a  role="button" data-toggle="modal" href="#confirmDeleteModal" class="pull-right btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete Student</a>
			<h4>Update Student</h4>
		  </div>
   			<div class="panel-body">

<div class="row">

<?php if( $student->idn != '' ) { ?>
	<div class="col-md-3">
	<a role="button" data-toggle="modal" href="#profilePicModal">
	
	<?php 

$pic_url = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTlhYThkMDc2MyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1OWFhOGQwNzYzIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQ1LjUiIHk9Ijc0LjUiPjE0MHgxNDA8L3RleHQ+PC9nPjwvZz48L3N2Zz4=';

	if( $profile_pic ) { 
		$pic_url = base_url("uploads/{$profile_pic->meta_value}");
	 }
	  ?>
	<img src="<?php echo $pic_url; ?>" alt="Profile Pic" class="img-circle" style="min-height: 140px;min-width: 140px">
	</a>
	</div>
<?php } ?>

	<div class="<?php echo ( $student->idn == '' ) ? "col-md-12" : "col-md-9"; ?>">

	<?php echo form_open("students/update/" . $student->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

	$forms = array(
		'idn' => array("title"=>"Student ID", 'type'=>"text", "required"=>true, "default"=>$student->idn),
		'lastname' => array("title"=>"Last Name", 'type'=>"text", "required"=>true, "default"=>$student->lastname),
		'firstname' => array("title"=>"First Name", 'type'=>"text", "required"=>true, "default"=>$student->firstname),
		'middlename' => array("title"=>"Middle Name", 'type'=>"text", "required"=>false, "default"=>$student->middlename),
		'birthday' => array("title"=>"Birthday", 'type'=>"datepicker", "required"=>false, "default"=> date("m/d/Y", strtotime($student->birthday))),
		'birthplace' => array("title"=>"Birth Place", 'type'=>"text", "required"=>false, "default"=>$student->birthplace),
		'religion' => array("title"=>"Religion", 'type'=>"text", "required"=>false, "default"=>$student->religion),
		'gender' => array("title"=>"Gender", 'type'=>"radio_multiline", "required"=>false,'options'=>array("m"=>"Male","f"=>"Female"), "default"=>$student->gender),
		'lrn' => array("title"=>"LRN", 'type'=>"text", "required"=>false, "default"=>$student->lrn),
	);
	
	foreach($forms as $key=>$form ) {
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	}
	
?>
				
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Update <i class="fa fa-save"></i></button>
                      </div>
                    </div>
					 </form>
			</div>
</div>


            </div>
   		</div>
<?php if( $student->idn != '' ) { ?>
<?php echo form_open("students/update_google/" . $student->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>

		<div class="panel panel-default">
          <div class="panel-heading">
<button type="submit" class="btn btn-success btn-xs pull-right">Update <i class="fa fa-save"></i></button>
          <h4>Google Account</h4></div>
   			<div class="panel-body" style="padding-top:0">

<?php

	$forms = array(
		'google_id' => array("title"=>"Google ID", 'type'=>"text", "required"=>false, "default"=>$student->google_id),
		'google_userid' => array("title"=>"Google User ID", 'type'=>"text", "required"=>false, "default"=>$student->google_userid, 'description' => (($student->google_userid=="") ? '<strong>Suggestion:</strong> ' . ams_slugify(url_title($student->lastname . " " . ams_initials($student->firstname)), "_", true) . "@slcd.edu.ph" : "") ),
	);
	
	foreach($forms as $key=>$form ) {
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'], ((isset($form['description']))?$form['description']:'') ); 
	}
	
?>					 

			</div>
		</div>
</form>
<?php } ?>

	</div>
	
<?php if( $student->idn != '' ) { ?>
	<div class="col-sm-6 col-md-6">


		<div class="panel panel-default">
          <div class="panel-heading">
          <h4>Enrollment History</h4></div>
   			<div class="panel-body">
				<div class="list-group">
				<?php 
				foreach( $enrollment_history as $history ) { ?>
				  <a href="<?php echo site_url('enrollment/update/' . $history->id ); ?>" class="list-group-item">
				  <?php echo ($history->section_name!='') ? '<span class="badge">'.$history->section_name.'</span>' : ''; ?>
				  <strong><?php echo $history->sy_label; ?> &middot; <?php echo $level_names[$history->grade_level]; ?></strong>
				  </a>
				<?php } ?>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
          <div class="panel-heading"><a role="button" data-toggle="modal" href="#assignParentsModal" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i> Assign</a><h4>Parents and Guardians</h4></div>
   			<div class="panel-body">
				<div class="list-group">
				<?php foreach( $parents as $parent ) { ?>
				  <a href="<?php echo site_url("students/update_parent/" . $student->id . "/" . $parent->sp_id ); ?>" class="list-group-item">
				  <span class="badge"><?php echo ucwords( $parent->relationship ); ?></span>
				  <?php echo $parent->lastname; ?>, <?php echo $parent->firstname; ?></a>
				<?php } ?>
				</div>
			</div>
		</div>
	
		<div class="panel panel-default">
          <div class="panel-heading"><a role="button" data-toggle="modal" href="#updateEmergencyModal" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-pencil"></i> Change</a><h4>In case of emergency</h4></div>
   			<div class="panel-body">
			<?php if( $emergency ) { ?>
				<h4><?php echo $emergency->lastname; ?>, <?php echo $emergency->firstname; ?> <small><?php echo ucwords( $emergency->relationship ); ?></small></h4>
				<p><strong>Address:</strong> <?php echo $emergency->primary_address; ?></p>
				<p><strong>Contact Number:</strong> <?php echo $emergency->primary_contact_number; ?> (<?php echo $emergency->primary_contact_remarks; ?>)</p>
			<?php } ?>
			</div>
		</div>

        <div class="panel panel-default">
          <div class="panel-heading"><a role="button" data-toggle="modal" href="#addAddressModal" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i> Add</a><h4> Address </h4></div>
   			<div class="panel-body">
				<div class="list-group">
				<?php foreach( $addresses as $address ) { ?>
				  <a href="<?php echo site_url("students/update_address/" . $student->id . "/" . $address->id ); ?>" class="list-group-item">
				  <?php echo $address->address; ?></a>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="panel panel-default">
          <div class="panel-heading">
		  <a role="button" data-toggle="modal" href="#addContactModal" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i> Add</a>
		  <h4>Contact Numbers</h4>
		  </div>
   			<div class="panel-body">
				<div class="list-group">
				<?php foreach( $contacts as $contact ) { ?>
				  <a href="<?php echo site_url("students/update_contact/" . $student->id . "/" . $contact->id ); ?>" class="list-group-item">
				  <span class="badge"><?php echo $contact->remarks; ?></span><?php echo $contact->contact; ?></a>
				<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="panel panel-default">
          <div class="panel-heading">
          <a role="button" data-toggle="modal" href="#addMedicalInfoModal" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i> Add</a><h4>Medical Information</h4></div>
   			<div class="panel-body">
				<div class="list-group">
				<?php foreach( $medicals as $medical ) { ?>
					<a href="<?php echo site_url("students/update_medical/" . $student->id . "/" . $medical->id ); ?>" class="list-group-item">
						<?php echo $medical->info; ?>
					</a>
				<?php } ?>
				</div>
			</div>
		</div>
		
	</div>
	<?php } ?>
	
</div>


<?php $this->load->view('students/students_update_modal'); ?>
<?php $this->load->view('footer'); ?>
