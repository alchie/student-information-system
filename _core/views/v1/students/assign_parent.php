<?php $this->load->view('header'); ?>

<?php $this->load->view('students/students_navbar'); ?>

<div class="row">
	<div class="col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">

<a href="<?php echo site_url("parents/add"); ?>?next=<?php echo urlencode( "students/assign_parent/{$student->id}" . "/\$new_id"); ?>" class="btn btn-success btn-xs pull-right"><i class="fa fa-plus"></i> Add Parent</a>

          <h4>Assign Parent of: <?php echo $student->lastname; ?>, <?php echo $student->firstname; ?> (<?php echo $student->idn; ?>)</h4>

          </div>
   			<div class="panel-body">

<?php if(isset($current_parent)) { ?>

<form method="post">
<div class="list-group">
	<div class="list-group-item">
		<input type="hidden" name="parent_id" value="<?php echo $current_parent->id; ?>"> 
		<strong><?php echo $current_parent->lastname; ?>, <?php echo $current_parent->firstname; ?></strong>
	</div>
	<div class="list-group-item">
		<label>Relationship</label>
		<select name="relationship" class="form-control">
			<option value="">-- Select a Relationship --</option>
			<option value="father">Father</option>
			<option value="mother">Mother</option>
			<option value="sibling">Sibling</option>
			<option value="guardian">Guardian</option>
		</select>
	</div> 
</div> 
	
	<div class="form-group">
                      
        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
		<a href="<?php echo site_url("students/update/" . $student->id ); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
            
    </div>
    
</form>

<?php } else { ?>

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

	<?php echo form_open("students/assign_parent/" . $student->id, array("method"=>"get")); ?>
	<div class="form-group">
         <div class="input-group">
		  <input type="text" class="form-control" placeholder="Search Parent Name" name="name" value="<?php echo $this->input->get("name"); ?>">
		  <span class="input-group-btn">
			<button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		  </span>
		</div><!-- /input-group -->
	</div>
	</form>
	
	  </div>




<?php if($parents) { ?>

<form method="post">
<div class="panel-body">


				<label>Select a Parent</label>
	<div class="list-group">
	<?php foreach( $parents as $parent ) { ?>
		<label class="list-group-item">
		 <div class="checkbox">
		<input type="radio" name="parent_id" value="<?php echo $parent->id; ?>"> <?php echo $parent->lastname; ?>, <?php echo $parent->firstname; ?>
		</div>
		</label>
		
	<?php } ?>
	

<div class="list-group-item">
	<label>Relationship</label>
	<select name="relationship" class="form-control">
		<option value="">-- Select a Relationship --</option>
		<option value="father">Father</option>
		<option value="mother">Mother</option>
		<option value="sibling">Sibling</option>
		<option value="guardian">Guardian</option>
	</select>
</div> 
</div> 

</div>
			
<div class="panel-footer">
	<div class="form-group">
                      
        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
		<a href="<?php echo site_url("students/update/" . $student->id ); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
            
    </div>
</div>
</form>
<?php } ?>

<?php } ?>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>
