<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
<?php if( $address->primary_address != $address->id ) { ?>
		  <a onclick="javascript:if(confirm('Are you sure?')){}else{return false;};" href="<?php echo site_url("students/update_address/" . $student_id . "/" . $address_id  ); ?>?action=set_primary" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i> Set as Primary</a>
<?php } ?>
		  <h4>Update Address</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

<?php echo form_open("students/update_address/" . $student_id . "/" . $address_id ); ?>
<div class="form-group">
	<label>Address</label>
	<textarea class="form-control" rows="10" name="address" placeholder="Address"><?php echo $address->address; ?></textarea>
</div>

 
	</div> 
<div class="panel-footer">
<a href="<?php echo site_url("students/update_address/" . $student_id . "/" . $address_id  ); ?>?action=delete" class="pull-right btn btn-danger btn-xs confirm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
	<div class="form-group">
                      
        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
		<a href="<?php echo site_url("students/update/" . $student_id ); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
            
    </div>
</div>
                  
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>
