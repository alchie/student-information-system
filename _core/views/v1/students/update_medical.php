<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Update Medical</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

<?php echo form_open("students/update_medical/" . $student_id . "/" . $medical_id ); ?>
<div class="form-group">
	<label>Medical Info</label>
	<textarea class="form-control" rows="10" name="info" placeholder="Medical Info"><?php echo $medical->info; ?></textarea>
</div>

 
	</div> 
<div class="panel-footer">
<a href="<?php echo site_url("students/update_medical/" . $student_id . "/" . $medical_id  ); ?>?action=delete" class="pull-right btn btn-danger btn-xs confirm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
	<div class="form-group">
                      
        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
		<a href="<?php echo site_url("students/update/" . $student_id ); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
            
    </div>
</div>
                  
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>
