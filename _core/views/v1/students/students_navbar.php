<nav class="navbar navbar-default navbar-student">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand"><?php echo $student->lastname; ?>, <?php echo $student->firstname; ?> <?php echo ($student->middlename) ? substr($student->middlename, 0,1)."." : ""; ?> <sup><?php echo $student->idn; ?></sup></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="<?php echo site_url("students/profile/{$student->id}"); ?>">Student Profile</a></li>
        <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Account Ledgers <span class="caret"></span></a>
          <ul class="dropdown-menu">
          <?php foreach($enrollment_history as $enroll) { ?>
            <li><a href="<?php echo site_url("finance/ledger/{$enroll->school_year}/{$enroll->id}"); ?>"><?php echo $enroll->sy_label; ?></a></li>
          <?php } ?>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<?php if( $previous_student ) { ?>
        <a id="navArrowPrevious" class="fixed_arrow previous" href="<?php echo site_url("students/" . ((isset($method_name)) ? $method_name : 'profile') . "/{$previous_student->student_id}"); ?>"><i class="glyphicon glyphicon-chevron-left"></i></a>
<?php } ?>
<?php if( $next_student ) { ?>
         <a id="navArrowNext" class="fixed_arrow next" href="<?php echo site_url("students/" . ((isset($method_name)) ? $method_name : 'profile') . "/{$next_student->student_id}"); ?>"><i class="glyphicon glyphicon-chevron-right"></i></a>
<?php } ?>