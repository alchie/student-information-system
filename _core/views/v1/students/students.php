<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><a href="<?php echo site_url('students/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add New Student</a> <h4>Students List <small>(TOTAL: <?php echo $students_count; ?>)</small>
		  <?php echo ($this->input->get("q")) ? "<small>Search for \"<strong>" . $this->input->get("q") . "</strong>\" <a href='".site_url("students")."'>Clear Filter</a></small>" : ""; ?>
		  </h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
				<thead>
				<tr>
				  <th>ID Number</th>
				  <th>Last Name</th>
				  <th>First Name</th>
				  <th>Middle Name</th>
				  <th width="5%">Action</th>
				</tr>
			  </thead>
				<tbody>
				<?php foreach( $students as $student ) { ?>
					<tr>
						<td><?php echo $student->idn; ?></td>
						<td><?php echo $student->lastname; ?></td>
						<td><?php echo $student->firstname; ?></td>
						<td><?php echo $student->middlename; ?></td>
						<td><a class="btn btn-warning btn-xs" href="<?php echo site_url("students/profile/" . $student->id); ?>">Show Profile</a></td>
					</tr>
				<?php } ?>
				<tbody>
              </table>
			  <center>
					<?php echo $pagination; ?>
			  </center>
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
