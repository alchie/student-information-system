<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
<?php if( $sp->emergency != $sp->sp_id ) { ?>
		  <a onclick="javascript:if(confirm('Are you sure?')){}else{return false;};" href="<?php echo site_url("students/update_parent/" . $student->id . "/" . $sp->sp_id  ); ?>?action=set_emergency" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-check"></i> Set as Emergency</a>
<?php } ?>
		  <h4>Student: <?php echo $student->lastname; ?>, <?php echo $student->firstname; ?></h4></div>
   			<div class="panel-body">
<h4><?php echo $sp->lastname; ?>, <?php echo $sp->firstname; ?> <small><?php echo ucwords( $sp->relationship ); ?></small></h4>

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

<?php echo form_open("students/update_parent/" . $student->id . "/" . $sp->sp_id ); ?>

<div class="list-group-item">
	<label>Relationship</label>
	<select name="relationship" class="form-control">
		<option value="">-- Select a Relationship --</option>
		<option <?php echo ($sp->relationship=='father') ? 'selected': ''; ?> value="father">Father</option>
		<option <?php echo ($sp->relationship=='mother') ? 'selected': ''; ?> value="mother">Mother</option>
		<option <?php echo ($sp->relationship=='sibling') ? 'selected': ''; ?> value="sibling">Sibling</option>
		<option <?php echo ($sp->relationship=='guardian') ? 'selected': ''; ?> value="guardian">Guardian</option>
	</select>
</div> 
</div> 
<div class="panel-footer">
<a href="<?php echo site_url("students/update_parent/" . $student->id . "/" . $sp->sp_id ); ?>?action=delete" class="pull-right btn btn-danger btn-xs confirm"><i class="glyphicon glyphicon-trash"></i> Delete Assignment</a>
	<div class="form-group">
                      
        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
		<a href="<?php echo site_url("students/update/" . $student->id ); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
            
    </div>
</div>
                  
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>
