<!DOCTYPE html>
<html lang="en"><head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="<?php echo base_url("assets/v1/css/bootstrap.css"); ?>" rel="stylesheet">
        
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
		<link href="<?php echo base_url("assets/v1/css/login.css"); ?>" rel="stylesheet">
		
    </head>
    
    <body class="">
        
        <div class="container">
    <div class="row" style="margin-top:100px;">
        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <img class="profile-img" src="<?php echo base_url("assets/v1/img/logo160x160.png"); ?>"
                    alt="">
					
                <form class="form-signin" method="post">
				
<?php if( isset( $error ) ) { ?>
			 <span class="alert alert-danger"><?php echo $error; ?></span>
<?php } ?>
                <input name="username" type="text" class="form-control" placeholder="Username" required autofocus>
                <input name="password" type="password" class="form-control" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                </form>
            </div>
        </div>
    </div>
</div>
    
</body></html>