<?php $this->load->view('header'); 
$level_names  = unserialize(LEVEL_NAMES);
?>

<div class="row">
	<div class="col-xs-12 col-sm-4 col-md-4 col-md-offset-4">
       
               <div class="panel panel-default">
          <div class="panel-heading" style="padding-bottom:0">

          <h4>Change Password</h4></div>

<form method="post">
   			<div class="panel-body" style="padding-top:0">

   				<?php echo (validation_errors()) ? "<div class='alert alert-danger'>" . validation_errors() . "</div>" : ''; ?>
   				<?php echo ($this->input->get('success')) ? "<div class='alert alert-success'>Successfully Changed Password!</div>" : ''; ?>
   				<div class="form-group">
   					<label>Current Password</label>
   					<input type="password" class="form-control" name="current_password" value="">
   				</div>
   				<div class="form-group">
   					<label>New Password</label>
   					<input type="password" class="form-control" name="new_password" value="">
   				</div>
   				<div class="form-group">
   					<label>Repeat New Password</label>
   					<input type="password" class="form-control" name="new_password2" value="">
   				</div>
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
</form>
		</div>

	</div>
</div>
  
<?php $this->load->view('footer'); ?>
