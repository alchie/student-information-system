<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a href="<?php echo site_url('services/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add New Service</a> <h4>School Services</h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                     <thead>
                      <tr class="headings">
                        <th>Section Name</th>
                        <th>Status</th>
                        <th width="70px"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $services as $service ): ?>
                      <tr class="<?php echo ($service->active==1) ? "" : "danger"; ?>">
                        <td><?php echo $service->name; ?></td>
                        <td><?php echo ($service->active==1) ? "Active" : "Disabled"; ?></td>
                        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("services/update/" . $service->id); ?>">Update</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>
            </div>
   		</div>

	</div>
</div>
  
<?php $this->load->view('footer'); ?>
