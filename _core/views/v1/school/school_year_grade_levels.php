<?php $this->load->view('header'); 

	$grade_levels = array(
					'K1' => 'Kinder 1',
					'K2' => 'Kinder 2',
					'G1' => 'Grade 1',
					'G2' => 'Grade 2',
					'G3' => 'Grade 3',
					'G4' => 'Grade 4',
					'G5' => 'Grade 5',
					'G6' => 'Grade 6',
					'G7' => 'Grade 7',
					'G8' => 'Grade 8',
					'G9' => 'Grade 9',
					'G10' => 'Grade 10',
					'G11' => 'Grade 11',
					'G12' => 'Grade 12',
				);


?>
<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a role="button" data-toggle="modal" href="#addGradeLevelModal" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add Grade Level</a>
		  <a href="<?php echo site_url("school_year" ); ?>" class="pull-right btn btn-warning btn-sm" style="margin-right:5px;"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
		  <h4><?php echo $school_year->label; ?> - Grade Levels</h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                    <thead>
                      <tr class="headings">
                        <th>Grade Level </th>
                        <th class="text-right">Total Fees</th>
                        <th width="245px"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
					<?php 
					$added = array();
					foreach( $levels as $level ): $added[] = $level->level; ?>
                      <tr>
                        <td><?php echo $grade_levels[$level->level]; ?></td>
                        <td class="text-right"><?php echo number_format($level->total_fees,2); ?></td>
                        <td>
                        <a class="btn btn-danger btn-xs confirm" href="<?php echo site_url("school_year/grade_levels/" . $school_year->id . "/delete/" . $level->id); ?>">Delete</a>
						<a class="btn btn-success btn-xs" href="<?php echo site_url("school_year/sections/" . $school_year->id . "/" . $level->id); ?>">Assign Sections</a>
						<a class="btn btn-primary btn-xs" href="<?php echo site_url("school_year/fees/" . $school_year->id . "/" . $level->id); ?>">Assign Fees</a>
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
			  
            </div>
   		</div>

	</div>
</div>

<!--Grade Levels modal-->
<div style="display: none;" id="addGradeLevelModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Grade Level</strong>
      </div>
	 <?php echo form_open("school_year/grade_levels/" . $school_year->id . '/add' ); ?>
      <div class="modal-body">
         <div class="form-group">
			<select class="form-control" name="grade_level">
				<?php 
				foreach( $grade_levels as $lvl => $level ) { 
					echo ( !in_array($lvl, $added ) ) ? "<option value=\"{$lvl}\">{$level}</option>" : ''; 
				 } 
				 ?>
			</select>
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>
