<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
      <a  href="<?php echo site_url('fees/groups/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add New Group</a> 
		  <a href="<?php echo site_url("fees" ); ?>" class="pull-right btn btn-warning btn-sm" style="margin-right:5px;"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
      <h4>School Fees Groups</h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                     <thead>
                      <tr class="headings">
                        <th>Group Name</th>
                        <th width="70px"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $groups as $group ): ?>
                      <tr>
                        <td><?php echo $group->name; ?></td>
                        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("fees/groups/update/" . $group->id); ?>">Update</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
