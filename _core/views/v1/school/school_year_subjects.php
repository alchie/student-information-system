<?php $this->load->view('header'); ?>
<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a role="button" data-toggle="modal" href="#addSubjectModal" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add Subject</a>
		  <a href="<?php echo site_url("school_year" ); ?>" class="pull-right btn btn-warning btn-sm" style="margin-right:5px;"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
		  <h4><?php echo $school_year->label; ?> - Subjects</h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                    <thead>
                      <tr class="headings">
                        <th>Subjects</th>
                        <th width="60px"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
					<?php 
					$added = array();
					foreach( $sy_subjects as $sy_subject ): $added[] = $sy_subject->subject_id; ?>
                      <tr>
                        <td><?php echo $sy_subject->title; ?></td>
                        <td><a class="btn btn-danger btn-xs confirm" href="<?php echo site_url("school_year/subjects/" . $school_year->id . "/delete/" . $sy_subject->id); ?>">Delete</a></td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
			  
            </div>
   		</div>

	</div>
</div>

<!--Grade Levels modal-->
<div style="display: none;" id="addSubjectModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Subject</strong>
      </div>
	 <?php echo form_open("school_year/subjects/" . $school_year->id . '/add' ); ?>
      <div class="modal-body">
         <div class="form-group">
			<select class="form-control" name="subject_id">
				<?php 
				foreach( $subjects as $subject ) { 
					echo ( !in_array($subject->id, $added ) ) ? "<option value=\"{$subject->id}\">{$subject->title}</option>" : ''; 
				 } 
				 ?>
			</select>
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>
