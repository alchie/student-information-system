<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a href="<?php echo site_url('sections/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add New Section</a> <h4>School Year</h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                     <thead>
                      <tr class="headings">
                        <th>Section Name</th>
                        <th>Status</th>
                        <th width="70px"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $sections as $section ): ?>
                      <tr class="<?php echo ($section->active==1) ? "" : "danger"; ?>">
                        <td><?php echo $section->name; ?></td>
                        <td><?php echo ($section->active==1) ? "Active" : "Disabled"; ?></td>
                        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("sections/update/" . $section->id); ?>">Update</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
