<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a href="<?php echo site_url('subjects/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add New Subject</a> <h4>School Year</h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                     <thead>
                      <tr class="headings">
                        <th>Subject Title</th>
                        <th>Status</th>
                        <th width="70px"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $subjects as $subject ): ?>
                      <tr class="<?php echo ($subject->active==1) ? "" : "danger"; ?>">
                        <td><?php echo $subject->title; ?></td>
                        <td><?php echo ($subject->active==1) ? "Active" : "Disabled"; ?></td>
                        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("subjects/update/" . $subject->id); ?>">Update</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
