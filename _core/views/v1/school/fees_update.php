<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
<a  style="margin-right:5px;" href="<?php echo site_url('fees/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add New Fee</a> 
          <h4>Update Fee</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
<?php echo form_open("fees/update/" . $fee->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<input type="hidden" name="redirect_to" value="<?php echo ams_referrer("fees", uri_string()); ?>">
<?php

  $options = array();
  foreach($groups as $group) {
    $options[$group->id] = $group->name;
  }
	$forms = array(
    'name' => array("title"=>"Fee Name", 'type'=>"text", "required"=>true, "default"=>$fee->name),
    'group_id' => array("title"=>"Group", 'type'=>"select_single", "required"=>true, "options"=>$options, "default"=>$fee->group_id),
    'installment' => array("title"=>"Installment", 'type'=>"text", "required"=>true, "default"=>$fee->installment),
    'discount' => array("title"=>"Discountable", 'type'=>"checkbox", "default"=>$fee->discount, "required"=>false,'options'=>array("1"=>"Active")),
		'active' => array("title"=>"Status", 'type'=>"checkbox", "default"=>$fee->active, "required"=>false,'options'=>array("1"=>"Active")),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo ams_referrer("fees", uri_string()); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

