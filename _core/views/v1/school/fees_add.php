<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Add New Fee</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
<?php echo form_open("fees/add", array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

  $options = array();
  foreach($groups as $group) {
    $options[$group->id] = $group->name;
  }
	$forms = array(
    'name' => array("title"=>"Fee Name", 'type'=>"text", "required"=>true, "default"=>""),
    'group_id' => array("title"=>"Group", 'type'=>"select_single", "required"=>true, "options"=>$options, "default"=>""),
    'installment' => array("title"=>"Installment", 'type'=>"text", "required"=>true, "default"=>"1"),
    'discount' => array("title"=>"Discountable", 'type'=>"checkbox", "default"=>"0", "required"=>false,'options'=>array("1"=>"Active")),
		'active' => array("title"=>"Status", 'type'=>"checkbox", "default"=>"1", "required"=>false,'options'=>array("1"=>"Active")),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("fees"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

