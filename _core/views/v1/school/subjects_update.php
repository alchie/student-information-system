<?php $this->load->view('header'); 

	$grade_levels = array(
					'K1' => 'Kinder 1',
					'K2' => 'Kinder 2',
					'G1' => 'Grade 1',
					'G2' => 'Grade 2',
					'G3' => 'Grade 3',
					'G4' => 'Grade 4',
					'G5' => 'Grade 5',
					'G6' => 'Grade 6',
					'G7' => 'Grade 7',
					'G8' => 'Grade 8',
					'G9' => 'Grade 9',
					'G10' => 'Grade 10',
					'G11' => 'Grade 11',
					'G12' => 'Grade 12',
				);
	$added = array();
?>

<div class="row">
	<div class="col-sm-6 col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a href="<?php echo site_url('subjects/delete/' . $subject->id); ?>" class="pull-right btn btn-danger btn-xs confirm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
		  <h4>Update Subject</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
<?php echo form_open("subjects/update/" . $subject->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

	$forms = array(
		'title' => array("title"=>"Subject Title", 'type'=>"text", "required"=>true, "default"=>$subject->title),
		'active' => array("title"=>"Status", 'type'=>"checkbox", "default"=>$subject->active, "required"=>false,'options'=>array("1"=>"Active")),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("subjects"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>
	</div>
	<div class="col-sm-6 col-md-6">
		<div class="panel panel-default">
          <div class="panel-heading"><a role="button" data-toggle="modal" href="#addGradeLevelModal" class="pull-right btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i> Add</a><h4>Grade Levels</h4></div>
   			<div class="panel-body">
				<div class="list-group">
					<?php 
					foreach( $levels as $lvl ) {
						$added[] = $lvl->grade_level;
						?>
						<span href="<?php echo site_url("subjects/grade_levels/" . $subject->id . "/update/" . $lvl->id ); ?>" class="list-group-item">
						
						<a href="<?php echo site_url('subjects/grade_levels/' . $subject->id . "/delete/" . $lvl->id); ?>" class="pull-right btn btn-danger btn-xs confirm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
						<?php echo $grade_levels[$lvl->grade_level]; ?>
						</span>
						<?php
					}
					?>
				</div>
			</div>
		</div>
		
		<div class="panel panel-default">
          <div class="panel-heading"><h4>School Year</h4></div>
   			<div class="panel-body">
				<div class="list-group">
				
				</div>
			</div>
		</div>
	</div>
</div>

<!--Grade Levels modal-->
<div style="display: none;" id="addGradeLevelModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Grade Level</strong>
      </div>
	 <?php echo form_open("subjects/grade_levels/" . $subject->id . "/assign" ); ?>
      <div class="modal-body">
         <div class="form-group">
			<select class="form-control" name="grade_level">
				<?php 
				foreach( $grade_levels as $lvl => $level ) { 
					echo ( !in_array($lvl, $added ) ) ? "<option value=\"{$lvl}\">{$level}</option>" : ''; 
				 } 
				 ?>
			</select>
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

