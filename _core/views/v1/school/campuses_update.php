<?php $this->load->view('header'); 

$grade_levels = array(
					'K1' => 'Kinder 1',
					'K2' => 'Kinder 2',
					'G1' => 'Grade 1',
					'G2' => 'Grade 2',
					'G3' => 'Grade 3',
					'G4' => 'Grade 4',
					'G5' => 'Grade 5',
					'G6' => 'Grade 6',
					'G7' => 'Grade 7',
					'G8' => 'Grade 8',
					'G9' => 'Grade 9',
					'G10' => 'Grade 10',
					'G11' => 'Grade 11',
					'G12' => 'Grade 12',
				);

?>

<div class="row">
	<div class="col-sm-6 col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
<a href="<?php echo site_url("campuses/delete/" . $campus->id); ?>" class="pull-right btn btn-danger btn-xs confirm"><i class="glyphicon glyphicon-trash"></i> Delete</a>
          <h4>Update Campus</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

<?php echo form_open("campuses/update/" . $campus->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

	$forms = array(
		'name' => array("title"=>"Campus Name", 'type'=>"text", "required"=>true, "default"=>$campus->name),
		'address' => array("title"=>"Address", 'type'=>"text", "required"=>true, "default"=>$campus->address),
		'active' => array("title"=>"Status", 'type'=>"checkbox", "default"=>$campus->active, "required"=>false,'options'=>array("1"=>"Active")),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("campuses"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>

	<div class="col-sm-6 col-md-6">
   		 <div class="panel panel-default">
          <div class="panel-heading">
<?php if( $school_year ) { ?>
           <a role="button" data-toggle="modal" href="#assignSchoolYearModal" class="btn btn-success btn-xs pull-right">Assign School Year</a>
<?php } ?>
          <h4>School Year</h4></div>
   			<div class="panel-body" style="padding-top: 0px">
<?php
foreach($campus_school_year as $csy) {  ?>
<div class="panel panel-default">
  <div class="panel-heading">
  <div class="btn-group pull-right">
  	<a role="button" data-toggle="modal" href="#assignGradeLevelModal" class="btn btn-success btn-xs assignGradeLevelModalBtn" data-school_year="<?php echo $csy->school_year; ?>">Add Grade Level</a>
  	<?php if( !$csy->grade_levels ) { ?>
  	<a href="<?php echo site_url("campuses/delete_school_year/" . $campus->id . "/" . $csy->id); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
  	<?php } ?>
  </div>
  	<h3 class="panel-title"><?php echo $csy->sy_label; ?></h3>

  </div>
<?php if( $csy->grade_levels ) { ?>
  <div class="panel-body">
    <ul class="list-group">
    	<?php foreach($csy->grade_levels as $cgl) { ?>
		  <li class="list-group-item">
		  <a href="<?php echo site_url("campuses/delete_school_year_grade_level/" . $cgl->campus_id . "/" . $cgl->school_year . "/" .$cgl->id); ?>" class="btn btn-danger btn-xs confirm pull-right">Delete</a>
		  <?php echo $cgl->grade_level; ?>
		  </li>
		 <?php } ?>
	</ul>
  </div>
 <?php } ?>
</div>
<?php } ?>


            </div>
   		</div>

	</div>

</div>


<div style="display: none;" id="assignGradeLevelModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Assign Grade Level</strong>
      </div>
<?php echo form_open("campuses/update_grade_level/" . $campus->id ); ?>
      <div class="modal-body">
      <input type="hidden" name="school_year" value="" id="assignGradeLevelModal_schoo_year">
<div class="form-group">
			<select class="form-control" name="grade_level">
				<?php 
				foreach( $grade_levels as $lvl => $level ) { 
					echo ( !in_array($lvl, $added ) ) ? "<option value=\"{$lvl}\">{$level}</option>" : ''; 
				 } 
				 ?>
			</select>
		  </div>
		

      </div>
      
	  <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  
	  </form>
  </div>
  </div>
</div>

<div style="display: none;" id="assignSchoolYearModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Assign School Year</strong>
      </div>
<?php echo form_open("campuses/update_school_year/" . $campus->id ); ?>
      <div class="modal-body">

<div class="form-group">
			<select class="form-control" name="school_year">
<?php 
				foreach( $school_year as $sy ) { 
					echo "<option value=\"{$sy->id}\">{$sy->label}</option>"; 
				 } 
?>
			</select>
		  </div>
		

      </div>
      
	  <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  
	  </form>
  </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

