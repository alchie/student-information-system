<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
          <a href="<?php echo site_url('fees/groups'); ?>" class="pull-right btn btn-warning btn-sm" ><i class="glyphicon glyphicon-folder-open"></i> Groups</a>
      <a  style="margin-right:5px;" href="<?php echo site_url('fees/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add New Fee</a> 
		  
      <h4>School Fees <?php echo ($this->input->get('q')) ? '<small>Filter: <strong>'.$this->input->get('q').'</strong> <a href="' . site_url(uri_string()) . '">Clear Filter</a></small>' : ''; ?></h4>

      </div>
   			<div class="panel-body">
              <table class="table table-striped">
                     <thead>
                      <tr class="headings">
                        <th>Fee Name</th>
                        <th>Group</th>
                        <th class="text-center">Installment</th>
                        <th class="text-center">Discountable</th>
                        <th class="text-center">Status</th>
                        <th width="65px"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $fees as $fee ): ?>
                      <tr class="<?php echo ($fee->active==1) ? "" : "danger"; ?>">
                        <td><?php echo $fee->name; ?></td>
                        <td><?php echo $fee->group_name; ?></td>
                        <td class="text-center"><?php echo $fee->installment; ?></td>
                        <td class="text-center"><?php echo ($fee->discount==1) ? "<i class='glyphicon glyphicon-ok' style='color:green'></i>" : "<i class='glyphicon glyphicon-remove' style='color:red'></i>"; ?></td>
                        <td class="text-center"><?php echo ($fee->active==1) ? "<i class='glyphicon glyphicon-ok' style='color:green'></i>" : "<i class='glyphicon glyphicon-remove' style='color:red'></i>"; ?></td>
                        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("fees/update/" . $fee->id); ?>">Update</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo isset($pagination) ? $pagination : ''; ?>
			  </center>
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
