<?php $this->load->view('header'); 
$assigned = array();
?>

<div class="row">
	<div class="col-sm-6 col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a class="btn btn-danger btn-xs pull-right confirm" href="<?php echo site_url("school_year/delete/" . $school_year->id); ?>"><i class="glyphicon glyphicon-trash"></i> Delete</a>
		  <?php if($current_school_year!=$school_year->id) { ?>
			<a style="margin-right: 5px;" class="btn btn-success btn-xs pull-right confirm" href="<?php echo site_url("school_year/set_current/" . $school_year->id); ?>"><i class="glyphicon glyphicon-check"></i> Set as Current</a>
		  <?php } ?>
		  <h4>Update School Year</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
                                      <?php echo form_open("school_year/update/" . $school_year->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

	$forms = array(
		'label' => array("title"=>"Label", 'type'=>"text", "required"=>true, "default"=>$school_year->label),
		'start' => array("title"=>"Date Start", 'type'=>"datepicker", "required"=>true, "default"=>date("m/d/Y", strtotime($school_year->date_start)) ),
		'end' => array("title"=>"Date End", 'type'=>"datepicker", "required"=>true, "default"=>date("m/d/Y", strtotime($school_year->date_end)) ),
		'active' => array("title"=>"Status", 'type'=>"checkbox", "default"=>$school_year->active, "required"=>false,'options'=>array("1"=>"Active")),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Update <i class="fa fa-save"></i></button>
						<a href="<?php echo site_url("school_year"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>
	<div class="col-sm-6 col-md-6">
			<div class="panel panel-default">
          <div class="panel-heading">
          <a role="button" data-toggle="modal" href="#assignMonthModal" class="btn btn-success btn-xs pull-right">Assign a Month</a>
          	<h4>Months</h4>
          </div>
          <div class="panel-body">
          	<ul class="list-group">
          	<?php foreach($months as $month) { ?>
          		<li class="list-group-item">
          		<a class="btn btn-danger btn-xs pull-right confirm" href="<?php echo site_url("school_year/months/" . $school_year->id . "/delete/" . $month->month); ?>">Delete</a>
          		<?php echo date('F', strtotime($month->month."/1/1990")) . " " . $month->year; 
          				$assigned[$month->month] = $month->month;
          		?>

          		</li>
          	<?php } ?>
          	</ul>
          </div>
          </div>
	</div>
</div>


<!--parents modal-->
<div style="display: none;" id="assignMonthModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Assign a Month</strong>
      </div>
	 <?php echo form_open("school_year/months/" . $school_year->id); ?>
      <div class="modal-body">
         <div class="form-group">
		  <select class="form-control" name="month">
		  	<?php for($i=1;$i<=12;$i++) {
		  			if( !isset($assigned[$i]) ) {
		  				echo '<option value="'.$i.'">'.date('F', strtotime($i."/1/1990")).'</option>';
		  			}
		  		} ?>
		  </select>
		  </div><!-- /input-group -->
		  <div class="form-group">
		  <input type="text" class="form-control" name="year" value="<?php echo date('Y'); ?>">
		  </div>
		  <div class="form-group">
			<button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-ok"></i> Submit</button>
		  </div>
		

      </div>
      <!--
	  <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  -->
	  </form>
  </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

