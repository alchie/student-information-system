<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>School Information</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
<?php echo form_open("school", array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

	$forms = array(
		'name' => array("title"=>"School Name", 'type'=>"text", "required"=>true, 'default'=>get_ams_config('school', 'school_name')),
		'address' => array("title"=>"Address", 'type'=>"text", "required"=>true, 'default'=>get_ams_config('school', 'school_address')),
		'telephone' => array("title"=>"Telephone", 'type'=>"text", "required"=>true, 'default'=>get_ams_config('school', 'school_phone')),
		'email' => array("title"=>"Email Address", 'type'=>"text", "required"=>true, 'default'=>get_ams_config('school', 'school_email')),
		'website' => array("title"=>"Website URL", 'type'=>"text", "required"=>true, 'default'=>get_ams_config('school', 'school_website')),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>

	<div class="form-group">
	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
	  </div>
	</div>
 </form>
			
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
