<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">

          <div class="panel-heading">
          <h4>Update Fee</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
<?php echo form_open("school_year/fees/" . $school_year->id . "/" . $level->id . "/add", array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

  $options = array();
  foreach($fees as $fee1) {
    $options[$fee1->id] = $fee1->name;
  }
	$forms = array(
    'fee_id' => array("title"=>"Fee", 'type'=>"select_single", "required"=>true, "options"=>$options, "default"=>''),
    'amount' => array("title"=>"Amount", 'type'=>"text", "required"=>true, "default"=>'0.00'),
    'skip' => array("title"=>"Payment Skip", 'type'=>"text", "required"=>true, "default"=>'0'),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("school_year/fees/" . $school_year->id . "/" . $level->id); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

