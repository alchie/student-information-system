<?php $this->load->view('header'); 

$grade_levels = unserialize(LEVEL_NAMES);
?>
<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a role="button" data-toggle="modal" href="#addSectionModal" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Assign a Fee</a>
		  <a href="<?php echo site_url("school_year/grade_levels/" . $school_year->id ); ?>" class="pull-right btn btn-warning btn-sm" style="margin-right:5px;"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
		  <h4><?php echo $school_year->label; ?> &middot; <?php echo $grade_levels[$level->level]; ?> &middot; Fees</h4></div>
   			<div class="panel-body">
              <table class="table table-striped table-hover">
                    <thead>
                      <tr class="headings">
                        <th>Fee</th>
                        <th>Amount</th>
                        <th>Payment Skip</th>
                        <th width="70px"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
<?php 
$total_fees = 0;
foreach( $school_fees_groups as $sfg) { ?>
                    <tr>
                    	<th colspan="4"><?php echo $sfg->name; ?></th>
                    </tr>

	<?php 
	$added = array();
	foreach( $lvl_fees as $lvl_fee ): 
		if( $lvl_fee->group_id == $sfg->id ) {
			$added[] = $lvl_fee->fee_id; 
			$total_fees += floatval($lvl_fee->amount);
	?>
      <tr>
        <td>- <?php echo $lvl_fee->name; ?></td>
        <td><?php echo number_format($lvl_fee->amount, 2); ?></td>
        <td><?php echo ($lvl_fee->skip>1) ? $lvl_fee->skip." Months" : $lvl_fee->skip." Month"; ?></td>
        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("school_year/fees/" . $school_year->id . '/' . $level->id . '/update/' . $lvl_fee->id ); ?>">Update</a>
		</td>
      </tr>
	<?php } 
	endforeach; ?>
<?php } ?>
<tr>
	<th colspan="4">Ungrouped Fees</th>
</tr>
<?php foreach( $lvl_fees as $lvl_fee ): 
if( $lvl_fee->group_id == 0) {
	$added[] = $lvl_fee->fee_id; 
	$total_fees += floatval($lvl_fee->amount);
?>
<tr>
        <td>- <?php echo $lvl_fee->name; ?></td>
        <td><?php echo number_format($lvl_fee->amount, 2); ?></td>
        <td><?php echo ($lvl_fee->skip>1) ? $lvl_fee->skip." Months" : $lvl_fee->skip." Month"; ?></td>
        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("school_year/fees/" . $school_year->id . '/' . $level->id . '/update/' . $lvl_fee->id ); ?>">Update</a>
		</td>
      </tr>
<?php } 
endforeach; ?>
					</tbody>

                  </table>
			  
            </div>
            <div class="panel-footer">

            <!-- Single button -->
<?php if( count( $lvl_fees ) == 0 ) { ?>
  <button type="button" class="btn btn-danger btn-xs dropdown-toggle pull-right" role="button" data-toggle="modal" href="#resetCopyFeesModal">
    Copy Fees
  </button>
<?php } ?>

            	TOTAL FEES: <strong><?php echo number_format($total_fees,2); ?></strong>
            </div>
   		</div>

	</div>
</div>

<!--Address modal-->
<div style="display: none;" id="addSectionModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Assign Fee</strong>
      </div>
	 <?php echo form_open("school_year/fees/" . $school_year->id . '/' . $level->id . '/add' ); ?>
      <div class="modal-body">
         <div class="form-group">
			<select class="form-control" name="fee_id">
				<option value="" disabled="" selected="">- - Select a Fee - -</option>
				<?php 
				foreach( $fees as $fee ) { 
					echo ( !in_array($fee->id, $added ) ) ? "<option value=\"{$fee->id}\">{$fee->name}</option>" : ''; 
				 } 
				 ?>
			</select>
		  </div>
		  <div class="form-group">
		  	<label>Amount</label>
		  		<input type="text" name="amount" value="0.00" class="form-control">
		  </div>
		  <div class="form-group">
		  <label>Skip</label>
		  		<input type="text" name="skip" value="0" class="form-control">
		  </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
	  </form>
  </div>
  </div>
</div>

<?php if( count( $lvl_fees ) == 0 ) { ?>
<!--resetCopyFeesModal-->
<div style="display: none;" id="resetCopyFeesModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Reset &amp; Copy Fees</strong>
      </div>
   <?php echo form_open("school_year/fees/" . $school_year->id . '/' . $level->id . '/reset_copy' ); ?>
      <div class="modal-body">
         <div class="form-group">
         <label>Source</label>
      <select class="form-control" name="sylvl_id">
        <option value="" disabled="" selected="">- - Copy From - -</option>
        <?php 
        foreach( $copy_source as $source ) { 
          $level = $grade_levels[$source->level];
          echo "<option value=\"{$source->id}\">{$level} ({$source->label})</option>"; 
         } 
         ?>
      </select>
      </div>
      <strong>Reminder: </strong> Upon submission, all currently assigned fees will be removed and will be replaced by the fees assigned from the source you selected above. 
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary confirm">Submit</button>
      </div>
    </form>
  </div>
  </div>
</div>
<?php } ?>

<?php $this->load->view('footer'); ?>
