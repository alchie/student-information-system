<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><a href="<?php echo site_url('school_year/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add School Year</a> <h4>School Year <?php echo ($this->input->get("q")) ? "<small>Search for \"<strong>" . $this->input->get("q") . "</strong>\"" : ""; ?></small></h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                    <thead>
                      <tr class="headings">
                        <th>Label </th>
                        <th>Date Start </th>
                        <th>Date End </th>
                        <th>Status </th>
                        <th width="345px"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $school_year as $year ): ?>
                      <tr class="<?php echo ($year->active==1) ? "" : "danger"; ?> <?php echo ($current_school_year==$year->id) ? "success" : ""; ?>">
                        <td><?php echo $year->label; ?></td>
                        <td><?php echo $year->date_start; ?></td>
                        <td><?php echo $year->date_end; ?></td>
                        <td><?php echo ($year->active==1) ? "Active" : "Disabled"; ?></td>
                        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("school_year/update/" . $year->id); ?>">Update</a> 
						<a class="btn btn-primary btn-xs" href="<?php echo site_url("school_year/grade_levels/" . $year->id); ?>">Grade Levels</a>
						<a class="btn btn-primary btn-xs" href="<?php echo site_url("school_year/subjects/" . $year->id); ?>">Subjects</a>
            <a class="btn btn-primary btn-xs" href="<?php echo site_url("enrollment/index/" . $year->id); ?>">Enrollment</a>
						<a class="btn btn-primary btn-xs" href="<?php echo site_url("finance/index/" . $year->id); ?>">Finance</a>
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
