<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><a href="<?php echo site_url('campuses/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add Campus</a> <h4>Campuses <?php echo ($this->input->get("q")) ? "<small>Search for \"<strong>" . $this->input->get("q") . "</strong>\"" : ""; ?></small></h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                    <thead>
                      <tr class="headings">
                        <th>Name</th>
                        <th>Address</th>
                        <th>Status </th>
                        <th width="65px"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $campuses as $campus ): ?>
                      <tr class="<?php echo ($campus->active==1) ? "" : "danger"; ?>">
                        <td><?php echo $campus->name; ?></td>
                        <td><?php echo $campus->address; ?></td>
                        <td><?php echo ($campus->active==1) ? "Active" : "Disabled"; ?></td>
                        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("campuses/update/" . $campus->id); ?>">Update</a> 
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
