<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><a href="<?php echo site_url('grade_levels/add'); ?>" class="pull-right btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add Grade Level</a> <h4>Grade Levels <?php echo ($this->input->get("q")) ? "<small>Search for \"<strong>" . $this->input->get("q") . "</strong>\"" : ""; ?></small></h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                    <thead>
                      <tr class="headings">
                        <th>Grade Level </th>
                        <th>Status </th>
                        <th width="70px"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $levels as $level ): ?>
                      <tr class="<?php echo ($level->active==1) ? "" : "danger"; ?>">
                        <td><?php echo $level->name; ?></td>
                        <td><?php echo ($level->active==1) ? "Active" : "Disabled"; ?></td>
                        <td><a class="btn btn-warning btn-xs" href="<?php echo site_url("grade_levels/update/" . $level->id); ?>">Update</a>
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>
            </div>
   		</div>

	</div>
</div>

<?php $this->load->view('footer'); ?>
