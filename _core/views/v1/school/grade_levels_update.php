<?php $this->load->view('header'); ?>

<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
		  <a class="btn btn-danger btn-xs pull-right confirm" href="<?php echo site_url("grade_levels/delete/" . $level->id); ?>"><i class="glyphicon glyphicon-trash"></i> Delete</a>
		  <h4>Add Grade Level</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

			
<?php echo form_open("grade_levels/update/" . $level->id , array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

	$forms = array(
		'name' => array("title"=>"Grade Level", 'type'=>"text", "required"=>true, "default"=>$level->name),
		'active' => array("title"=>"Status", 'type'=>"checkbox", "default"=>$level->active, "required"=>false,'options'=>array("1"=>"Active")),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("grade_levels"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

