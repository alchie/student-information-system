<?php if( $output != 'ajax') { ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
  <div class="col-sm-12 col-md-12">

      <div class="panel panel-default">
          <div class="panel-heading">
            <h4><?php echo $primary_school_year->label; ?> - <?php echo date('F', strtotime($month . '/1/1990')); ?> - Invoice Detail</h4>
          </div>
          <div class="panel-body">
<?php } ?>

          <table class="table table-condensed table-hover">
          <thead>
            <tr>
              <th>Fee Name</th>
              <th>Month</th>
              <th>Payment Date</th>
              <th>Receipt Number</th>
              <th class="text-right">Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $total = 0;
            foreach( $applied_payments as $apay ) { ?>
              <tr>
                <td class="text-left"><?php echo $apay->fee_name; ?></td>
                <td class="text-left"><?php echo date('F', strtotime($month . '/1/1990')); ?></td>
                <td class="text-left"><?php echo  date('m/d/Y', strtotime($apay->payment_date)); ?></td>
                <td class="text-left"><a href="<?php echo site_url("finance/update_payment/{$id}/{$enrolled_id}/{$apay->payment_id}"); ?>" target="_blank"><?php echo $apay->receipt_number; ?></a></td>
                <td class="text-right"><?php 
                $total += $apay->amount;
                echo number_format($apay->amount,2); ?></td>
              </tr>
            <?php } ?>
            <tr>
                <td class="text-left"><strong>Total</strong></td>
                <td class="text-left"></td>
                <td class="text-left"></td>
                <td class="text-left"></td>
                <td class="text-right"><strong><?php 
                echo number_format($total,2); ?></strong></td>
              </tr>
          </tbody>
          </table>

<?php if( $output != 'ajax') { ?>
          </div>
      </div>
  </div>
</div>



<?php $this->load->view('footer'); ?>

<?php } ?>