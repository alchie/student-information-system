<?php $this->load->view('header');
$level_names = unserialize(LEVEL_NAMES);
 ?>
<div class="row">
  <div class="col-sm-6 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading"><h4><?php echo $level_names[$grade_level]; ?> - <?php echo $campus->name; ?> Students</h4></div>
<?php echo form_open("finance/print_soa/{$primary_school_year->id}/{$grade_level}/{$campus_id}", array("method"=>'get', "id"=>"","class"=>"form-horizontal form-label-left",'target'=>'_soa')); ?>

<?php foreach($this->input->get() as $key=>$value) {   
  if( is_array($value) ) {
    foreach($value as $val) {
      echo '<input type="hidden" name="'.$key.'[]" value="'.$val.'">';
    }
  } else {
    echo '<input type="hidden" name="'.$key.'" value="'.$value.'">';
  }
  } ?>

          <div class="panel-body text-center">
<p class="btn-group" role="group" aria-label="...">
  <button type="button" class="btn btn-success btn-xs btn-group-select btn-group-select-all" data-target=".student_checkbox">Select All</button>
  <button type="button" class="btn btn-default btn-xs btn-group-select btn-group-select-none" data-target=".student_checkbox">Select None</button>
</p>
<ul class="list-group sortable">
  <?php foreach($enrollees as $enrollee) {  

$balance = (($enrollee->whole_year + $enrollee->services) - ($enrollee->discounts + $enrollee->payments + $enrollee->downpayments));
  if(intval($balance) == 0) {
    continue;
  }

    ?>
    <label class="list-group-item text-left">
    <span class="glyphicon glyphicon-sort pull-right" style="margin-left: 5px;"></span>
<span class="badge"><?php echo $level_names[$enrollee->grade_level]; ?></span>
<input type="checkbox" name="students[]" value="<?php echo $enrollee->id; ?>" class="student_checkbox" CHECKED>
    <?php echo $enrollee->lastname; ?>, <?php echo $enrollee->firstname; ?> <?php echo $enrollee->middlename; ?> (<?php echo $enrollee->idn; ?>)
    </label>
  <?php } ?>
</ul>
                    
          </div>
          <div class="panel-footer">
            
            <div class="">
                      <div class="">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-print"></i> Print</button>
            <a href="javascript:history.back(-1);" class="btn btn-warning"><i class="fa fa-arrow-left"></i></i> Back</a>
                      </div>
                    </div>
          

          </div>
           </form>
      </div>

  </div>
</div>
<?php $this->load->view('footer'); ?>