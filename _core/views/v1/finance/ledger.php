<?php 

$level_names = unserialize(LEVEL_NAMES);

$no_down_payments = true;
$remaining_balance = 0;
$remaining_balance2 = 0;
$total_whole_year_fee = 0;
$total_discounts = 0;
$schedule_fee = array();
$schedule_invoices = array();
$oldaccount_m = ($oldaccount) ? (($oldaccount->amount - $oldaccount->old_down) / $oldaccount->installment) : 0;
$oldaccount_i = ($oldaccount) ? $oldaccount->installment : 1;
foreach( $school_fees as $sf) { 
    $total_fee_amount = ($sf->amount - $sf->discounts) - $sf->downpayments;
    $sf_installment = (($sf->installment - count($excluded_months)) > 0) ? ($sf->installment - count($excluded_months)) : 1;
    $month_installment = ($total_fee_amount / $sf_installment);
    $skip = intval($sf->skip);
    for($i=0;$i<$sf_installment;$i++) {
        $i2 = $i + $skip;
        if( !isset($schedule_fee[$i]) ) {
          $schedule_fee[$i] = 0;
        }
        $schedule_fee[$i2] = $schedule_fee[$i2] + $month_installment;
    }
  }

  $n=0;
  foreach( $schedule_fee as $i=>$sFee ) {
      if( $n < $oldaccount_i ) {
         $schedule_fee[$i] = ($sFee + $oldaccount_m);
        } 
      $n++;
  }

foreach($invoices as $invoice) {
  if(!isset($schedule_invoices[$invoice->month])) {
    $schedule_invoices[$invoice->month] = 0;
  }
  $schedule_invoices[$invoice->month] += $invoice->amount;
}
$total_student_fees = 0;

$sss = array();
foreach($student_services as $ss) {
  if( !isset($sss[$ss->month]) ) {
    $sss[$ss->month] = 0;
  }
  $sss[$ss->month] += $ss->amount;
}

$this->load->view('header');
?>
<?php if( !isset($primary_school_year) ) : ?>
<div class="alert alert-danger alert-dismissable">
	<strong>SCHOOL YEAR ERROR!</strong> No current school year found! Please assign a current school year or select a school year <a href="<?php echo site_url('school_year'); ?>">here</a>.
</div>
<?php else: ?>


<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
	<div class="col-sm-6 col-md-6">


<div class="panel panel-default">
  <div class="panel-heading">

<!-- Single button -->
<div class="btn-group pull-right">

  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Print Statement <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <?php foreach( $months as $month ) { ?>
    <li><a href="<?php echo site_url("finance/print_statement/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $month->id ); ?>" target="print_student_statement"><?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></a></li>
  <?php } ?>
    <li role="separator" class="divider"></li>
    <li><a href="<?php echo site_url("finance/print_statement/" . $primary_school_year->id . "/" . $enroll_data->id ); ?>" target="print_student_statement">Print All</a></li>
   <!-- <li role="separator" class="divider"></li>
    <li><a href="<?php echo site_url("finance/recalculate/" . $primary_school_year->id . "/" . $enroll_data->id ); ?>" class="confirm">Recalculate Invoices</a></li>-->
    <li role="separator" class="divider"></li>
    <li><a href="javascript:void(0);" class="show_delete_invoice_input">Delete Invoices</a></li>
  </ul>
</div>

      <h4>Monthly Installments</h4></div>
        <div class="panel-body">

<?php if( $invoices ) { ?>
  <form action="<?php echo site_url("finance/delete_invoices/{$primary_school_year->id}/{$enroll_data->id}"); ?>" method="post">
          <table class="table table-striped table-hover table-condensed">
            <thead>
              <tr>
                <th width="5px" class="delete_invoice_input_item" style="display:none;"><input type="checkbox" id="checkall_delete_invoice"> </th>
                <th>Month</th>
                <th class="text-right" width="20%">Invoices</th>
                <!--<th class="text-right" width="20%">Services</th>-->
                <th class="text-right" width="20%">Balance</th>
              </tr>
            </thead>
            <tbody>
            <?php 
            $remaining_payments = $enroll_data->payments;
              $total_months = 0;
              $total_months2 = 0;
              $total_invoices = 0;
              $total_invoices2 = 0;
              $month_n = 0;
              $total_services = 0;
            foreach( $months as $month ) { 
              $total_months_d = '0.00';
              $service_month = ((isset($sss[$month->month])) ? $sss[$month->month] : 0);
              $total_services += $service_month;
              $total_invoices_d = $month->invoiced;
              $total_invoices += $month->invoiced;
              /*
              if(isset($schedule_fee[$month_n])) {
                  $total_months_d = $schedule_fee[$month_n];  
                  $total_months_d2 = $schedule_fee[$month_n] + $service_month; 
                  $total_months += $total_months_d;
                  $total_months2 += $total_months_d2;
                }  
                $total_invoices_d = '0.00';

              if (isset($schedule_invoices[$month->month])) {
                  $total_invoices_d = $schedule_invoices[$month->month];
                  $total_invoices += $total_invoices_d; 
                } 
                
                $total_invoices_d2 = $total_invoices_d + $service_month;
                $total_invoices2 += $total_invoices_d2;
              */
              if( intval($total_invoices_d) <= 0) {
                continue;
              }
              ?>
              <tr class="<?php 
              $tr_empty = '';
              if( time() >= strtotime($month->month . "/1/" . $month->year)) {
                $tr_empty = 'danger';
              }
              echo ( round($enroll_data->payments,2) >= round($total_invoices,2) ) ? 'success' : $tr_empty; 
              ?>">
                <td class="delete_invoice_input_item" style="display:none;">
<!--
                <a href="<?php echo site_url("finance/delete_invoice/{$primary_school_year->id}/{$enroll_data->id}/{$month->month}"); ?>" class="confirm"><i class="glyphicon glyphicon-trash"></i></a>
-->
                <input class="delete_invoice_item" type="checkbox" name="delete_invoice_item[]" value="<?php echo $month->month; ?>"> 
                </td>
                <td>

                <?php echo date('F Y', strtotime($month->month . "/1/" . $month->year)) ; ?>
                  <?php if( $month->promised ) { ?>
                  <span class="glyphicon glyphicon-exclamation-sign pull-right"></span>
                  <?php } ?>
                </td>
                <td class="text-right">
                <a role="button" data-toggle="modal" href="#invoiceDetailsModal" class="money_format invoiceDetailsModal_button" data-ajax="<?php echo site_url("finance/invoice_detail/{$primary_school_year->id}/{$enroll_data->id}/{$month->month}/ajax"); ?>">
                  <?php echo number_format($total_invoices_d,2); ?>
                </a>
                </td>
                 <td class="text-right money_format"><?php echo (($total_invoices-$remaining_payments)>0) ? number_format($total_invoices-$remaining_payments,2) : 0.00; ?></td>
              </tr>
            <?php $month_n++; } ?>
              <tr>
              <th class="text-right delete_invoice_input_item" style="display:none;"><button class="confirm btn btn-danger btn-xs"  type="submit"><i style="color:#FFF;" class="glyphicon glyphicon-trash"></i></button></th>
                <th>TOTAL</th>
                <th class="text-right"><?php echo number_format($total_invoices,2); ?></th>
                <!--<th class="text-right"><?php echo number_format($total_services,2); ?></th>-->
                <th class="text-right"></th>
              </tr>

            </tbody>
          </table>
</form>
<?php } else { ?>
  <center style="margin-bottom:20px;">

      <p>No Invoice Found!</p>
      <a class="btn btn-danger btn-xs confirm" href="<?php echo site_url("finance/recalculate/" . $primary_school_year->id . "/" . $enroll_data->id); ?>">Create Invoices</a>
  </center>
   
<?php } ?>
        </div>
            
      </div>



      <div class="panel panel-default">
          <div class="panel-heading">
          <a target="print_summary" href="<?php echo site_url(str_replace('ledger','print_summary', uri_string())); ?>" class="btn btn-primary btn-xs pull-right">Print Details</a>
<h4>
SUMMARY  
      </h4></div>
        <div class="panel-body">
        <table class="table table-default">
            <tbody>
            <tr>
                <th>Old Account</th>
                <th class="text-right">
<a href="<?php echo site_url(str_replace('ledger','oldaccount', uri_string())); ?>">
                <?php 
                $total_student_fees += ($oldaccount) ? $oldaccount->amount : 0;
                echo ($oldaccount) ? number_format($oldaccount->amount,2) : '0.00'; 
                ?>
</a>
                </th>
                 <td></td>
              </tr>
              <tr>
                <th><?php echo $primary_school_year->label; ?> FEES</th>
                <th class="text-right">
<a href="<?php echo site_url(str_replace('ledger','school_fees', uri_string())); ?>">
<?php 
  $total_student_fees += $enroll_data->whole_year;
  echo number_format($enroll_data->whole_year,2); 
?>
</a>
                </th>
                 <td></td>
              </tr>
               <tr>
                <th>TOTAL FEES</th>
                <td></td>
                <th class="text-right"><?php echo number_format($total_student_fees,2); ?></th>
              </tr>
              <tr>
                <th>DEDUCTIONS:</th>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>- Total Discounts</td>
                <td class="text-right">
<a href="<?php echo site_url("finance/discounts/" . $primary_school_year->id . "/" . $enroll_data->id); ?>">
                <?php echo number_format($enroll_data->discounts,2); ?>
</a>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>- Total Downpayments</td>
                <td class="text-right"><?php echo number_format($enroll_data->downpayments,2); ?></td>
                <td></td>
              </tr>
              <tr class="danger">
                <th>TOTAL DEDUCTIONS</th>
                <td></td>
                <th class="text-right">(<?php echo number_format($enroll_data->discounts+$enroll_data->downpayments,2); ?>)</th>
              </tr>
              <tr>
                <th>BALANCE SUBJECT TO MONTHLY INSTALLMENT</th>
                <td></td>
                <th class="text-right">
<a href="<?php echo site_url("finance/detailed_schedule/" . $primary_school_year->id . "/" . $enroll_data->id); ?>">
                <?php echo number_format(($total_student_fees-($enroll_data->discounts+$enroll_data->downpayments)),2); ?>
</a>
                </th>
              </tr>
              <tr>
                <th>PLUS: Additional Services</th>
                
                <th class="text-right">
<a href="<?php echo site_url("finance/services/" . $primary_school_year->id . "/" . $enroll_data->id); ?>">
                <?php echo number_format($enroll_data->services,2); ?>
</a>
                </th>
                <td></td>
              </tr>
<tr>
                <th>TOTAL</th>
                <td></td>
                <th class="text-right">
                <?php echo number_format((($total_student_fees-($enroll_data->discounts+$enroll_data->downpayments)) + $enroll_data->services),2); ?>
                </th>
              </tr>
              <tr class="danger">
                <th>LESS: TOTAL PAYMENTS</th>
                <td></td>
                <th class="text-right">(<?php echo number_format($enroll_data->payments,2); ?>)</th>
              </tr>
              <tr class="success">
                <th>REMAINING BALANCE</th>
                <td></td>
                <th class="text-right"><?php 
$remaining_balance = ($total_student_fees-($enroll_data->discounts+$enroll_data->payments+$enroll_data->downpayments));
$remaining_balance2 = ($total_student_fees-($enroll_data->discounts+$enroll_data->payments+$enroll_data->downpayments))+$enroll_data->services;
                echo number_format(($total_student_fees-($enroll_data->discounts+$enroll_data->payments+$enroll_data->downpayments)+$enroll_data->services),2); 

                ?></th>
              </tr>
            </tbody>
        </table>
        </div>
      </div>

<!--
<div class="panel panel-default">
          <div class="panel-heading">
          <a role="button" data-toggle="modal" href="#addDiscounts2Modal" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i> Add Discount</a>
      <h4>Discounts</h4></div>
        <div class="panel-body">
<?php if( $discounts ) { ?>
        <?php $total_payments = 0; ?>
          <table class="table table-striped table-hover table-condensed">
            <thead>
              <tr>
                <th>Fee</th>
                <th class="text-right">Amount</th>
                <th>Description</th>
                <th class="text-right" style="width:100px">Action</th>
              </tr>
            </thead>
            <tbody>
<?php 
foreach( $discounts as $discount ) { ?>
  <tr class="">
    <td><?php echo $discount->fee_name; ?></td>
    <td class="text-right"><?php echo number_format($discount->amount,2); $total_discounts+=$discount->amount; ?></td>
    <td class="small"><?php echo $discount->description; ?></td>
     <td class="text-right"><a class="btn btn-warning btn-xs pull-right" href="<?php echo site_url("finance/discount/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $discount->id); ?>">Update</a></td>
  </tr>
<?php } ?>
              <tr>
                <th>TOTAL DISCOUNTS</th>
                <th class="text-right"><?php echo number_format($total_discounts,2); ?></th>
                <th class="text-right"></th>
                <td></td>
              </tr>
            </tbody>
          </table>
<?php } else { ?>
  <p class="text-center">No Discount Found!</p>
<?php } ?>
        </div>
            
      </div>
-->

	</div>


		<div class="col-sm-6 col-md-6">

       <div class="panel panel-default">
          <div class="panel-heading">
          <a role="button" data-toggle="modal" href="#addPaymentModal" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i> Add Payment</a>
      <h4>Payments</h4></div>
        <div class="panel-body">
        <?php $total_payments = 0; ?>
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>Date Paid</th>
                <th class="text-right">OR #</th>
                <th class="text-right">Amount</th>
                <th class="text-right"  style="width:70px">Action</th>
              </tr>
            </thead>
            <tbody>
<tr class="">
  <th colspan="4">DOWNPAYMENTS</th>
</tr>
<?php 

$total_downpayments = 0;
foreach( $payments as $payment ) { 
if($payment->down==0) {
  continue;
}
$no_down_payments = false;
$downpayment_not_applied = true;
if( $payment->total_applied ) {
    $downpayment_not_applied = false;
}
  ?>
  <tr>
    <td><?php echo date('m/d/Y', strtotime($payment->payment_date)); ?>
    <?php if( $payment->memo != '') { ?>
      <a href="javascript:void(0);" class="glyphicon glyphicon-comment pull-right" data-toggle="tooltip" data-placement="top" title="<?php echo $payment->memo; ?>"></a>
    <?php } ?>
    </td>
    <td class="text-right"><?php echo $payment->receipt_number; ?>
      

    </td>
    <td class="text-right">
<a role="button" data-toggle="modal" href="#paymentDetailsModal" class="money_format paymentDetailsModal_button" data-ajax="<?php echo site_url("finance/payment_detail/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $payment->id . "/ajax"); ?>">
    <?php echo number_format($payment->amount,2); 
    $total_downpayments+=$payment->amount;
    $total_payments+=$payment->amount; 
    ?>
</a>

<?php if( number_format($payment->total_applied, 2) != number_format($payment->amount, 2) ) { ?>
      <span class="glyphicon glyphicon-exclamation-sign"></span>
    <?php } ?>
    </td>
     <td class="text-right"><a class="btn btn-warning btn-xs pull-right" href="<?php echo site_url("finance/update_downpayment/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $payment->id); ?>">Update</a></td>
  </tr>
<?php } ?>
<tr class="warning">
  <th colspan="3">TOTAL DOWNPAYMENTS</th>
  <th class="text-right money_format"><?php echo $total_downpayments; ?></th>
</tr>
<tr>
                <td colspan="4"></th>
              </tr>
<tr class="">
  <th colspan="4">MONTHLY PAYMENTS</th>
</tr>
<?php 
$total_monthly_payments = 0;
foreach( $payments as $payment ) { 
if($payment->down==1) {
  continue;
}
  ?>
  <tr>
    <td><?php echo date('m/d/Y', strtotime($payment->payment_date)); ?>
      
    <?php if( $payment->memo != '') { ?>
      <a href="javascript:void(0);" class="glyphicon glyphicon-comment pull-right" data-toggle="tooltip" data-placement="top" title="<?php echo $payment->memo; ?>"></a>
    <?php } ?>

    </td>
    <td class="text-right"><?php echo $payment->receipt_number; ?></td>
    <td class="text-right">
<a role="button" data-toggle="modal" href="#paymentDetailsModal" class="money_format paymentDetailsModal_button" data-ajax="<?php echo site_url("finance/payment_detail/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $payment->id . "/ajax"); ?>">
    <?php echo number_format($payment->amount,2); 
    $total_monthly_payments+=$payment->amount; 
    $total_payments+=$payment->amount; 
    ?>
</a>
<?php if( number_format($payment->total_applied2, 2) != number_format($payment->amount, 2) ) { ?>
      <span class="glyphicon glyphicon-exclamation-sign"></span>
    <?php } ?>
    </td>
     <td class="text-right">
<?php if( $invoices ) { ?>
     <a class="btn btn-warning btn-xs pull-right" href="<?php echo site_url("finance/update_payment/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $payment->id); ?>">Update</a>
<?php } ?>
    </td>
  </tr>
<?php } ?>
<tr class="warning">
  <th colspan="3">TOTAL MONTHLY PAYMENTS</th>
  <th class="text-right money_format"><?php echo $total_monthly_payments; ?></th>
</tr>
<tr>
                <td colspan="4"></th>
              </tr>
              <tr class="success">
                <th colspan="3">TOTAL PAYMENTS</th>
                <th class="text-right"><?php echo number_format($total_payments,2); ?></th>
              </tr>
              <tr>
                <th colspan="3">TOTAL APPLIED PAYMENTS <a class="confirm" href="<?php echo site_url("finance/reset_payment_applied/{$primary_school_year->id}/{$enroll_data->id}"); ?>" title="Reset Applied Payments"><span class="glyphicon glyphicon-refresh"></span></a></th>
                <th class="text-right"><?php echo number_format(($applied_payments->total_applied+$applied_payments2->total_applied),2); ?></th>
              </tr>
            </tbody>
          </table>

        </div>
            
      </div>


 <div class="panel panel-default">
          <div class="panel-heading">
          <a href="<?php echo site_url("finance/add_promissory_note/" . $primary_school_year->id . "/" . $enroll_data->id ); ?>" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i> Add a Note</a>
      <h4>Promissory Notes</h4></div>
        <div class="panel-body">

         <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th width="20%">Month</th>
                <th class="">Reason</th>
                <th style="width:10px"></th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($promises as $promise) { ?>
              <tr class="<?php echo ($promise->settled) ? 'success' : 'danger'; ?>">
                <td><?php echo date('F Y', strtotime($promise->month . "/01/" . $promise->year)); ?></td>
                <td><?php echo $promise->reason; ?> 
<?php if( $promise->parent_id ) { ?>
<span href="javascript:void(0);" class="glyphicon glyphicon-comment pull-right" data-toggle="tooltip" data-placement="top" title="<?php echo $promise->lastname . ", " .$promise->firstname . " (".ucwords($promise->relationship).")"; ?> <?php echo $promise->contact; ?>"></span>
<?php } ?>
</td>
                <td><a href="<?php echo site_url("finance/edit_promissory_note/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $promise->id ); ?>"><i class="glyphicon  glyphicon-pencil"></i></a></td>
              </tr>
              <?php } ?>
            </tbody>
            </table>

        </div>
</div>


	</div>
</div>

<!-- addPaymentModal modal -->
<div style="display: none;" id="addPaymentModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width:700px;">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Payment</strong>
      </div>
	 <?php echo form_open("finance/add_payment/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
      <div class="modal-body">
      <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label>Payment Date</label>
              <input class="form-control datepicker" name="payment_date" value="<?php echo date('m/d/Y'); ?>" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                <label>Receipt Number</label>
              <input class="form-control text-right" name="receipt_number" required="">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                <label>Amount</label>
              <input class="form-control text-right" id="payment_total_amount" name="total_amount" required>
            </div>
          </div>
        </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label><input type="checkbox" name="downpayment" value="1"> Downpayment</label>
          </div>
        </div>
        <div class="col-md-8">
          <div class="form-group">
            <label>Memo</label>
            <textarea class="form-control" name="memo"></textarea>
          </div>
        </div>
      </div>

<!-- 
<center>
<a class="btn btn-primary btn-xs show-hide" data-id="addPaymentModal-applied">Show / Hide Details</a>
<a class="btn btn-danger btn-xs un-apply-payment">Un-Apply Payment</a>
<a class="btn btn-success btn-xs auto-apply-payment">Auto Apply Payment</a>
</center>   

          <table class="table table-condensed" id="addPaymentModal-applied" style="display:none;">
            <thead>
              <tr>
                <th></th>
                <th class="text-right">Balance</th>
                <th class="text-right" width="30%">Amount</th>
                <th class="text-right" width="23px"></th>
              </tr>
            </thead>
            <tbody>
            <?php if($oldaccount&&($oldaccount->amount>0)) { 
              $balance = ($oldaccount->amount-$oldaccount->payments);
               if( round($balance,2) > 0) {
              ?>
             <tr class="warning">
                <td><strong>Old Account</strong></td>
                <td class="text-right money_format"><?php echo number_format($oldaccount->amount,2); ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format($oldaccount->amount,2); ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_0" name="applied[school_fees][0]" required value="0"></td>
              </tr>
            <?php } } ?>
            <?php foreach($school_fees_groups as $sfg) { ?>
              <tr class="success">
                <td colspan="4">
                <strong class="pull-right autosum-class money_format" data-class="school_fees_balance_<?php echo $sfg->id; ?>">0.00</strong>
                <strong><?php echo $sfg->name; ?></strong> <button type="button" class="btn btn-xs btn-success show-hide-class" data-class="school_fees_groups_<?php echo $sfg->id; ?>">Show</button></td>
              </tr>
              <?php 
              foreach( $school_fees as $sf) { 
                if( $sf->group_id == $sfg->id) {
                  if( round(($sf->amount-$sf->discounts-$sf->payments),2) > 0) {
                ?>
              <tr class="school_fees_groups_<?php echo $sf->group_id; ?>" style="display:none;">
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right money_format school_fees_balance_<?php echo $sf->group_id; ?>"><?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_<?php echo $sf->fee_id; ?>" data-check_id="fee_include_<?php echo $sf->fee_id; ?>" name="applied[school_fees][<?php echo $sf->fee_id; ?>]" required>
                </td>
                <td><input type="checkbox" checked id="fee_include_<?php echo $sf->fee_id; ?>"></td>
              </tr>
              <?php } } } ?>
            <?php } ?>

              <tr class="success" id="ungrouped_feed_apply_payment">
                <td colspan="4">
                <strong class="pull-right autosum-class money_format" data-class="school_fees_balance_0">0.00</strong>
                <strong>Ungrouped Fees</strong>  <button type="button" class="btn btn-xs btn-success show-hide-class" data-class="school_fees_groups_0">Show</button></td>
              </tr>
              <?php 

              foreach( $school_fees as $sf) { 
                if( $sf->group_id == 0) {
                  if(round(($sf->amount-$sf->discounts-$sf->payments),2) > 0) {
                ?>
              <tr class="school_fees_groups_0" style="display:none;">
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right money_format school_fees_balance_0"><?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); 
$total_whole_year_fee += ($sf->amount-$sf->discounts-$sf->payments);
                ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_<?php echo $sf->fee_id; ?>" data-check_id="fee_include_<?php echo $sf->fee_id; ?>" name="applied[school_fees][<?php echo $sf->fee_id; ?>]" required></td>
                <td><input type="checkbox" checked id="fee_include_<?php echo $sf->fee_id; ?>"></td>
              </tr>
               <?php } 
                } } ?>
<tr class="success" id="ungrouped_feed_apply_payment">
  <td colspan="4">
  <strong class="pull-right autosum-class money_format" data-class="services_balance">0.00</strong>
  <strong>Services</strong> <button type="button" class="btn btn-xs btn-success show-hide-class" data-class="services_balance_item">Show</button></td>
</tr>
<?php 
$sss2 = array();
foreach($student_services as $ss2) {
  if( !isset($sss2[$ss2->service_id]) ) {
    $sss2[$ss2->service_id]['id'] = $ss2->service_id;
    $sss2[$ss2->service_id]['name'] = $ss2->service_name;
    $sss2[$ss2->service_id]['amount'] = 0;
  }
  $sss2[$ss2->service_id]['amount'] += ($ss2->amount - $ss2->applied);
}
foreach($sss2 as $ssss) {
  if( $ssss['amount'] <= 0 ) {
    continue;
  }
?>
<tr class="services_balance_item" style="display:none;">
      <td> - <?php echo $ssss['name']; ?></td>
      <td class="text-right money_format services_balance"><?php echo $ssss['amount']; ?></td>
      <td class="text-right"><input data-balance="<?php echo $ssss['amount']; ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_<?php echo $ssss['id']; ?>" data-check_id="service_include_<?php echo $ssss['id']; ?>" name="applied[services][<?php echo $ssss['id']; ?>]" required></td>
      <td><input type="checkbox" checked id="service_include_<?php echo $ssss['id']; ?>"></td>
</tr>
<?php } ?>
              <tr>
                <th><strong>TOTAL</strong></th>
                <th class="text-right money_format"><?php echo number_format($remaining_balance2,2); ?></th>
                <th class="text-right money_format" id="applied-balance"></th>
              </tr>
              <tr>
                <th></th>
                <th class="text-right"><a class="pull-right btn btn-danger btn-xs un-apply-payment">Un-Apply Payment</a></th>
                <td class="text-right"><a class="pull-right btn btn-success btn-xs auto-apply-payment">Auto Apply Payment</a></td>
              </tr>
            </tbody>
          </table>
-->
      </div>
	  <div class="modal-footer">
    
          <button type="submit" class="btn btn-primary" id="addPaymentModalSubmit">Submit</button>
      </div>

	  </form>
  </div>
  </div>
</div>

<!--addDiscounts2Modal modal-->
<div style="display: none;" id="addDiscounts2Modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width:700px;">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Discount</strong>
      </div>
   <?php echo form_open("finance/add_discount/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
      <div class="modal-body">
      <div class="row">
          <div class="form-group col-md-6">
              <label>Description</label>
            <input class="form-control text-right" name="description" required>
          </div>
          <div class="form-group col-md-6">
              <label>Amount</label>
            <input class="form-control text-right" id="discount_total_amount" name="amount" required>
          </div>
        </div>

<center>
<a class="btn btn-primary btn-xs show-hide" data-id="addDiscounts2Modal-applied">Show / Hide Details</a>
<a class="btn btn-danger btn-xs un-apply-discount">Un-Apply Discount</a>
<a class="btn btn-success btn-xs auto-apply-discount">Auto Apply Discount</a>
</center>   

          <table class="table table-condensed" id="addDiscounts2Modal-applied" style="display:none;">
            <thead>
              <tr>
                <th></th>
                <th class="text-right">Balance</th>
                <th class="text-right" width="30%">Amount</th>
                <th class="text-right" width="23px"></th>
              </tr>
            </thead>
            <tbody>
            
            <?php foreach($school_fees_groups as $sfg) { ?>
              <tr class="success">
                <td colspan="4">
                <strong class="pull-right autosum-class money_format" data-class="dschool_fees_balance_<?php echo $sfg->id; ?>">0.00</strong>
                <strong><?php echo $sfg->name; ?></strong>
<button type="button" class="btn btn-xs btn-success show-hide-class" data-class="dschool_fees_groups_<?php echo $sfg->id; ?>">Show</button>
                </td>
              </tr>
              <?php 
              foreach( $school_fees as $sf) { 
                if( $sf->group_id == $sfg->id) {
                  if( round(($sf->amount-$sf->discounts-$sf->payments),2) > 0) {
                ?>
              <tr class="dschool_fees_groups_<?php echo $sf->group_id; ?>" style="display:none;">
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right money_format dschool_fees_balance_<?php echo $sf->group_id; ?>"><?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); ?>" class="form-control text-right discount_total_amount money_format" id="discount_total_amount_<?php echo $sf->fee_id; ?>" data-check_id="dfee_include_<?php echo $sf->fee_id; ?>" name="discount[<?php echo $sf->fee_id; ?>]" required>
                </td>
                <td><input type="checkbox" checked id="dfee_include_<?php echo $sf->fee_id; ?>"></td>
              </tr>
              <?php } } } ?>
            <?php } ?>

              <tr class="success" id="ungrouped_feed_apply_payment">
                <td colspan="4">
                <strong class="pull-right autosum-class money_format" data-class="dschool_fees_balance_0">0.00</strong>
                <strong>Ungrouped Fees</strong>  <button type="button" class="btn btn-xs btn-success show-hide-class" data-class="dschool_fees_groups_0">Show</button>
                </td>
              </tr>
              <?php 
              foreach( $school_fees as $sf) { 
                if( $sf->group_id == 0) {
                  if(round(($sf->amount-$sf->discounts-$sf->payments),2) > 0) {
                ?>
              <tr class="dschool_fees_groups_<?php echo $sf->group_id; ?>" style="display:none;">
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right money_format dschool_fees_balance_0"><?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); 
$total_whole_year_fee += ($sf->amount-$sf->discounts-$sf->payments);
                ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); ?>" class="form-control text-right discount_total_amount money_format" id="discount_total_amount_<?php echo $sf->fee_id; ?>" data-check_id="dfee_include_<?php echo $sf->fee_id; ?>" name="discount[<?php echo $sf->fee_id; ?>]" required></td>
                <td><input type="checkbox" checked id="dfee_include_<?php echo $sf->fee_id; ?>"></td>
              </tr>
               <?php } 
                } } ?>

              <tr>
                <th><strong>TOTAL</strong></th>
                <th class="text-right money_format"><?php echo number_format($remaining_balance,2); ?></th>
                <th class="text-right money_format" id="applied-balance2"></th>
              </tr>
              <tr>
                <th></th>
                <th class="text-right"><a class="pull-right btn btn-danger btn-xs un-apply-discount">Un-Apply Discount</a></th>
                <td class="text-right"><a class="pull-right btn btn-success btn-xs auto-apply-discount">Auto Apply Discount</a></td>
              </tr>
            </tbody>
          </table>
      </div>
    <div class="modal-footer">
    
          <button type="submit" class="btn btn-primary" id="addDiscounts2ModalSubmit">Submit</button>
      </div>

    </form>
  </div>
  </div>
</div>

<!--parents modal-->
<div style="display: none;" id="addDiscountsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Discount</strong>
      </div>
   <?php echo form_open("finance/discount/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
      <div class="modal-body">
    <div class="form-group">
          <label>School Fee</label>
          <select class="form-control autofill-select" name="fee_id" required data-autofill="discount_amount">
          <option value="">- - Select Discountable Fee - -</option>

    <?php foreach( $school_fees as $sfd ) { ?>
      <?php if( ($sfd->discountable==1) && ($sfd->group_id == 0)) { ?>
        <option value="<?php echo $sfd->fee_id; ?>" style="font-weight:bold;" data-value="<?php echo number_format($sfd->amount,2);  ?>"><?php echo $sfd->name; ?> (<?php echo number_format($sfd->amount,2);  ?>)</option>
      <?php } ?>
    <?php } ?>

<?php foreach($school_fees_groups as $sfg) { ?> 
  <optgroup label="<?php echo $sfg->name; ?>">
    <?php foreach( $school_fees as $sfd ) { ?>
      <?php if( ($sfd->discountable==1) && ($sfd->group_id == $sfg->id)) { ?>
        <option value="<?php echo $sfd->fee_id; ?>" data-value="<?php echo number_format($sfd->amount,2);  ?>"><?php echo $sfd->name; ?> (<?php echo number_format($sfd->amount,2);  ?>)</option>
      <?php } ?>
    <?php } ?>
  </optgroup>
<?php } ?>

          </select>
    </div><!-- /form-group -->
    <div class="form-group">
          <label>Amount</label>
        <input id="discount_amount" type="text" class="form-control" name="amount" value="0.00" required>
    </div><!-- /form-group -->
    <div class="form-group">
          <label>Description</label>
        <input type="text" class="form-control" name="description" value="" required>
    </div><!-- /form-group -->
      </div>

    <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>

    </form>
  </div>
  </div>
</div>


<!--invoiceDetails modal-->
<div style="display: none;" id="invoiceDetailsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width:60%">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Invoice Detail</strong>
      </div>

      <div class="modal-body text-center" data-img="<?php echo base_url("assets/v1/img/ajax-loader.gif"); ?>">
         
      </div>

  </div>
  </div>
</div>

<!-- paymentDetails modal-->
<div style="display: none;" id="paymentDetailsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width:60%">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Payment Details</strong>
      </div>

      <div class="modal-body text-center" data-img="<?php echo base_url("assets/v1/img/ajax-loader.gif"); ?>">
         
      </div>

  </div>
  </div>
</div>

<!-- paymentDetails modal-->
<div style="display: none;" id="addPromissoryNoteModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width:60%">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add a Promissory Note</strong>
      </div>

      <div class="modal-body">
         
         <div class="row">
           <div class="col-md-6">
             <div class="form-group">
                <label>Date</label>
                <input type="text" class="form-control datepicker" name="date" value="<?php echo date('m/d/Y'); ?>">
             </div>
           </div>
           <div class="col-md-6">
             <div class="form-group">
                <label>Exam Date</label>
                <input type="text" class="form-control datepicker" name="exam_date" value="">
             </div>
           </div>
         </div>

            <div class="form-group">
                <label>Parent / Guardian</label>
                <select class="form-control" name="parent_id">
                    <option value="">Father</option>
                </select>
             </div>

             <div class="form-group">
                <label>Reason</label>
                <input type="text" class="form-control" name="reason" value="">
             </div>

          <div class="row">
           <div class="col-md-6">
             <div class="form-group">
                <label>Promised to Pay On</label>
                <input type="text" class="form-control datepicker" name="exam_date" value="">
             </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                <label>Contact Number</label>
                <input type="text" class="form-control" name="contact_number" value="">
             </div>
            </div>
            </div>

      </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
  </div>
  </div>
</div>

<?php endif; ?>
<?php $this->load->view('footer'); ?>
