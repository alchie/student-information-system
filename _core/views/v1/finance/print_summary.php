<?php 

$level_names = unserialize(LEVEL_NAMES);

$remaining_balance = 0;
$remaining_balance2 = 0;
$total_whole_year_fee = 0;
$total_discounts = 0;
$schedule_fee = array();
$schedule_invoices = array();
$oldaccount_m = ($oldaccount) ? (($oldaccount->amount - $oldaccount->old_down) / $oldaccount->installment) : 0;
$oldaccount_i = ($oldaccount) ? $oldaccount->installment : 1;
foreach( $school_fees as $sf) { 
    $total_fee_amount = ($sf->amount - $sf->discounts) - $sf->downpayments;
    $sf_installment = (($sf->installment - count($excluded_months)) > 0) ? ($sf->installment - count($excluded_months)) : 1;
    $month_installment = ($total_fee_amount / $sf_installment);
    $skip = intval($sf->skip);
    for($i=0;$i<$sf_installment;$i++) {
        $i2 = $i + $skip;
        if( !isset($schedule_fee[$i]) ) {
          $schedule_fee[$i] = 0;
        }
        $schedule_fee[$i2] = $schedule_fee[$i2] + $month_installment;
    }
  }

  $n=0;
  foreach( $schedule_fee as $i=>$sFee ) {
      if( $n < $oldaccount_i ) {
         $schedule_fee[$i] = ($sFee + $oldaccount_m);
        } 
      $n++;
  }

foreach($invoices as $invoice) {
  if(!isset($schedule_invoices[$invoice->month])) {
    $schedule_invoices[$invoice->month] = 0;
  }
  $schedule_invoices[$invoice->month] += $invoice->amount;
}
$total_student_fees = 0;

$sss = array();
foreach($student_services as $ss) {
  if( !isset($sss[$ss->month]) ) {
    $sss[$ss->month] = 0;
  }
  $sss[$ss->month] += $ss->amount;
}

?>
<html>
<head>
<style>

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
/* CSS */
html, body {
    height: 100%;
}
body {
	font-size:11px;
	font-family: Arial;
	padding:0;
	margin: 0;
}
p {
	padding: 0;
	margin: 0;
}
#school_name {
	font-weight: bold;
	font-size:12px;
	text-transform: uppercase;
}
#school_address,
#school_contacts {
	font-size: 10px;
}
.outer {
	margin:0 10px 10px 0;
	width: 100%;
}
.outer.outer-3,
.outer.outer-4 {
	margin-bottom:6px;
	}
.soa-body {
	padding: 3px;
	margin: 10px;
	position: relative;
}
.soa-title {
	text-transform: uppercase;
	margin-top:15px;
	font-weight: bold;
	font-family: serif;
	letter-spacing: 3px;
	margin-bottom:15px;
}
.border {
	border: 1px solid #000;
}
.float-left {
	float:left;
}
.text-center {
	text-align: center;
}
.text-right {
	text-align: right;
}
.text-left {
	text-align: left;
}
.bold {
	font-weight: bold;
}
.underlined {
	text-decoration: underline;
}
.student_details tr td {
	padding-top: 5px;
}
#student_name {
	text-decoration: underline;
	text-transform: uppercase;
	font-weight: bold;
}
#student_name span {
	font-size: 10px;
	font-weight: normal;
}
#grade_level, #campus {
	margin-top:5px;
	font-weight: bold;
	text-transform: uppercase;
}
#grade_level span, #campus span {
	font-size: 10px;
	font-weight: normal;
}
#fees, #services {
	border:1px solid #000;
	margin-top:10px;
}
#fees thead td,
#services thead td {
	padding:5px;
	text-transform: uppercase;
	font-size: 10px;
	font-weight: bold;
}
#fees tbody td,
#services tbody td {
	padding:2px 5px;
	font-size: 10px;
}
#amount_due {
	margin-top: 10px;
	font-size: 10px;
	text-transform: uppercase;
}
#amount_due td {
	padding: 5px;
	}
.bigger {
	font-size: 12px;
}
.logo {
	width: 90px;
	position: absolute;
	top:15px;
	left:15px;
}
.logo.right {
	left:auto;
	right:15px;
}
.details td {
	padding:3px 5px;
}
.allcaps {
	text-transform: uppercase;
}
</style>
</head>
<body>
<center>

<table cellpadding="0" cellspacing="0" class="outer border">
	<tr>
		<td class="text-center">
			<div class="soa-body">
			<img src="<?php echo base_url("assets/v1/img/logo160x160.png"); ?>" class="logo">
			<img src="<?php echo base_url("assets/v1/img/rcbdi_logo.png"); ?>" class="logo right">
			<h1 id="school_name"><?php echo get_ams_config('school', 'school_name'); ?></h1>
			<p id="school_address"><?php echo get_ams_config('school', 'school_address'); ?></p>
			<p id="school_contacts"><?php echo get_ams_config('school', 'school_phone'); ?> &middot; <?php echo get_ams_config('school', 'school_email'); ?></p>
			<p id="school_contacts"><?php echo $primary_school_year->label; ?></p>

			<h3 class="soa-title underlined">Student Account Details</h3>
			 <p class="allcaps bold">as of <?php echo date('F d, Y'); ?></p>
			</div>
		</td>
	</tr>
	<tr>
		<td style="padding:20px 10px 10px 10px">
			<table class="border outer details">
				<tr>
					<td><h3 class="allcaps"><strong class="bold">Name:</strong> <?php echo $enroll_data->lastname; ?>, <?php echo $enroll_data->firstname; ?> <?php echo ($enroll_data->middlename) ? substr($enroll_data->middlename, 0,1)."." : ""; ?></h3></td>
					<td><h3 class="allcaps"><strong class="bold">ID:</strong> <?php echo $enroll_data->idn; ?></h3></td>
					<td><h3 class="allcaps"><strong class="bold">Grade Level:</strong> <?php echo $level_names[$enroll_data->grade_level]; ?></h3></td>
					<td><h3 class="allcaps"><strong class="bold">Campus:</strong> <?php echo $enroll_data->campus_name; ?></h3></td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td style="padding:0px 10px 10px 10px">
			<table class="border outer details">
				<tr>
					<td colspan="3"><h3 class="allcaps bold underlined">Account Summary</h3></td>
				</tr>
<?php 
if( $oldaccount ) {
	$total_student_fees += ($oldaccount) ? $oldaccount->amount : 0;
?>
				<tr>
					<td><h3 class="allcaps">Old Account</h3></td>
					<td class="text-right">
<?php 
	echo ($oldaccount) ? number_format($oldaccount->amount,2) : '0.00'; 
?>
					</td>
					<td></td>
				</tr>
<?php } ?>
				<tr>
					<td><h3 class="allcaps"><?php echo $primary_school_year->label; ?> Fees</h3></td>
					<td class="text-right underlined">
					<?php 
  $total_student_fees += $enroll_data->whole_year;
  echo number_format($enroll_data->whole_year,2); 
?>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="bold"><h3 class="allcaps">Total Fees</h3></td>
					<td></td>
					<td class="text-right bold">
					<?php echo number_format($total_student_fees,2); ?>
					</td>
				</tr>

<tr>
                <td>DEDUCTIONS:</td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>- Total Discounts</td>
                <td class="text-right"><?php echo number_format($enroll_data->discounts,2); ?></td>
                <td></td>
              </tr>
              <tr>
                <td>- Total Downpayments</td>
                <td class="text-right underlined"><?php echo number_format($enroll_data->downpayments,2); ?></td>
                <td></td>
              </tr>
              <tr class="danger">
                <td class="bold">LESS: TOTAL DEDUCTIONS</td>
                <td class="text-right"><?php echo number_format($enroll_data->discounts+$enroll_data->downpayments,2); ?></td>
                <td class="text-right underlined">(<?php echo number_format($enroll_data->discounts+$enroll_data->downpayments,2); ?>)</td>
              </tr>
              <tr>
                <td class="bold">BALANCE SUBJECT TO MONTHLY INSTALLMENT</td>
                <td></td>
                <td class="text-right bold">

                <?php echo number_format(($total_student_fees-($enroll_data->discounts+$enroll_data->downpayments)),2); ?>

                </td>
              </tr>
<?php if( $enroll_data->services ) { ?>
              <tr>
                <td>PLUS: Additional Services</td>
                
                <td class="text-right">

                <?php echo number_format($enroll_data->services,2); ?>

                </td>
                <td></td>
              </tr>
<tr>
                <td>TOTAL</td>
                <td></td>
                <td class="text-right bold">
                <?php echo number_format((($total_student_fees-($enroll_data->discounts+$enroll_data->downpayments)) + $enroll_data->services),2); ?>
                </td>
              </tr>
<?php } ?>
              <tr class="danger">
                <td>LESS: TOTAL MONTHLY PAYMENTS</td>
                <td></td>
                <td class="text-right underlined">(<?php echo number_format($enroll_data->payments,2); ?>)</td>
              </tr>
              <tr class="success">
                <td class="bold">REMAINING BALANCE</td>
                <td></td>
                <td class="text-right bold"><?php 
$remaining_balance = ($total_student_fees-($enroll_data->discounts+$enroll_data->payments+$enroll_data->downpayments));
$remaining_balance2 = ($total_student_fees-($enroll_data->discounts+$enroll_data->payments+$enroll_data->downpayments))+$enroll_data->services;
                echo number_format(($total_student_fees-($enroll_data->discounts+$enroll_data->payments+$enroll_data->downpayments)+$enroll_data->services),2); 

                ?></td>
              </tr>


			</table>
		</td>
	</tr>

	<tr>
		<td style="padding:0px 10px 10px 10px">
			<table class="border outer details">
				<tr>
					<td colspan="3"><h3 class="allcaps bold underlined">Monthly Installments</h3></td>
				</tr>
				<tr>
					<td class="bold allcaps">Month</td>
					<td class="text-right bold allcaps">Monthly Bill</td>
					<td class="text-right bold allcaps">Monthly Balance</td>
				</tr>

<?php 
            $remaining_payments = $enroll_data->payments;
              $total_months = 0;
   						$total_months2 = 0;
   						$total_invoices = 0;
              $total_invoices2 = 0;
   						$month_n = 0;
              $total_services = 0;
   					foreach( $months as $month ) { 
              $total_months_d = '0.00';
              $service_month = ((isset($sss[$month->month])) ? $sss[$month->month] : 0);
              $total_services += $service_month;
              if(isset($schedule_fee[$month_n])) {
                  $total_months_d = $schedule_fee[$month_n];  
                  $total_months += $total_months_d;
                }  
                $total_invoices_d = '0.00';
              if (isset($schedule_invoices[$month->month])) {
                  $total_invoices_d = $schedule_invoices[$month->month];
                  $total_invoices += $total_invoices_d;
                } 
              
              ?>
   						<tr class="<?php 
              $tr_empty = '';
              if( time() >= strtotime($month->month . "/1/" . $month->year)) {
                $tr_empty = 'danger';
              }
              echo ( round($enroll_data->payments,2) >= round($total_invoices,2) ) ? 'success' : $tr_empty; 
              ?>">
   				<td><?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></td>
                <td class="text-right money_format"><?php echo number_format($total_invoices_d,2); ?></td>
                <td class="text-right money_format"><?php echo (($total_invoices-$remaining_payments)>0) ? number_format($total_invoices-$remaining_payments,2) : 0.00; ?></td>
   						</tr>
   		<?php $month_n++; } ?>
   			<tr>
                <td class="bold allcaps">TOTAL</td>
                <td class="text-right bold"><?php echo number_format($total_invoices,2); ?></td>
                <td class="text-right"></td>
              </tr>
			</table>
		</td>
	</tr>

<?php if( $discounts ) { ?>
	<tr>
		<td style="padding:0px 10px 10px 10px">
			<table class="border outer details">
				<tr>
					<td colspan="3"><h3 class="allcaps bold underlined">Discounts</h3></td>
				</tr>


<?php 
foreach( $discounts as $discount ) { ?>
  <tr class="">
    <td><?php echo $discount->fee_name; ?></td>
    <td class="text-right"><?php echo number_format($discount->amount,2); $total_discounts+=$discount->amount; ?></td>
    <td class="small"><?php echo $discount->description; ?></td>
  </tr>
<?php } ?>
              <tr>
                <td class="bold">TOTAL DISCOUNTS</td>
                <td class="text-right bold"><?php echo number_format($total_discounts,2); ?></td>
                <td class="text-right"></td>
              </tr>
			</table>
		</td>
	</tr>
<?php } ?>

<?php if( $payments ) { ?>
	<tr>
		<td style="padding:0px 10px 10px 10px">
			<table class="border outer details">
				<tr>
					<td colspan="3"><h3 class="allcaps bold underlined">Payments</h3></td>
				</tr>
              <tr>
                <td class="bold">Date Paid</td>
                <td class="text-right bold">OR #</td>
                <td class="text-right bold">Amount</td>
              </tr>

<tr class="success">
    <td colspan="3" class="bold">DOWNPAYMENTS</td>
 </tr>
<?php 
$total_payments = 0;
$total_downpayments = 0;
foreach( $payments as $payment ) { 
if($payment->down==0) {
  continue;
}
  ?>
  <tr class="<?php echo ($payment->down==1) ? 'warning2' : ''; ?>">
    <td><?php echo date('m/d/Y', strtotime($payment->payment_date)); ?></td>
    <td class="text-right"><?php echo $payment->receipt_number; ?></td>
    <td class="text-right"><?php echo number_format($payment->amount,2); 
    $total_downpayments+=$payment->amount;
    $total_payments+=$payment->amount; 
    ?></td>
  </tr>
<?php } ?>
<tr class="success">
                <td colspan="2" class="bold">TOTAL DOWNPAYMENTS</td>
                <td class="text-right bold"><?php echo number_format($total_downpayments,2); ?></td>
              </tr>
<tr class="success">
    <td colspan="3" class="bold"><hr>MONTHLY PAYMENTS</td>
 </tr>

<?php 
$total_monthly_payments = 0;
foreach( $payments as $payment ) { 
if($payment->down==1) {
  continue;
}
  ?>
  <tr class="<?php echo ($payment->down==1) ? 'warning2' : ''; ?>">
    <td><?php echo date('m/d/Y', strtotime($payment->payment_date)); ?></td>
    <td class="text-right"><?php echo $payment->receipt_number; ?></td>
    <td class="text-right"><?php echo number_format($payment->amount,2); 
    $total_monthly_payments+=$payment->amount; 
    $total_payments+=$payment->amount; 
    ?></td>
  </tr>
<?php } ?>
<tr class="success">
                <td colspan="2" class="bold">TOTAL MONTHLY PAYMENTS</td>
                <td class="text-right bold"><?php echo number_format($total_monthly_payments,2); ?></td>
              </tr>
<tr class="success">
    <td colspan="3" class="bold"><hr></td>
 </tr>
              <tr class="success">
                <td colspan="2" class="bold">TOTAL PAYMENTS</td>
                <td class="text-right bold"><?php echo number_format($total_payments,2); ?></td>
              </tr>

			</table>
		</td>
	</tr>

<?php } ?>

<tr>
		<td style="padding:0px 10px 10px 10px">
			<table class="outer details">
				<tr>
					<td colspan="3" class="allcaps underlined"><h3><span class=" bold">Prepeared By:</span> <?php echo $this->session->userdata('logged_name'); ?></h3></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</center>
</body>
</html>