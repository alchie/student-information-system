<?php $level_names = unserialize(LEVEL_NAMES); ?>
<html>
<head>
<title>Print SOA</title>
<style>
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
/* CSS */
html, body {
    height: 100%;
}
body {
	font-size:12px;
	font-family: Arial;
	padding:0;
	margin: 0;
}
p {
	padding: 0;
	margin: 0;
}
#school_name {
	font-weight: bold;
	font-size:12px;
	text-transform: uppercase;
}
#school_address,
#school_contacts {
	font-size: 10px;
}
.outer {
	margin:0 10px 10px 0;
	max-height:500px;
	min-height:500px;
	width: 378px;
}
.outer.outer-3,
.outer.outer-4 {
	margin-bottom:6px;
	}
.soa-body {
	padding: 3px;
	margin: 10px;
	position: relative;
}
.soa-title {
	text-transform: uppercase;
	margin-top:10px;
	font-weight: bold;
	font-family: serif;
	letter-spacing: 3px;
	margin-bottom:10px;
}
.border {
	border: 1px solid #000;
}
.float-left {
	float:left;
}
.text-center {
	text-align: center;
}
.text-right {
	text-align: right;
}
.text-left {
	text-align: left;
}
.bold {
	font-weight: bold;
}
.underlined {
	text-decoration: underline;
}
.student_details tr td {
	padding-top: 5px;
}
#student_name {
	text-decoration: underline;
	text-transform: uppercase;
	font-weight: bold;
}
#student_name span {
	font-size: 10px;
	font-weight: normal;
}
#grade_level, #campus {
	margin-top:5px;
	font-weight: bold;
	text-transform: uppercase;
}
#grade_level span, #campus span {
	font-size: 10px;
	font-weight: normal;
}
#fees, #services {
	border:1px solid #000;
	margin-top:10px;
}
#fees thead td,
#services thead td {
	padding:5px;
	text-transform: uppercase;
	font-size: 10px;
	font-weight: bold;
}
#fees tbody td,
#services tbody td {
	padding:2px 5px;
	font-size: 10px;
}
#amount_due {
	margin-top: 10px;
	font-size: 10px;
	text-transform: uppercase;
}
#amount_due td {
	padding: 5px;
	}
#amount_due.with_services td {
	padding: 2px;
	}
.bigger {
	font-size: 12px;
}
.logo {
	width: 55px;
	position: absolute;
	top:5px;
	left:-5px;
}
.logo.right {
	left:auto;
	right:0;
}
</style>
</head>
<body>
<center>
<?php 
$count_n = 1;
$overall_amount_due = 0;
foreach( $enrollees as $enrollee ): 
	?>
<table cellpadding="0" cellspacing="0" class="outer border float-left outer-<?php echo $count_n; ?>">
	<tr>
		<td class="text-center">
			<div class="soa-body">
			<img src="<?php echo base_url("assets/v1/img/logo160x160.png"); ?>" class="logo">
			<img src="<?php echo base_url("assets/v1/img/rcbdi_logo.png"); ?>" class="logo right">
			<h1 id="school_name"><?php echo get_ams_config('school', 'school_name'); ?></h1>
			<p id="school_address"><?php echo get_ams_config('school', 'school_address'); ?></p>
			<p id="school_contacts"><?php echo get_ams_config('school', 'school_phone'); ?> &middot; <?php echo get_ams_config('school', 'school_email'); ?></p>
			<p id="school_contacts"><?php echo $enrollee->school_year_label; ?></p>

			<h3 class="soa-title underlined">Statement of Account</h3>
			<h3 class="soa-title"><?php echo date('F', strtotime($enrollee->current_month->month."/1/" . $enrollee->current_month->year)); ?> <?php echo $enrollee->current_month->year; ?></h3>

<table width="100%" class="student_details">
	<tr>
		<td style="width:100%"><span id="student_name"><span>Name:</span> <?php echo $enrollee->lastname; ?>, <?php echo $enrollee->firstname; ?> <?php echo ($enrollee->middlename!='') ? substr($enrollee->middlename,0,1)."." : ""; ?></span></td>
		<td class="text-right">
			<span class="underlined"><?php echo date('m/d/Y'); ?></span>
		</td>
	</tr>
</table>
<table width="100%" class="student_details">
	<tr>
		<td style="width:50%" id="grade_level"><span>Level:</span><?php echo $level_names[$enrollee->grade_level]; ?></td>
		<td style="width:50%; text-align: right;" id="campus"><span>Campus:</span><?php echo $enrollee->campus_name; ?></td>
	</tr>
</table>
<table width="100%" id="fees">
	<thead>
		<tr>
			<td width="40%">Particulars</td>
			<td width="20%" class="text-right">Payable</td>
			<td width="20%" class="text-right">Payments</td>
			<td width="20%" class="text-right">Balance</td>
		</tr>
	</thead>
<?php 
$overall_payable = 0;
$overall_paid = 0;
$overall_balance = 0;

$overall_payable += $enrollee->old_account;
$overall_paid  += ($enrollee->old_payments + $enrollee->old_payments2);
$overall_balance += ($enrollee->old_account - ($enrollee->old_payments + $enrollee->old_payments2));

?>
	<tbody>
	<tr>
			<td>Old Account</td>
			<td class="text-right"><?php echo number_format($enrollee->old_account,2); ?></td>
			<td class="text-right"><?php echo number_format(($enrollee->old_payments + $enrollee->old_payments2),2); ?></td>
			<td class="text-right"><?php echo number_format(($enrollee->old_account - ($enrollee->old_payments + $enrollee->old_payments2)),2); ?></td>
		</tr>
<?php 

foreach($school_fees_groups as $sfgrp) {

$total_payable = 0;
$total_paid = 0;
$total_balance = 0;

foreach($enrollee->school_fees as $sfees) {
	if( $sfees->group_id == $sfgrp->id) {
		$total_payable += $sfees->amount;
		$total_paid += ($sfees->payments+$sfees->discounts+$sfees->payment_applied);
		$total_balance += ($sfees->amount-($sfees->payments+$sfees->discounts+$sfees->payment_applied));
	}
}

$overall_payable += $total_payable;
$overall_paid += $total_paid;
$overall_balance += $total_balance;

?>
		<tr>
			<td><?php echo $sfgrp->name; ?></td>
			<td class="text-right"><?php echo number_format($total_payable,2); ?></td>
			<td class="text-right"><?php echo number_format($total_paid,2); ?></td>
			<td class="text-right"><?php echo number_format($total_balance,2); ?></td>
		</tr>
	<?php } ?>
<?php 
foreach($enrollee->school_fees as $sfees) {
if( $sfees->group_id == 0) {
	?>
		<tr>
			<td><?php echo $sfees->name; ?></td>
			<td class="text-right"><?php echo number_format($sfees->amount,2); ?></td>
			<td class="text-right"><?php echo number_format(($sfees->discounts+$sfees->payments+$sfees->payment_applied),2); ?></td>
			<td class="text-right"><?php echo number_format(($sfees->amount-($sfees->discounts+$sfees->payments+$sfees->payment_applied)),2); ?></td>
		</tr>
<?php 
$overall_payable += $sfees->amount;
$overall_paid += ($sfees->discounts+$sfees->payments+$sfees->payment_applied);
$overall_balance += ($sfees->amount-($sfees->discounts+$sfees->payments+$sfees->payment_applied));
	}
} ?>
<tr>
			<td class="text-left bold">TOTAL</td>
			<td class="text-right bold"><?php echo number_format($overall_payable,2); ?></td>
			<td class="text-right bold"><?php echo number_format($overall_paid,2); ?></td>
			<td class="text-right bold"><?php echo number_format($overall_balance,2); ?></td>
		</tr>
	</tbody>
</table>
<?php if( count( $enrollee->services ) > 0 ) { ?>
<table width="100%" id="services">
	<thead>
		<tr>
			<td>Products / Services</td>
			<td class="text-right">Amount</td>
		</tr>
	</thead>
	<tbody>
<?php 
foreach($enrollee->services as $service) { 

	?>
		<tr>
			<td><?php echo $service->service_name; ?></td>
			<td class="text-right bold"><?php echo number_format($service->amount,2); ?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php } ?>
<?php 

$overall_amount_due += ($enrollee->amount_due + $enrollee->old_account);
$applied_payment = ($enrollee->payments + $enrollee->old_payments) - $overall_amount_due;
$monthly_due = array();
foreach( $months as $month ) {
	$monthly_due[$month->month] = 0;
	foreach( $enrollee->monthly_fees as $mFee ) {
		if( $mFee->month == $month->month ) {
			$monthly_due[$month->month] += $mFee->amount;
		}
	}
}

$balance_month = (isset($enrollee->balance_month)) ? $enrollee->balance_month->month : 0;

	$previous_months = 0; 
	foreach($monthly_due as $mm => $mDue) {
		if( $mm == $enrollee->balance_month->month) {
			break;
		}
		$previous_months += $mDue;
	}
	$due = $monthly_due[$enrollee->balance_month->month];

	$d_payments = $previous_months - $enrollee->payments;
	$d_final = $d_payments + $due;

	if( $d_final < 0) {
		$d_final = 0;
	}

?>

<?php if($this->input->get('installment')) { ?>
<table width="100%" id="amount_due" class="<?php echo ( count( $enrollee->services ) > 0 ) ? "with_services1" : ""; ?>">
<tr>
	<td>Total Amount Due</td>
	<td width="25%" class="text-right underlined bold money_format bigger"><?php echo number_format($d_final,2); ?></td>
</tr>
	<tr>
		<td colspan="2" class="text-center">Should be paid in the remaining <br><?php echo count($installment); ?> months of the school year.</td>
	</tr>
<?php 
$insta_month = $this->input->get('installment_month');
foreach($installment as $inst) { ?>
<tr class="border">
	<td>On or before <span class="underlined bold bigger"><?php echo date('F d, Y', strtotime( $insta_month[$inst] ));?></span></td>
	<td width="25%" class="text-right underlined bold money_format bigger"><?php echo number_format(($d_final/count($installment)),2); ?></td>
</tr>
<?php } ?>
<tr>
	<td></td>
	<td width="25%" class="text-right underlined bold money_format bigger"><?php echo number_format($d_final,2); ?></td>
</tr>
	<tr>
		<td colspan="2" class="text-center bigger">Please be guided accordingly.</td>
	</tr>
</table>

<?php } else { ?>
<table width="100%" id="amount_due" class="<?php echo ( count( $enrollee->services ) > 0 ) ? "with_services" : ""; ?>">
<tr>
	<td>Previous Balance</td>
	<td width="25%" class="text-right money_format"><?php echo number_format($d_payments,2); ?></td>
	<td width="25%"></td>
</tr>
	<tr>
		<td>Due This Month</td>
		<td class="text-right money_format"><?php echo number_format($due,2); ?></td>
		<td></td>
	</tr>
<tr>
	<td>Please pay the amount of</td>
	<td>
	<td class="text-right underlined bold money_format bigger"><?php echo number_format($d_final,2); ?></td>
</tr>
	<tr>
		<td>On or Before</td>
		<td colspan="2" class="text-right underlined bold bigger"><?php echo date('F d, Y', strtotime($enrollee->due_date)); ?></td>
	</tr>
</table>
<?php } ?>

<!--
<table width="100%" style="margin-top: 10px;">
	<tr>
		<td colspan="3" class="text-center border" style="padding:10px;">For more info, login to <span class="bold">SIS.SLCD.EDU.PH</span> <br>using your SLCD Account: <span class="bold"><?php echo ams_slugify(url_title($enrollee->lastname . " " .ams_initials($enrollee->firstname)), "_", true); ?>@slcd.edu.ph</span>
		<br>Visit the <span class="bold">Finance Office</span> to get your temporary password!
		</td>
	</tr>
</table>
-->
<?php if($this->input->get('notes')) { ?>
<table width="100%" style="margin-top: 10px;">
	<tr>
		<td class="text-center border" style="padding:10px;">
			<?php echo $this->input->get('notes'); ?>
		</td>
	</tr>
</table>
<?php } ?>
			</div>
		</td>
	</tr>
</table>
<?php $count_n++; 
if( $count_n == 5 ) {
	$count_n=1;
}
endforeach; ?>
<?php  if( !$preview_first ) { ?>
<script>
	 //window.print();
</script>
<?php } ?>
</center>
</body>
</html>