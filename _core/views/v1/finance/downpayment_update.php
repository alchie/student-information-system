<?php 
$total_amount = 0;
$total_whole_year_fee=0;
$total_discounts=0;
$total_payments=0;
$total_applied = 0;
$this->load->view('header'); ?>

<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
	<div class="col-sm-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">
          <a href="<?php echo site_url("finance/delete_payment/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $payment->id); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete Payment</a>
          <h4>Update Downpayment</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

<form class="form-horizontal form-label-left" method="post">
<div class="row">
      	<div class="col-md-4">

            <label>Payment Date</label>
            <input class="form-control datepicker" name="payment_date" required value="<?php echo date('m/d/Y', strtotime($payment->payment_date)); ?>">
          </div>

         <div class="col-md-4">

              <label>Receipt Number</label>
            <input class="form-control text-right" name="receipt_number" required="" value="<?php echo $payment->receipt_number; ?>">

          </div>
          <div class="col-md-4">
              <label>Amount</label>
            <input class="form-control text-right money_format" id="payment_total_amount" name="total_amount" required value="<?php echo $payment->amount; ?>">
          </div>
</div>
<div class="row">
  <div class="col-md-12">
 
    <label>Memo</label>
    <input class="form-control" name="memo"  value="<?php echo $payment->memo; ?>">
  
  </div>
</div>

<center style="margin-top:20px;">
<a class="btn btn-primary btn-xs show-hide" data-id="addPaymentModal-applied">Show / Hide Details</a>
<a class="btn btn-danger btn-xs un-apply-payment">Un-Apply Payment</a>
<a class="btn btn-success btn-xs auto-apply-payment">Auto Apply Payment</a>
</center>  

          <table class="table table-condensed"  id="addPaymentModal-applied" style="display:none;">
            <thead>
              <tr>
                <th></th>
                <th class="text-right">Balance without this payment</th>
                <th class="text-right" width="30%">Amount</th>
              </tr>
            </thead>
            <tbody>
            <?php if($oldaccount&&($oldaccount->amount>0)) { 
              $balance = ($oldaccount->amount-$oldaccount->payments) + $oldaccount->applied;
               if($balance > 0) {
                 $total_amount += $balance;
              ?>
             <tr class="warning">
                <td><strong>Old Account</strong></td>
                <td class="text-right money_format"><?php echo number_format($balance,2); ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format($balance,2); ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_0" name="applied[school_fees][0]" required value="<?php echo $oldaccount->applied; $total_applied += $oldaccount->applied; ?>"></td>
              </tr>
            <?php  } }  ?>
            <?php foreach($school_fees_groups as $sfg) { 

            	?>
              <tr class="success">
                <td colspan="4"><strong><?php echo $sfg->name; ?></strong></td>
              </tr>
              <?php 
              foreach( $school_fees as $sf) { 
                if( $sf->group_id == $sfg->id) { 
                	$balance = ($sf->amount-$sf->discounts-$sf->payments) + $sf->applied;
                  if($balance > 0) {
                     $total_amount += $balance;
                ?>
              <tr>
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right money_format"><?php echo number_format($balance,2); ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format($balance,2); ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_<?php echo $sf->fee_id; ?>" name="applied[school_fees][<?php echo $sf->fee_id; ?>]" required value="<?php echo $sf->applied; $total_applied += $sf->applied; ?>"></td>
              </tr>
              <?php } } } ?>
            <?php } ?>


              <tr class="success">
                <td colspan="4"><strong>Ungrouped Fees</strong></td>
              </tr>
              <?php 
              foreach( $school_fees as $sf) { 
                if( $sf->group_id == 0) { 
                  $balance = ($sf->amount-$sf->discounts-$sf->payments) + $sf->applied;
                  if($balance > 0) {
                     $total_amount += $balance;
                ?>
              <tr>
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right"><?php echo number_format($balance,2); ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format($balance,2); ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_<?php echo $sf->fee_id; ?>" name="applied[school_fees][<?php echo $sf->fee_id; ?>]" required value="<?php echo $sf->applied; $total_applied += $sf->applied; ?>"></td>
              </tr>
              <?php } } } ?>


<tr class="success" id="ungrouped_feed_apply_payment">
  <td colspan="4"><strong>Services</strong></td>
</tr>
<?php 
/*
$sss2 = array();
foreach($student_services as $ss2) {
  if( !isset($sss2[$ss2->service_id]) ) {
    $sss2[$ss2->service_id]['id'] = $ss2->service_id;
    $sss2[$ss2->service_id]['name'] = $ss2->service_name;
    $sss2[$ss2->service_id]['amount'] = 0;
    $sss2[$ss2->service_id]['applied'] = $ss2->applied;
  }
  $sss2[$ss2->service_id]['amount'] += $ss2->amount;
}

foreach($sss2 as $ssss) {
  if( ($ssss['applied']) && ($ssss['applied'] <= 0 ) ) {
    continue;
  }
  $balance = $ssss['amount'] + $ssss['applied'];
  if( $balance > 0) {
    $total_amount += $ssss['amount'];
?>
<tr>
      <td> - <?php echo $ssss['name']; ?></td>
      <td class="text-right money_format"><?php 
 
      echo $ssss['amount']; ?></td>
      <td class="text-right"><input data-balance="<?php echo $balance; ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_<?php echo $ssss['id']; ?>" name="applied[services][<?php echo $ssss['id']; ?>]" value="<?php echo $ssss['applied']; $total_applied += $ssss['applied']; ?>" required></td>
</tr>
<?php } } */ ?>
<?php 

foreach($student_services as $ss3) {
  $balance = ($ss3->amount-$ss3->total_applied) + $ss3->applied;
?>
<tr>
      <td> - <?php echo $ss3->service_name; ?></td>
      <td class="text-right money_format"><?php echo $balance; ?></td>
      <td class="text-right"><input data-balance="<?php echo $balance; ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_<?php echo $ss3->service_id; ?>" name="applied[services][<?php echo $ss3->service_id; ?>]" value="<?php echo $ss3->applied; $total_applied += $ss3->applied; ?>" required></td>
</tr>
<?php } ?>

              <tr>
                <th><strong>TOTAL</strong></th>
                <th class="text-right money_format2"><?php 
$oldaccount_a = ($oldaccount) ? $oldaccount->amount : 0;
$total = (($oldaccount_a+$enroll_data->whole_year)-($enroll_data->discounts+$enroll_data->payments));
                echo number_format($total_amount,2); ?></th>
                <th class="text-right money_format" id="applied-balance"><?php echo number_format($total_applied,2); ?></th>
              </tr>
               <tr>
                <th></th>
                <th class="text-right"><a class="pull-right btn btn-danger btn-xs un-apply-payment">Un-Apply Payment</a></th>
                <td class="text-right"><a class="pull-right btn btn-success btn-xs auto-apply-payment" >Auto Apply Payment</a></td>
              </tr>
            </tbody>
          </table>
			
<div class="form-group" style="margin-top:20px">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("finance/ledger/" . $primary_school_year->id . "/" . $enroll_data->id); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

