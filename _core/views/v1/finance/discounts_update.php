<?php 
$this->load->view('header'); 
?>

<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
	<div class="col-sm-8 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">
<a href="<?php echo site_url(str_replace("discount", 'delete_discount', uri_string())); ?>" class="confirm btn btn-danger btn-xs pull-right">Delete</a>
          <h4>
<?php if( $discount_id ) { 
  echo "Update Discount";
} else {
  echo "Add Discount";
}
  ?>
          </h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

<?php if( $discount_id ) { ?>

<?php echo form_open("finance/discount/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $discount->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>

  <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Discountable Fee</label>
           <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-control">
              <input name="fee_id" type="hidden" value="<?php echo $discount->fee_id; ?>">
                <?php echo $discount->fee_name; ?> (<?php echo number_format($discount->fee_amount,2); ?>)
            </div>
          </div>
    </div><!-- /form-group -->
<?php } else { ?>

<?php echo form_open("finance/discount/" . $primary_school_year->id . "/" . $enroll_data->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>

<div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Discountable Fee</label>
           <div class="col-md-6 col-sm-6 col-xs-12">
          <select class="form-control" name="fee_id" required>
          <option value="">- - Select Discountable Fee - -</option>
<?php foreach($school_fees_groups as $sfg) { ?> 
  <optgroup label="<?php echo $sfg->name; ?>">
    <?php foreach( $school_fees as $sfd ) { ?>
      <?php if($sfd->group_id == $sfg->id) { ?>
        <option value="<?php echo $sfd->fee_id; ?>"><?php echo $sfd->name; ?> (<?php echo number_format($sfd->amount,2);  ?>)</option>
      <?php } ?>
    <?php } ?>
  </optgroup>
<?php } ?>
  <?php foreach( $school_fees as $sfd ) { ?>
    <?php if($sfd->group_id == 0) { ?>
      <option value="<?php echo $sfd->fee_id; ?>" style="font-weight:bold;"><?php echo $sfd->name; ?> (<?php echo number_format($sfd->amount,2);  ?>)</option>
    <?php } ?>
  <?php } ?>
          </select>
          </div>
    </div><!-- /form-group -->
<?php } ?>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Amount</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" name="amount" class="form-control col-md-7 col-xs-12 money_format" required="required" value="<?php echo ($discount) ? $discount->amount : '0.00'; ?>">
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Description</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
  <input type="text" name="description" class="form-control col-md-7 col-xs-12" required="required" value="<?php echo ($discount) ? $discount->description : ''; ?>">
  </div>
</div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("finance/discounts/" . $primary_school_year->id . "/" . $enroll_data->id); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			 </div>
      </div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

