<?php 
$this->load->view('header'); ?>

<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
	<div class="col-sm-8 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Update Old Account</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

<?php echo form_open("finance/oldaccount/" . $primary_school_year->id . "/" . $enroll_data->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Amount</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
  <input type="text" name="amount" class="form-control col-md-7 col-xs-12 money_format" required="required" value="<?php echo ($oldaccount) ? $oldaccount->amount : '0.00'; ?>">
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Installments</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
  <select name="installments" class="form-control col-md-7 col-xs-12" required="required">
<?php for($i=1;$i<=count($months);$i++) {
  $selected = ($oldaccount&&$oldaccount->installment==$i) ? 'selected' : '';
  echo "<option value='{$i}' {$selected}>{$i}</option>";
} ?>
  </select></div>
</div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("finance/ledger/" . $primary_school_year->id . "/" . $enroll_data->id); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			 </div>
      </div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

