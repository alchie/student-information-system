<?php  
$total_whole_year_fee=0;
$total_discounts=0;
$total_payments=0;
$total_applied = 0;
$this->load->view('header'); ?>

  <?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
<a class="btn btn-warning btn-sm pull-right" href="<?php echo site_url("finance/ledger/{$primary_school_year->id}/{$enroll_data->id}"); ?>">Back</a>
          <h4>School Fees</h4></div>
   			<div class="panel-body">

		

<table class="table table-condensed">
            <thead>
              <tr>
                <th></th>
                <th class="text-right">Fees</th>
                <th class="text-right">Discounts</th>
                <th class="text-right">Balance</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($school_fees_groups as $sfg) { ?>
              <tr class="success">
                <td colspan="4"><strong><?php echo $sfg->name; ?></strong></td>
              </tr>
              <?php foreach( $school_fees as $sf) { 
                if($sf->group_id == $sfg->id) {
                  $total_whole_year_fee+=round($sf->amount,2);
                  $total_discounts+=round($sf->discounts,2);
                ?>
              <tr>
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right fees_<?php echo $sf->group_id; ?>"><?php echo number_format($sf->amount,2);  ?></td>
                <td class="text-right discount_<?php echo $sf->group_id; ?>""><?php echo number_format($sf->discounts,2);  ?></td>
                <th class="text-right balance_<?php echo $sf->group_id; ?>""><?php echo number_format(($sf->amount-$sf->discounts),2); ?></th>
              </tr>
              <?php } } ?>
              <tr class="warning">
                <th>TOTAL <?php echo $sfg->name; ?></th>
                <th class="text-right bold money_format autosum-class" data-class="fees_<?php echo $sfg->id; ?>">0.00</th>
                <th class="text-right bold money_format autosum-class" data-class="discount_<?php echo $sfg->id; ?>">0.00</th>
                <th class="text-right bold money_format autosum-class" data-class="balance_<?php echo $sfg->id; ?>">0.00</th>
              </tr>
               
            <?php } ?>
              <tr class="success">
                <td colspan="4"><strong>Ungrouped Fees</strong></td>
              </tr>
            <?php foreach( $school_fees as $sf) { 
                if( $sf->group_id == 0) {
                  $total_whole_year_fee+=round($sf->amount,2);
                  $total_discounts+=round($sf->discounts,2);
                ?>
              <tr>
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right fees_0"><?php echo number_format($sf->amount,2);  ?></td>
                <td class="text-right discount_0"><?php echo number_format($sf->discounts,2);  ?></td>
                <th class="text-right balance_0"><?php echo number_format(($sf->amount-$sf->discounts),2); ?></th>
              </tr>
              <?php } } ?>
               <tr class="warning">
                <th>TOTAL Ungrouped Fees</th>
                <th class="text-right bold money_format autosum-class" data-class="fees_0">0.00</th>
                <th class="text-right bold money_format autosum-class" data-class="discount_0">0.00</th>
                <th class="text-right bold money_format autosum-class" data-class="balance_0">0.00</th>
              </tr>
              <tr>
                <td colspan="4">
                  <hr>
                </td>
              </tr>
              <tr class="danger">
                <th><strong>OVERALL TOTAL</strong></th>
                <th class="text-right"><?php echo number_format($total_whole_year_fee,2); ?></th>
                <th class="text-right"><?php echo number_format($total_discounts,2); ?></th>
                <th class="text-right"><?php echo number_format(($total_whole_year_fee-$total_discounts),2); ?></th>
              </tr>
            </tbody>
          </table>

            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

