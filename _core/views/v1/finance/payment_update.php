<?php 
$total_amount = 0;
$total_whole_year_fee=0;
$total_discounts=0;
$total_payments=0;
$total_applied = 0;
$this->load->view('header'); ?>

<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
	<div class="col-sm-8 col-md-8 col-md-offset-2 loading-block-container">
        <div class="panel panel-default">
          <div class="panel-heading">
          <a href="<?php echo site_url("finance/delete_payment/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $payment->id); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete Payment</a>
          <h4>Update Payment</h4></div>
   			<div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

<form class="form-horizontal form-label-left" method="post">

<div class="row">
      	<div class="col-md-4">

            <label>Payment Date</label>
            <input class="form-control datepicker" name="payment_date" required value="<?php echo date('m/d/Y', strtotime($payment->payment_date)); ?>">
          </div>

         <div class="col-md-4">

              <label>Receipt Number</label>
            <input class="form-control text-right" name="receipt_number" required="" value="<?php echo $payment->receipt_number; ?>">

          </div>
          <div class="col-md-4">
         
              <label>Amount</label>
            <input class="form-control text-right money_format" id="payment_total_amount" name="total_amount" required value="<?php echo $payment->amount; ?>">
          </div>
</div>

<div class="row">
  <div class="col-md-12">
 
    <label>Memo</label>
    <input class="form-control" name="memo"  value="<?php echo $payment->memo; ?>">
  
  </div>
</div>

<?php if( $invoices_count ) { ?>

<center style="margin:20px 0">
  <a class="btn btn-primary btn-xs show-hide" data-id="addPaymentModal-applied">Show / Hide Details</a>
  <a class="btn btn-danger btn-xs un-apply-payment">Un-Apply Payment</a>
  <a class="btn btn-success btn-xs auto-apply-payment">Auto Apply Payment</a>
</center>  

          <table class="table table-condensed"  id="addPaymentModal-applied" style="display:none;">
            <thead>
              <tr>
                <th></th>
                <th class="text-right">Balance without this payment</th>
                <th class="text-right" width="30%">Amount</th>
              </tr>
            </thead>
            <tbody>
<?php 
$n = 0;
$balance = 0;
foreach( $months as $month ) { 
?>
<?php if( $month->invoices ) { $n++; ?>
              <tr class="success">
                <td colspan="3">
                <strong><?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></strong>
<button type="button" class="btn btn-xs btn-success show-hide-class pull-right" data-class="month_fees_<?php echo $month->month; ?>">Show</button>
                </td>
              </tr>
<?php 
$balance_month = 0;
              foreach( $month->invoices as $invoice) { 
                 $total_applied += $invoice->applied;
                 $balance_item = ($invoice->amount - $invoice->total_applied) + $invoice->applied;

                 if( intval($balance_item) == 0) {
                  continue;
                 }
                ?>
              <tr style="display:none;" class="month_fees_<?php echo $month->month; ?>">
                <td> &nbsp; &nbsp; &nbsp; &nbsp; <?php echo $invoice->fee_name; ?></td>
                <td class="text-right money_format"><?php 

                echo number_format($balance_item,2); 
                  $balance+=$balance_item; 
                  $balance_month+=$balance_item;
                ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format($balance_item,2);  ?>" class="form-control text-right payment_total_amount money_format" id="payment_total_amount_<?php echo $invoice->id; ?>" name="applied[<?php echo $invoice->id; ?>]" required value="<?php echo $invoice->applied; ?>"></td>
              </tr>
              <?php }  ?>
              <tr class="warning month_fees_<?php echo $month->month; ?>" style="display:none;">
                <td colspan="2">
                <strong>TOTAL <?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></strong>
                </td>
                <td class="text-right">
                  <strong><?php echo number_format($balance_month,2); ?></strong>
                </td>
              </tr>
<?php } ?>
<?php } ?>
<?php if( $n > 0) { ?>
              <tr>
                <th><strong>TOTAL</strong></th>
                <th class="text-right money_format2"><?php echo number_format($balance,2); ?></th>
                <th class="text-right money_format" id="applied-balance"><?php echo number_format($total_applied,2); ?></th>
              </tr>
               <tr>
                <th><a href="<?php echo site_url("finance/reset_payment/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $payment->id); ?>" class="pull-left btn btn-danger btn-xs confirm">Reset Payment</a></th>
                <th class="text-right"><a class="pull-right btn btn-danger btn-xs un-apply-payment">Un-Apply Payment</a></th>
                <td class="text-right"><a class="pull-right btn btn-success btn-xs auto-apply-payment" >Auto Apply Payment</a></td>
              </tr>
<?php } ?>
            </tbody>
          </table>
			
<?php } ?>

<div class="form-group" style="margin-top:20px">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("finance/ledger/" . $primary_school_year->id . "/" . $enroll_data->id); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
					 </form>
			
            </div>
   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

