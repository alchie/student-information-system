<?php 
$level_names = unserialize(LEVEL_NAMES);

$ex_months = array();
foreach( $excluded_months as $em ) { 
  $ex_months[$em->excluded] = $em;
}

$total_whole_year_fee = 0;
$total_discounts = 0;


$schedule_fee = array();
$schedule_invoices = array();
$oldaccount_m = ($oldaccount) ? (($oldaccount->amount - $oldaccount->old_down) / $oldaccount->installment) : 0;
$oldaccount_i = ($oldaccount) ? $oldaccount->installment : 1;
/*
foreach( $school_fees as $sf) { 
    $total_fee_amount = ($sf->amount - $sf->discounts) - $sf->downpayments;
    $sfim = ($sf->installment2) ? $sf->installment2 : $sf->installment;
    //$sf_installment = (($sfim - count($ex_months))>0) ? ($sfim - count($ex_months)): 1;
    $sf_installment = ($sfim) ? $sfim : 1;
    $month_installment = $total_fee_amount / $sf_installment ;
    $skip = ($sf->skip2) ? intval($sf->skip2) : intval($sf->skip);
    for($i=0;$i<$sf_installment;$i++) {
       $i2 = $i + $skip;
        if( !isset($schedule_fee[$i2]) ) {
          $schedule_fee[$i2] = 0;
        }
        $schedule_fee[$i2] = $schedule_fee[$i2] + $month_installment;
    }
  }

foreach($invoices as $invoice) {
  if(!isset($schedule_invoices[$invoice->month])) {
    $schedule_invoices[$invoice->month] = 0;
  }
  $schedule_invoices[$invoice->month] += $invoice->amount;
}
*/
// initialize months
foreach( $months as $month ) {
  $var1 = 'month_' . $month->month;
  $$var1 = 0;
}

$this->load->view('header'); 
?>
<?php if( !isset($primary_school_year) ) : ?>
<div class="alert alert-danger alert-dismissable">
	<strong>SCHOOL YEAR ERROR!</strong> No current school year found! Please assign a current school year or select a school year <a href="<?php echo site_url('school_year'); ?>">here</a>.
</div>
<?php else: ?>

  <?php $this->load->view('finance/finance_navbar'); ?>
  
<div class="row">
	<div class="col-sm-12 col-md-12">

      <div class="panel panel-default">
          <div class="panel-heading">
          <a class="btn btn-warning btn-sm pull-right" href="<?php echo site_url("finance/ledger/{$primary_school_year->id}/{$enroll_data->id}"); ?>">Back</a>
          <button style="margin-right:5px;" class="btn btn-danger btn-sm pull-right" data-toggle="modal" href="#excludeMonthsModal">Exclude Months</button>
		  <h4>Detailed Schedule of Fees</h4></div>
   			<div class="panel-body">
   				<table class="table table-condensed">
            <thead>
              <tr>
                <th class="text-left">Fees</th>
                <?php foreach( $months as $month ) { ?>
                <th class="text-right"><?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></th>
                <?php } ?>
              </tr>
            </thead>
   					<tbody>
            <tr class="warning">
                <td><strong>Old Account</strong></td>
                <?php 
                $i=0;
                foreach( $months as $month ) { ?>
                  <td class="text-right money_format"><?php 
                  if( ($oldaccount) && ($i < $oldaccount->installment) ) {
                    $var4 = 'month_' . $month->month;
                    $$var4 += $oldaccount_m;
                    echo $oldaccount_m;
                  }
                  ?></td>
                <?php $i++; } ?>
              </tr>
   					<?php foreach($school_fees_groups as $sfg) { ?>
   						<tr class="success">
   							<td colspan="<?php echo count($months) + 1; ?>"><strong><?php echo $sfg->name; ?></strong></td>
   						</tr>
   						<?php 
              foreach( $school_fees as $sf) { 
   							if( $sf->group_id == $sfg->id) {
                  $total_whole_year_fee += round($sf->amount,2);
                  $total_discounts += round($sf->discounts,2);
                  $sfim = ($sf->installment2) ? $sf->installment2 : $sf->installment;
                  $sf_installment = ($sfim) ? $sfim : 1; 
                  if( $sf_installment > count($months) ) {
                     $sf_installment = count($months);
                  }
                  $sfkip = ($sf->skip2) ? $sf->skip2 : $sf->skip;
   							?>
   						<tr>
   							<td> - <a class="installment-override" data-toggle="modal" href="#installmentOverrideModal" data-id="<?php echo $sf->fee_id; ?>" data-name="<?php echo $sf->name; ?>" data-installment="<?php echo $sf_installment; ?>" data-skip="<?php echo $sfkip; ?>"><?php echo $sf->name; ?></a></td>
                 <?php $n = 0;
                  foreach( $months as $month ) { ?>
   							    <td class="text-right money_format"><?php
                    
                     $amount = (($sf->amount - ($sf->discounts + $sf->downpayments)) / $sf_installment); 
                    if( ( $n >= $sfkip ) && ($n < ($sf_installment + $sfkip)) ) {
                        echo $amount; 
                        $var2 = 'month_' . $month->month;
                        $$var2 += $amount;

                    }
                    ?></td>
                <?php $n++; } ?>
   						</tr>
   						<?php } } ?>
   					<?php } ?>
              <tr class="success">
                <td colspan="<?php echo count($months) + 1; ?>"><strong>Ungrouped Fees</strong></td>
              </tr>
            <?php foreach( $school_fees as $sf) { 
                if( $sf->group_id == 0) {
                  $total_whole_year_fee+=round($sf->amount,2);
                  $total_discounts+=round($sf->discounts,2);
                  $sfim = ($sf->installment2) ? $sf->installment2 : $sf->installment;
                  $sf_installment = ($sfim) ? $sfim : 1; 
                  if( $sf_installment > count($months) ) {
                     $sf_installment = count($months);
                  }
                  $sfkip = ($sf->skip2) ? $sf->skip2 : $sf->skip;
                ?>
              <tr>
                <td> - <a class="installment-override" data-toggle="modal" href="#installmentOverrideModal" data-id="<?php echo $sf->fee_id; ?>" data-name="<?php echo $sf->name; ?>" data-installment="<?php echo $sf_installment; ?>" data-skip="<?php echo $sfkip; ?>"><?php echo $sf->name; ?></a></td>
                <?php 
                $n = 0;
                foreach( $months as $month ) { ?>
                    <td class="text-right money_format"><?php 
                     
                  $amount = (($sf->amount - ($sf->discounts + $sf->downpayments)) / $sf_installment); 
                  if( ( $n >= $sfkip ) && ($n < ($sf_installment+ $sfkip)) ) {
                    echo $amount; 
                    $var2 = 'month_' . $month->month;
                    $$var2 += $amount;
                  }
                     ?></td>
                <?php $n++; } ?>
              </tr>
              <?php } } ?>
   						<tr>
   							<th><strong>TOTAL MONTHLY FEES</strong></th>
                 <?php 
$grand_total = 0;
                 foreach( $months as $month ) { ?>
                    <th class="text-right money_format">
                      <?php 
                        $var3 = 'month_' . $month->month;
                        echo $$var3;
                        $grand_total += $$var3;
                      ?>
                    </th>
                <?php } ?>
   						</tr>
<tr class="success">
                <td><strong>TOTAL</strong></td>
                <td colspan="<?php echo count($months); ?>" class="text-right money_format bold"><?php echo $grand_total; ?></td>
</tr>
<tr class="">
                <td colspan="<?php echo count($months) + 1; ?>">&nbsp;</td>
              </tr>
               <tr class="success">
                <td colspan="<?php echo count($months) + 1; ?>"><strong>Services</strong></td>
              </tr>
               
<?php 
$student_services_arr = array();
foreach($student_services as $sser) {
  $student_services_arr[$sser->service_id][$sser->month] = $sser->amount;
  $var2 = 'month_' . $sser->month;
  $$var2 += $sser->amount;
}
foreach( $services as $service ) { ?>
              <tr>
                <td> - <?php echo $service->name; ?></td>
                 <?php foreach( $months as $month ) { ?>
                    <td class="text-right money_format"><?php echo (isset($student_services_arr[$service->id][$month->month])) ? $student_services_arr[$service->id][$month->month] : 0; ?></td>
                  <?php } ?>
              </tr>
 <?php } ?>

 <tr>
                <th><strong>Grand Total</strong></th>
                 <?php 
$grand_total = 0;
                 foreach( $months as $month ) { ?>
                    <th class="text-right money_format">
                      <?php 
                        $var3 = 'month_' . $month->month;
                        echo $$var3;
                        $grand_total += $$var3;
                      ?>
                    </th>
                <?php } ?>
              </tr>

   					</tbody>
   				</table>

   			</div>

   		</div>

	</div>
		
</div>


<!--excludeMonthsModal modal-->
<div style="display: none;" id="excludeMonthsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Exclude Months</strong>
      </div>
      <div class="modal-body">
<?php echo form_open("finance/detailed_schedule/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
        <div class="input-group">
        <select class="form-control" name="exclude">
          <?php foreach( $months as $month ) { 
            if( isset($ex_months[$month->id])) {
              continue;
            }
            ?>
            <option value="<?php echo $month->id; ?>"><?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></option>
          <?php } ?>
        </select>
        <span class="input-group-btn">
          <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-plus"></i></button>
        </span>
        </div>
</form>

<br>

<ul class="list-group">
<?php foreach( $excluded_months as $em ) { 
  ?>
  <li class="list-group-item">
  <?php echo date('F', strtotime($em->month . "/1/1990")) . " " . $em->year; ?>
    <a href="<?php echo site_url(uri_string()) . "?delete_exclude=" . $em->id; ?>" class="pull-right btn btn-xs btn-danger">Delete</a>
  </li>
<?php } ?>
</ul>

      </div>

  </div>
  </div>
</div>

<!--installmentOverrideModal modal-->
<div style="display: none;" id="installmentOverrideModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong id="installmentOverrideModal-title">Override</strong>
      </div>
      <div class="modal-body">
<?php echo form_open("finance/detailed_schedule/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
<input type="hidden" name="override[fee_id]" id="installmentOverrideModal-fee_id">
        <div class="form-group">
          <label>Installments</label>
          <input class="form-control" type="text" name="override[installment]" id="installmentOverrideModal-installment">
        </div>
        <div class="form-group">
          <label>Months Skip</label>
          <input class="form-control" type="text" name="override[skip]" id="installmentOverrideModal-skip" value="0">
        </div>


      </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>
  </div>
  </div>
</div>

<?php endif; ?>
<?php $this->load->view('footer'); ?>
