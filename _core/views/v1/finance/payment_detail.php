<?php if( $output != 'ajax') { ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
  <div class="col-sm-12 col-md-12">

      <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Payment Detail</h4>
          </div>
          <div class="panel-body">
<?php } ?>
<?php if( count($applied_payments) > 0 ) { ?>

          <table class="table table-condensed table-hover">
          <thead>
            <tr>
              <th>Fee Name</th>
              <th class="text-right">Amount</th>
            </tr>
          </thead>
          <tbody>
<?php 
$total = 0;
if( isset($amonths) ) { ?>
  <?php foreach($amonths as $amonth) { 
$month_total = 0;
    ?>
          <tr class="success">
            <td colspan="2"><strong><?php echo (isset($amonth->month)) ? date('F Y', strtotime($amonth->month . "/1/" . $amonth->year)) : ''; ?></strong></td>
          </tr>
            <?php foreach( $applied_payments as $apay ) { 
                if( $apay->month != $amonth->month ) {
                  continue;
                }
              ?>
              <tr>
                <td class="text-left"><?php echo ($apay->fee_name) ? $apay->fee_name : 'Old Account'; ?></td>
                <td class="text-right"><?php 
                $total += $apay->amount;
                $month_total += $apay->amount;
                echo number_format($apay->amount,2); ?></td>
              </tr>
            <?php } ?>

             <tr>
                <td class="text-left"><strong><?php echo (isset($amonth->month)) ? date('F', strtotime($amonth->month . "/1/1990")) : ''; ?> Total</strong></td>
                <td class="text-right"><strong><?php 
                echo number_format($month_total,2); ?></strong></td>
              </tr>

  <?php } ?>
<?php } else { ?>
   <?php foreach( $applied_payments as $apay ) { ?>
              <tr>
                <td class="text-left"><?php echo ($apay->fee_name) ? $apay->fee_name : 'Old Account'; ?> <?php echo (isset($apay->month)) ? "<span class='badge pull-right'>" . date('F', strtotime($apay->month . "/1/1990")) . "</span>": ''; ?></td>
                <td class="text-right"><?php 
                $total += $apay->amount;
                echo number_format($apay->amount,2); ?></td>
              </tr>
            <?php } ?>
<?php }  ?>
            <tr class="warning">
                <td class="text-left"><strong>Grand Total</strong></td>
                <td class="text-right"><strong><?php 
                echo number_format($total,2); ?></strong></td>
              </tr>
          </tbody>
          </table>



<?php } else { ?>
  <p class="text-center">Nothing Applied!</p>
  <a href="<?php echo site_url( str_replace('payment_detail',(($payment_detail->down)?'update_downpayment':'update_payment'), uri_string()) ); ?>" class="btn btn-warning btn-xs">Update Payment</a>
<?php }  ?>
<?php if( $output != 'ajax') { ?>
          </div>
      </div>
  </div>
</div>



<?php $this->load->view('footer'); ?>

<?php } ?>