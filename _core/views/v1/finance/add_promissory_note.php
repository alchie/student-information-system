<?php 
$this->load->view('header'); ?>

<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
	<div class="col-sm-8 col-md-8 col-md-offset-2 loading-block-container">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Add a Promissory Note</h4></div>
<form method="post">
   			  <div class="panel-body">

<?php echo (validation_errors()) ? "<div class=\"alert alert-danger\">" . validation_errors() . "</div>" : ""; ?>

        
         <div class="row">
           <div class="col-md-6">
             <div class="form-group">
                <label>Date</label>
                <input type="text" class="form-control datepicker" name="date_filed" value="<?php echo ($this->input->post('date_filed')) ? $this->input->post('date_filed') : date('m/d/Y'); ?>">
             </div>
           </div>
           <div class="col-md-6">
             <div class="form-group">
                <label>Exam Date</label>
                <input type="text" class="form-control datepicker" name="exam_date" value="<?php echo $this->input->post('exam_date'); ?>">
             </div>
           </div>
         </div>

         <div class="row">
           <div class="col-md-6">
              <div class="form-group">
                  <label>Month</label>
                  <select class="form-control" name="month">
                      <?php foreach($months as $month) { ?>
                        <option value="<?php echo $month->month; ?>" <?php echo ($this->input->post('month')) ?'selected':''; ?>><?php echo date('F Y', strtotime($month->month . "/1/" . $month->year)); ?></option>
                      <?php } ?>
                  </select>
               </div>
             </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label>Parent / Guardian</label>
                  <select class="form-control" name="parent_id">
                      <?php foreach($parents as $parent) { ?>
                        <option value="<?php echo $parent->parent_id; ?>"><?php echo $parent->lastname; ?>, <?php echo $parent->lastname; ?> (<?php echo ucwords($parent->relationship); ?>)</option>
                      <?php } ?>
                  </select>
               </div>
            </div>
          </div>
             <div class="form-group">
                <label>Reason</label>
                <textarea class="form-control" name="reason"><?php echo $this->input->post('reason'); ?></textarea>
             </div>

          <div class="row">
           <div class="col-md-6">
             <div class="form-group">
                <label>Promised to Pay On</label>
                <input type="text" class="form-control datepicker" name="promised_date" value="<?php echo $this->input->post('promised_date'); ?>">
             </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                <label>Contact Number</label>
                <input type="text" class="form-control" name="contact_number" value="<?php echo $this->input->post('contact_number'); ?>">
             </div>
            </div>
            </div>
			
          </div>
          <div class="panel-footer">
              <button class="btn btn-success" type="submit">Add Note</button>
          </div>
</form>

   		</div>

	</div>
</div>



<?php $this->load->view('footer'); ?>

