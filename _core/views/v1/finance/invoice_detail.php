<?php if( $output != 'ajax') { ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
  <div class="col-sm-12 col-md-12">

      <div class="panel panel-default">
          <div class="panel-heading">
            <h4><?php echo $primary_school_year->label; ?> - <?php echo date('F', strtotime($month . '/1/1990')); ?> - Invoice Detail</h4>
          </div>
          <div class="panel-body">
<?php } ?>
          <table class="table table-condensed table-hover">
          <thead>
            <tr>
              <th>Fee Name</th>
              <th class="text-right">Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $total = 0;
            foreach( $invoices as $invoice ) { ?>
              <tr>
                <td class="text-left"><?php echo $invoice->fee_name; ?></td>
                <td class="text-right"><?php 
                $total += $invoice->amount;
                echo number_format($invoice->amount,2); ?></td>
              </tr>
            <?php } ?>
            <tr>
                <td class="text-left"><strong>Total</strong></td>
                <td class="text-right"><strong><?php 
                echo number_format($total,2); ?></strong></td>
              </tr>
          </tbody>
          </table>
<?php if( $output != 'ajax') { ?>
          </div>
      </div>
  </div>
</div>



<?php $this->load->view('footer'); ?>

<?php } ?>