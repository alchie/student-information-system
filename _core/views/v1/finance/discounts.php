<?php 
$this->load->view('header'); 
?>

<?php $this->load->view('finance/finance_navbar'); ?>

<div class="row">
	<div class="col-sm-12 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
<a role="button" data-toggle="modal" href="#addDiscounts2Modal" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i> Add Discount</a>
          <h4>Discounts</h4></div>
   			<div class="panel-body">
        
<?php if( $discounts ) { 
  $total_payments = 0; 
  $total_discounts = 0;
        ?>
          <table class="table table-striped table-hover table-condensed">
            <thead>
              <tr>
                <th>Fee</th>
                <th class="text-right">Amount</th>
                <th>Description</th>
                <th class="text-right" style="width:100px">Action</th>
              </tr>
            </thead>
            <tbody>
<?php 
foreach( $discounts as $discount ) { ?>
  <tr class="">
    <td><?php echo $discount->fee_name; ?></td>
    <td class="text-right"><?php echo number_format($discount->amount,2); $total_discounts+=$discount->amount; ?></td>
    <td class="small"><?php echo $discount->description; ?></td>
     <td class="text-right"><a class="btn btn-warning btn-xs pull-right" href="<?php echo site_url("finance/discount/" . $primary_school_year->id . "/" . $enroll_data->id . "/" . $discount->id); ?>">Update</a></td>
  </tr>
<?php } ?>
              <tr>
                <th>TOTAL DISCOUNTS</th>
                <th class="text-right"><?php echo number_format($total_discounts,2); ?></th>
                <th class="text-right"></th>
                <td></td>
              </tr>
            </tbody>
          </table>
<?php } else { ?>
  <p class="text-center">No Discount Found!</p>
<?php } ?>


        </div>
	</div>
</div>


<!--addDiscounts2Modal modal-->
<div style="display: none;" id="addDiscounts2Modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width:700px;">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Discount</strong>
      </div>
   <?php echo form_open("finance/add_discount/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
      <div class="modal-body">
      <div class="row">
          <div class="form-group col-md-6">
              <label>Description</label>
            <input class="form-control text-right" name="description" required>
          </div>
          <div class="form-group col-md-6">
              <label>Amount</label>
            <input class="form-control text-right" id="discount_total_amount" name="amount" required>
          </div>
        </div>

<center>
<a class="btn btn-primary btn-xs show-hide" data-id="addDiscounts2Modal-applied">Show / Hide Details</a>
<a class="btn btn-danger btn-xs un-apply-discount">Un-Apply Discount</a>
<a class="btn btn-success btn-xs auto-apply-discount">Auto Apply Discount</a>
</center>   

          <table class="table table-condensed" id="addDiscounts2Modal-applied" style="display:none;">
            <thead>
              <tr>
                <th></th>
                <th class="text-right">Balance</th>
                <th class="text-right" width="30%">Amount</th>
                <th class="text-right" width="23px"></th>
              </tr>
            </thead>
            <tbody>
            
            <?php foreach($school_fees_groups as $sfg) { ?>
              <tr class="success">
                <td colspan="4">
                <strong class="pull-right autosum-class money_format" data-class="dschool_fees_balance_<?php echo $sfg->id; ?>">0.00</strong>
                <strong><?php echo $sfg->name; ?></strong>
<button type="button" class="btn btn-xs btn-success show-hide-class" data-class="dschool_fees_groups_<?php echo $sfg->id; ?>">Show</button>
                </td>
              </tr>
              <?php 
              foreach( $school_fees as $sf) { 
                if( $sf->group_id == $sfg->id) {
                  if( round(($sf->amount-$sf->discounts-$sf->payments),2) > 0) {
                ?>
              <tr class="dschool_fees_groups_<?php echo $sf->group_id; ?>" style="display:none;">
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right money_format dschool_fees_balance_<?php echo $sf->group_id; ?>"><?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); ?>" class="form-control text-right discount_total_amount money_format" id="discount_total_amount_<?php echo $sf->fee_id; ?>" data-check_id="dfee_include_<?php echo $sf->fee_id; ?>" name="discount[<?php echo $sf->fee_id; ?>]" required>
                </td>
                <td><input type="checkbox" checked id="dfee_include_<?php echo $sf->fee_id; ?>"></td>
              </tr>
              <?php } } } ?>
            <?php } ?>

              <tr class="success" id="ungrouped_feed_apply_payment">
                <td colspan="4">
                <strong class="pull-right autosum-class money_format" data-class="dschool_fees_balance_0">0.00</strong>
                <strong>Ungrouped Fees</strong>  <button type="button" class="btn btn-xs btn-success show-hide-class" data-class="dschool_fees_groups_0">Show</button>
                </td>
              </tr>
              <?php 
              foreach( $school_fees as $sf) { 
                if( $sf->group_id == 0) {
                  if(round(($sf->amount-$sf->discounts-$sf->payments),2) > 0) {
                ?>
              <tr class="dschool_fees_groups_<?php echo $sf->group_id; ?>" style="display:none;">
                <td> - <?php echo $sf->name; ?></td>
                <td class="text-right money_format dschool_fees_balance_0"><?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); 
$total_whole_year_fee += ($sf->amount-$sf->discounts-$sf->payments);
                ?></td>
                <td class="text-right"><input data-balance="<?php echo number_format(($sf->amount-$sf->discounts-$sf->payments),2); ?>" class="form-control text-right discount_total_amount money_format" id="discount_total_amount_<?php echo $sf->fee_id; ?>" data-check_id="dfee_include_<?php echo $sf->fee_id; ?>" name="discount[<?php echo $sf->fee_id; ?>]" required></td>
                <td><input type="checkbox" checked id="dfee_include_<?php echo $sf->fee_id; ?>"></td>
              </tr>
               <?php } 
                } } ?>

              <tr>
                <th><strong>TOTAL</strong></th>
                <th class="text-right money_format"><?php echo number_format($remaining_balance,2); ?></th>
                <th class="text-right money_format" id="applied-balance2"></th>
              </tr>
              <tr>
                <th></th>
                <th class="text-right"><a class="pull-right btn btn-danger btn-xs un-apply-discount">Un-Apply Discount</a></th>
                <td class="text-right"><a class="pull-right btn btn-success btn-xs auto-apply-discount">Auto Apply Discount</a></td>
              </tr>
            </tbody>
          </table>
      </div>
    <div class="modal-footer">
    
          <button type="submit" class="btn btn-primary" id="addDiscounts2ModalSubmit">Submit</button>
      </div>

    </form>
  </div>
  </div>
</div>

<!--parents modal-->
<div style="display: none;" id="addDiscountsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong>Add Discount</strong>
      </div>
   <?php echo form_open("finance/discount/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
      <div class="modal-body">
    <div class="form-group">
          <label>School Fee</label>
          <select class="form-control autofill-select" name="fee_id" required data-autofill="discount_amount">
          <option value="">- - Select Discountable Fee - -</option>

    <?php foreach( $school_fees as $sfd ) { ?>
      <?php if( ($sfd->discountable==1) && ($sfd->group_id == 0)) { ?>
        <option value="<?php echo $sfd->fee_id; ?>" style="font-weight:bold;" data-value="<?php echo number_format($sfd->amount,2);  ?>"><?php echo $sfd->name; ?> (<?php echo number_format($sfd->amount,2);  ?>)</option>
      <?php } ?>
    <?php } ?>

<?php foreach($school_fees_groups as $sfg) { ?> 
  <optgroup label="<?php echo $sfg->name; ?>">
    <?php foreach( $school_fees as $sfd ) { ?>
      <?php if( ($sfd->discountable==1) && ($sfd->group_id == $sfg->id)) { ?>
        <option value="<?php echo $sfd->fee_id; ?>" data-value="<?php echo number_format($sfd->amount,2);  ?>"><?php echo $sfd->name; ?> (<?php echo number_format($sfd->amount,2);  ?>)</option>
      <?php } ?>
    <?php } ?>
  </optgroup>
<?php } ?>

          </select>
    </div><!-- /form-group -->
    <div class="form-group">
          <label>Amount</label>
        <input id="discount_amount" type="text" class="form-control" name="amount" value="0.00" required>
    </div><!-- /form-group -->
    <div class="form-group">
          <label>Description</label>
        <input type="text" class="form-control" name="description" value="" required>
    </div><!-- /form-group -->
      </div>

    <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>

    </form>
  </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

