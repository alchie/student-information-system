<?php 
$level_names = unserialize(LEVEL_NAMES);

$sss = array();
$total_services = 0;
foreach($student_services as $ss) {
  $sss[$ss->service_id][$ss->month] = $ss->amount;
  $total_services += $ss->amount;
}

$sss2 = array();
$total_services = 0;
foreach($student_services as $ss2) {
  $sss2[$ss2->month][] = $ss2;
}

$sss3 = array();
$total_services3 = 0;
foreach($student_services as $ss3) {
  if( !isset($sss3[$ss3->service_id])) {
    $sss3[$ss3->service_id] = 0;
  }
  $sss3[$ss3->service_id] += $ss3->amount;
  $total_services3 += $ss3->amount;
}

$this->load->view('header'); 
?>
<?php if( !isset($primary_school_year) ) : ?>
<div class="alert alert-danger alert-dismissable">
	<strong>SCHOOL YEAR ERROR!</strong> No current school year found! Please assign a current school year or select a school year <a href="<?php echo site_url('school_year'); ?>">here</a>.
</div>
<?php else: ?>

  <?php $this->load->view('finance/finance_navbar'); ?>
  
  <!--
<div class="row">
	<div class="col-sm-12 col-md-12">

      <div class="panel panel-default">
          <div class="panel-heading">
          <a class="btn btn-warning btn-sm pull-right" href="<?php echo site_url("finance/ledger/{$primary_school_year->id}/{$enroll_data->id}"); ?>">Back</a>
		  <h4>Services</h4></div>
   			<div class="panel-body">
   				<table class="table table-condensed">
            <thead>
              <tr>
                <th class="text-left">Fees</th>
                <?php foreach( $months as $month ) { ?>
                  <th class="text-right"><?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></th>
                <?php } ?>
              </tr>
            </thead>
   					<tbody>
   					<?php foreach($services as $service) { ?>
   						<tr>
   							<td><strong><?php echo $service->name; ?></strong></td>
                <?php foreach( $months as $month ) { ?>
                  <td class="text-right">
                    <a role="button" data-toggle="modal" href="#addServiceModal" class="money_format addServiceModal_button" data-month_title="<?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?>" data-month="<?php echo $month->month; ?>" data-service_id="<?php echo $service->id; ?>" data-service_name="<?php echo $service->name; ?>"><?php echo (isset($sss[$service->id][$month->month])) ? $sss[$service->id][$month->month] : 0; ?></a>
                  </td>
                <?php } ?>
   						</tr>
             <?php } ?>
   					</tbody>
   				</table>

   			</div>
        <div class="panel-footer">
        <strong>TOTAL: </strong> <span class="money_format"><?php echo number_format($total_services,2); ?></span>
        </div>
   		</div>

	</div>
</div>
-->
<div class="row">

<div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
      <a class="btn btn-warning btn-sm pull-right" href="<?php echo site_url("finance/ledger/{$primary_school_year->id}/{$enroll_data->id}"); ?>">Back</a>
        <h4>Services</h4>
      </div>
      <div class="panel-body">
<?php if( $student_services ) { ?>
        <table class="table table-default">
        <?php 
        foreach($services as $service) { ?>
        <tr>
          <td><?php echo $service->name; ?></td>
          <td class="text-right money_format"><?php echo (isset($sss3[$service->id])) ? number_format($sss3[$service->id],2) : 0; ?></td>
        </tr>
        <?php } ?>
        <tr>
          <th>TOTAL</th>
          <th class="text-right money_format"><?php echo number_format($total_services3,2); ?></th>
        </tr>
        </table>
 <?php } ?>
      </div>
    </div>
  </div>

<?php 
$n=1;
foreach( $months as $month ) { ?>
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a role="button" data-toggle="modal" href="#addService2Modal" class="pull-right addService2Modal_button" data-month_title="<?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?>" data-month="<?php echo $month->month; ?>"><i class="glyphicon glyphicon-plus"></i></a>
        <h4><?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></h4>
      </div>
      <div class="panel-body">
        <table class="table table-default">
        <?php 
        if( isset( $sss2[$month->month] ) ) {
        foreach($sss2[$month->month] as $ss2) { 
          ?>
          <tr>
            <td><?php echo $ss2->service_name; ?></td>
            <td class="text-right">
<a role="button" data-toggle="modal" href="#updateService2Modal" class="pull-right updateService2Modal_button" data-month_title="<?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?>" data-month="<?php echo $month->month; ?>" data-service_id="<?php echo $ss2->service_id; ?>" data-service_name="<?php echo $ss2->service_name; ?>" data-id="<?php echo $ss2->id; ?>">
            <?php echo number_format($ss2->amount,2); ?>
</a>
            </td>
          </tr>
        <?php } 
          }
        ?>
        </table>        
      </div>
    </div>
  </div>
<?php 
  $n++;
  if( $n==3) {
    echo '</div><div class="row">';
    $n=0;
  }
} ?>
</div>

<!--#addServiceModal modal-->
<!--
<div style="display: none;" id="addServiceModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong id="addServiceModal-title">Add Service</strong>
      </div>
   <?php echo form_open("finance/services/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
      <div class="modal-body">
       <input type="hidden" name="month" value="" id="addServiceModal-month">
       <input type="hidden" name="service_id" value="" id="addServiceModal-service_id">
        <div class="form-group">
          <label>Amount</label>
           <input id="addServiceModal-amount" type="text" class="form-control" name="amount" value="0.00" required>
        </div>
      </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
  </div>
  </div>
</div>
-->
<!--#addServiceModal modal-->
<div style="display: none;" id="addService2Modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong id="addService2Modal-title">Add Service</strong>
      </div>
   <?php echo form_open("finance/services/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
      <div class="modal-body">
       <input type="hidden" name="month" value="" id="addService2Modal-month">
       <div class="form-group">
          <label>Service</label>
           <select id="addService2Modal-service_id" type="text" class="form-control" name="service_id" required>
            <?php foreach($services as $service) { ?>
                <option value="<?php echo $service->id; ?>"><?php echo $service->name; ?></option>
            <?php } ?>
           </select>
        </div><!-- /form-group -->
        <div class="form-group">
          <label>Amount</label>
           <input id="addService2Modal-amount" type="text" class="form-control" name="amount" value="0.00" required>
        </div><!-- /form-group -->
      </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
  </div>
  </div>
</div>

<!--#addServiceModal modal-->
<div style="display: none;" id="updateService2Modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
          <strong id="updateService2Modal-title">Update Service</strong>
      </div>
   <?php echo form_open("finance/services/" . $primary_school_year->id . "/" . $enroll_data->id); ?>
      <div class="modal-body">
       <input type="hidden" name="month" value="" id="updateService2Modal-month">
       <input type="hidden" name="service_id" value="" id="updateService2Modal-service_id">
        <div class="form-group">
          <label>Amount</label>
           <input id="updateService2Modal-amount" type="text" class="form-control" name="amount" value="0.00" required>
        </div><!-- /form-group -->
      </div>
        <div class="modal-footer">
            <a id="updateService2Modal-delete" data-href="<?php echo site_url("finance/delete_service/" . $primary_school_year->id . "/" . $enroll_data->id); ?>/" class="btn btn-danger confirm pull-left"><i class="glyphicon glyphicon-trash"></i> Delete</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
  </div>
  </div>
</div>

<?php endif; ?>
<?php $this->load->view('footer'); ?>
