<?php $this->load->view('header');
$level_names = unserialize(LEVEL_NAMES);
 ?>
<div class="row">
  <div class="col-sm-6 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Collection Letters Options</h4></div>
<?php echo form_open("finance/print_collection_letters_students/{$primary_school_year->id}/{$grade_level}/{$campus_id}", array("method"=>'get',"target"=>"_students","id"=>"","class"=>"form-horizontal form-label-left")); ?>
          <div class="panel-body">

<?php
$months_opt = array();
foreach( $months as $month ) { 
  $months_opt[$month->id] = date('F', strtotime($month->month . "/1/1990")) . " " . $month->year;
}
$levels_opt = array();
foreach( $levels as $level ) { 
  $levels_opt[$level->level] = $level_names[$level->level];
}
$campus_opt = array();
foreach( $campuses as $campus ) { 
  $campus_opt[$campus->id] = $campus->name;
}
  $forms = array(
    //'month' => array("title"=>"Statement Month", 'type'=>"select_single", "required"=>true,'options'=>$months_opt, 'attributes'=>array('required'=>'required'),'default'=>''),
    'grade_level' => array("title"=>"Grade Level", 'type'=>"select_single", "required"=>true,'options'=>$levels_opt,'default'=>$grade_level, 'attributes'=>array('required'=>'required')),
    'campus' => array("title"=>"Campus", 'type'=>"select_single", "required"=>true,'options'=>$campus_opt, 'default'=>$campus_id, 'attributes'=>array('required'=>'required')),
  );
  
  foreach($forms as $key=>$form ) {
    echo plus_form( $form['type'], $form['title'], $key, $form, $form['default'] ); 
  
  }
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
      <table class="table table-default">
      <?php foreach($months as $key=>$month) { ?>
          <tr>
            <td><input type="checkbox" name="installment[]" value="<?php echo $key; ?>"></td>
            <td><?php echo date('F', strtotime($month->month . "/1/1990")) . " " . $month->year; ?></td>
            <td><input type="text" class="form-control datepicker text-center" name="installment_month[]" value="<?php echo date($month->month.'/d/Y'); ?>"></td>
          </tr>
      <?php } ?>
      </table>
  </div>
</div>
                    
          </div>
          <div class="panel-footer">
            
            <div class="">
                      <div class="">
                        <button type="submit" class="btn btn-success pull-right"> Next <i class="fa fa-arrow-right"></i></button>
            <a href="javascript:history.back(-1);" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back</a>
                      </div>
                    </div>
          

          </div>
           </form>
      </div>

  </div>
</div>
<?php $this->load->view('footer'); ?>