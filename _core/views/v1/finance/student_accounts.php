<?php $this->load->view('header'); 
$level_names = unserialize(LEVEL_NAMES);
?>
<?php if( !isset($primary_school_year) ) : ?>
<div class="alert alert-danger alert-dismissable">
	<strong>SCHOOL YEAR ERROR!</strong> No current school year found! Please assign a current school year or select a school year <a href="<?php echo site_url('school_year'); ?>">here</a>.
</div>
<?php else: ?>
<div class="row">
	<div class="col-sm-612 col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">


<!-- Split button -->
<div class="btn-group btn-group-xs pull-right">
  <a href="<?php 
$uri = sprintf('finance/print_soa_options/%s/%s/%s', $primary_school_year->id, $grade_level, $campus_id);
          echo site_url($uri); ?>" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-print"></i> Print SOA</a>
  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php 
$uri = sprintf('finance/print_collection_letters_options/%s/%s/%s', $primary_school_year->id, $grade_level, $campus_id);
          echo site_url($uri); ?>">Print Collection Letters</a></li>
  </ul>
</div>


          

          <!-- Single button -->
<div class="btn-group pull-right" style="margin-right:10px;">
  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   Print Reports <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a target="_blank" href="<?php echo site_url("reports/projected_monthly_collectibles/{$primary_school_year->id}"); ?>">Projected Monthly Collectibles</a></li>
  </ul>
</div>

 <!-- Single button -->
<div class="btn-group pull-right" style="margin-right:10px;">
  <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   View Report <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo site_url("reports/payments/{$primary_school_year->id}"); ?>">Payments</a></li>
    <li><a href="<?php echo site_url("reports/open_payments/{$primary_school_year->id}"); ?>">Open Payments</a></li>
    <li><a href="<?php echo site_url("reports/unsettled_promissory_notes/{$primary_school_year->id}"); ?>">Unsettled Promissory Notes</a></li>
  </ul>
</div>

		  <h4>Student Accounts: <?php echo $primary_school_year->label; ?>

<?php echo ($this->input->get("q")) ? " <small>Search for \"<strong>" . $this->input->get("q") . "</strong>\" <a href='".site_url(uri_string())."'>Clear Search</a></small> " : ""; ?>
<?php echo ((isset($grade_level) && ($grade_level!='0')) || (isset($campus_id) && ($campus_id!='0'))) ? " <small>Filter: " : ""; ?>
<?php echo (isset($campus)) ? " <strong>" . $campus->name . "</strong> " : ""; ?>
<?php echo (isset($grade_level)&&isset($campus)) ? " &middot; " : ""; ?>
<?php echo (isset($grade_level) && isset($level_names[$grade_level])) ? " <strong>" . $level_names[$grade_level] . "</strong> " : ""; ?>
<?php echo ((isset($grade_level) && ($grade_level!='0')) || (isset($campus_id) && ($campus_id!='0'))) ? " <a href='".site_url("finance")."'>Clear Filter</a></small> " : ""; ?>

		  </h4></div>
   			<div class="panel-body">
              <table class="table table-striped">
                    <thead>
                      <tr class="headings">
						  <th>ID Number</th>
						  <th>Last Name</th>
						  <th>First Name</th>
						  
 <th>

<div class="btn-group">
  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo (isset($campus)) ? $campus->name : 'Campus'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
   <li><a href="<?php echo site_url(sprintf('finance/index/%s/%s/%s', $primary_school_year->id, $grade_level, 0)); ?>">Show All</a></li>
   <li role="separator" class="divider"></li>
  <?php 
  foreach($campuses as $campuss) { ?>
    <li><a href="<?php echo site_url(sprintf('finance/index/%s/%s/%s', $primary_school_year->id, $grade_level, $campuss->id)); ?>"><?php echo $campuss->name; ?></a></li>
  <?php } ?>
  </ul>
</div>

						  </th>
						  
						  <th>

<div class="btn-group">
  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo ($grade_level) ? $level_names[$grade_level] : 'Grade Level'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <li><a href="<?php echo site_url(sprintf('finance/index/%s/%s/%s', $primary_school_year->id, 0, $campus_id)); ?>">Show All</a></li>
  <li role="separator" class="divider"></li>
  <?php foreach($grade_levels as $gLevel) { ?>
    <li><a href="<?php echo site_url(sprintf('finance/index/%s/%s/%s', $primary_school_year->id, $gLevel->level, $campus_id)); ?>"><?php echo $level_names[$gLevel->level]; ?></a></li>
   <?php } ?>
  </ul>
</div>


						  </th>

                        <th width="70px"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
<?php 
$total_accounts = 0;
foreach( $enrollees as $enrollee ): ?>
                      <tr class="">
                        <td><?php echo $enrollee->idn; ?></td>
						<td><?php echo $enrollee->lastname; ?></td>
						<td><?php echo $enrollee->firstname; ?></td>
						<td><?php echo $enrollee->campus_name; ?></td>
						<td><?php echo $level_names[$enrollee->grade_level]; ?></td>
						
                        <td>
                        <a href="<?php echo site_url('finance/ledger/' . $primary_school_year->id . "/" . $enrollee->id); ?>" class="btn btn-warning btn-xs">Show Ledger</a>
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                

			  <center>
					<?php echo $pagination; ?>
			  </center>

            </div>
           
   		</div>

	</div>
</div>
<?php endif; ?>



<?php $this->load->view('footer'); ?>
