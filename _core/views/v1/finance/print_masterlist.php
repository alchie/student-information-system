<?php $level_names = unserialize(LEVEL_NAMES); ?>
<html>
<head>
<style>
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
/* CSS */
html, body {
    height: 100%;
}
body {
	font-size:12px;
	font-family: Arial;
	padding:0;
	margin: 0;
}
p {
	padding: 0;
	margin: 0;
}
#school_name {
	font-weight: bold;
	font-size:12px;
	text-transform: uppercase;
}
#school_address,
#school_contacts {
	font-size: 10px;
}
.outer {
	margin:0;
	height:500px;
	width: 100%;
}
.outer.outer-3,
.outer.outer-4 {
	margin-bottom:6px;
	}
.soa-body {
	padding: 3px;
	margin: 10px;
	position: relative;
}
.soa-title {
	text-transform: uppercase;
	margin-top:15px;
	font-weight: bold;
	font-family: serif;
	letter-spacing: 3px;
	margin-bottom:15px;
}
.border {
	border: 1px solid #000;
}
.float-left {
	float:left;
}
.text-center {
	text-align: center;
}
.text-right {
	text-align: right;
}
.text-left {
	text-align: left;
}
.bold {
	font-weight: bold;
}
.underlined {
	text-decoration: underline;
}
.student_details tr td {
	padding-top: 5px;
}
#student_name {
	text-decoration: underline;
	text-transform: uppercase;
	font-weight: bold;
}
#student_name span {
	font-size: 10px;
	font-weight: normal;
}
#grade_level, #campus {
	margin-top:5px;
	font-weight: bold;
	text-transform: uppercase;
}
#grade_level span, #campus span {
	font-size: 10px;
	font-weight: normal;
}
#fees {
	border:1px solid #000;
	margin-top:10px;
}
#fees thead td {
	padding:5px;
	text-transform: uppercase;
	font-size: 10px;
	font-weight: bold;
}
#fees tbody td {
	padding:5px;
	font-size: 10px;
}
#amount_due {
	margin-top: 10px;
	font-size: 10px;
	text-transform: uppercase;
}
#amount_due td {
	padding: 5px;
	}
.bigger {
	font-size: 12px;
}
.logo {
	width: 55px;
	position: absolute;
	top:5px;
	left:-5px;
}
.logo.right {
	left:auto;
	right:0;
}
.allcaps {
	text-transform: uppercase;
}
#studentlist {
	margin-bottom: 10px;
}
#studentlist tr.heading td {
	font-weight: bold;
	padding:10px;
}
#studentlist tr.item td {
	padding:2px;
	border-bottom: 1px solid #000;
}
.level_name {
	border: 1px solid #000;
	padding: 5px;
    background-color: #eee;
    font-size: 14px;
}
</style>
</head>
<body>
<center>
<?php 

	?>
<table cellpadding="0" cellspacing="0" class="outer border float-left">
	<tr>
		<td class="text-center" colspan="2">
			<div class="soa-body">
			<img src="<?php echo base_url("assets/v1/img/logo160x160.png"); ?>" class="logo">
			<img src="<?php echo base_url("assets/v1/img/rcbdi_logo.png"); ?>" class="logo right">
			<h1 id="school_name"><?php echo get_ams_config('school', 'school_name'); ?></h1>
			<p id="school_address"><?php echo get_ams_config('school', 'school_address'); ?></p>
			<p id="school_contacts"><?php echo get_ams_config('school', 'school_phone'); ?> &middot; <?php echo get_ams_config('school', 'school_email'); ?></p>
			<p id="school_contacts"><?php echo $primary_school_year->label; ?></p>

			<h3 class="soa-title underlined">SOA Masterlist</h3>

			<h3 class="soa-title"><?php echo date('F Y', strtotime($due_date)); ?></h3>
			

<?php 
$count_n = 1;
$overall_amount_due = 0;
$overall_total = 0;

foreach( $levels as $level_key ):

	if( $this->input->get('grade_level') && ($level_key->level != $this->input->get('grade_level')) ) {
		continue;
	}

	foreach( $campuses as $campus ):

if( $this->input->get('campus') && ($this->input->get('campus') != $campus->id )) {
	continue;
}

$var = 'level_' . $level_key->level;
$campus2 = (array) $campus;
if( $campus2[$var] == 0 ) {
	continue;
}

echo "<h3 class='bigger bold allcaps text-left level_name'>" . $level_names[$level_key->level] . " - " . $campus->name . "</h3>\n";

?>
<table width="100%" id="studentlist">
<tr class="heading">
	<td>Name</td>
	<!--<td class="text-right">Campus</td>-->
	<td class="text-right">Previous Balance / Payments</td>
	<td class="text-right">Due this Month</td>
	<td class="text-right">Total Payable this Month</td>
</tr>
<?php 
$total_per_campus = 0;
foreach( $enrollees as $enrollee ): 
	if( $level_key->level != $enrollee->grade_level) {
		continue;
	}

	if( $campus->id != $enrollee->campus_id ) {
		continue;
	}

$overall_payable = 0;
$overall_paid = 0;
$overall_balance = 0;

$overall_payable += $enrollee->old_account;
$overall_paid  += $enrollee->old_payments;
$overall_balance += ($enrollee->old_account - $enrollee->old_payments);

foreach($school_fees_groups as $sfgrp) { 

$total_payable = 0;
$total_paid = 0;
$total_balance = 0;

foreach($enrollee->school_fees as $sfees) {
	if( $sfees->group_id == $sfgrp->id) {
		$total_payable += $sfees->amount;
		$total_paid += ($sfees->payments+$sfees->discounts);
		$total_balance += ($sfees->amount-($sfees->payments+$sfees->discounts));
	}
}

$overall_payable += $total_payable;
$overall_paid += $total_paid;
$overall_balance += $total_balance;

 } 
foreach($enrollee->school_fees as $sfees) {
if( $sfees->group_id == 0) {

$overall_payable += $sfees->amount;
$overall_paid += ($sfees->discounts+$sfees->payments);
$overall_balance += ($sfees->amount-($sfees->discounts+$sfees->payments));
	}
} 

$overall_amount_due += ($enrollee->amount_due + $enrollee->old_account);
$applied_payment = ($enrollee->payments + $enrollee->old_payments) - $overall_amount_due;
$monthly_due = array();
foreach( $months as $month ) {
	$monthly_due[$month->month] = 0;
	foreach( $enrollee->monthly_fees as $mFee ) {
		if( $mFee->month == $month->month ) {
			$monthly_due[$month->month] += $mFee->amount;
		}
	}
}

$previous_months = 0;
foreach($monthly_due as $mm => $mDue) {
	if( $mm == $enrollee->current_month->month) {
		break;
	}
	$previous_months += $mDue;
}
$due = $monthly_due[$enrollee->current_month->month];
$d_payments = $previous_months - $enrollee->payments;
$d_final = $d_payments + $due;
if( $d_final < 0) {
	$d_final = 0;
}
?>

<tr class="item">
	<td width="25%" class="text-left"><?php echo $enrollee->lastname; ?>, <?php echo $enrollee->firstname; ?> <?php echo ($enrollee->middlename!='') ? substr($enrollee->middlename,0,1)."." : ""; ?></td>
	<!--<td width="25%" class="text-right money_format"><?php echo $enrollee->campus_name; ?></td>-->
	<td width="25%" class="text-right money_format"><?php echo number_format($d_payments,2); ?></td>
	<td class="text-right money_format"><?php echo number_format($due,2); ?></td>
	<td class="text-right bold money_format bigger"><?php echo number_format($d_final,2); $total_per_campus += $d_final; 
		$overall_total += $d_final;
	?></td>
</tr>
<?php endforeach; ?>
 
<tr class="item">
	<td width="25%" class="text-left"></td>
	<td width="25%" class="text-right money_format"></td>
	<td class="text-right bigger bold">TOTAL</td>
	<td class="text-right bold money_format bigger"><?php echo number_format($total_per_campus,2); ?></td>
</tr>
</table>

<?php endforeach; ?>

<?php endforeach; ?>

<table width="100%" id="studentlist">
<tr class="heading">
	<td class="text-right" width="80%">OVERALL TOTAL</td>
	<td class="text-right"><?php echo number_format($overall_total,2); ?></td>
</tr>
</table>
</center>
</body>
</html>