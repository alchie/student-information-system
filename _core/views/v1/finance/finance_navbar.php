<?php $level_names = unserialize(LEVEL_NAMES); ?>
<nav class="navbar navbar-default navbar-student">
  
  <div class="container-fluid">

<div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">
      <?php echo $enroll_data->lastname; ?>, <?php echo $enroll_data->firstname; ?> <?php echo ($enroll_data->middlename) ? substr($enroll_data->middlename, 0,1)."." : ""; ?> <sup><?php echo $enroll_data->idn; ?></sup>
     
      <!--<i class="pull-right hidden-sm hidden-xs"> - - - 
      <a href="<?php echo site_url("finance/index/{$primary_school_year->id}/{$enroll_data->grade_level}/{$enroll_data->campus_id}"); ?>"><?php echo $level_names[$enroll_data->grade_level]; ?> - <?php echo $enroll_data->campus_name; ?> - <?php echo $primary_school_year->label; ?></a>
      </i>-->

      </div>
    </div>
    

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo site_url("students/profile/{$enroll_data->student_id}"); ?>">Student Profile</a></li>
        <li class="active"><a href="<?php echo site_url("finance/ledger/{$primary_school_year->id}/{$enroll_data->id}"); ?>">Account Ledgers</a>
        </li>
         <li class="active"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></a>
          <ul class="dropdown-menu">
          <?php foreach($enrollment_history as $enroll) { ?>
            <li><a href="<?php echo site_url("finance/ledger/{$enroll->school_year}/{$enroll->id}"); ?>"><?php echo $enroll->sy_label; ?></a></li>
          <?php } ?>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->

  </div><!-- /.container-fluid -->
</nav>

<?php if( $previous_student ) { ?>
        <a id="navArrowPrevious" class="fixed_arrow previous" href="<?php echo site_url("finance/" . ((isset($method_name)) ? $method_name : 'ledger') . "/{$previous_student->school_year}/{$previous_student->id}"); ?>"><i class="glyphicon glyphicon-chevron-left"></i></a>
<?php } ?>
<?php if( $next_student ) { ?>
         <a id="navArrowNext" class="fixed_arrow next" href="<?php echo site_url("finance/" . ((isset($method_name)) ? $method_name : 'ledger') . "/{$next_student->school_year}/{$next_student->id}"); ?>"><i class="glyphicon glyphicon-chevron-right"></i></a>
<?php } ?>