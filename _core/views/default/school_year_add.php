<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              
			  <h3>Add School Year</h3>
			  
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
				  <div class="x_content">
				  <?php 
				  if( validation_errors() ) {
					echo "<div class=\"alert alert-danger\">";
					echo validation_errors(); 
					echo "</div>";
				  }
				  ?>
                  <br />
                   <?php echo form_open("school_year/add", array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

	$forms = array(
		'label' => array("title"=>"Label", 'type'=>"text", "required"=>true, "default"=>""),
		'start' => array("title"=>"Date Start", 'type'=>"datepicker", "required"=>true, "default"=>""),
		'end' => array("title"=>"Date End", 'type'=>"datepicker", "required"=>true, "default"=>""),
		'active' => array("title"=>"Status", 'type'=>"checkbox", "default"=>"1", "required"=>false,'options'=>array("1"=>"Active")),
	);
	
	foreach($forms as $key=>$form ) {
	
		echo gentelella_form1( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	
	}
?>
				
					
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("school_year"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>

                  </form>
                </div>
				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
