<?php $this->load->view('header');  ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>School Year <a href="<?php echo site_url("school_year/add"); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add School Year</a></h3>
            </div>

          </div>
          <div class="clearfix"></div>

                   <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Label </th>
                        <th class="column-title">Date Start </th>
                        <th class="column-title">Date End </th>
                        <th class="column-title">Status </th>
                        <th class="column-title no-link last"><span class="nobr">Action</span>
                        </th>
                        <th class="bulk-actions" colspan="7">
                          <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $school_year as $year ): ?>
                      <tr class="pointer">
                        <td class=" "><?php echo $year->label; ?></td>
                        <td class=" "><?php echo $year->date_start; ?></td>
                        <td class=" "><?php echo $year->date_end; ?></td>
                        <td class=" "><?php echo ($year->active==1) ? "Active" : "Disabled"; ?></td>
                        <td class=" last"><a href="<?php echo site_url("school_year/update/" . $year->id); ?>">Update</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                
				  
				  
                </div>
              </div>
            </div>
          </div>
     
		 
		 </div>

<?php $this->load->view('footer'); ?>
