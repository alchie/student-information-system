<?php $this->load->view('header'); ?>

<body style="background:#F7F7F7;">

  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
      <div id="login" class="animate form">
        <section class="login_content">
         
		  <form method="post">
            
			<h1>Login Form</h1>
			
            <div>
              <input name="username" type="text" class="form-control" placeholder="Username" required="" />
            </div>
            <div>
              <input name="password" type="password" class="form-control" placeholder="Password" required="" />
            </div>
            <div>
              <button class="btn btn-default submit" type="submit">Log in</button>
            </div>
            <div class="clearfix"></div>
            <div class="separator">
<?php if( isset( $error ) ) { ?>
			 <span class="alert alert-danger"><?php echo $error; ?></span>
<?php } ?>
			 
              <div class="clearfix"></div>
              <br />
              <div>
                <h1>San Lorenzo College of Davao</h1>

                <p>Academic Management System &copy; 2016</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
      
    </div>
  </div>

</body>

</html>
