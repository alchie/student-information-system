<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              
			  <h3>Add Account</h3>
			  
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
				  <div class="x_content">
				  <?php 
				  if( validation_errors() ) {
					echo "<div class=\"alert alert-danger\">";
					echo validation_errors(); 
					echo "</div>";
				  }
				  ?>
                  <br />
                  <?php echo form_open("user_accounts/add", array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php

	$forms = array(
		'username' => array("title"=>"Username", 'type'=>"text", "attributes"=>array("required"=>"required")),
		'password' => array("title"=>"Password", 'type'=>"password", "attributes"=>array("required"=>"required")),
		'password2' => array("title"=>"Repeat Password", 'type'=>"password", "attributes"=>array("required"=>"required")),
		'name' => array("title"=>"Name", 'type'=>"text", "attributes"=>array("required"=>"required")),
		'account_type' => array("title"=>"Account Type", 'type'=>"select_single", 'options'=>array("admin"=>"Admin","faculty"=>"Faculty")),
		'status' => array("title"=>"Status", 'type'=>"checkbox", 'options'=>array("1"=>"Active")),
	);
	
	foreach($forms as $key=>$form ) {
		echo gentelella_form1( $form['type'], $form['title'], $key, $form ); 
	}
	?>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("user_accounts"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>

                  </form>
                </div>
				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
