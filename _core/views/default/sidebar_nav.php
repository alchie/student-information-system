 <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo site_url("welcome"); ?>" class="site_title"><i class="fa fa-paw"></i> <span>SLCD</span></a>
          </div>
          <div class="clearfix"></div>


          <br />
	  <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

<?php $sidebar_menus = array(
	array(	
			'main'=>array('title' => "Students Information","icon"=>"fa fa-users","uri"=>"students_information"),
			'sub' => array(
				array('title' => "Student Profiles","uri"=>"students"),
				array('title' => "Statement of Account","uri"=>"soa"),
				array('title' => "Form 137","uri"=>"form137"),
			) 
	),
	array(	
			'main'=>array('title' => "Academic Program","icon"=>"fa fa-book","uri"=>"academic_program"),
			'sub' => array(
				array('title' => "Courses","uri"=>"courses"),
				array('title' => "Subjects","uri"=>"subjects"),

			) 
	),
	array(	
			'main'=>array('title' => "Administration","icon"=>"fa fa-cogs","uri"=>"administration"),
			'sub' => array(
				array('title' => "School Settings","uri"=>"school_settings"),
				array('title' => "User Accounts","uri"=>"user_accounts"),
				array('title' => "School Year","uri"=>"school_year"),
				array('title' => "Sections","uri"=>"sections"),
			) 
	),
);
?>
	<?php foreach( $sidebar_menus as $menu ): ?>
            <div class="menu_section">
              <h3><?php echo $menu['main']['title']; ?></h3>
              <ul class="nav side-menu">
                <li class="<?php echo ($menu['main']['uri']==$sidebar_menu_main) ? "active" : ""; ?>"><a><i class="<?php echo $menu['main']['icon']; ?>"></i> <?php echo $menu['main']['title']; ?> <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: <?php echo ($menu['main']['uri']==$sidebar_menu_main) ? "block" : "none"; ?>">
					<?php foreach( $menu['sub'] as $sub ): ?>
                    <li class="<?php echo ($sub['uri']==$sidebar_menu_sub) ? "active current-page" : ""; ?>"><a href="<?php echo site_url($sub['uri']); ?>"><?php echo $sub['title']; ?></a></li>
					<?php endforeach; ?>
                  </ul>
                </li>
                </li>
              </ul>
            </div>
	<?php endforeach; ?>
	<!--
			<div class="menu_section">
              <h3>Students Information</h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-users"></i> Students Information <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?php echo site_url("students"); ?>">Student Profiles</a></li>
                    <li><a href="<?php echo site_url("soa"); ?>">Statement of Account</a></li>
					<li><a href="<?php echo site_url("form137"); ?>">Form 137</a></li>
                  </ul>
                </li>
                </li>
              </ul>
            </div>
			<div class="menu_section">
              <h3>Academic Program</h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-book"></i> Academic Program <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
					<li><a href="<?php echo site_url("courses"); ?>">Courses</a></li>
                    <li><a href="<?php echo site_url("subjects"); ?>">Subjects</a></li>
                  </ul>
                </li>
                </li>
              </ul>
            </div>
			
            <div class="menu_section">
              <h3>Administration</h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-cogs"></i> Administration <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?php echo site_url("school_settings"); ?>">School Settings</a></li>
                    <li><a href="<?php echo site_url("user_accounts"); ?>">User Accounts</a></li>
					<li><a href="<?php echo site_url("school_year"); ?>">School Year</a></li>
					<li><a href="<?php echo site_url("sections"); ?>">Sections</a></li>
                  </ul>
                </li>
                </li>
              </ul>
            </div>
-->
          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo site_url("welcome/logout"); ?>">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>