<!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">Academic Management System by <a target="_blank" href="http://www.trokis.com">Trokis Philippines</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url("assets/default/js/bootstrap.min.js"); ?>"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo base_url("assets/default/js/progressbar/bootstrap-progressbar.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/default/js/nicescroll/jquery.nicescroll.min.js"); ?>"></script>
  <!-- icheck -->
  <script src="<?php echo base_url("assets/default/js/icheck/icheck.min.js"); ?>"></script>

  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url("assets/default/js/moment/moment.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("assets/default/js/datepicker/daterangepicker.js"); ?>"></script>
  
  <script src="<?php echo base_url("assets/default/js/custom.js"); ?>"></script>

  <!-- pace -->
  <script src="<?php echo base_url("assets/default/js/pace/pace.min.js"); ?>"></script>

</body>

</html>