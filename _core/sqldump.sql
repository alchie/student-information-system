-- Table structure for table `account_sessions` 

CREATE TABLE `account_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `account_sessions_timestamp` (`timestamp`)
);

-- Table structure for table `ams_config` 

CREATE TABLE `ams_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `module` varchar(100) NOT NULL DEFAULT 'ams',
  `key` varchar(200) NOT NULL,
  `value` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `campuses` 

CREATE TABLE `campuses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `campuses_grade_levels` 

CREATE TABLE `campuses_grade_levels` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `campus_id` int(20) NOT NULL,
  `school_year` int(20) NOT NULL,
  `grade_level` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `campuses_school_year` 

CREATE TABLE `campuses_school_year` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `campus_id` int(20) NOT NULL,
  `school_year` int(20) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `parents_address` 

CREATE TABLE `parents_address` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
);

-- Table structure for table `parents_contacts` 

CREATE TABLE `parents_contacts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
);

-- Table structure for table `parents_information` 

CREATE TABLE `parents_information` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `deceased` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `parents_meta` 

CREATE TABLE `parents_meta` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_fees` 

CREATE TABLE `school_fees` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `group_id` int(10) NOT NULL,
  `installment` int(2) NOT NULL DEFAULT '1',
  `discount` int(1) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_fees_groups` 

CREATE TABLE `school_fees_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '99',
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_year` 

CREATE TABLE `school_year` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `label` varchar(200) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_year_level_fees` 

CREATE TABLE `school_year_level_fees` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sylvl_id` int(10) NOT NULL,
  `fee_id` int(10) NOT NULL,
  `amount` float(10,5) NOT NULL DEFAULT '0.00000',
  `skip` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_year_level_sections` 

CREATE TABLE `school_year_level_sections` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sylvl_id` int(20) NOT NULL,
  `section_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_year_levels` 

CREATE TABLE `school_year_levels` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `school_year` int(20) NOT NULL,
  `level` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_year_months` 

CREATE TABLE `school_year_months` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `school_year` int(20) NOT NULL,
  `month` int(20) NOT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_year_subjects` 

CREATE TABLE `school_year_subjects` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `school_year` int(20) NOT NULL,
  `subject_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `sections` 

CREATE TABLE `sections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `services` 

CREATE TABLE `services` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_address` 

CREATE TABLE `students_address` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `student_id` int(20) NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`)
);

-- Table structure for table `students_contacts` 

CREATE TABLE `students_contacts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `student_id` int(20) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`)
);

-- Table structure for table `students_discounts` 

CREATE TABLE `students_discounts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `enrolled_id` int(20) NOT NULL,
  `fee_id` int(20) NOT NULL,
  `amount` float(10,5) NOT NULL DEFAULT '0.00000',
  `description` varchar(200) DEFAULT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_enrolled` 

CREATE TABLE `students_enrolled` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `student_id` int(20) NOT NULL,
  `school_year` int(20) NOT NULL,
  `grade_level` varchar(3) NOT NULL,
  `section_id` int(10) DEFAULT NULL,
  `campus_id` int(10) DEFAULT NULL,
  `date_enrolled` date DEFAULT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_fees_installment_override` 

CREATE TABLE `students_fees_installment_override` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `enrolled_id` int(20) NOT NULL,
  `fee_id` int(20) NOT NULL,
  `installment` int(2) NOT NULL,
  `skip` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_information` 

CREATE TABLE `students_information` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `idn` varchar(10) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) DEFAULT NULL,
  `birthday` date NOT NULL,
  `birthplace` varchar(200) DEFAULT NULL,
  `religion` varchar(100) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `lrn` varchar(50) DEFAULT NULL,
  `google_id` varchar(100) DEFAULT NULL,
  `google_userid` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idn` (`idn`,`lrn`),
  UNIQUE KEY `google_id` (`google_id`),
  UNIQUE KEY `google_userid` (`google_userid`)
);

-- Table structure for table `students_invoices` 

CREATE TABLE `students_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enrolled_id` int(11) NOT NULL,
  `fee_id` int(20) NOT NULL,
  `month` int(11) NOT NULL,
  `amount` float(10,5) NOT NULL DEFAULT '0.00000',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fee_type` varchar(50) NOT NULL DEFAULT 'SCHOOLFEE',
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_medical` 

CREATE TABLE `students_medical` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `student_id` int(20) NOT NULL,
  `info` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`)
);

-- Table structure for table `students_meta` 

CREATE TABLE `students_meta` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `student_id` int(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_month_exclude` 

CREATE TABLE `students_month_exclude` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `enrolled_id` int(20) NOT NULL,
  `excluded` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_oldaccounts` 

CREATE TABLE `students_oldaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enrolled_id` int(11) NOT NULL,
  `amount` float(10,5) NOT NULL DEFAULT '0.00000',
  `installment` int(11) NOT NULL DEFAULT '1',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `enrolled_id` (`enrolled_id`)
);

-- Table structure for table `students_parents` 

CREATE TABLE `students_parents` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `student_id` int(20) NOT NULL,
  `parent_id` int(20) NOT NULL,
  `relationship` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_payments` 

CREATE TABLE `students_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enrolled_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `receipt_number` int(11) NOT NULL,
  `amount` float(10,5) NOT NULL DEFAULT '0.00000',
  `down` int(1) NOT NULL DEFAULT '0',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `memo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_payments_applied` 

CREATE TABLE `students_payments_applied` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` int(11) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `amount` float(10,5) NOT NULL,
  `applied_to` varchar(100) NOT NULL DEFAULT 'school_fees',
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_payments_applied2` 

CREATE TABLE `students_payments_applied2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `amount` float(10,5) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_promissory` 

CREATE TABLE `students_promissory` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `enrolled_id` int(20) NOT NULL,
  `date_filed` date NOT NULL,
  `exam_date` date NOT NULL,
  `month` int(2) NOT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `reason` text NOT NULL,
  `promised_date` date NOT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `settled` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_services` 

CREATE TABLE `students_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enrolled_id` int(11) NOT NULL,
  `service_id` int(20) NOT NULL,
  `month` int(11) NOT NULL,
  `amount` float(10,5) NOT NULL DEFAULT '0.00000',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

-- Table structure for table `students_sessions` 

CREATE TABLE `students_sessions` (
  `session_id` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  UNIQUE KEY `session_id` (`session_id`)
);

-- Table structure for table `subjects` 

CREATE TABLE `subjects` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `subjects_levels` 

CREATE TABLE `subjects_levels` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `subject_id` int(20) NOT NULL,
  `grade_level` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `user_accounts` 

CREATE TABLE `user_accounts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `usergroup` int(10) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `control_mode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
);

-- Table structure for table `user_accounts_permissions` 

CREATE TABLE `user_accounts_permissions` (
  `user_id` int(20) NOT NULL,
  `module` varchar(100) NOT NULL,
  `action` varchar(50) DEFAULT NULL,
  `on` int(1) NOT NULL,
  KEY `user_id` (`user_id`)
);

-- Table structure for table `user_groups` 

CREATE TABLE `user_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `user_groups_permissions` 

CREATE TABLE `user_groups_permissions` (
  `group_id` int(20) NOT NULL,
  `module` varchar(100) NOT NULL,
  `view` int(1) NOT NULL DEFAULT '0',
  `add` int(1) NOT NULL DEFAULT '0',
  `edit` int(1) NOT NULL DEFAULT '0',
  `delete` int(1) NOT NULL DEFAULT '0',
  `print` int(1) NOT NULL DEFAULT '0',
  KEY `group_id` (`group_id`)
);

